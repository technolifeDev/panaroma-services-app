<?php
return [
    'book-service-btn' => 'Valige teenus',
    'more-info-btn' => 'Rohkem informatsiooni',
    'book-confirm-btn' => 'Kinnitage',
    'welcome-text' => 'Valige teenused',
    'welcome-message' => 'Techno Life pakub koduautomaatika, juurdepääsu kontrollimise ja integreerimise teenuseid.',
    'back-to-srvc-btn' => 'Tagasi teenindusse',
    'order_confirm_txt' => 'Täname teid tellimuse eest, teie päring saadeti edukalt, teenusepakkuja võtab teiega ühendust 24 tunni jooksul.',
    'date_time_select' => 'Valige kuupäev ja kellaaeg',
    'specific_time_select' => 'Konkreetne kuupäev ja kellaaeg',
    'urgent_text' => 'pakiline',
    'term_of_use_text' => 'Kasutustingimused',
    'agree_term_check' => 'Leppige kokku selles terminis',
    'sign_agreement_btn_text' => 'SIGNI LEPING',
    'agreement_text' => "<strong>Ärilepingud pakuvad teile elutähtsat kaitset.</strong> Meie kliendid teavad, et kirjalike
								kokkulepete kasutamine tähendab head ärivaistu. Siiski on enamus kokkulepete
								näited saadaval vähestele ning neist enamus on koostatud välisriikide
								jurisdiktsioonis. Meie dokumendid on koostatud meie kollektiivsete teadmiste
								ning spetsialistide kogemuse baasil, et pakkuda teile põhjalikud lepingud, mis
								ühendavad ärivajadused Lõuna-Aafrika seaduslike nõuetega.
                                <br><br>
                                <strong>Neid on lihtne redigeerida ja lõpule viia.</strong> Meie ärilepingud ning dokumendid on Wordi
									vormingus, mis lihtsustab nende muutmise ning võimaldavad teil neid
									kohandada vastavalt teie ettevõtte vajadustele. Dokumendid on arusaadavad.
									Need on kirjutatud inglese keeles. Tavaline, lihtsastimõistetavas inglese keeles.<br><br>
									Nõustun kasutustingimustega
                                ",
    'order-welcome-text' => 'Eelnevate tellimuste loend',
    'description' => 'Kirjeldus',
    'category' => 'Kategooria',
    'price' => 'Hind',
    'service_provider' => 'Teenusepakkuja',
];
