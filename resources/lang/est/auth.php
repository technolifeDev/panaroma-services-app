<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Need mandaadid ei vasta meie andmetele.',
    'throttle' => 'Liiga palju sisselogimiskatseid. Proovige uuesti sekundi pärast.',
    'reg_page_title' => 'Lisage oma andmed',
    'reg_page_desc' => 'Techno Life pakub koduautomaatika, juurdepääsu kontrollimise ja integreerimise teenuseid.',
    'field-name' => 'Nimi',
    'field-email' => 'E-post',
    'field-address' => 'Aadress',
    'field-password' => 'Parool',
    'field-confirm_password' => 'Kinnita salasõna',
    'button-next' => 'JÄRGMINE',
    //TermPage
    'general_term_page_title' => 'Kasutamise tähtaeg',
    'general_term_page_desc' => 'Techno Life pakub koduautomaatika, juurdepääsu kontrollimise ja integreerimise teenuseid.',
    'button-agree' =>'Leppige kokku selles terminis',
	'sign-agreement' =>'Sign Agreement',
    //SignaturePage
    'signature_page_title'=>'Sinu allkiri',
    'signature_page_desc'=>'Techno Life pakub koduautomaatika, juurdepääsu kontrollimise ja integreerimise teenuseid. Oleme USS grupi uhke liige ja seetõttu hõlmab meie kompetents grupina: kinnisvara hooldamist ja haldamist.',
    'button-submit_signature'=>'ESTIGA ALLKIRI',
    'button-clear_signature'=>'TÜHJENDA VÄLI',
    'button-sign_signature'=>'ALLKIRJASTA LEPING',
    //LoginPage
    'button-login' => 'Logi sisse',
    'login_page_title'=>'Logi sisse',
    'forget-password' =>'Unustasite parooli',
	'sign-up' =>'Registreeri',
    //Index Page
    'index_page_title_1' => 'Tere tulemast',
    'index_page_title_2' => 'oma UUDE koju',
    'index_page_desc' => 'Techno Life pakub koduautomaatika, juurdepääsu kontrollimise ja integreerimise teenuseid. Oleme USS grupi uhke liige ja seetõttu hõlmab meie kompetents grupina: kinnisvara hooldamist ja haldamist.',
    'top-menu-button-login' => 'Logi sisse',
    'top-menu-button-logout' => 'Logi välja',
    'top-menu-button-register' => 'Registreeri',
    'top-menu-button-language' => 'Keel',
    'right_side_page_browse' =>'Sirvige meie',
    'right_side_page_service' =>'teenustega lähemalt',
    'button-click_service'=>'TEENUSTE VALIMISEKS KLÕPSAKE SIIA',
    'top-menu-welcome' => 'Tere tulemast',
    //Forget Password
	'forget_pass_page_title'=>'Lähtesta parool',
	'button-forget-pass' => 'Lähtesta',
    'field-new-password' => 'uus salasõna',
    'order-history-btn' => 'Tellimuste ajalugu',
    'not_authorized' => 'Teil puudub ligipääs',
    "login_to_continue" => 'Jätkamiseks logige sisse!',
    'terms_condition' => 'Kasutustingimused',
    'privacy_policy' => 'Privaatsuseeskirjad',
];
