<?php

return [
    'service' => 'Teenus',
    'amount' => 'Hind',
    'order_status' => 'Tellimuse staatus',
    'service_date' => 'Teenuse kuupäev',
    'service_status' => ' Teenuse staatus',
    'service_time' => 'Teenuse kestvus',
    'order_details' => 'Tellimuse üksikasjad',
    'total' => 'KOKKU',
    'sub_total' => 'SUB KOKKU',
    'grand_total' => 'KOGUSUMMA',
    'order_date' => 'Tellimuse kuupäev',
    'customer_details' => 'KLIENDIANDMED',
    'order_no' => 'TELLIMUS EI'
];
