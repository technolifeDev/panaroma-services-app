<?php
return [
    'book-service-btn' => 'BOOK SERVICE',
    'more-info-btn' => 'More information',
    'book-confirm-btn' => 'Confirm',
    'welcome-text' => 'Select services',
    'welcome-message' => 'Techno Life provides home automation, access control and integration services.',
    'back-to-srvc-btn' => 'Back to service',
    'order_confirm_txt' => 'Thank you for your order, your request was send successfully, Servicer provider will contact you during 24h.',
    'date_time_select' => 'Select date & time',
    'specific_time_select' => 'Specific date & time',
    'urgent_text' => 'Urgent',
    'term_of_use_text' => 'Terms of use',
    'agree_term_check' => 'Agree on these term',
    'sign_agreement_btn_text' => 'SIGN AGREEMENT',
    'agreement_text' => "<strong>Business contracts offer you vital protection.</strong> Our customers know that
                                having written agreements in place makes good business sense. However,
                                most of the legal agreement templates available online offer only the bare minimum,
                                and majority have been drafted in foreign jurisdictions. Our documents use the
                                 collective knowledge and experience of our specialist drafters to offer you
                                 comprehensive agreements that combine business imperatives with South African
                                 legal requirements.
                                <br><br>
                                <strong>They’re easy to edit and complete.</strong> Our business agreements
                                and documents are provided in Word format, which makes it easy for you to edit
                                 and customise them to suit your business requirements.
                                You can understand our documents. Yes, they are written in English! Plain,
                                easy-to-understand English.
                                ",
    'order-welcome-text' => 'Previous Order List',
	'confirm_send' => 'Confirm And Send',
	'i_disagree' => 'I disagree',
    'description' => 'Description',
    'category' => 'Category',
    'price' => 'Price',
    'service_provider' => 'Service Provider',
];
