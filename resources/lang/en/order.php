<?php

return [
    'service' => 'Service',
    'amount' => 'Amount',
    'order_status' => 'Order Status',
    'service_date' => 'Service Date ',
    'service_status' => 'Service Status',
    'service_time' => 'Service Time',
    'order_details' => 'Order Details',
    'total' => 'TOTAL',
    'sub_total' => 'SUB TOTAL',
    'grand_total' => 'GRAND TOTAL',
    'order_date' => 'Order Date',
    'customer_details' => 'CUSTOMER DETAILS',
    'order_no' => 'ORDER NO',
];
