<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    //RegistrationPage
    'reg_page_title' => 'Add your data',
    'reg_page_desc' => 'Techno Life provides home automation, access control and integration services.',
    'field-name' => 'Name',
    'field-email' => 'Email',
    'field-address' => 'Address',
    'field-password' => 'Password',
    'field-confirm_password' => 'Confirm Password',
    'button-next' => 'NEXT',
    //TermPage
    'general_term_page_title' => 'Term of use',
    'general_term_page_desc' => 'Techno Life provides home automation, access control and integration services.',
    'button-agree' =>'Agree on these term',
	'sign-agreement' =>'Sign Agreement',

    //SignaturePage
    'signature_page_title'=>'Your signature',
    'signature_page_desc'=>'Techno Life provides home automation, access control and integration services. We are a proud member of USS group and therefore our competence as a group covers: maintenance and administration of property.',
    'button-submit_signature'=>'SUBMIT SIGNATURE',
    'button-clear_signature'=>'CLEAR SIGNATURE',
    'button-sign_signature'=>'SIGN AGREEMENT',
    //LoginPage
    'button-login' => 'Login',
    'login_page_title'=>'Login',
    'forget-password' =>'Forget Password',
	'sign-up' =>'Sign Up',
    //Index Page
    'index_page_title_1' => 'Welcome to your',
    'index_page_title_2' => 'NEW Home',
    'index_page_desc' => 'Techno Life provides home automation, access control and integration services. We are a proud member of USS group and therefore our competence as a group covers: maintenance and administration of property.',
    'top-menu-button-login' => 'Login',
    'top-menu-button-logout' => 'Logout',
    'top-menu-button-register' => 'Register',
    'top-menu-button-language' => 'Language',
    'right_side_page_browse' =>'Browse our',
    'right_side_page_service' =>'services for you',
    'button-click_service'=>'CLICK HERE TO CHOOSE SERVICES',
    'top-menu-welcome' => 'Welcome',
    //Forget Password
	'forget_pass_page_title'=>'Reset Password',
    'field-new-password' => 'New Password',
	'button-forget-pass' => 'RESET',
	'order-history-btn' => 'Order History',
    'not_authorized' => 'You are not an authorized user',
    "login_to_continue" => 'Please login to continue!',
    'terms_condition' => 'Terms of use',
    'privacy_policy' => 'Privacy policy',



];
