
@extends('customer.mail.master-mail')

@section('mail-content')

        <h3 style="margin-top: 0; font-weight: normal; color: #38434d; font-family: 'Exo', sans-serif; font-size: 16px;	margin-bottom: 14px; line-height: 24px;">
            Hello,
        </h3>
        <p> You have received an order. Please contact admin for more details</p>
        <center>
            <h4 style="background-color: #bcdbee3b; padding:20px; font-weight:bold;margin-top: 0; font-weight: normal; color: #38434d; font-family: 'Exo', sans-serif; font-size: 16px;	margin-bottom: 14px; line-height: 24px;text-align: left;">
                <b>Order #:</b> {{ $order->order_id}}<br>
                <b>Customer Details :</b>
                <address style="font-family: 'Exo', sans-serif">
                    {{ isset($customer_details[0]['customer_name']) ? $customer_details[0]['customer_name'] : ''}}, <br/>
                    {{ isset($customer_details[0]['customer_email']) ? $customer_details[0]['customer_email'] : ''}}, <br/>
                    {{ isset($customer_details[0]['customer_address']) ? $customer_details[0]['customer_address'] : ''}} <br/>
                </address>
            </h4>
        </center>
        <br>
        <p style="margin-top: 0; font-weight: 400; font-size: 14px;	line-height: 22px; color: #7c7e7f; font-family: 'Exo', sans-serif; margin-bottom: 22px;">
            Please contact admin for further details. <br />
        </p>



{{--        <div class="footer" style="padding-top: 25px;">--}}
{{--            <h2 style="margin-top: 0; font-weight: normal; color: #38434d; font-family: 'Exo', sans-serif; font-size: 14px;	margin-bottom: 7px;">Thanks</h2>--}}
{{--            <p style="margin-top: 0; font-weight: 400; font-size: 14px;	line-height: 22px; color: #7c7e7f; font-family: 'Exo', sans-serif; margin-bottom: 22px;">If you have any query, feel free to contact our support team :<a href="mailto:support@technolife.ee" style="text-decoration: none;"> support@technolife.ee </a></p>--}}
{{--        </div>--}}

@endsection
