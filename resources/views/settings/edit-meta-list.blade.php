@extends('layout.master')
@section('content')
    @inject('ACL', 'App\Repositories\RolePermissionForBlade')
    <div class="row">
        <div class="col-md-12">
            @if ($errors->count() > 0 )
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <h6>The following errors have occurred:</h6>
                    <ul>
                        @foreach( $errors->all() as $message )
                            <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (Session::has('message'))
                <div class="alert alert-success" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('message') }}
                </div>
            @endif
            @if (Session::has('errormessage'))
                <div class="alert alert-danger" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('errormessage') }}
                </div>
            @endif
        </div>
    </div>
    <div class="row ">
        <div class="col-md-12">
            <ul id="myTab" class="nav nav-tabs tab-bricky">
                <li class="active">
                    <a href="">
                        <i class="green fa fa-wrench"></i> Edit Settings Meta Data
                    </a>
                </li>
                <li class="">
                    <a href="{{url('/admin/service/settings-meta/list')}}">
                        <i class="green fa fa-plus-circle"></i> Create Settings Meta Data
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="add_push">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-external-link-square"></i>
                                    Create Setting Meta
                                    <div class="panel-tools">
                                        <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                        </a>
                                        <a class="btn btn-xs btn-link panel-close" href="#">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <form method="post" action="{{url('/admin/service/settings-meta/edit',$setting_meta->id)}}">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <div class="form-group">
                                            <label for="form-field-23">Setting Name</label>
                                            <input type="text" id="form-field-3" class="form-control" name="setting_name" value="{{isset($settings->settings_name)?$settings->settings_name:''}}" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="form-field-24">Data Type</label>
                                            <select class="form-control setting_data_type" name="setting_data_type" required>
                                                <option value="">Choose a Type</option>
                                                <option value="text" {{isset($settings->settings_data_type)&&($settings->settings_data_type=='text')? 'selected':''}}>Text</option>
                                                <option value="numeric" {{isset($settings->settings_data_type)&&($settings->settings_data_type=='numeric')? 'selected':''}}>Numeric</option>
                                                <option value="array" {{isset($settings->settings_data_type)&&($settings->settings_data_type=='array')? 'selected':''}}>Multiple</option>
                                            </select>
                                        </div>
                                        <div class="row single_type_meta" style="display: {{isset($settings->settings_data_type)&&($settings->settings_data_type !='array')? 'block':'none'}}">
                                            <div class="form-group  col-md-8">
                                                <label for="form-field-23">Meta Name</label>
                                                <input type="text" id="form-field-3" class="form-control" name="meta_name" value="{{isset($setting_meta->meta_field_name)?$setting_meta->meta_field_name:''}}" >
                                            </div>
                                            <div class="form-group  col-md-4">
                                                <label for="form-field-23">Meta value</label>
                                                <input type="text" id="form-field-3" class="form-control" name="meta_value" value="{{isset($setting_meta->meta_field_value)?$setting_meta->meta_field_value:''}}" >
                                            </div>
                                        </div>

                                        <div class="multiple_type_meta" style="display: {{isset($settings->settings_data_type)&&($settings->settings_data_type =='array')? 'block':'none'}}">
                                            <table class="table ">
                                                <thead>
                                                    <tr>
                                                        <th colspan="2" class="">
                                                            Meta Data Entry

                                                        </th>
                                                        <th style="text-align: right;">
                                                            <a id="btnAdd" class="btn btn-xs btn-success" href="#">
                                                                <i class="fa fa-plus"></i> Add More
                                                            </a>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label for="form-field-23">Meta Name</label>
                                                            <input type="text" id="form-field-3" class="form-control" name="meta_name_list[]" value="{{isset($setting_meta->meta_field_name)?$setting_meta->meta_field_name:''}}" ></td>
                                                        <td>
                                                            <label for="form-field-23">Meta value</label>
                                                            <input type="text" id="form-field-3" class="form-control" name="meta_value_list[]" value="{{isset($setting_meta->meta_field_value)?$setting_meta->meta_field_value:''}}" >
                                                        </td>
                                                        <td><button type="button" class="btn btn-danger remove"><i class="glyphicon glyphicon-remove-sign"></i></button></td>
                                                    </tr>
                                                </thead>
                                                <tbody id="TextBoxContainer">


                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="form-group">
                                            <input type="reset" class="btn btn-danger" value="Reset">
                                            <input type="submit" class="btn btn-primary" value="Update">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>



                                <div class="col-md-6">

                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <i class="fa fa-external-link-square"></i>
                                            Settings Meta Data List
                                            <div class="panel-tools">
                                                <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                                </a>
                                                <a class="btn btn-xs btn-link panel-close" href="#">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-bordered table-striped nopadding" id="settingMetaDataTableEdit">
                                                    <thead>
                                                    <tr>
                                                        <th>SL</th>
                                                        <th>Meta Field Name</th>
                                                        <th>Meta Field Value</th>
                                                        <th>Setting Name</th>
                                                        <th>Data Type</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>



                                </div>

                            </div>
                        </div>

                    </div>
            </div>
        </div>
    </div>
@endsection
@section('JScript')
    <script>
        $(function () {
            var site_url = $('.site_url').val();

            //MultipleEntry Form
            $("#btnAdd").bind("click", function () {
                var div = $("<tr />");
                div.html(GetDynamicTextBox());
                $("#TextBoxContainer").append(div);
            });
            $("body").on("click", ".remove", function () {
                $(this).closest("tr").remove();
            });

            //single and multiple
            $('.setting_data_type').change(function(){
                var setting_data_type = $(this).val();
                if(setting_data_type == 'array'){
                    $(".single_type_meta").css("display", "none");
                    $(".multiple_type_meta").css("display", "block");
                }else{
                    $(".multiple_type_meta").css("display", "none");
                    $(".single_type_meta").css("display", "block");

                }
            });


            //data table ajax load
            $('#settingMetaDataTableEdit').DataTable({
                "processing": false,
                "serverSide": true,
                "ordering": false,
                "ajax": {
                    "url": site_url+"/admin/ajax/settings-meta/list",
                    "dataType": "json",
                    "type": "POST",
                    "data":{ _token: $('#token').val()}
                },
                "aoColumns": [
                    { mData: 'sno'},
                    { mData: 'field_name'},
                    { mData: 'field_value'},
                    { mData: 'settings_name'},
                    { mData: 'settings_type'},
                    { mData: 'actions'}
                ]
            });


            // news delete

            $("body").on("click", ".meta-data-delete", function (e) {
                e.preventDefault();
                var id = $(this).data('settings');
                bootbox.dialog({
                    message: "Are you sure you want to delete this Settings Meta?",
                    title: "<i class='glyphicon glyphicon-trash'></i> Delete !",
                    buttons: {
                        success: {
                            label: "No",
                            className: "btn-success btn-squared",
                            callback: function() {
                                $('.bootbox').modal('hide');
                            }
                        },
                        danger: {
                            label: "Delete!",
                            className: "btn-danger btn-squared",
                            callback: function() {
                                $.ajax({
                                    type: 'GET',
                                    url: site_url+'/admin/ajax/settings-meta/delete/'+id,
                                }).done(function(response){
                                    bootbox.alert(response,
                                        function(){
                                            location.reload(true);
                                        }
                                    );
                                }).fail(function(response){
                                    bootbox.alert(response);
                                })
                            }
                        }
                    }
                });
            });
        });

        function GetDynamicTextBox() {
            return '<td><label>Meta Name</label><input type="text" class="form-control" name="meta_name_list[]" value=""></td><td><label>Meta value</label><input type="text" class="form-control" name="meta_value_list[]" value="" ></td><td><button type="button" class="btn btn-danger remove"><i class="glyphicon glyphicon-remove-sign"></i></button></td>'
        }
        </script>
@endsection