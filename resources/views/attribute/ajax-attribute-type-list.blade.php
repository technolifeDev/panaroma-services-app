@if(!empty($types_list) && count($types_list)>0)
    <option value="0">Select a Type</option>
    @foreach($types_list as $key => $list)
        <option value="{{$list->id}}">{{$list->attribute_type_name}}</option>
    @endforeach
@else
    <option value="0">Select Types</option>
@endif