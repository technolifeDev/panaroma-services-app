@extends('layout.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            @if ($errors->count() > 0 )
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <h6>The following errors have occurred:</h6>
                    <ul>
                        @foreach( $errors->all() as $message )
                            <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (Session::has('message'))
                <div class="alert alert-success" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('message') }}
                </div>
            @endif
            @if (Session::has('errormessage'))
                <div class="alert alert-danger" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('errormessage') }}
                </div>
            @endif
        </div>
    </div>
    <div class="row ">
        <div class="col-md-12">
            <ul id="myTab" class="nav nav-tabs tab-bricky">
                <li class="active">
                    <a href="">
                        <i class="green cip-pencil-3"></i>Edit Attribute Class
                    </a>
                </li>
                <li class="">
                    <a href="{{url('/admin/service/attribute-class/create')}}">
                        <i class="green clip-puzzle-2"></i> Attribute Class
                    </a>
                </li>
                <li class="">
                    <a href="{{url('/admin/service/attribute-type/create')}}">
                        <i class="green clip-puzzle-4"></i> Attribute Type
                    </a>
                </li>
                <li class="">
                    <a href="{{url('/admin/service/attribute-values/create')}}">
                        <i class="green clip-cube-2"></i> Attribute Value Assign
                    </a>
                </li>

            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="add_push">
                    <div class="row">

                        <div class="col-md-4">
                            <form method="post" action="{{url('/admin/service/attribute-class/edit',$class_info->id)}}">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="form-group">
                                    <label for="form-field-23">Attribute Class Name</label>
                                    <input type="text" id="form-field-3" class="form-control" name="attribute_class_name" value="{{$class_info->attribute_class_name}}">
                                </div>

                                <div class="form-group">
                                    <input type="reset" class="btn btn-danger" value="Reset">
                                    <input type="submit" class="btn btn-primary" value="Update">
                                </div>
                            </form>
                        </div>

                        <div class="col-md-8">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-external-link-square"></i>
                                    Attribute Class List
                                    <div class="panel-tools">
                                        <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                        </a>
                                        <a class="btn btn-xs btn-link panel-close" href="#">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered table-striped nopadding" id="sample-table-1">
                                            <thead>
                                            <tr>
                                                <th>SL</th>
                                                <th>Attribute Class</th>
                                                <th>Date</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if(!empty($attribute_class_all_data) && count($attribute_class_all_data) > 0)
                                                <?php $page=isset($_GET['page'])? ($_GET['page']-1):0;?>
                                                @foreach($attribute_class_all_data as $key => $list)
                                                    <tr >
                                                        <td>{{ ($key+1+($perPage*$page)) }}</td>
                                                        <td>{{$list->attribute_class_name}}</td>
                                                        <td>{{$list->updated_at}}</td>
                                                        <td>
                                                            <a href="{{url('/admin/service/attribute-class/edit',$list->id)}}" class="btn btn-green btn-xs tooltips"><i class="fa fa-pencil-square-o" aria-hidden="true" data-toggle1="tooltip" title="Attribute Class Edit"></i></a>
                                                            <a data-class-id="{{ $list->id}}" class="btn btn-xs btn-bricky tooltips class_delete"><i class="fa  fa-trash-o" data-toggle1="tooltip" title="Attribute Class Delete"></i></a>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr class="text-center">
                                                    <td colspan="5">No Data available</td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                        <?php echo isset($pagination) ? $pagination:"";?>
                                    </div>
                                </div>
                            </div>


                    </div>

                </div>
            </div>
            </div>
        </div>
    </div>
@endsection
@section('JScript')
    <script>
        $(function () {
            var site_url = $('.site_url').val();

            // news delete
            $('.class_delete').on('click', function (e) {
                e.preventDefault();
                var id = $(this).data('class-id');
                bootbox.dialog({
                    message: "Are you sure you want to delete this Attribute class ?",
                    title: "<i class='glyphicon glyphicon-trash'></i> Delete !",
                    buttons: {
                        success: {
                            label: "No",
                            className: "btn-success btn-squared",
                            callback: function() {
                                $('.bootbox').modal('hide');
                            }
                        },
                        danger: {
                            label: "Delete!",
                            className: "btn-danger btn-squared",
                            callback: function() {
                                $.ajax({
                                    type: 'GET',
                                    url: site_url+'/admin/service/attribute-class/delete/'+id,
                                }).done(function(response){
                                    bootbox.alert(response,
                                        function(){
                                            location.reload(true);
                                        }
                                    );
                                }).fail(function(response){
                                    bootbox.alert(response);
                                })
                            }
                        }
                    }
                });
            });
        });
    </script>
@endsection