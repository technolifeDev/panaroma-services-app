@extends('layout.master')
@section('content')
    @inject('ACL', 'App\Repositories\RolePermissionForBlade')
    <div class="row">
        <div class="col-md-12">
            @if ($errors->count() > 0 )
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <h6>The following errors have occurred:</h6>
                    <ul>
                        @foreach( $errors->all() as $message )
                            <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (Session::has('message'))
                <div class="alert alert-success" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('message') }}
                </div>
            @endif
            @if (Session::has('errormessage'))
                <div class="alert alert-danger" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('errormessage') }}
                </div>
            @endif
        </div>
    </div>

    <div class="row ">
        <div class="col-md-12">
            <div style="padding:10px;">
                <button href="#responsive" class="demo btn btn-blue createServiceCatalogue">
                    Create Service Catalogue
                </button>
            </div>


            <!-- List of Categories -->

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i>

                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                        </a>
                        <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <a class="btn btn-xs btn-link panel-refresh" href="#">
                            <i class="fa fa-refresh"></i>
                        </a>
                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>
                        <a class="btn btn-xs btn-link panel-close" href="#">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-body">

                    <table class="table table-hover serviceCatalogueTbl">
                        <thead class="thead-dark">
                            <tr>
                                <th style="width: 10% !important;">SL.no</th>
                                <th class="hidden-xs" style="width: 20% !important;">Name</th>
                                <th class="hidden-xs" style="width: 20% !important;">Category</th>
                                <th class="hidden-xs" style="width: 20% !important;">Status</th>
                                <th class="hidden-xs" style="width: 20% !important;">Description</th>
                                <th class="hidden-xs" style="width: 10% !important;">Action</th>
                            </tr>
                        </thead>
                        <tbody>


                        </tbody>
                    </table>
                </div>
            </div>

            <!-- END Categoreis -->
        </div>
    </div>

    <!-- start: Form to create categories -->
    <div id="responsive" class="modal fade addServiceCatalogueModal" tabindex="-1" data-width="600" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close btn-warning" data-dismiss="modal" aria-hidden="true">
                <i class="clip-close-3"></i>
            </button>
            <h4 class="modal-title">Create new Service Catalogue</h4>
        </div>
        <form role="form" class="addServiceCatalogueForm" enctype="multipart/form-data">
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <input type="hidden" name="service_id" class="service_id">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-offset-2 col-md-8">
                        <div class="form-group ">
                            <label for="service_name" class="col-form-label">Service Name</label>
                            <input type="text" class="form-control service_name" name="service_name" placeholder="Enter Service Name">

                        </div>
                        <div class="form-group ">
                            <label for="service_name" class="col-form-label">Select Category</label>
                            <select class="form-control services_categories_id " name="services_categories_id">
                                <option value="">Select Category</option>
                                @if(!empty($category_all_data) && count($category_all_data)>0)
                                    @foreach($category_all_data as $key => $list)
                                        <option value="{{$list->id}}">{{$list->category_name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group ">
                            <label for="form-field-select-1">
                                Status
                            </label>
                            <select id="status" class="form-control" name="status">
                                <option value="">Select Status</option>
                                <option value="draft">Draft</option>
                                <option value="active">Active</option>
                                <option value="inactive">Inactive</option>
                                <option value="pending">Pending</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="form-field-select-1">
                                Remarks
                            </label>
                            <textarea class="form-control description" rows="4" name="description" id="description" placeholder="Remarks"></textarea>
                        </div>
{{--                        <div class="form-group row">--}}
{{--                            <input type="file" id="fileToUpload" name="service_catalogue_image" class="service_catalogue_image">--}}
{{--                        </div>--}}
                        <div class="form-group imageDiv">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" style="width: 336px; height: 166px; margin-bottom: 20px !important;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA?text=no+image" alt=""/>
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="width: 336px; height: 166px; line-height: 166px; margin-bottom: 20px !important;"></div>
                                <div>
                                    <span class="btn btn-light-grey btn-file"><span class="fileupload-new"><i class="fa fa-picture-o"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture-o"></i> Change</span>
                                        <input type="file" name="service_catalogue_image" class="service_catalogue_image">
                                    </span>
                                    <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
                                        <i class="fa fa-times"></i> Remove
                                    </a>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
{{--                <button type="button" data-dismiss="modal" class="btn btn-light-grey ">--}}
{{--                    Close--}}
{{--                </button>--}}
                <button type="button" class="btn btn-blue saveServiceCatalogueBtn">
                    Save Service
                </button>
            </div>
        </form>
    </div>

    <div id="responsive" class="modal fade editServiceCatalogueModal" tabindex="-1" data-width="600" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="clip-close-3"></i>
            </button>
            <h4 class="modal-title">Create new Service Catalogue</h4>
        </div>
        <form role="form" class="editServiceCatalogueForm" enctype="multipart/form-data">
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <input type="hidden" name="service_id" class="service_id">
            <input type="hidden" name="service_catalogue_image_id" class="service_catalogue_image_id">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-offset-2 col-md-8">
                        <div class="form-group ">
                            <label for="service_name" class="col-form-label">Service Name</label>

                            <input type="text" class="form-control service_name" name="service_name" placeholder="Enter Service Name">

                        </div>
                        <div class="form-group ">
                            <label for="service_name" class="col-form-label">Select Category</label>
                            <select class="form-control services_categories_id " name="services_categories_id">
                                <option value="">Select Category</option>
                                @if(!empty($category_all_data) && count($category_all_data)>0)
                                    @foreach($category_all_data as $key => $list)
                                        <option value="{{$list->id}}">{{$list->category_name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group ">
                            <label for="form-field-select-1">
                                Status
                            </label>
                            <select id="status" class="form-control" name="status">
                                <option value="">Select Status</option>
                                <option value="draft">Draft</option>
                                <option value="active">Active</option>
                                <option value="inactive">Inactive</option>
                                <option value="pending">Pending</option>
                            </select>
                        </div>
                        <div class="form-group ">
                            <label for="form-field-select-1">
                                Remakrs
                            </label>
                            <textarea class="form-control description" rows="4" name="description" id="description" placeholder="Remarks"></textarea>
                        </div>
                        <div class="form-group imageDiv">
                            <div class="fileupload fileupload-exists" data-provides="fileupload"><input type="hidden" value="" name="">
                                <div class="fileupload-new thumbnail" style="width: 340px; height: 166px; margin-bottom: 20px !important;">
                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA?text=no+image" alt="">
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="width: 336px; height: 166px; line-height: 166px;margin-bottom: 20px !important;">

                                </div>
                                <div>
                                <span class="btn btn-light-grey btn-file"><span class="fileupload-new"><i class="fa fa-picture-o"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture-o"></i> Change</span>
                                    <input type="file" name="service_catalogue_image" id="service_catalogue_image">
                                </span>
                                    <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
                                        <i class="fa fa-times"></i> Remove
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
{{--                <button type="button" data-dismiss="modal" class="btn btn-light-grey ">--}}
{{--                    Close--}}
{{--                </button>--}}
                <button type="button" class="btn btn-blue updateServiceCatalogueBtn">
                    Update Service
                </button>
            </div>
        </form>
    </div>

    <!-- End form--->

@endsection
