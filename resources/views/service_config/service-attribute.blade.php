@extends('layout.master')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/custom.css') }}"/>
@section('content')
    @inject('ACL', 'App\Repositories\RolePermissionForBlade')
    <div class="row">
        <div class="col-md-12">
            @if ($errors->count() > 0 )
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <h6>The following errors have occurred:</h6>
                    <ul>
                        @foreach( $errors->all() as $message )
                            <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (Session::has('message'))
                <div class="alert alert-success" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('message') }}
                </div>
            @endif
            @if (Session::has('errormessage'))
                <div class="alert alert-danger" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('errormessage') }}
                </div>
            @endif
        </div>
    </div>
    <div class="row ">
        <div class="col-sm-12">
            <div class="panel panel-default serviceAttributePanel">
                <div class="panel-body">
                    <form role="form" class="form-horizontal addServiceAttributeForm">
                        <input type="hidden" name="service_pricing_item_id" value="{{$service_item_id}}" class="service_pricing_item_id">
                        @if($attr_details)
                           {!! $attr_details !!}
                        @else
                            <table class="table attributeTable">
                                <tbody>
                                <tr>
                                    <th colspan="3" class="attrTitle">
                                        Attribute 1

                                    </th>
                                    <th colspan="2" style="text-align: right;">
                                        <input type="hidden" name="tbl_serial" class="tbl_serial" value="0">
                                        <a class="btn btn-xs btn-success addMoreAttrsBtn" href="#">
                                            <i class="fa fa-plus"></i> Add More Attribute
                                        </a>
                                    </th>
                                </tr>
                                <tr class="attributeTypeClone">
                                    <td>
                                        <div class="entry input-group col-xs-12">
                                            <input class="form-control attribute_class_name" name="attribute_class_name[0]" type="text" placeholder="Select Config(text/dropdown/checkbox)" />
                                            <input class="service_pricing_class_id" name="service_pricing_class_id[0]" type="hidden" />

                                        </div>
                                    </td>
                                    <td>
                                        <div class="entry input-group col-xs-12">
                                            <input class="form-control attribute_type_name" name="attribute_type_name[0][0]" type="text" placeholder="Config type" />
                                            <input class="service_pricing_type_id" name="service_pricing_type_id[0][0]" type="hidden" />
                                            <span class="input-group-btn">
                                                    <button class="btn btn-success btn-add addMoreAttrType" type="button">
                                                        <span class="glyphicon glyphicon-plus" style="font-size: 12px;"></span>
                                                    </button>
                                                </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="entry input-group col-xs-12">
                                            <input class="form-control attribute_field_value" name="attribute_field_value[0][0][0]" type="text" placeholder="Config name" />
                                            <input class="service_pricing_type_value" name="service_pricing_type_value[0][0][0]" type="hidden" />
                                        </div>
                                    </td>
                                    <td style="width: 15%">
                                        <div class="entry input-group col-xs-12">
                                            <input class="form-control service_pricing_type_cost" name="service_pricing_type_cost[0][0][0]" type="text" placeholder="0.00" />
                                            <span class="input-group-btn">
                                                    <button class="btn btn-success btn-add addMoreAttrValue" type="button">
                                                        <span class="glyphicon glyphicon-plus" style="font-size: 12px;"></span>
                                                    </button>
                                                </span>
                                        </div>
                                    </td>
                                    <td style="width: 5%">
                                        <label class="switch"><input type="checkbox" class="is_selected" name="is_selected[0][0][0]"><div class="slider round"></div></label>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        @endif
                    </form>



                    <div class="form-group">
                        <input type="submit" class="btn btn-primary btn-group-justified col-xs-4 saveServiceAttributeBtn" value="Save">
                    </div>
                </div>
            </div>

        </div>
    </div>

    <table class="hide cloneAttrTbl">
        <tr class="attributeTypeClone">
            <td></td>
            <td>
                <div class="entry input-group col-xs-12">
                    <input class="form-control attribute_type_name" name="attribute_type_name[][]" type="text" placeholder="Config type" />
                    <input class="form-control service_pricing_type_id" name="service_pricing_type_id" type="hidden" />
                    <span class="input-group-btn">
                        <button class="btn btn-danger btn-add removeMoreAttrType" type="button">
                            <span class="glyphicon glyphicon-minus" style="font-size: 12px;"></span>
                        </button>
                    </span>
                </div>
            </td>
            <td>
                <div class="entry input-group col-xs-12">
                    <input class="form-control attribute_field_value" name="attribute_field_value[][][]" type="text" placeholder="Config name" />
                    <input class="service_pricing_type_value" name="service_pricing_type_value[][][]" type="hidden" />
                </div>
            </td>
            <td style="width: 15%">
                <div class="entry input-group col-xs-12">
                    <input class="form-control service_pricing_type_cost" name="service_pricing_type_cost[][][]" type="text" placeholder="0.00" />
                    <span class="input-group-btn">
                        <button class="btn btn-success btn-add addMoreAttrValue" type="button">
                            <span class="glyphicon glyphicon-plus" style="font-size: 12px;"></span>
                        </button>
                    </span>
                </div>
            </td>
            <td>
                <label class="switch"><input type="checkbox" class="is_selected" name="is_selected[][][]"><div class="slider round"></div></label>
            </td>
        </tr>
        <tr class="attributeValueClone">
            <td colspan="2"></td>
            <td>
                <div class="entry input-group col-xs-12">
                    <input class="form-control attribute_field_value" name="attribute_field_value[][][]" type="text" placeholder="Config name" />
                    <input class="service_pricing_type_value" name="service_pricing_type_value[][][]" type="hidden" />
                </div>
            </td>
            <td style="width: 15%">
                <div class="entry input-group col-xs-12">
                    <input class="form-control service_pricing_type_cost" name="service_pricing_type_cost[][][]" type="text" placeholder="0.00" />
                    <span class="input-group-btn">
                        <button class="btn btn-danger btn-add removeMoreAttrValue" type="button">
                            <span class="glyphicon glyphicon-minus" style="font-size: 12px;"></span>
                        </button>
                    </span>
                </div>
            </td>
            <td>
                <label class="switch"><input type="checkbox" class="is_selected" name="is_selected[][][]"><div class="slider round"></div></label>
            </td>
        </tr>
    </table>

    <div class="tableDiv hide">
        <table class="table attributeTable">
            <tbody>
            <tr>
                <th colspan="3" class="attrTitle">
                    Attribute 1
                </th>
                <th colspan="2" style="text-align: right;">
                    <input type="hidden" name="tbl_serial" class="tbl_serial">
                    <a class="btn btn-xs btn-danger removeMoreAttrsBtn" href="#">
                        <i class="fa fa-minus"></i> Remove Attribute
                    </a>
                </th>
            </tr>
            <tr class="attributeTypeClone">
                <td>
                    <div class="entry input-group col-xs-12">
                        <input class="form-control attribute_class_name" name="attribute_class_name[]" type="text" placeholder="Select Config(text/dropdown/checkbox)" />
                        <input class="service_pricing_class_id" name="service_pricing_class_id[]" type="hidden" />

                    </div>
                </td>
                <td>
                    <div class="entry input-group col-xs-12">
                        <input class="form-control attribute_type_name" name="attribute_type_name[][]" type="text" placeholder="Config type" />
                        <input class="form-control service_pricing_type_id" name="service_pricing_type_id" type="hidden" />
                        <span class="input-group-btn">
                            <button class="btn btn-success btn-add addMoreAttrType" type="button">
                                <span class="glyphicon glyphicon-plus" style="font-size: 12px;"></span>
                            </button>
                        </span>
                    </div>
                </td>
                <td>
                    <div class="entry input-group col-xs-12">
                        <input class="form-control attribute_field_value" name="attribute_field_value[][][]" type="text" placeholder="Config name" />
                        <input class="service_pricing_type_value" name="service_pricing_type_value[][][]" type="hidden" />
                    </div>
                </td>
                <td style="width: 15%">
                    <div class="entry input-group col-xs-12">
                        <input class="form-control service_pricing_type_cost" name="service_pricing_type_cost[][][]" type="text" placeholder="0.00" />
                        <span class="input-group-btn">
                            <button class="btn btn-success btn-add addMoreAttrValue" type="button">
                                <span class="glyphicon glyphicon-plus" style="font-size: 12px;"></span>
                            </button>
                        </span>
                    </div>
                </td>
                <td style="width: 5%">
                    <label class="switch"><input type="checkbox" class="is_selected" name="is_selected[][][]"><div class="slider round"></div></label>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <!-- start: Form to create categories -->

    <!-- End form--->
@endsection
