@extends('layout.master')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/custom.css') }}"/>
@section('content')
    @inject('ACL', 'App\Repositories\RolePermissionForBlade')
{{--    <div class="row">--}}
{{--        <div class="col-md-12">--}}
{{--            @if ($errors->count() > 0 )--}}
{{--                <div class="alert alert-danger">--}}
{{--                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>--}}
{{--                    <h6>The following errors have occurred:</h6>--}}
{{--                    <ul>--}}
{{--                        @foreach( $errors->all() as $message )--}}
{{--                            <li>{{ $message }}</li>--}}
{{--                        @endforeach--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--            @endif--}}
{{--            @if (Session::has('message'))--}}
{{--                <div class="alert alert-success" role="alert">--}}
{{--                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>--}}
{{--                    {{ Session::get('message') }}--}}
{{--                </div>--}}
{{--            @endif--}}
{{--            @if (Session::has('errormessage'))--}}
{{--                <div class="alert alert-danger" role="alert">--}}
{{--                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>--}}
{{--                    {{ Session::get('errormessage') }}--}}
{{--                </div>--}}
{{--            @endif--}}
{{--        </div>--}}
{{--    </div>--}}
    <div class="row ">
        <div class="col-md-12">
            <form role="form" class="form-horizontal addPartnerServiceItemForm">
                <div class="box box-success partnerServiceCategoryDiv">
                    <div class="box-header">
                        <h5 class="box-title">Categories</h5>
                    </div>
                    <div class="box-body">
                        @if(!empty($category_all_data) && count($category_all_data)>0)
                            @foreach($category_all_data as $key => $list)
                                <a class="btn btn-app service-cat {{$key == 0 ? 'selectedCat' : ''}}" href="#" id="{{ $list->id }}">
                                    <i class="fa fa-folder-open fa-4x"></i> <br> {{ $list->category_name }}
                                </a>
                            @endforeach
                        @endif
                        <input type="hidden" name="service_pricing_category_id" class="service_pricing_category_id" value="{{$first_cat_id}}">
                    </div>
                </div>

                <div class="box box-success partnerServiceCatalogueDiv">
                    <div class="box-header">
                        <h5 class="box-title">Service Catalogues</h5>
                    </div>
                    <div class="box-body">
                        @if(!empty($service_catalogue_all_data) && count($service_catalogue_all_data)>0)
                            @foreach($service_catalogue_all_data as $key => $list)
                                <a class="btn btn-app service-catalogue {{$key == 0 ? 'selectedCat' : ''}}" href="#" id="{{ $list->id }}">
                                    <i class="fa fa-folder-open fa-2x"></i> <br> {{ $list->service_name }}
                                </a>
                            @endforeach
                        @endif
                            <input type="hidden" name="service_pricing_catalogue_id" class="service_pricing_catalogue_id" value="{{$first_catalogue_id}}">
                    </div>
                </div>

                <div class="panel panel-default addMoreProductsPanel productItemPanel">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i>
                        Generate Service Items with price
                        <div class="panel-tools">
                            <a class="btn btn-xs btn-success addMoreProductsBtn" href="#">
                                <i class="fa fa-plus"></i> Add More Products
                            </a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="form-field-1">
                                Product Title
                            </label>
                            <div class="col-sm-9" style="margin-bottom: 8px !important;">
                                <input type="text" placeholder="Add product short title" name="service_pricing_title[]" class="form-control service_pricing_title">
                            </div>
                            <span class="errorMsg hide">*Required</span>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="form-field-1">
                                Product Description
                            </label>
                            <div class="col-sm-9" style="margin-bottom: 8px !important;">
                                <textarea placeholder="Product Description" name="service_pricing_description[]" class="form-control service_pricing_description" spellcheck="false"></textarea>
                            </div>
                            <span class="errorMsg hide">*Required</span>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="form-field-1">
                                Currency
                            </label>
                            <div class="col-sm-9" style="margin-bottom: 8px !important;">
                                <select class="form-control service_pricing_currency_unit" name="service_pricing_currency_unit[]">
                                    <option value="">Select Currency</option>
                                    <option value="€">€</option>
                                </select>
                            </div>
                            <span class="errorMsg hide">*Required</span>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="form-field-1">
                                Price
                            </label>
                            <div class="col-sm-9" style="margin-bottom: 8px !important;">
                                <input type="text" placeholder="0.00" name="service_pricing_unit_cost_amount[]" class="form-control service_pricing_unit_cost_amount">
                            </div>
                            <span class="errorMsg hide">*Required</span>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="form-field-1">
                                Commission
                            </label>
                            <div class="col-sm-9" style="margin-bottom: 8px !important;">
                                <input type="text" placeholder="0.00" name="service_commission_price[]" class="form-control service_commission_price" readonly>
                            </div>
                            <span class="errorMsg hide">*Required</span>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="form-field-1">
                                Tax
                            </label>
                            <div class="col-sm-9" style="margin-bottom: 8px !important;">
                                <input type="text" placeholder="0.00" name="service_tax_price[]" class="form-control service_tax_price" readonly>
                            </div>
                            <span class="errorMsg hide">*Required</span>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="form-field-1">
                                Service Type
                            </label>
                            <div class="col-sm-9" style="margin-bottom: 8px !important;">
                                <select multiple="multiple" class="form-control service_frequency_id" name="service_frequency_id[0][]">
                                    <option value="">Select Type</option>
                                    @if(!empty($service_frequency_list) && count($service_frequency_list)>0)
                                        @foreach($service_frequency_list as $key => $list)
                                            <option value="{{$list->id}}">{{$list->frequency_name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <span class="errorMsg hide">*Required</span>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="form-field-1">
                                Upload Images
                            </label>
                            <div class="col-sm-9">
                                <div class="input-images"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <!-- List of Categories -->
            <div class="form-group">
                <input type="submit" class="btn btn-primary btn-group-justified col-xs-4 savePartnerServiceItem" value="Save" style="margin-bottom: 10px;">
            </div>
{{--            <input type="hidden" name="service_partner_id" value="{{isset($partner_info->id) ? $partner_info->id:''}}" class="service_partner_id">--}}
        </div>
    </div>

    <!-- start: Form to create categories -->
    <div id="responsive" class="modal fade viewServiceModal" tabindex="-1" data-width="750" style="display: none;">
        <div class="modal-header p-2">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title text-info" style="padding-left:6%; padding-right:6%;">Service Item Details</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-xs-12" style="padding-left:8%; padding-right:8%;">
                    <h4>Description</h4>
                    <p class="service_desc text-justify">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus nibh sed elimttis adipiscing. Fusce in hendrerit purus. Suspendisse potenti. Proin quis eros odio, dapibus dictum mauris. Donec nisi libero, adipiscing id pretium eget, consectetur sit amet leo. Nam at eros quis mi egestas fringilla non nec purus.
                    </p>
                    <p class="text-capitalize">
                        <strong>Category : </strong><span class="service_category"></span>
                    </p>
                    <p class="text-capitalize">
                        <strong>Service Name : </strong><span class="service_name"></span>
                    </p>
                    <p>
                        <strong>Price : </strong><span class="service_price"></span>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="hide addProductPanelClone">
        <div class="panel panel-default addMoreProductsPanel">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i>
                Generate Service Items with price
                <div class="panel-tools">
                    <a class="btn btn-xs btn-success addMoreProductsBtn" href="#">
                        <i class="fa fa-plus"></i> Add More Products
                    </a>
                    <a class="btn btn-xs panel-close btn-danger removeProducts" href="#">
                        <i class="fa fa-minus"></i> Remove This Product
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="form-field-1">
                        Product Title
                    </label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="Add product short title" name="service_pricing_title[]" class="form-control service_pricing_title">
                    </div>
                    <span class="errorMsg hide">*Required</span>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="form-field-1">
                        Product Description
                    </label>
                    <div class="col-sm-9">
                        <textarea placeholder="Product Description" name="service_pricing_description[]" class="form-control service_pricing_description" spellcheck="false"></textarea>
                    </div>
                    <span class="errorMsg hide">*Required</span>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="form-field-1">
                        Currency
                    </label>
                    <div class="col-sm-9">
                        <select class="form-control service_pricing_currency_unit" name="service_pricing_currency_unit[]">
                            <option value="">Select Currency</option>
                            <option value="€">€</option>
                        </select>
                        {{--                        <input type="text" placeholder="currency" name="service_pricing_currency_unit[]" class="form-control service_pricing_currency_unit">--}}
                    </div>
                    <span class="errorMsg hide">*Required</span>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="form-field-1">
                        Price
                    </label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="0.00" name="service_pricing_unit_cost_amount[]" class="form-control service_pricing_unit_cost_amount">
                    </div>
                    <span class="errorMsg hide">*Required</span>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="form-field-1">
                        Commission
                    </label>
                    <div class="col-sm-9" style="margin-bottom: 8px !important;">
                        <input type="text" placeholder="0.00" name="service_commission_price[]" class="form-control service_commission_price" readonly>
                    </div>
                    <span class="errorMsg hide">*Required</span>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="form-field-1">
                        Tax
                    </label>
                    <div class="col-sm-9" style="margin-bottom: 8px !important;">
                        <input type="text" placeholder="0.00" name="service_tax_price[]" class="form-control service_tax_price" readonly>
                    </div>
                    <span class="errorMsg hide">*Required</span>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="form-field-1">
                        Service Type
                    </label>
                    <div class="col-sm-9" style="margin-bottom: 8px !important;">
                        <select multiple="multiple" class="form-control service_frequency_id" name="service_frequency_id[][]">
                            <option value="">Select Type</option>
                            @if(!empty($service_frequency_list) && count($service_frequency_list)>0)
                                @foreach($service_frequency_list as $key => $list)
                                    <option value="{{$list->id}}">{{$list->frequency_name}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <span class="errorMsg hide">*Required</span>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="form-field-1">
                        Upload Images
                    </label>
                    <div class="col-sm-9">
                        <div class="input-images"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End form--->

@endsection
