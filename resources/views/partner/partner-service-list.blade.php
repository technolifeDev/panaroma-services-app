@extends('layout.master')
@section('content')
    @inject('ACL', 'App\Repositories\RolePermissionForBlade')
    <div class="row">
        <div class="col-md-12">
            @if ($errors->count() > 0 )
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <h6>The following errors have occurred:</h6>
                    <ul>
                        @foreach( $errors->all() as $message )
                            <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (Session::has('message'))
                <div class="alert alert-success" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('message') }}
                </div>
            @endif
            @if (Session::has('errormessage'))
                <div class="alert alert-danger" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('errormessage') }}
                </div>
            @endif
        </div>
    </div>
    <div class="row ">
        <div class="col-md-12">
            <div style="padding:10px;">
                <button href="#responsive" class="demo btn btn-blue createPartnerService">
                    Create Service
                </button>
            </div>
            <!-- List of Categories -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                        </a>
                        <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <a class="btn btn-xs btn-link panel-refresh" href="#">
                            <i class="fa fa-refresh"></i>
                        </a>
                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>
                        <a class="btn btn-xs btn-link panel-close" href="#">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-hover partnerServiceListTbl">
                        <thead class="thead-dark">
                            <tr>
                                <th>SL.no</th>
                                <th class="hidden-xs">Product Title</th>
                                <th class="hidden-xs">Service</th>
                                <th class="hidden-xs">Category</th>
                                <th class="hidden-xs">Price</th>
                                <th class="hidden-xs">Status</th>
                                <th class="hidden-xs">Action</th>
                            </tr>
                        </thead>
                        <tbody>


                        </tbody>
                    </table>
                </div>
            </div>
            <input type="hidden" name="service_partner_id" value="{{isset($partner_info->id) ? $partner_info->id:''}}" class="service_partner_id">
        </div>
    </div>

    <!-- start: Form to create categories -->
    <div id="responsive" class="modal fade viewServiceModal" tabindex="-1" data-width="750" style="display: none;">
        <div class="modal-header p-2">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title text-info" style="padding-left:6%; padding-right:6%;">Service Item Details</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-xs-12" style="padding-left:8%; padding-right:8%;">
                    <h4>Description</h4>
                    <p class="service_desc text-justify">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus nibh sed elimttis adipiscing. Fusce in hendrerit purus. Suspendisse potenti. Proin quis eros odio, dapibus dictum mauris. Donec nisi libero, adipiscing id pretium eget, consectetur sit amet leo. Nam at eros quis mi egestas fringilla non nec purus.
                    </p>
                    <p class="text-capitalize">
                        <strong>Category : </strong><span class="service_category"></span>
                    </p>
                    <p class="text-capitalize">
                        <strong>Service Name : </strong><span class="service_name"></span>
                    </p>
                    <p>
                        <strong>Price : </strong><span class="service_price"></span>
                    </p>
                    <ul class="list-group attr_list">
                        <li class="list-group-item">Cras justo odio</li>
                    </ul>
                    <div class="images_ser">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End form--->

@endsection
