@extends('layout.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            @if ($errors->count() > 0 )
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <h6>The following errors have occurred:</h6>
                    <ul>
                        @foreach( $errors->all() as $message )
                            <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (Session::has('message'))
                <div class="alert alert-success" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('message') }}
                </div>
            @endif
            @if (Session::has('errormessage'))
                <div class="alert alert-danger" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('errormessage') }}
                </div>
            @endif
        </div>
    </div>
    <div class="row ">
        <div class="col-md-12">

                <form method="POST" action="{{url('/admin/category/update').'/'.$category->id }}" >
                    @csrf
                    @method("PUT")

                    <div class="row">
                        <div class="col-md-offset-2 col-md-8">

                            <div class="form-group">
                                <label for="category_name">Category Name</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    name="category_name"
                                    id="category_name"
                                    value="{{$category->category_name}}"
                                    placeholder="Enter Category Name">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Parent Category</label>
                                <select class="form-control" name="parent_id" id="parent_id">
                                    <option value="-1">No parent</option>
                                    @foreach($categories as $cat )
                                        <option value="{{$cat->id}}" {{$cat->id === $category->parent_id ? 'selected' : ''}}> {{ $cat->category_name }} </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" rows="4" name="description" id="description"> {{ $category->description }}</textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-blue">
                                    Save changes
                                </button>
                            </div>
                        </div>
                    </div>
                </form>

        </div>
    </div>
@endsection
