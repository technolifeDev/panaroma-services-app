@extends('layout.master')
<link rel="stylesheet" type="text/css" href="{{ asset('/css/drag-drop.css') }}"/>
@section('content')
    @inject('ACL', 'App\Repositories\RolePermissionForBlade')
    <div class="row">
        <div class="col-md-12">
            @if ($errors->count() > 0 )
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <h6>The following errors have occurred:</h6>
                    <ul>
                        @foreach( $errors->all() as $message )
                            <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (Session::has('message'))
                <div class="alert alert-success" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('message') }}
                </div>
            @endif
            @if (Session::has('errormessage'))
                <div class="alert alert-danger" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('errormessage') }}
                </div>
            @endif
                <div class="alert alert-danger hide" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    Category deleted successfully
                </div>
        </div>
    </div>
    <div class="row ">
        <div class="col-md-12">
            <div style="padding:10px;">
                @if($ACL->hasPermission('create'))
                <button href="#responsive" data-toggle="modal" class="demo btn btn-blue">
                    Add new category
                </button>
                @endif
            </div>


            <!-- List of Categories -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i>

                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                        </a>
                        <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <a class="btn btn-xs btn-link panel-refresh" href="#">
                            <i class="fa fa-refresh"></i>
                        </a>
                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>
                        <a class="btn btn-xs btn-link panel-close" href="#">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered table-hover category_table" id="category_table">
                        <thead>
                            <tr>
                                <th>Sr.no</th>
                                <th>Name</th>
                                <th class="hidden-xs">Parent </th>
                                <th class="hidden-xs">Description</th>
                                <th class="hidden-xs">Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody id="tablecontents">


                        </tbody>
                    </table>

                </div>
            </div>

            <!-- END Categoreis -->
        </div>
    </div>

    <!-- start: Form to create categories -->
    <div id="responsive" class="modal fade" tabindex="-1" data-width="600" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="clip-close-3"></i>
            </button>
            <h4 class="modal-title">Add new category</h4>
        </div>
        <form method="POST" action="{{url('/admin/category/store')}}" enctype="multipart/form-data">
            @csrf
        <div class="modal-body">


                <div class="row">
                    <div class="col-md-offset-2 col-md-8">

                        <div class="row">
                            <ul id="myTab" class="nav nav-tabs tab-green">
                                <li class="active"><a data-toggle="tab" href="#EN">EN</a></li>
                                <li class=""><a data-toggle="tab" href="#EST">EST</a></li>
                                <li class=""><a data-toggle="tab" href="#RUS">RUS</a></li>
                            </ul>
                            <div class="tab-content" style="margin-bottom: 10px;">
                                <div id="EN" class="tab-pane fade in active">
                                    <div class="form-group">
                                        <label for="category_name">Category Name*</label>
                                        <input type="text" class="form-control" name="category_name" id="category_name" placeholder="Enter Category Name" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="description">Description </label>
                                        <textarea class="form-control" rows="4" name="description" id="description"></textarea>
                                    </div>

                                </div>
                                <div id="EST" class="tab-pane fade">
                                    <div class="form-group">
                                        <label for="category_name_est">Category Name </label>
                                        <input type="text" class="form-control" name="category_name_est"  placeholder="Enter Category Name EST" >
                                    </div>
                                    <div class="form-group">
                                        <label for="description_est">Description </label>
                                        <textarea class="form-control" rows="4" name="description_est" ></textarea>
                                    </div>
                                </div>
                                <div id="RUS" class="tab-pane fade">
                                    <div class="form-group">
                                        <label for="category_name_rus">Category Name </label>
                                        <input type="text" class="form-control" name="category_name_rus"  placeholder="Enter Category Name RUS" >
                                        <div class="form-group">
                                            <label for="description_rus">Description </label>
                                            <textarea class="form-control" rows="4" name="description_rus"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputPassword1">Parent Category</label>
                            <select class="form-control" name="parent_id" id="parent_id">
                                <option value="-1">No parent</option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="form-field-select-1">
                                Status
                            </label>
                            <select id="status" class="form-control" name="status">
                                <option value="">Select Status</option>
                                <option value="draft">Draft</option>
                                <option value="active">Active</option>
                                <option value="inactive">Inactive</option>
                                <option value="pending">Pending</option>
                            </select>
                        </div>


                        <div class="form-group">
                            <label>
                                Image ( <small>max size: 50mb</small> )
                            </label>
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px; margin-bottom: 20px !important"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA?text=no+image" alt=""/>
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px; margin-bottom: 20px !important"></div>
                                <div>
                                    <span class="btn btn-light-grey btn-file"><span class="fileupload-new"><i class="fa fa-picture-o"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture-o"></i> Change</span>
                                        <input type="file" name="category_image" id="category_image">
                                    </span>
                                    <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
                                        <i class="fa fa-times"></i> Remove
                                    </a>
                                </div>
                            </div>
                            <br>
                        </div>

                        <div class="form-group">

                        </div>
                    </div>
                </div>

        </div>
        <div class="modal-footer">
{{--            <button type="button" data-dismiss="modal" class="btn btn-light-grey">--}}
{{--                Close--}}
{{--            </button>--}}
            <button type="submit" class="btn btn-blue">
                Save changes
            </button>
        </div>

        </form>
    </div>
    <!-- End form--->

    <!-- start: Form to edit a category -->
    <div id="responsive2" class="modal fade categoryModal" tabindex="-1" data-width="600" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="clip-close-3"></i>
            </button>

                <h4 class="modal-title">Edit category</h4>

        </div>
        <div class="modal-body">
            <form method="POST" action="{{url('/admin/category/update')}}" enctype="multipart/form-data">
                @csrf
                @method("PUT")
                <input type="hidden" class="category_image_id" name="category_image_id">
                <div class="row">
                    <div class="col-md-offset-2 col-md-8">

                        {{--<div class="form-group">
                            <label for="category_name">Category Name *</label>
                            <input type="text" class="form-control" name="category_name" id="category_name_e" placeholder="Enter Category Name" required>
                        </div>--}}
                        <div class="row">
                            <ul id="myTab" class="nav nav-tabs tab-green">
                                <li class="active"><a data-toggle="tab" href="#EN_EDIT">EN</a></li>
                                <li class=""><a data-toggle="tab" href="#EST_EDIT">EST</a></li>
                                <li class=""><a data-toggle="tab" href="#RUS_EDIT">RUS</a></li>
                            </ul>
                            <div class="tab-content" style="margin-bottom: 10px;">
                                <div id="EN_EDIT" class="tab-pane fade in active">
                                    <div class="form-group">
                                        <label for="category_name">Category Name*</label>
                                        <input type="text" class="form-control" name="category_name" id="category_name_e" placeholder="Enter Category Name" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="description">Description </label>
                                        <textarea class="form-control" rows="4" name="description" id="description_e"></textarea>
                                    </div>

                                </div>
                                <div id="EST_EDIT" class="tab-pane fade">
                                    <div class="form-group">
                                        <label for="category_name_est">Category Name </label>
                                        <input type="text" class="form-control" name="category_name_est" id="category_name_est" placeholder="Enter Category Name EST" >
                                    </div>
                                    <div class="form-group">
                                        <label for="description_est">Description </label>
                                        <textarea class="form-control" rows="4" name="description_est" id="description_est"></textarea>
                                    </div>
                                </div>
                                <div id="RUS_EDIT" class="tab-pane fade">
                                    <div class="form-group">
                                        <label for="category_name_rus">Category Name </label>
                                        <input type="text" class="form-control" name="category_name_rus" id="category_name_rus" placeholder="Enter Category Name RUS" >
                                        <div class="form-group">
                                            <label for="description_rus">Description </label>
                                            <textarea class="form-control" rows="4" name="description_rus" id="description_rus"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group" id="parent_id_e">

                        </div>
                        <div class="form-group">
                            <label for="form-field-select-1">
                                Status
                            </label>
                            <select id="status_e" class="form-control" name="status">

                            </select>
                        </div>
                        {{--<div class="form-group">
                            <textarea class="form-control" rows="4" name="description" id="description_e"></textarea>
                        </div>--}}

                        <div class="form-group">
                            <label>
                                Category Image
                            </label>
                            <div class="fileupload fileupload-exists" data-provides="fileupload"><input type="hidden" value="" name="">
                                <div class="fileupload-new thumbnail" style="width: 340px; height: 166px; margin-bottom: 20px !important;">
                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA?text=no+image" alt="">
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="width: 336px; height: 166px; line-height: 166px;  margin-bottom: 20px !important">
{{--                                    @if(isset($partner_image->id))--}}
{{--                                        <img src="{{URL::asset("/partner_images/" .$partner_image->image_name)}}">--}}
{{--                                    @endif--}}
                                </div>
                                <div>
                                    <span class="btn btn-light-grey btn-file"><span class="fileupload-new">
                                            <i class="fa fa-picture-o"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture-o"></i> Change</span>
                                        <input type="file" name="category_image" id="category_image">
                                    </span>
                                    <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
                                        <i class="fa fa-times"></i> Remove
                                    </a>
                                </div>
                            </div>
                            <input type="hidden" class="" value="" name="category_id" id="category_id">
                            <br>
                            <br>
                            <div class="form-group">
                                <button type="submit" class="btn btn-blue pull-right">
                                    Save changes
                                </button>
                            </div>
                    </div>
                </div>
            </form>
        </div>
{{--        <div class="modal-footer">--}}
{{--            <button type="button" data-dismiss="modal" class="btn btn-light-grey">--}}
{{--                Close--}}
{{--            </button>--}}
{{--        </div>--}}
    </div>

    <!-- start: Form to View category -->
    <div id="viewCategory" class="modal fade categoryViewModal" tabindex="-1" data-width="600" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="clip-close-3"></i>
            </button>

            <h4 class="modal-title">View category</h4>

        </div>
        <div class="modal-body">

            <div class="container">
                <div class="row">
                    <div class="col-xs-12">

                            <div class="row">
                                <div class="col-sm-4">
                                    <img width="150px" height="150px" src="" alt="category photo" id="image_v">
                                </div>
                                <div class="col-sm-8">
                                    <strong>Name: </strong><span id="name_v"></span><br>
                                    <strong>Parent: </strong><span id="parent_v"></span><br>

                                    <strong>status: </strong><span id="status_v"></span><br>
                                    <strong>Description: </strong><p id="des_v">San Francisco, USA</p><br>
                                </div>
                            </div>

                    </div>
                </div>
            </div>

        </div>
        <div class="modal-footer">
{{--            <button type="button" data-dismiss="modal" class="btn btn-light-grey">--}}
{{--                Close--}}
{{--            </button>--}}
        </div>
    </div>
@endsection
@section('JScript')
    <script>
        $(function () {
            var site_url = $('.site_url').val();

            $(".category-delete").on('click', function(e) {

                 e.preventDefault();
                var data= {
                    category_id: $(this).data('class-id'),
                    _token: "{{ csrf_token() }}"
                };
                bootbox.dialog({
                    message: "Are you sure you want to delete category?",
                    title: "<i class='glyphicon glyphicon-trash'></i> Delete !",
                    buttons: {
                        success: {
                            label: "No",
                            className: "btn-success btn-squared",
                            callback: function() {
                                $('.bootbox').modal('hide');
                            }
                        },
                        danger: {
                            label: "Delete!",
                            className: "btn-danger btn-squared",
                            callback: function() {
                                $.ajax({
                                    type: 'POST',
                                    url: site_url+'/admin/category/delete',
                                    data: data,
                                }).done(function(response){
                                    bootbox.alert(response,
                                        function(){
                                            location.reload(true);
                                        }
                                    );
                                }).fail(function(response){
                                    bootbox.alert(response);
                                })
                            }
                        }
                    }
                });
            });

        });

    </script>
@endsection
