@extends('layout.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            @if ($errors->count() > 0 )
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <h6>The following errors have occurred:</h6>
                    <ul>
                        @foreach( $errors->all() as $message )
                            <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (Session::has('message'))
                <div class="alert alert-success" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('message') }}
                </div>
            @endif
            @if (Session::has('errormessage'))
                <div class="alert alert-danger" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('errormessage') }}
                </div>
            @endif
        </div>
    </div>
    <div class="row ">
        <div class="col-md-12">

            <form method="POST" action="{{url('/admin/roles/update') }}" >
                @csrf
                @method("PUT")

                <div class="row">
                    <div class="col-md-offset-2 col-md-8">

                        <div class="form-group">
                            <label for="category_name">Role Name</label>
                            <input
                                type="text"
                                class="form-control"
                                name="name"
                                id="role_name"
                                value="{{ $role->name }}"
                                placeholder="Enter Role Name">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Slug</label>
                            <input
                                type="text"
                                class="form-control"
                                name="slug"
                                id="role_slug"
                                value="{{ $role->slug }}"
                                placeholder="Enter Role Slug">
                        </div>

                        <div class="form-group">
                            <textarea class="form-control" rows="4" name="description" id="description">{{ $role->description }}</textarea>
                        </div>
                        <div class="form-group row">
                            @foreach($permissions as $p)
                                <div class="col-md-3">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" name="permissions[]" class="green" value="{{$p->id}}" {{ (in_array($p->id, $role_permissions)) ? "checked": "" }} >
                                            {{$p->name}}
                                    </label>
                                </div>
                            @endforeach
                        </div>
                        <input
                            type="hidden"
                            name="role_id"
                            value="{{ $role->id }}"
                        />

                        <div class="form-group">
                            <button type="submit" class="btn btn-blue">
                                Save changes
                            </button>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
@endsection
