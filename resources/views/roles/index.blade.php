@extends('layout.master')
@section('content')
    @inject('ACL', 'App\Repositories\RolePermissionForBlade')
    <div class="row">
        <div class="col-md-12">
            @if ($errors->count() > 0 )
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <h6>The following errors have occurred:</h6>
                    <ul>
                        @foreach( $errors->all() as $message )
                            <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (Session::has('message'))
                <div class="alert alert-success" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('message') }}
                </div>
            @endif
            @if (Session::has('errormessage'))
                <div class="alert alert-danger" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('errormessage') }}
                </div>
            @endif
            <div class="alert alert-danger hide" role="alert">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                Role deleted successfully
            </div>
        </div>
    </div>
    <div class="row ">
        <div class="col-md-12">
            <div style="padding:10px;">
                @if($ACL->hasPermission('create'))
                    <button href="#add-role" data-toggle="modal" class="demo btn btn-blue">
                        Add new role
                    </button>
                @endif
            </div>


            <!-- List of Categories -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i>

                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                        </a>
                        <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <a class="btn btn-xs btn-link panel-refresh" href="#">
                            <i class="fa fa-refresh"></i>
                        </a>
                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>
                        <a class="btn btn-xs btn-link panel-close" href="#">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered table-hover role_table" id="role_table">
                        <thead>
                        <tr>
                            <th>Sr.no</th>
                            <th>Name</th>
                            <th class="hidden-xs">slug </th>
                            <th class="hidden-xs">description</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>


                        </tbody>
                    </table>

                </div>
            </div>

            <!-- END Categoreis -->
        </div>
    </div>

    <!-- start: Form to create categories -->
    <div id="add-role" class="modal fade" tabindex="-1" data-width="600" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">Add new role</h4>
        </div>
        <div class="modal-body">
            <form method="POST" action="{{url('/admin/roles/store')}}" >
                @csrf
                <div class="row">
                    <div class="col-md-offset-2 col-md-8">

                        <div class="form-group">
                            <label for="category_name">role Name</label>
                            <input type="text" class="form-control" name="name" id="name" placeholder="Enter Role Name" required>
                        </div>
                        <div class="form-group">
                            <label for="slug">Slug</label>
                            <input type="text" id="slug" name="slug" class="form-control" placeholder="Enter Slug">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" rows="4" name="description" id="description"></textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-blue">
                                Save changes
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Close
            </button>
        </div>
    </div>
    <!-- End form--->

    <!-- start: Form to create categories -->
    <div id="edit-role" class="modal fade roleModal" tabindex="-1" data-width="600" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">Edit new role</h4>
        </div>
        <div class="modal-body">
            <form method="POST" action="{{url('/admin/roles/update')}}" >
                @csrf
                <div class="row">
                    <div class="col-md-offset-2 col-md-8">

                        <div class="form-group">
                            <label for="category_name">role Name</label>
                            <input type="text" class="form-control" name="name" id="role_name" placeholder="Enter Role Name" required>
                        </div>
                        <div class="form-group">
                            <label for="slug">Slug</label>
                            <input type="text" id="role_slug" name="slug" class="form-control" placeholder="Enter Slug">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" rows="4" name="description" id="role_description"></textarea>
                        </div>
                        <input type="hidden" class="" value="" name="role_id" id="role_id">
                        <div class="form-group">
                            <button type="submit" class="btn btn-blue">
                                Save changes
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Close
            </button>
        </div>
    </div>

    <!-- End form--->
@endsection
@section('JScript')
    <script>
        $(function () {

        });
    </script>
@endsection
