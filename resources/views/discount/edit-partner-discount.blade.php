@extends('layout.master')
@section('content')
    @inject('ACL', 'App\Repositories\RolePermissionForBlade')
    <div class="row">
        <div class="col-md-12">
            @if ($errors->count() > 0 )
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <h6>The following errors have occurred:</h6>
                    <ul>
                        @foreach( $errors->all() as $message )
                            <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (Session::has('message'))
                <div class="alert alert-success" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('message') }}
                </div>
            @endif
            @if (Session::has('errormessage'))
                <div class="alert alert-danger" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('errormessage') }}
                </div>
            @endif
        </div>
    </div>
    <div class="row ">
        <div class="col-md-12">
            <ul id="myTab" class="nav nav-tabs tab-bricky">
                <li class="active">
                    <a href="">
                        <i class="green fa fa-pencil-square"></i> Edit Partner Discount
                    </a>
                </li>
                <li class="">
                    <a href="{{url('/partner/discount/list')}}">
                        <i class="green fa fa-money"></i> Add Partner Discount
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="add_push">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-external-link-square"></i>
                                    Add Partner Discount
                                    <div class="panel-tools">
                                        <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                        </a>
                                        <a class="btn btn-xs btn-link panel-close" href="#">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <form method="post" action="{{url('/partner/discount/edit',isset($discount->id)?$discount->id:0)}}">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <div class="form-group">
                                            <label for="form-field-23"> Discount Name<span class="symbol required"></span></label>
                                            <input type="text" id="form-field-3" class="form-control" name="partner_discount_name" value="{{isset($discount->partner_discount_name)?$discount->partner_discount_name:''}}" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="form-field-23"> Discount Code <span class="symbol required"></span> </label>
                                            <input type="text" id="form-field-3" class="form-control" name="partner_discount_code" value="{{isset($discount->partner_discount_code)?$discount->partner_discount_code:''}}" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="form-field-23"> Discount Rate<span class="symbol required"></span></label>
                                            <input type="number" id="form-field-3" class="form-control" name="partner_discount_rate" value="{{isset($discount->partner_discount_rate)?$discount->partner_discount_rate:''}}" required>
                                        </div>


                                        <div class="form-group ">
                                            <label for="form-field-23">
                                                 Discount From <span class="symbol required"></span>
                                            </label>
                                            <div class="input-group">
                                                <input type="text" data-date-format="yyyy-mm-dd" data-date-viewmode="years" class="form-control date-picker" name="partner_discount_from" value="{{isset($discount->partner_discount_from)?$discount->partner_discount_from:''}}">
                                                <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <label for="form-field-23">
                                                 Discount To <span class="symbol required"></span>
                                            </label>
                                            <div class="input-group">
                                                <input type="text" data-date-format="yyyy-mm-dd" data-date-viewmode="years" class="form-control date-picker" name="partner_discount_to" value="{{isset($discount->partner_discount_to)?$discount->partner_discount_to:''}}">
                                                <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <input type="reset" class="btn btn-danger" value="Reset">
                                            <input type="submit" class="btn btn-primary" value="Update">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>



                        <div class="col-md-8">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-external-link-square"></i>
                                    Partner Discount List
                                    <div class="panel-tools">
                                        <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                        </a>
                                        <a class="btn btn-xs btn-link panel-close" href="#">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered table-striped nopadding " id="DiscountListDataTableEdit">
                                            <thead>
                                            <tr>
                                                <th>SL</th>
                                                <th>Discount Name</th>
                                                <th>Promo Code</th>
                                                <th>Discount Rate(%)</th>
                                                <th>Start</th>
                                                <th>End</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" class="partner_id" value="{{(isset(\Auth::user()->company_id)&&!empty(\Auth::user()->company_id))?\Auth::user()->company_id:0}}"/>

                        </div>

                            </div>
                        </div>

                    </div>
            </div>
        </div>
    </div>
@endsection
@section('JScript')
    <script>
        $(function () {
            var site_url = $('.site_url').val();
            var partner_id = $('.partner_id').val();

            //MultipleEntry Form
            $("#btnAdd").bind("click", function () {
                var div = $("<tr />");
                div.html(GetDynamicTextBox());
                $("#TextBoxContainer").append(div);
            });
            $("body").on("click", ".remove", function () {
                $(this).closest("tr").remove();
            });

            //single and multiple
            $('.setting_data_type').change(function(){
                var setting_data_type = $(this).val();
                if(setting_data_type == 'array'){
                    $(".single_type_meta").css("display", "none");
                    $(".multiple_type_meta").css("display", "block");
                }else{
                    $(".multiple_type_meta").css("display", "none");
                    $(".single_type_meta").css("display", "block");

                }
            });


            //data table ajax load
            $('#DiscountListDataTableEdit').DataTable({
                "processing": false,
                "serverSide": true,
                "ordering": false,
                "ajax": {
                    "url": site_url+"/partner/ajax/discount/list",
                    "dataType": "json",
                    "type": "POST",
                    "data":{ _token: $('#token').val(),'partner_id':partner_id}
                },
                "aoColumns": [
                    { mData: 'sno'},
                    { mData: 'discount_name'},
                    { mData: 'discount_code'},
                    { mData: 'discount_rate'},
                    { mData: 'discount_start'},
                    { mData: 'discount_end'},
                    { mData: 'discount_status'},
                    { mData: 'actions'}
                ]
            });


            // news delete

            $("body").on("click", ".discount-data-delete", function (e) {
                e.preventDefault();
                var id = $(this).data('discount');
                bootbox.dialog({
                    message: "Are you sure you want to delete this Discount Offer?",
                    title: "<i class='glyphicon glyphicon-trash'></i> Delete !",
                    buttons: {
                        success: {
                            label: "No",
                            className: "btn-success btn-squared",
                            callback: function() {
                                $('.bootbox').modal('hide');
                            }
                        },
                        danger: {
                            label: "Delete!",
                            className: "btn-danger btn-squared",
                            callback: function() {
                                $.ajax({
                                    type: 'GET',
                                    url: site_url+'/partner/ajax/discount/delete/'+id,
                                }).done(function(response){
                                    bootbox.alert(response,
                                        function(){
                                            location.reload(true);
                                        }
                                    );
                                }).fail(function(response){
                                    bootbox.alert(response);
                                })
                            }
                        }
                    }
                });
            });
        });

        </script>
@endsection