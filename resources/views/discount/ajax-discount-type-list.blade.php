@if(!empty($types_list) && count($types_list)>0)
    <option value="0">Select a {{isset($discount_type)?$discount_type:''}}</option>
    @foreach($types_list as $key => $list)
        <option value="{{$list['discount_type_id']}}">{{$list['discount_type_name']}}</option>
    @endforeach
@else
    <option value="0">No data in {{isset($discount_type)?$discount_type:''}}</option>
@endif