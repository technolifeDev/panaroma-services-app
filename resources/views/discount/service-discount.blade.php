@extends('layout.master')
@section('content')
    @inject('ACL', 'App\Repositories\RolePermissionForBlade')
    <div class="row">
        <div class="col-md-12">
            @if ($errors->count() > 0 )
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <h6>The following errors have occurred:</h6>
                    <ul>
                        @foreach( $errors->all() as $message )
                            <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (Session::has('message'))
                <div class="alert alert-success" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('message') }}
                </div>
            @endif
            @if (Session::has('errormessage'))
                <div class="alert alert-danger" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('errormessage') }}
                </div>
            @endif
        </div>
    </div>
    <div class="row ">
        <div class="col-md-12">
            <ul id="myTab" class="nav nav-tabs tab-bricky">
                <li class="active">
                    <a href="">
                        <i class="green fa fa-tags"></i> Service Discount Settings
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="add_push">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-external-link-square"></i>
                                    Add Service Discount
                                    <div class="panel-tools">
                                        <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                        </a>
                                        <a class="btn btn-xs btn-link panel-close" href="#">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <form method="post" action="{{url('/partner/service/discount/list')}}">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <div class="form-group">
                                            <label for="form-field-23">Discount Type</label>
                                            <select class="form-control service_discount_type" name="service_discount_type" required>
                                                <option value="">Select Discount Type</option>
                                                <option {{(old('service_discount_type')== "categories") ? "selected" :''}} value="categories">Categories</option>
                                                <option {{(old('service_discount_type')== "catalogues") ? "selected" :''}} value="catalogues">Catalogues</option>
                                                <option  {{(old('service_discount_type')== "items") ? "selected" :''}} value="items">Service Items</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="form-field-23">Discount Type Id</label>
                                            <select class="form-control discount_type_list" name="service_discount_type_id" required>
                                                <option value="">Select Type Id</option>
                                            </select>
                                        </div>


                                        <div class="form-group">
                                            <label for="form-field-23">Discount Rate (%)</label>
                                            <input type="number"  id="form-field-3" class="form-control" name="service_discount_rate" value="{{old('service_discount_rate')}}" required>
                                        </div>

                                        <div class="form-group ">
                                            <label for="form-field-23">
                                                Discount From <span class="symbol required"></span>
                                            </label>
                                            <div class="input-group">
                                                <input type="text" data-date-format="yyyy-mm-dd" data-date-viewmode="years" class="form-control date-picker" name="service_discount_from" required>
                                                <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <label for="form-field-23">
                                                Discount To <span class="symbol required"></span>
                                            </label>
                                            <div class="input-group">
                                                <input type="text" data-date-format="yyyy-mm-dd" data-date-viewmode="years" class="form-control date-picker" name="service_discount_to" required>
                                                <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <input type="reset" class="btn btn-danger" value="Reset">
                                            <input type="submit" class="btn btn-primary" value="Save">
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>



                        <div class="col-md-8">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-external-link-square"></i>
                                    Service Discount List
                                    <div class="panel-tools">
                                        <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                        </a>
                                        <a class="btn btn-xs btn-link panel-close" href="#">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered table-striped nopadding" id="ServiceDiscountListDataTable">
                                            <thead>
                                            <tr>
                                                <th>SL</th>
                                                <th>Discount Type</th>
                                                <th>Discount Name</th>
                                                <th>Discount Rate(%)</th>
                                                <th>Start</th>
                                                <th>End</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" class="partner_id" value="{{(isset(\Auth::user()->company_id)&&!empty(\Auth::user()->company_id))?\Auth::user()->company_id:0}}"/>

                        </div>

                            </div>
                        </div>

                    </div>
            </div>
        </div>
    </div>
@endsection
@section('JScript')
    <script>
        $(function () {
            var site_url = $('.site_url').val();
            var partner_id = $('.partner_id').val();

            //MultipleEntry Form
            $("#btnAdd").bind("click", function () {
                var div = $("<tr />");
                div.html(GetDynamicTextBox());
                $("#TextBoxContainer").append(div);
            });
            $("body").on("click", ".remove", function () {
                $(this).closest("tr").remove();
            });

            //single and multiple
            $('.setting_data_type').change(function(){
                var setting_data_type = $(this).val();
                if(setting_data_type == 'array'){
                    $(".single_type_meta").css("display", "none");
                    $(".multiple_type_meta").css("display", "block");
                }else{
                    $(".multiple_type_meta").css("display", "none");
                    $(".single_type_meta").css("display", "block");

                }
            });


            //data table ajax load
            $('#ServiceDiscountListDataTable').DataTable({
                "processing": false,
                "serverSide": true,
                "ordering": false,
                "ajax": {
                    "url": site_url+"/partner/ajax/service/discount/list",
                    "dataType": "json",
                    "type": "POST",
                    "data":{ _token: $('#token').val(),'partner_id':partner_id}
                },
                "aoColumns": [
                    { mData: 'sno'},
                    { mData: 'discount_type'},
                    { mData: 'discount_name'},
                    { mData: 'discount_rate'},
                    { mData: 'discount_start'},
                    { mData: 'discount_end'},
                    { mData: 'discount_status'},
                    { mData: 'actions'}
                ]
            });


            // news delete

            $("body").on("click", ".service-discount-data-delete", function (e) {
                e.preventDefault();
                var id = $(this).data('service_discount');
                bootbox.dialog({
                    message: "Are you sure you want to delete this Discount Offer?",
                    title: "<i class='glyphicon glyphicon-trash'></i> Delete !",
                    buttons: {
                        success: {
                            label: "No",
                            className: "btn-success btn-squared",
                            callback: function() {
                                $('.bootbox').modal('hide');
                            }
                        },
                        danger: {
                            label: "Delete!",
                            className: "btn-danger btn-squared",
                            callback: function() {
                                $.ajax({
                                    type: 'GET',
                                    url: site_url+'/partner/ajax/service/discount/delete/'+id,
                                }).done(function(response){
                                    bootbox.alert(response,
                                        function(){
                                            location.reload(true);
                                        }
                                    );
                                }).fail(function(response){
                                    bootbox.alert(response);
                                })
                            }
                        }
                    }
                });
            });
        });

        /*##########################################
       # discount type
       ############################################
       */

        jQuery(function(){
            jQuery('.service_discount_type').change(function(){

                var discount_type_name = jQuery(this).val();
                var site_url = jQuery('.site_url').val();

                if(discount_type_name.length != 0){
                    var request_url = site_url+'/partner/service/ajax/discount-type-list/'+discount_type_name;
                    jQuery.ajax({
                        url: request_url,
                        type: 'get',
                        success:function(data){
                            jQuery('.discount_type_list').html(data);
                        }
                    });
                }
            });
        });
        </script>
@endsection