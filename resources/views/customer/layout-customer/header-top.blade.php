<div class="page-top-r">
    <div class='nav-div'>
        <ul class="nav justify-content-end nav-page">
            <li class="nav-item">
                <div class="dropdown">
                    <button class="btn btn-green blue-color dropdown-toggle nav-link" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                       {{-- @lang('auth.top-menu-button-language')--}}
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item language-dropdown" href="{{url('/customer/language/en')}}">EN</a>
						<a class="dropdown-item language-dropdown" href="{{url('/customer/language/est')}}">EST</a>
						<a class="dropdown-item language-dropdown" href="{{url('/customer/language/rus')}}">RUS</a>
                    </div>
                </div>
            </li>
            @if(\Auth::check())
                <li class="nav-item">
                    <a class="nav-link" href="#">|</a>
                </li>
				<li class="nav-item">
					<div class="dropdown">
						<button class="btn blue-color dropdown-toggle nav-link " type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						 @lang('auth.top-menu-welcome') {{\Auth::user()->name}}
						</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							{{--<a class="dropdown-item" href="{{url('/customer/profile',\Auth::user()->email)}}">@lang('auth.top-menu-button-logout')</a>--}}
                            <a class="dropdown-item" href="{{url('/customer/order-list')}}">@lang('auth.order-history-btn')</a>
                            <a class="dropdown-item" href="#">@lang('auth.terms_condition')</a>
                            <a class="dropdown-item" href="#">@lang('auth.privacy_policy')</a>
                            <a class="dropdown-item" href="{{url('/customer/logout',\Auth::user()->email)}}">@lang('auth.top-menu-button-logout')</a>
                        </div>
					</div>
                </li>
            @endif

        </ul>
    </div>
</div>
