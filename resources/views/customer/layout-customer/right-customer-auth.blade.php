<div class="home-right">
    <div class='nav-div'>
        <ul class="nav justify-content-end">
            <li class="nav-item">
                <div class="dropdown">
                    <button class="btn btn-green dropdown-toggle nav-link" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      {{-- @lang('auth.top-menu-button-language')--}}
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">                        
                        <a class="dropdown-item language-dropdown" href="{{url('/customer/language/en')}}">EN</a>
						<a class="dropdown-item language-dropdown" href="{{url('/customer/language/est')}}">EST</a>
						<a class="dropdown-item language-dropdown" href="{{url('/customer/language/rus')}}">RUS</a>
                    </div>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">|</a>
            </li>
            <li class="nav-item">
                <a class="nav-link"></a>
            </li>
            @if(\Auth::check())
                <li class="nav-item">
                    <a class="nav-link" href="#">{{\Auth::user()->name}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">|</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/customer/logout',\Auth::user()->email)}}">@lang('auth.top-menu-button-logout')</a>
                </li>
            @else
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/customer/login')}}">@lang('auth.top-menu-button-login')</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">|</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/customer/registration')}}">@lang('auth.top-menu-button-register')</a>
                </li>
            @endif
        </ul>
    </div>
    <div class='kalevi_logo'>
        <img class="img-fluid" src="{{asset('/front-end/images/kalevi_logo.png')}}" />
    </div>
    <div class='start-text'>
        @if(isset($page_title) && ($page_title=='Kalevi Panorama'))
            <p>@lang('auth.right_side_page_browse')</p>
            <p>@lang('auth.right_side_page_service')</p>
        @endif
    </div>
    <div class='sign_up_button'>
        @if(isset($page_title) && ($page_title=='Kalevi Panorama'))
            <button class="btn btn-large btn-primary" onclick="location.href='{{url('/customer/category-items')}}';">@lang('auth.button-click_service')</button>
        @endif
    </div>
    <div class='logo'>
        <img class="img-fluid" src="{{asset('/front-end/images/logo.png')}}" />
    </div>

    <div class='contact'>
        <span class="contact-left">nutikodu@technolife.ee </span><span class="contact-right">+372 5624 6276</span>
    </div>
</div>