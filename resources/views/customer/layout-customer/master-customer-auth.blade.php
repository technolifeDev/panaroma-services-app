<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{asset('/front-end/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('/front-end/css/glyphicons.css')}}">
        <link href="https://fonts.googleapis.com/css?family=Exo:400,600,700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('/front-end/css/style.css')}}">

        <title>{{isset($page_title) ? $page_title : ''}} | Kalevi Panorama</title>
        @yield('CSS')
    </head>
    <body>
        <div class="container">
            @yield('auth-content')
        </div>

        @yield('JScript')
        <script src="{{asset('/front-end/js/jquery-3.4.1.min.js')}}"></script>
        <script src="{{asset('/front-end/js/popper.min.js')}}"></script>
        <script src="{{asset('/front-end/js/bootstrap.min.js')}}"></script>

		<script>
		$(function(){
			//----------------------------------Language dropdown------------------------------------
			//written by Momit
			var language = localStorage.getItem('language');
			if($.trim(language)==""){
				var default_language = $('.dropdown-item:first').html();
				localStorage.setItem('language', default_language);
				language = localStorage.getItem('language');
			}
			$('#dropdownMenuButton').html(language);

			$('.dropdown-item').on('click', function (e) {
				var selected_language = $(this).html();
				$('#dropdownMenuButton').html(selected_language);
				localStorage.setItem('language', selected_language);
			})

			localStorage.removeItem('selected_category');
			localStorage.removeItem('selected_id');
			localStorage.removeItem('selected_date');
			localStorage.removeItem('selected_time');
			localStorage.removeItem('selected_urgent');
			localStorage.removeItem('selected_date_html');
			localStorage.removeItem('order_step');
			/*-------------------------------- end of language ---------------------------------*/
		})
		</script>



    </body>
</html>
