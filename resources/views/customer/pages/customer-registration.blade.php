@extends('customer.layout-customer.master-customer-auth')
@section('auth-content')
<div class="home-left">
    
    <div class="reg-l-top">
		<div  class="hr-div"><hr class="top-dash"></hr></div>
        <p class="welcome-text">@lang('auth.reg_page_title')</p>
        <p class="detail-text">@lang('auth.reg_page_desc') </p>
    </div>
    <div class="reg-l-bottom">
        @if ($errors->count() > 0 )
            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <h6>The following errors have occurred:</h6>
                <ul>
                    @foreach( $errors->all() as $message )
                        <li>{{ $message }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (Session::has('message'))
            <div class="alert alert-success" role="alert">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ Session::get('message') }}
            </div>
        @endif
        @if (Session::has('errormessage'))
            <div class="alert alert-danger" role="alert">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ Session::get('errormessage') }}
            </div>
        @endif
        <form method="post" action="{{url('/customer/registration')}}" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <div class="form-group">
                <label for="inputName">@lang('auth.field-name')</label>
                <input type="text" class="form-control reg_field" name="name" required="">
            </div>

            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="inputEmail4">@lang('auth.field-email')</label>
                    <input type="email" class="form-control reg_field" name="email" required>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="inputPassword4">@lang('auth.field-password')</label>
                    <input type="password" class="form-control reg_field" name="password" required>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="inputPassword4">@lang('auth.field-confirm_password')</label>
                    <input type="password" class="form-control reg_field" name="confirm_password" required>
                </div>
            </div>
            <div class="form-group">
                <label for="inputAddress2">@lang('auth.field-address')</label>
                <input type="text" class="form-control reg_field" name="address" required>
            </div>
            <br \>
            <div class='register_button text-right'>
                <button type="submit" class="btn btn-large btn-primary" value="" >@lang('auth.button-next')</button>
            </div>
        </form>
    </div>
</div>
@include('customer.layout-customer.right-customer-auth')
@endsection