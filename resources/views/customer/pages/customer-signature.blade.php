@extends('customer.layout-customer.master-customer')
@section('CSS')
    <style type="text/css">
    </style>
@endsection
@section('page-content')
    <div class="page-top">
        <div class="page-top-l">
            <div class="page-top-l-text">
                <div  class="hr-div"><hr class="top-dash"></hr></div>
                <p class="welcome-text">@lang('auth.signature_page_title')</p>
            </div>
        </div>
        @include('customer.layout-customer.header-top')
    </div>

    <div class="page-bottom">
        <p class="sig-bottom-text">@lang('auth.signature_page_desc')</p>
        <div class="page-bottom-content">
            <div class="signn-box" >
                <div id="canvas-box" style="display: block;">
                    <canvas id="sig-canvas"  height="266" width="1298">{{--<canvas id="sig-canvas" width="380" height="160">--}}
                        Get a better browser, bro.
                    </canvas>
                </div>
                <div id="signature-box" style="display: none;">
                    <img id="sig-image" src="" alt=""/>
                </div>
            </div>


                <div class='register1_button text-left'>
                    <button class="btn btn-large btn-primary" id="sig-submitBtn" >@lang('auth.button-submit_signature')</button>
                    <button class="btn btn-large btn-primary" id="sig-clearBtn" >@lang('auth.button-clear_signature')</button>
                </div>

                <form method="post" action="{{url('/customer/registration')}}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" id="signature_image" name="signature_image" value="">
                    <div class='register1_button text-right' id="btn-finish" style="display: none;">
                        <button type="submit" class="btn btn-large btn-primary" id="sign_signature" >@lang('auth.button-sign_signature')</button>
                    </div>
                </form>
        </div>
    </div>


@endsection
@section('JScript')
    <script>
        (function() {
            window.requestAnimFrame = (function(callback) {
                return window.requestAnimationFrame ||
                    window.webkitRequestAnimationFrame ||
                    window.mozRequestAnimationFrame ||
                    window.oRequestAnimationFrame ||
                    window.msRequestAnimaitonFrame ||
                    function(callback) {
                        window.setTimeout(callback, 1000 / 60);
                    };
            })();

            var canvas = document.getElementById("sig-canvas");
            var ctx = canvas.getContext("2d");
            ctx.strokeStyle = "#222222";
            ctx.lineWidth = 3;

            var drawing = false;
            var mousePos = {
                x: 0,
                y: 0
            };
            var lastPos = mousePos;

            canvas.addEventListener("mousedown", function(e) {
                drawing = true;
                lastPos = getMousePos(canvas, e);
            }, false);

            canvas.addEventListener("mouseup", function(e) {
                drawing = false;
            }, false);

            canvas.addEventListener("mousemove", function(e) {
                mousePos = getMousePos(canvas, e);
            }, false);

            // Add touch event support for mobile
            canvas.addEventListener("touchstart", function(e) {

            }, false);

            canvas.addEventListener("touchmove", function(e) {
                var touch = e.touches[0];
                var me = new MouseEvent("mousemove", {
                    clientX: touch.clientX,
                    clientY: touch.clientY
                });
                canvas.dispatchEvent(me);
            }, false);

            canvas.addEventListener("touchstart", function(e) {
                mousePos = getTouchPos(canvas, e);
                var touch = e.touches[0];
                var me = new MouseEvent("mousedown", {
                    clientX: touch.clientX,
                    clientY: touch.clientY
                });
                canvas.dispatchEvent(me);
            }, false);

            canvas.addEventListener("touchend", function(e) {
                var me = new MouseEvent("mouseup", {});
                canvas.dispatchEvent(me);
            }, false);

            function getMousePos(canvasDom, mouseEvent) {
                var rect = canvasDom.getBoundingClientRect();
                return {
                    x: mouseEvent.clientX - rect.left,
                    y: mouseEvent.clientY - rect.top
                }
            }

            function getTouchPos(canvasDom, touchEvent) {
                var rect = canvasDom.getBoundingClientRect();
                return {
                    x: touchEvent.touches[0].clientX - rect.left,
                    y: touchEvent.touches[0].clientY - rect.top
                }
            }

            function renderCanvas() {
                if (drawing) {
                    ctx.moveTo(lastPos.x, lastPos.y);
                    ctx.lineTo(mousePos.x, mousePos.y);
                    ctx.strokeStyle = "#222222";
                    ctx.lineWidth = 3;
                    ctx.lineCap = 'round';
                    ctx.stroke();
                    lastPos = mousePos;
                }
            }

            // Prevent scrolling when touching the canvas
            document.body.addEventListener("touchstart", function(e) {
                if (e.target == canvas) {
                    e.preventDefault();
                }
            }, false);
            document.body.addEventListener("touchend", function(e) {
                if (e.target == canvas) {
                    e.preventDefault();
                }
            }, false);
            document.body.addEventListener("touchmove", function(e) {
                if (e.target == canvas) {
                    e.preventDefault();
                }
            }, false);

            (function drawLoop() {
                requestAnimFrame(drawLoop);
                renderCanvas();
            })();

            function clearCanvas() {
                canvas.width = canvas.width;
            }

            function isCanvasBlank(canvas) {
                const context = canvas.getContext('2d');

                const pixelBuffer = new Uint32Array(
                    context.getImageData(0, 0, canvas.width, canvas.height).data.buffer
                );

               return !pixelBuffer.some(color => color !== 0);
            }

            // Set up the UI
            var sigText = document.getElementById("sig-dataUrl");
            var sigImage = document.getElementById("sig-image");
            var clearBtn = document.getElementById("sig-clearBtn");
            var submitBtn = document.getElementById("sig-submitBtn");
            var signature_image = document.getElementById("signature_image");
            var finish_btn = document.getElementById("btn-finish");
            var signature_box = document.getElementById("signature-box");
            var canvas_box = document.getElementById("canvas-box");
            clearBtn.addEventListener("click", function(e) {
                clearCanvas();
                //sigText.innerHTML = "Data URL for your signature will go here!";
                sigImage.setAttribute("src", "");
                finish_btn.setAttribute('style','display: none');
                signature_box.setAttribute('style','display: none');
                canvas_box.setAttribute('style','display: block');

                //submitBtn.prop('disabled', false);
                submitBtn.removeAttribute('disabled');
				//$("#sig-submitBtn").css('display','block');

                //signature_box.innerHTML = '<canvas id="sig-canvas" width="1000" height="160">Get a better browser, bro.</canvas>';
            }, false);
            submitBtn.addEventListener("click", function(e) {
				//$(this).css('display','none');
                const blank = isCanvasBlank(document.getElementById('sig-canvas'));

                if(!blank){
                    var dataUrl = canvas.toDataURL();
                    //canvas.innerHTML = '<img id="sig-image" src="'+dataUrl+'" alt=""/>';
                    sigImage.setAttribute("src", dataUrl);
                    signature_image.value=dataUrl;
                    finish_btn.setAttribute('style','display: block');
                    canvas_box.setAttribute('style','display: none');
                    signature_box.setAttribute('style','display: block');
                   // submitBtn.prop('disabled', true);
                    submitBtn.setAttribute('disabled','disabled');
                }else alert('Signature Required !!')

            }, false);

        })();
    </script>
@endsection