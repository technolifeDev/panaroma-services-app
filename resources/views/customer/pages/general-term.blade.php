@extends('customer.layout-customer.master-customer')
@section('page-content')
    <div class="page-top">
        <div class="page-top-l">           
            <div class="page-top-l-text">
			    <div  class="hr-div"><hr class="top-dash"></hr></div>
                <p class="welcome-text">@lang('auth.general_term_page_title')</p>
            </div>
        </div>
        @include('customer.layout-customer.header-top')
    </div>
    <div class="page-bottom">
        <p class="page-bottom-text">@lang('auth.general_term_page_desc')</p>
        <div class="page-bottom-content">
            <p class="page-bottom-text">
                Getting legal agreements and contracts drawn up is usually experienced as expensive, time-consuming, complicated, and inconvenient. Not anymore. With Agreements Online you can get your legal agreements online, make any changes that you want, and have your business contracts and documents ready to use in no time.
                <br><br>
                The world is constantly changing and evolving, and with the advent of the internet, the law is no longer accessible only to the fortunate few. With Agreements Online you can manage your legal matters yourself without having to consult expensive lawyers every time you need a legal agreement template.
                <br><br>
                <strong>Thousands of satisfied customers have used Agreements Online because:</strong>
                <br><br>
                <strong>Our business agreements are drafted in South Africa, by South Africans, i</strong>n terms of South African la
                w. Almost all of the online agreements and documents that you find on the internet have been created in other countries, by people who do not consider South African law when they draft their contracts. If your contract contains clauses that are prohibited by law, you could find the agreement and even the entire business transaction being declared null and void. So you may want to check the nationality of the website before downloading your contract!
                <br><br>
                <strong>Business contracts offer you vital protection.</strong> Our customers know that having written agreements in place makes good business sense. However, most of the legal agreement templates available online offer only the bare minimum, and majority have been drafted in foreign jurisdictions. Our documents use the collective knowledge and experience of our specialist drafters to offer you comprehensive agreements that combine business imperatives with South African legal requirements.
                <br><br>
                <strong>They’re easy to edit and complete.</strong> Our business agreements and documents are provided in Word format, which makes it easy for you to edit and customise them to suit your business requirements.

                You can understand our documents. Yes, they are written in English! Plain, easy-to-understand English. We don’t use Latin, complicated cross-referencing or pretentious wording. Our contracts are in plain language so that all the parties understand what they’re signing. This eliminates ambiguity and misunderstanding, and potentially avoids disputes and lengthy legal battles down the line.
                <br>
                We offer a money back guarantee. If, after buying one of our agreements, you realise that it isn’t suitable for your needs then we will reimburse you what you paid for it. It really is that simple.
            </p>
            <br><br>
            <form method="post" action="{{url('/customer/registration')}}" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <label class="check-container"> @lang('auth.button-agree')
                    <input type="checkbox" name="agreement" value="yes" required>
                    <span class="checkmark"></span>
                </label>
                <div class='register_button general_agreement_btn no-top-padding text-right'>
                    <button type="submit" class="btn btn-large btn-primary" value="" >@lang('auth.sign-agreement')</button>
                </div>
            </form>
        </div>
    </div>
@endsection