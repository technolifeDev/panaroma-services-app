<div class="invoice overflow-auto">
    <div>
        <header>
            <div class="row">
                <div class="col">
                    <a target="_blank">
                        <img style="width: 20%;" src="{{url('service_images/partner_images/'. (isset($getOrderDtls[0]['image_name']) ? $getOrderDtls[0]['image_name'] : "" ))}}" data-holder-rendered="true" alt="{{ (isset($getOrderDtls[0]['partner_name']) ? $getOrderDtls[0]['partner_name'] : "" ) }}"/>
                    </a>
                </div>
                <div class="col company-details">
                    <h2 class="name">
                        <a target="_blank" href="#">
                            @if(Lang::locale() == 'est' && $getOrderDtls[0]['partner_name_est'])
                                {{isset($getOrderDtls[0]['partner_name_est']) ?$getOrderDtls[0]['partner_name_est'] : ''}}
                            @elseif(Lang::locale() == 'rus' && $getOrderDtls[0]['partner_name_rus'])
                                {{isset($getOrderDtls[0]['partner_name_rus']) ?$getOrderDtls[0]['partner_name_rus'] : ''}}
                            @else
                                {{isset($getOrderDtls[0]['partner_name']) ?$getOrderDtls[0]['partner_name'] : ''}}
                            @endif
                        </a>
                    </h2>
                    <div>
                        @if(Lang::locale() == 'est' && $getOrderDtls[0]['partner_address_est'])
                            {{isset($getOrderDtls[0]['partner_address_est']) ?$getOrderDtls[0]['partner_address_est'] : ''}}
                        @elseif(Lang::locale() == 'rus' && $getOrderDtls[0]['partner_address_rus'])
                            {{isset($getOrderDtls[0]['partner_address_rus']) ?$getOrderDtls[0]['partner_address_rus'] : ''}}
                        @else
                            {{isset($getOrderDtls[0]['partner_address']) ?$getOrderDtls[0]['partner_address'] : ''}}
                        @endif
                        {{ (isset($getOrderDtls[0]['partner_address']) ? $getOrderDtls[0]['partner_address'] : "" ) }}
                    </div>
{{--                    <div>(123) 456-789</div>--}}
                    <div>{{ (isset($getOrderDtls[0]['partner_email']) ? $getOrderDtls[0]['partner_email'] : "" ) }}</div>
                </div>
            </div>
        </header>
        <main>
            <div class="row contacts">
                <div class="col invoice-to">
                    <div class="text-gray-light">@lang('order.customer_details'):</div>
                    <h2 class="to">{{isset($getOrderDtls[0]['name']) ?$getOrderDtls[0]['name'] : ''}}</h2>
                    <div class="address">{{isset($getOrderDtls[0]['service_order_customer_address']) ?$getOrderDtls[0]['service_order_customer_address'] : ''}}</div>
                    <div class="email"><a href="#">{{isset($getOrderDtls[0]['customer_email']) ?$getOrderDtls[0]['customer_email'] : ''}}</a></div>
                </div>
                <div class="col invoice-details">
                    <h4 class="invoice-id">@lang('order.order_no')# {{isset($getOrderDtls[0]['service_orders_id']) ? str_pad($getOrderDtls[0]['service_orders_id'], 6, "0", STR_PAD_LEFT) : ''}}</h4>
                    <h4 class="invoice-id">
                        @if(Lang::locale() == 'est' && $getOrderDtls[0]['frequency_name_est'])
                            {{isset($getOrderDtls[0]['frequency_name_est']) ?$getOrderDtls[0]['frequency_name_est'] : ''}}
                        @elseif(Lang::locale() == 'rus' && $getOrderDtls[0]['frequency_name_rus'])
                            {{isset($getOrderDtls[0]['frequency_name_rus']) ?$getOrderDtls[0]['frequency_name_rus'] : ''}}
                        @else
                            {{isset($getOrderDtls[0]['frequency_name']) ?$getOrderDtls[0]['frequency_name'] : ''}}
                        @endif
                    </h4>
                    <h4 class="order_status">{{isset($getOrderDtls[0]['service_order_status']) ?$getOrderDtls[0]['service_order_status'] : ''}}</h4>
                    <div class="date">@lang('order.service_date'): {{isset($getOrderDtls[0]['service_date']) && $getOrderDtls[0]['service_date'] != '0000-00-00' ? \Carbon\Carbon::parse($getOrderDtls[0]['service_date'])->format('j F, Y') . ' | ' : 'NA' }}
                        {{isset($getOrderDtls[0]['service_time']) ? ($getOrderDtls[0]['service_time']) : ''}}
                    </div>
                    <div class="date">@lang('order.order_date'): {{isset($getOrderDtls[0]['order_date']) ? \Carbon\Carbon::parse($getOrderDtls[0]['order_date'])->format('j F, Y') : ''}}</div>

                </div>
            </div>
            <table border="0" cellspacing="0" cellpadding="0">
                <thead>
                <tr>
                    <th style="width: 5%">#</th>
                    <th class="text-left" style="text-transform: uppercase !important;">@lang('order.service')</th>
                    <th class="text-left" style="text-transform: uppercase !important;">@lang('service_categories.category')</th>
{{--                    <th class="text-left" style="width: 40%">DESCRIPTION</th>--}}
                    <th class="text-right" style="width: 15%; text-transform: uppercase !important;">@lang('order.total')</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="no">01</td>
                        <td class="text-left">
                            <h3>
                                @if(Lang::locale() == 'est' && isset($getOrderDtls[0]['service_pricing_title_est']))
                                    {{ isset($getOrderDtls[0]['service_pricing_title_est']) ? ($getOrderDtls[0]['service_pricing_title_est']) : '' }}
                                @elseif(Lang::locale() == 'rus' && isset($getOrderDtls[0]['service_pricing_title_rus']))
                                    {{ isset($getOrderDtls[0]['service_pricing_title_rus']) ? ($getOrderDtls[0]['service_pricing_title_rus']) : '' }}
                                @else
                                    {{ isset($getOrderDtls[0]['service_pricing_title']) ? ($getOrderDtls[0]['service_pricing_title']) : '' }}
                                @endif
                               </h3>
                        </td>
                        <td class="text-left">
                            @if(Lang::locale() == 'est' && isset($getOrderDtls[0]['category_name_est']))
                                {{ isset($getOrderDtls[0]['category_name_est']) ? ($getOrderDtls[0]['category_name_est']) : '' }}
                            @elseif(Lang::locale() == 'rus' && isset($getOrderDtls[0]['category_name_rus']))
                                {{ isset($getOrderDtls[0]['category_name_rus']) ? ($getOrderDtls[0]['category_name_rus']) : ''  }}
                            @else
                                {{ isset($getOrderDtls[0]['category_name']) ? ($getOrderDtls[0]['category_name']) : ''  }}
                            @endif
                        </td>
{{--                        <td class="text-left">--}}
{{--                            @if(Lang::locale() == 'est' && isset($getOrderDtls[0]['service_pricing_description_est']))--}}
{{--                                {!!  isset($getOrderDtls[0]['service_pricing_description_est']) ? ($getOrderDtls[0]['service_pricing_description_est']) : ''  !!}--}}
{{--                            @elseif(Lang::locale() == 'rus' && isset($getOrderDtls[0]['service_pricing_description_rus']))--}}
{{--                                {!! isset($getOrderDtls[0]['service_pricing_description_rus']) ? ($getOrderDtls[0]['service_pricing_description_rus']) : ''  !!}--}}
{{--                            @else--}}
{{--                                {!! isset($getOrderDtls[0]['service_pricing_description']) ? ($getOrderDtls[0]['service_pricing_description']) : ''  !!}--}}
{{--                            @endif--}}
{{--                        </td>--}}
                        <td class="total">
                            {{ isset($getOrderDtls[0]['service_pricing_currency_unit']) ? ($getOrderDtls[0]['service_pricing_currency_unit']) : '' }}
                            {{ isset($getOrderDtls[0]['orders_items_type_cost']) ? ($getOrderDtls[0]['orders_items_type_cost']) : '' }}
                        </td>
                    </tr>
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="3">@lang('order.sub_total')</td>
                    <td>
                        {{ isset($getOrderDtls[0]['service_pricing_currency_unit']) ? ($getOrderDtls[0]['service_pricing_currency_unit']) : '' }}
                        {{ isset($getOrderDtls[0]['service_order_grand_total']) ? ($getOrderDtls[0]['service_order_grand_total']) : '' }}
                    </td>
                </tr>
                <tr>
                    <td colspan="3">@lang('order.grand_total')</td>
                    <td>
                        {{ isset($getOrderDtls[0]['service_pricing_currency_unit']) ? ($getOrderDtls[0]['service_pricing_currency_unit']) : '' }}
                        {{ isset($getOrderDtls[0]['service_order_grand_total']) ? ($getOrderDtls[0]['service_order_grand_total']) : '' }}
                    </td>
                </tr>
                </tfoot>
            </table>
{{--            <div class="thanks">Thank you!</div>--}}
            {{--                                                    <div class="notices">--}}
            {{--                                                        <div>NOTICE:</div>--}}
            {{--                                                        <div class="notice">A finance charge of 1.5% will be made on unpaid balances after 30 days.</div>--}}
            {{--                                                    </div>--}}
        </main>
        <footer>
            Order details was created on a computer and is valid without the signature and seal.
        </footer>
    </div>
    <!--DO NOT DELETE THIS div. IT is responsible for showing footer always at the bottom-->
    <div></div>
</div>
