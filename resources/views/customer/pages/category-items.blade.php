@extends('customer.layout-customer.master-customer')
@section('auth-content')
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('/front-end/css/custom.css') }}"/>

@section('CSS')
    <style type="text/css">
        .thumbImage{
            width: 15%;
            border: 1px solid #4378ad40;
            margin-top: 5%;
            padding: 5px;
            border-radius: 8px;
            margin-left: 2%;
        }
    </style>
@endsection
@section('page-content')
{{--    <div class="page-content">--}}
<div class="page-content">
    <div class="page-top">
        <div class="page-top-l">
            <div class="page-top-l-text">
                <div  class="hr-div"><hr class="top-dash"></hr></div>
                <p class="welcome-text no-bottom-margin">@lang('service_categories.welcome-text')</p>
{{--                <p class="detail-text no-top-padding no-bottom-margin">@lang('service_categories.welcome-message')  </p>--}}
            </div>
        </div>
        @include('customer.layout-customer.header-top')
    </div>
    <div class="page-bottom top-padding-1pc">
        <div class="page-bottom-content ">
            <div class="category-button-group no-left-padding bottom-padding-20 iso-nav">
                @if(isset($service_all_item['all_service_item']['category']) && !empty($service_all_item['all_service_item']['category']))
                    @foreach($service_all_item['all_service_item']['category'] as $cat_id => $cat_name)
                        <div class="category-button active" id="{{str_replace(' ', '_',strtolower(preg_replace('/[^\da-z ]/i', '', $cat_name)))}}" data-filter=".{{str_replace(' ', '_',strtolower(preg_replace('/[^\da-z ]/i', '', $cat_name)))}}" cat_="{{$cat_id}}">
                          <span>
                            <svg width="20" height="20" viewBox="0 0 20 20">
                                <img class="button-img" src="{{isset($service_all_item['all_cat_images'][$cat_id]) ? url('/service_images/category_images').'/' . $service_all_item['all_cat_images'][$cat_id] : ''}}" />
                            </svg>
                          </span>
                            @if(Lang::locale() == 'en')
                                {{ $cat_name }}
                            @else
                                {{ isset($service_all_item['all_service_item']['category'. '_' . Lang::locale()][$cat_id]) ? $service_all_item['all_service_item']['category'. '_' . Lang::locale()][$cat_id] : $cat_name}}
                            @endif
                        </div>
                    @endforeach
                @endif
            </div>
            <div id="items-subcontent">
                <div class="item-container main-iso">
                    @if(isset($service_all_item['all_service_item']['dtl']) && !empty($service_all_item['all_service_item']['dtl']))
                        @foreach($service_all_item['all_service_item']['dtl'] as $cat_id => $cat_details)
                            @foreach($cat_details as $item_id => $cat_details)
							@php
							//dd($cat_details);
							@endphp
                                <div class="items {{str_replace(' ', '_',strtolower(preg_replace('/[^\da-z ]/i', '', $cat_details['category_name'])))}}" id="{{$item_id}}" data-category="{{str_replace(' ', '_',strtolower(preg_replace('/[^\da-z ]/i', '', $cat_details['category_name'])))}}" style="background: url('{{isset($service_all_item['all_cat_images'][$cat_id]) ? url('/service_images/category_images').'/' . $service_all_item['all_cat_images'][$cat_id] : ''}}') no-repeat bottom right;  background-size: 60% auto ;  background-position-x: 140%; background-position-y: 130%;">
                                    <input type="hidden" name="service_item_id" class="service_item_id" value="{{$item_id}}">
                                    <input type="hidden" name="selected_order_date" class="selected_order_date">
                                    <input type="hidden" name="selected_order_time" class="selected_order_time">
                                    <input type="hidden" name="is_urgent" class="is_urgent">
                                    <span data-toggle="tooltip" data-placement="top"></span>
                                    <div class="package" data-toggle="tooltip" title="{{Lang::locale() == 'en' ? $cat_details['service_pricing_title'] : (isset($cat_details['service_pricing_title'. '_' . Lang::locale()]) ? $cat_details['service_pricing_title'. '_' . Lang::locale()] : $cat_details['service_pricing_title'])}}" data-placement="top">
                                        @if(Lang::locale() == 'en')
                                            {{ Illuminate\Support\Str::limit($cat_details['service_pricing_title'], 22)}}
                                        @else
                                            {{ isset($cat_details['service_pricing_title'. '_' . Lang::locale()]) ? Illuminate\Support\Str::limit($cat_details['service_pricing_title'. '_' . Lang::locale()], 22) : Illuminate\Support\Str::limit($cat_details['service_pricing_title'], 22)}}
                                        @endif
                                    </div>
                                    <div class="rate">{{ $cat_details['service_pricing_unit_cost_amount'] + $cat_details['service_commission_price'] }} {{ $cat_details['service_pricing_currency_unit'] }} </div>
                                    <div class="per-month"> /
                                        @if(Lang::locale() == 'en')
                                            {{ $cat_details['frequency_name'] }}
                                        @else
                                            {{ isset($cat_details['frequency_name'. '_' . Lang::locale()]) ? $cat_details['frequency_name'. '_' . Lang::locale()] : $cat_details['frequency_name']}}
                                        @endif</div>
                                    <div class="uss-security-logo">
										<img class="img-fluid-30 " src="{{isset($service_all_item['all_part_images'][$cat_details['service_item_partner_id']]) ? url('/service_images/partner_images').'/' . $service_all_item['all_part_images'][$cat_details['service_item_partner_id']] : ''}}"/>
                                        <br>
										<!--<span class="service_partner_name">
                                            @if(Lang::locale() == 'en')
                                                {{ $cat_details['partner_name'] }}
                                            @else
                                                {{ isset($cat_details['partner_name'. '_' . Lang::locale()]) ? $cat_details['partner_name'. '_' . Lang::locale()] : $cat_details['partner_name']}}
                                            @endif
                                        </span>-->
                                    </div>
                                    <div class='service_button text-center'>
                                        <button class="btn btn-success more_info" id="more_info_{{$item_id}}" >@lang('service_categories.more-info-btn')</button>
                                        <button class="btn btn-primary book_service {{isset($cat_details['is_service_date']) && $cat_details['is_service_date'] == 0 ? 'd-none' : ''}}" >@lang('service_categories.book-service-btn')</button>
                                        <button class="btn btn-primary confirm_btn {{isset($cat_details['is_service_date']) && $cat_details['is_service_date'] == 1 ? 'd-none' : ''}}" >@lang('service_categories.book-confirm-btn')</button>
                                    </div>
                                </div>
                            @endforeach
                        @endforeach
                    @endif
                </div>
            </div>
            <div id="order-subcontent" class="d-none">
                <ul class="item-container">
                    <li class="details-items">
                        <div class="details-items-left">
                            <div class="uss-security-logo "><img class="img-fluid-60 service_item_logo" {{--src="images/USS_security.png"--}} style="width: 40% !important;"/></div>
                            <div class="package no-top-padding">Basic Packege</div>
                            <div class="rate">10 €</div>
                            <div class="per-month "> /month</div>
                        </div>
                        <div class="details-items-right">
                            <p class="detail-text">@lang('service_categories.order_confirm_txt')</p>
							<div class='service_button text-right right-padding-40'>
								<button class="btn btn-success more_info" value=""  >@lang('service_categories.more-info-btn')</button>
                                <button class="btn btn-primary" value="" id="order_complete_btn" >@lang('service_categories.back-to-srvc-btn')</button>
                            </div>

                        </div>
                    </li>
                </ul>
            </div>
            <div id="detail-subcontent" class="d-none">
                <div class="item-container">
                    <div class="details-items-d no-top-padding">
                        <div class="details-items-dleft">
                            <div class="uss-security-logo"> <img class="img-fluid-60 dtl_partner_logo" {{--src="images/USS_security.png"--}} id="mainImage"/>
                                <div class="myDiv"></div>
                            </div>
                        </div>
                        <div class="details-items-dright">
							<div class="close-div"><span class="glyphicon  close_detail back_item_list" /><img class="img-20px" src="{{url('front-end')}}/images/x.png"/></span></div>
                            <div class="package text-left packg_title">Basic Packege</div>
                            <div class="detail-text text-left"><strong>@lang('service_categories.description')</strong></div>
                            <div class="detail-text text-left package_details detail-text text-left package_details no-top-padding"></div>
                            <div class="detail-text text-left"><span class="de-left"><strong>@lang('service_categories.category')</strong></span><span class="det-right packg_dtl_cat"></span></div>
                            <div class="detail-text text-left"><span class="de-left"><strong>@lang('service_categories.service_provider')</strong></span><span class="det-right packg_dtl_prtnr"></span></div>
                            <div class="detail-text text-left"><span class="de-left"><strong>@lang('service_categories.price')</strong></span><span class="rate text-left packg_dtl_price"></span><span class="det-right small packg_dtl_frequncy"></span></div>
                            <br>
                            <div class='service_button text-right right-padding-40'>
								<button class="btn btn-success back_item_list" value=""  >@lang('service_categories.back-to-srvc-btn')</button>
                                <button class="btn btn-primary" value="" id="book_from_details_btn" >@lang('service_categories.book-service-btn')</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" class="albela" name="albela" value="{{\Session::get('auth_token')}}">
<input type="hidden" class="lng" name="lng" value="{{Lang::locale()}}">
<!-- date time selection  Modal -->
<div class="modal fade bd-example-modal-lg" id="dateModal" tabindex="-1" role="dialog" aria-labelledby="dateModalLongTitle" aria-hidden="true">
    <div class="modal-dialog panaroma-modal modal-dialog-15" role="document">
        <div class="modal-content">
            <!--<div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>-->
            <div class="modal-body">
                <input type="hidden" id="item_id" />
                <div class="page-content no-background top-padding-20">
                    <div class="page-top">
                        <div class="page-top-l-modal">
                            <div class="page-top-l-text">
								<div class="close-div"><div  class="hr-div"><hr class="top-dash"></hr></div>
								<span class="glyphicon  close_modal"  id="date_modal_close"/><img class="img-20px" src="{{url('front-end')}}/images/x.png"/></span>
								</div>
                                <p class="welcome-text no-bottom-padding">@lang('service_categories.date_time_select')</p>
                            </div>
                        </div>
                    </div>
                    <div class="page-bottom">
                        <div class="page-bottom-content">
                            <div class="date-time-box">
                                <div class="date-time-box-top">
                                    <button class=" btn-large date-type date-type-selected">@lang('service_categories.specific_time_select')

                                    </button>
                                    <button class=" btn-large date-type urgent_btn">@lang('service_categories.urgent_text')
                                    </button>
                                    <input type="hidden" class="is_urgent_selected" name="is_urgent_selected" value="0">
                                </div>
                                <div class="date-time-box-bottom">
                                    <input type="hidden" name="selected_service_id" class="selected_service_id">
                                    <div class="row" >
                                        <div class="col-md-12 " style="margin-top:10px;padding: 0px 40px;">
                                            <div class="date_calender"></div>
                                            <input type="hidden" name="order_date" class="order_date">
                                        </div>
                                    </div>
                                    <div class="row order_time_div" >
                                        <div class="col-md-12 " style="margin-top:10px;padding: 0px 40px; margin-bottom:10px;">
                                            <div class="input-group">
                                                <select class="form-control col-xs-3 order_hour_from option_calender" style="margin-right: 9px">
                                                    @for($i = 0; $i < 24; $i++)
                                                        <option value="{{$i < 10 ? 0 . $i : $i}}">{{$i < 10 ? 0 . $i : $i }}</option>
                                                    @endfor
                                                </select>
                                                <select class="form-control col-xs-3 order_min_from option_calender" style="margin-right: 9px">
                                                    @for($i = 0; $i < 60; $i++)
                                                        <option value="{{$i < 10 ? 0 . $i : $i}}">{{$i < 10 ? 0 . $i : $i }}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                        <input type="hidden" name="order_time" class="order_time">
                                    </div>
                                </div>
                                <div class="date-time-box-footer">
                                    <div class="date-type-footer1" id="selected_date_time">
                                        <span class="selected_order_date"></span><span class="selected_order_time"></span>
                                    </div>
                                    <div class=" register_button confirm_button no-top-padding text-right">
                                        <button class="btn btn-primary btn-sm" id="confirm">@lang('service_categories.book-confirm-btn')</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- terms condition  Modal -->
<div class="modal fade bd-example-modal-lg" id="agreementModal" tabindex="-1" role="dialog" aria-labelledby="dateModalLongTitle" aria-hidden="true">
    <div class="modal-dialog panaroma-modal" role="document">
        <div class="modal-content">
            <!--<div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>-->
            <div class="modal-body">
                <input type="hidden" id="item_id" />
                <div class="agreement-content no-background top-padding-20">
                    <div class="page-top">
                        <div class="page-top-l-modal">
                            <div class="page-top-l-text">
                                <div class="close-div"><div  class="hr-div"><hr class="top-dash"></hr></div>
								<span class="glyphicon  close_modal"  id="agreement_modal_close"/><img class="img-20px" src="{{url('front-end')}}/images/x.png"/></span>
								</div>
                                <p class="welcome-text no-bottom-padding">@lang('service_categories.term_of_use_text')</p>
                            </div>
                        </div>
                    </div>
                    <div class="page-bottom">
                        <div class="page-bottom-content">
                            <p class="page-bottom-text">
                                @lang('service_categories.agreement_text')
{{--                                <strong>Business contracts offer you vital protection.</strong> Our customers know that having written agreements in place makes good business sense. However, most of the legal agreement templates available online offer only the bare minimum, and majority have been drafted in foreign jurisdictions. Our documents use the collective knowledge and experience of our specialist drafters to offer you comprehensive agreements that combine business imperatives with South African legal requirements.--}}
{{--                                <br><br>--}}
{{--                                <strong>They’re easy to edit and complete.</strong> Our business agreements and documents are provided in Word format, which makes it easy for you to edit and customise them to suit your business requirements.--}}
{{--                                You can understand our documents. Yes, they are written in English! Plain, easy-to-understand English.--}}
{{--                                <br><br>--}}
                            </p>
                            <br><br>
                            <label class="check-container"> @lang('service_categories.agree_term_check')
                                <input type="checkbox" id="agreement_checkbox" ><span class="checkmark"></span>
                            </label>
							<div class='service_button ritem_terms_button  text-center'>
								<button class="btn btn-success " value=""  id="close_terms" >@lang('service_categories.i_disagree')</button>
                                <button class="btn btn-primary" value="" id="item_terms" >@lang('service_categories.confirm_send')</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('JScript')

@endsection
