@extends('customer.layout-customer.master-customer-auth')
@section('CSS')
@endsection
@section('auth-content')
<div class="home-left">
    @if ($errors->count() > 0 )
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <h6>The following errors have occurred:</h6>
            <ul>
                @foreach( $errors->all() as $message )
                    <li>{{ $message }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('message'))
        <div class="alert alert-success" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ Session::get('message') }}
        </div>
    @endif
    @if (Session::has('errormessage'))
        <div class="alert alert-danger" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ Session::get('errormessage') }}
        </div>
    @endif
        <div class="home-l-top">
            <div  class="hr-div"><hr class="top-dash"></hr></div>
            <p class="welcome-text">@lang('auth.index_page_title_1')</p><p class="welcome-text2"> @lang('auth.index_page_title_2')</p>
            <p class="detail-text detail-text-f">@lang('auth.index_page_desc')</p>
        </div>
        <div class="home-l-bottom">
            <img class="img-fluid" src="{{asset('/front-end/images/bottom_left.png')}}" />
        </div>
</div>
@include('customer.layout-customer.right-customer-auth')
@endsection