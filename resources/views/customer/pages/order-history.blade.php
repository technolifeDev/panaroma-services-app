@extends('customer.layout-customer.master-customer')
@section('auth-content')
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('/front-end/css/custom.css') }}"/>
<link rel="stylesheet" href="{{ asset('/front-end/css/order-details.css') }}">

@section('CSS')
    <style type="text/css">
        .page-item.active .page-link {
            background-color: #336599 !important;
            border-color: #336599 !important;
        }
    </style>
@endsection
@section('page-content')
{{--    <div class="page-content">--}}
<div class="page-content">
    <div class="page-top">
        <div class="page-top-l">
            <div class="page-top-l-text">
                <div  class="hr-div"><hr class="top-dash"></hr></div>
                <p class="welcome-text">@lang('service_categories.order-welcome-text')</p>
            </div>
        </div>
        @include('customer.layout-customer.header-top')
    </div>
    <div class="page-bottom">
		<div class='service_button text-right'>
			<a class="btn btn-large btn-success more_info pull-right" href="{{ url('/customer/category-items') }}" style="margin-top: -7% !important; position: relative !important;">@lang('service_categories.back-to-srvc-btn')</a>
		<div>
		<table class="table table-bordered orderHistoryTbl">
            <thead>
            <tr>
                <td scope="col">#</td>
                <td scope="col">@lang('order.service')</td>
                <td scope="col">@lang('order.service_date')</td>
                <td scope="col">@lang('order.service_time')</td>
                <td scope="col">@lang('order.amount')</td>
{{--                <td scope="col">@lang('order.order_status')</td>--}}
{{--                <td scope="col">@lang('order.service_status')</td>--}}
            </tr>
            </thead>
            <tbody>

            <?php $page=isset($_GET['page'])? ($_GET['page']-1):0; $perPage=10;?>
            @foreach($orders as $key=> $ord)
                @if($ord->service_order_status !== 'cancel')
                    <tr>
                        <td scope="row"> {{ ($key+1+($perPage*$page)) }}</td>
                        <td class="orderDetailsTd" style="cursor: pointer;" title="Click me for details">
                            @if(Lang::locale() == 'est' && $ord->service_config_title_est)
                                {{ $ord->service_config_title_est }}
                            @elseif(Lang::locale() == 'rus' && $ord->service_config_title_rus)
                                {{ $ord->service_config_title_rus }}
                            @else
                                {{  $ord->service_config_title }}
                            @endif
                            <input type="hidden" name="cus_order_id" class="cus_order_id" value="{{ $ord->id }}">
                        </td>

                        <td>@isset($ord->service_order_items[0]->service_date) {{ $ord->service_order_items[0]->service_date }} @endisset</td>
                        <td>@isset($ord->service_order_items[0]->service_time) {{ $ord->service_order_items[0]->service_time }} @endisset</td>
                        <td> {{ isset($ord->service_order_grand_total) ? $ord->service_order_grand_total : 0.00 }}</td>
{{--                        <td>{{ isset($ord->service_order_status) ? $ord->service_order_status : '' }}</td>--}}
{{--                        <td>{{ $ord->service_order_deliver_status }}</td>--}}
                    </tr>

                    <div class="modal fade" id="CancelModal{{ $ord->id }}" tabindex="-1" role="dialog" aria-labelledby="CancelModal" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Confirm Order Cancellation</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <h4 style="">Are you sure you want to cancel the order #<strong>{{ $ord->id }}</strong>?</h4>
                                    <p>After Cancelling order, you cannnot revert order status</p>
                                </div>
                                <div class="modal-footer">
                                    <a type="button" class="btn btn-danger" href="{{url('/order/cancel', $ord->id)}}">Cancel Order</a>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
            </tbody>
        </table>
            <span style="float: right;">
                {{ $orders->links() }}
            </span>

    </div>
</div>
{{--        <div class="modal fade viewOrderDetailsModal" tabindex="-1" data-width="350" style="display: none;">--}}
{{--        </div>--}}

        <div class="modal fade bd-example-modal-lg customer_order_preview" tabindex="-1" role="dialog" aria-labelledby="dateModalLongTitle" aria-hidden="true">
            <div class="modal-dialog panaroma-modal" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <input type="hidden" id="item_id" />
                        <div class="agreement-content no-background top-padding-20">
                            <div class="page-top">
                                <div class="page-top-l-modal">
                                    <div class="page-top-l-text">
                                        <div class="close-div"><div  class="hr-div"><hr class="top-dash"></hr></div>
                                            <span class="glyphicon  close_modal"  id="order_details_modal_close"/><img class="img-20px" src="{{url('front-end')}}/images/x.png"/></span>
                                        </div>
                                        <p class="welcome-text no-bottom-padding">@lang('order.order_details')</p>
                                    </div>
                                </div>
                            </div>
                            <div class="page-bottom">
                                <div class="page-bottom-content">
                                    <div id="invoice">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


@endsection
@section('JScript')

@endsection
