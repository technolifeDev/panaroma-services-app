@extends('customer.mail.master-mail')

@section('mail-content')
	<h2 class="welcome-text" style="color: #336699;font-size: 36px;font-weight: 620;line-height: 70%;">TechnoLife Invitation</h2>

	<p class="detail-text" style="color: #999999;font-size: 14px;padding: .1% 5%;"><b>{{$user_info->name}}</b>, Thank you for joining with Notify, We have more than a 6 million Readers Each Month,
		keeping you up to date with what’s going on in the world. Please click below button to complete email verification process.</p>
	<p class="detail-text" style="color: #999999;font-size: 14px;padding: .1% 5%;"> Email ID: <b>{{$user_info->email}}</b></p>
	<div class='more-info-button' style="margin: auto;flex: 5;padding: 6% 3%;">
		<a href="{{$reset_url}}"><button class="btn btn-large btn-primary" style="background: linear-gradient(183deg, #5488BD 0%, #336699 100%);padding: .7rem 2rem;margin: auto;font-weight: bold;font-size: 14px;border-color: #1585D8;border-radius: 4px;cursor: pointer;color: #fff;" value="">Accept Invitation</button></a>
	</div>
@endsection