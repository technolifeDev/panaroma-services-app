@extends('layout.master')
@section('content')
    @inject('ACL', 'App\Repositories\RolePermissionForBlade')
    <div class="row">
        <div class="col-md-12">
            @if ($errors->count() > 0 )
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <h6>The following errors have occurred:</h6>
                    <ul>
                        @foreach( $errors->all() as $message )
                            <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (Session::has('message'))
                <div class="alert alert-success" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('message') }}
                </div>
            @endif
            @if (Session::has('errormessage'))
                <div class="alert alert-danger" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('errormessage') }}
                </div>
            @endif
            <div class="alert alert-danger hide" role="alert">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                Permission deleted successfully
            </div>
        </div>
    </div>
    <div class="row ">
        <div class="col-md-12">
            <div style="padding:10px;">
                @if($ACL->hasPermission('create'))
                    <button href="#add-permission" data-toggle="modal" class="demo btn btn-blue">
                        Add new permission
                    </button>
                @endif
            </div>


            <!-- List of Categories -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i>

                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                        </a>
                        <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <a class="btn btn-xs btn-link panel-refresh" href="#">
                            <i class="fa fa-refresh"></i>
                        </a>
                        <a class="btn btn-xs btn-link panel-expand" href="#">
                            <i class="fa fa-resize-full"></i>
                        </a>
                        <a class="btn btn-xs btn-link panel-close" href="#">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered table-hover permission_table" id="permission_table">
                        <thead>
                        <tr>
                            <th>Sr.no</th>
                            <th>Name</th>
                            <th class="hidden-xs">slug </th>
                            <th class="hidden-xs">description</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>


                        </tbody>
                    </table>

                </div>
            </div>

            <!-- END Categoreis -->
        </div>
    </div>

    <!-- start: Form to create categories -->
    <div id="add-permission" class="modal fade" tabindex="-1" data-width="600" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">Add new permission</h4>
        </div>
        <div class="modal-body">
            <form method="POST" action="{{url('/admin/permissions/store')}}" >
                @csrf
                <div class="row">
                    <div class="col-md-offset-2 col-md-8">

                        <div class="form-group">
                            <label for="category_name">Permission Name</label>
                            <input type="text" class="form-control" name="name" id="name" placeholder="Enter Permission Name" required>
                        </div>

                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="view" name="permission[]" id="view" class="green" >
                                View
                            </label>
                        </div>

                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="create" name="permission[]" id="create" class="green">
                                Create
                            </label>
                        </div>

                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="update" name="permission[]" id="update" class="green">
                                Update
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="delete" name="permission[]" id="delete" class="green">
                                Delete
                            </label>
                        </div>
                        <br>
                        <div class="form-group">
                            <button type="submit" class="btn btn-blue">
                                Save changes
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Close
            </button>
        </div>
    </div>
    <!-- End form--->

    <!-- start: Form to create categories -->
    <div id="edit-role" class="modal fade permission-modal" tabindex="-1" data-width="600" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">Edit new permission</h4>
        </div>
        <div class="modal-body">
            <form method="POST" action="{{url('/admin/permissions/update')}}" >
                @csrf
                @method('put')
                <div class="row">
                    <div class="col-md-offset-2 col-md-8">

                        <div class="form-group">
                            <label for="category_name">Permission Name</label>
                            <input type="text" class="form-control" name="name" id="permission_name" placeholder="Enter Permission Name" required>
                        </div>
                        <div class="form-group">
                            <label for="slug">Slug</label>
                            <input type="text" id="permission_slug" name="slug" class="form-control" placeholder="Enter Slug">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" rows="4" name="description" id="permission_description"></textarea>
                        </div>

                        <input type="hidden" class="" value="" name="permission_id" id="permission_id">
                        <div class="form-group">
                            <button type="submit" class="btn btn-blue">
                                Save changes
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Close
            </button>
        </div>
    </div>
    <!-- End form--->
@endsection
@section('JScript')
    <script>
        $(function () {

        });
    </script>
@endsection
