@extends('layout.master')
@section('content')
    @inject('ACL', 'App\Repositories\RolePermissionForBlade')
    <div class="row">
        <div class="col-md-12">
            @if ($errors->count() > 0 )
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <h6>The following errors have occurred:</h6>
                    <ul>
                        @foreach( $errors->all() as $message )
                            <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (Session::has('message'))
                <div class="alert alert-success" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('message') }}
                </div>
            @endif
            @if (Session::has('errormessage'))
                <div class="alert alert-danger" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('errormessage') }}
                </div>
            @endif
        </div>
    </div>
    <div class="row ">
        <div class="col-md-12">
        <style>
            .invoice-title h2, .invoice-title h3 {
                display: inline-block;
            }

            .modal{
                width: 900px !important;
            }

            .orderDtlPreviewTable > tbody > tr > .no-line {
                border-top: none;
            }

            .orderDtlPreviewTable > thead > tr > .no-line {
                border-bottom: none;
            }

            .orderDtlPreviewTable > tbody > tr > .thick-line {
                border-top: 2px solid;
            }
        </style>


        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="invoice-title">
                        <h2>Order Details</h2><h3 class="pull-right">Order # {{isset($orderDetails[0]['service_orders_id']) ? str_pad($orderDetails[0]['service_orders_id'], 6, "0", STR_PAD_LEFT) : ''}}</h3>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-xs-6">
                            <address>
                                <strong>Ordered By:</strong><br>
                                {{isset($orderDetails[0]['name']) ? $orderDetails[0]['name'] : ''}}
                            </address>
                        </div>
                        <div class="col-xs-6 text-right">
                            <address>
                                <strong>Customer Address:</strong><br>
                                {{isset($orderDetails[0]['service_order_customer_address']) ? $orderDetails[0]['service_order_customer_address'] : ''}}
                            </address>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                        </div>
                        <div class="col-xs-6 text-right">
                            <address>
                                <strong>Order Date:</strong><br>
                                {{isset($orderDetails[0]['created_at']) ? \Carbon\Carbon::parse($orderDetails[0]['created_at'])->format('j F, Y') : ''}}<br><br>
                            </address>
                            <address>
                                <strong>Service Date:</strong><br>
                                {{isset($orderDetails[0]['service_date']) ? \Carbon\Carbon::parse($orderDetails[0]['service_date'])->format('j F, Y') : '' }}
                                {{ ' | ' }}
                                {{isset($orderDetails[0]['service_time']) ? ($orderDetails[0]['service_time']) : ''}}<br>
{{--                                {{isset($orderDetails[0]['service_time']) ? \Carbon\Carbon::parse($orderDetails[0]['service_time'])->format('g:i a') : ''}}<br>--}}
                            </address>
                        </div>
                    </div>
                </div>
            </div>
                <input type="hidden" name="order_id" class="order_id" value="{{isset($orderDetails[0]['service_orders_id']) ? $orderDetails[0]['service_orders_id'] : ''}}">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><strong>Order summary</strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-condensed orderDtlPreviewTable">
                                    <thead>
                                    <tr>
                                        <td><strong>Service Name</strong></td>
                                        <td class="text-center"><strong>Service Item</strong></td>
                                        <td class="text-center"><strong>Service type</strong></td>
                                        <td class="text-center"><strong>Service type value</strong></td>
                                        <td class="text-right"><strong>Quantity</strong></td>
                                        <td class="text-center"><strong>Price</strong></td>
                                        <td class="text-center"><strong>Status</strong></td>
                                        <td class="text-right"><strong>Action</strong></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($orderDetails as $key => $data)
                                    <tr>
                                        <td>{{ $data['service_name'] }}</td>
                                        <td class="text-center">{{ $data['service_pricing_title'] }}</td>
                                        <td class="text-center">{{ $data['attribute_type_name'] }}</td>
                                        <td class="text-center">{{ $data['attribute_field_value'] }}</td>
                                        <td class="text-center">1</td>
                                        <td class="text-right">{{ $data['service_pricing_currency_unit'] . ' ' .$data['orders_items_type_cost'] }}</td>
                                        <td class="text-center orderitemStatusLable"><span class="label {{$order_label[$data['id']]}}">{{ $data['order_items_status'] }}</span></td>
                                        <td class="text-right orderItemTd">
                                            @if($data['service_order_status'] == 'accepted' && $key >0)
                                                @if($data['order_items_status'] == 'pending')
                                                    <a href="#" class="btn btn-xs tooltips btn-primary orderItemStatusBtn accptBtn" td_ord_id="{{ $data['id'] }}" title="Accept Service" td_ord_type="accepted">
                                                        <i class="fa fa-check-square-o" aria-hidden="true"></i>
                                                    </a>
                                                    <a href="#" class="btn btn-xs tooltips btn-danger orderItemStatusBtn rejectBtn" td_ord_id="{{ $data['id'] }}" title="Reject Service" td_ord_type="rejected">
                                                        <i class="fa fa-times" aria-hidden="true"></i>
                                                    </a>
                                                @elseif($data['order_items_status'] == 'accepted')
                                                    <a href="#" class="btn btn-xs tooltips btn-info orderItemStatusBtn processBtn" td_ord_id="{{ $data['id'] }}" title="Process Start" td_ord_type="processed">
                                                        <i class="fa fa-wrench" aria-hidden="true"></i>
                                                    </a>
                                                @elseif($data['order_items_status'] == 'processed')
                                                    <a href="#" class="btn btn-xs tooltips btn-success orderItemStatusBtn compltBtn" td_ord_id="{{ $data['id'] }}" title="Complete Service" td_ord_type="completed">
                                                        <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                                                    </a>
                                                @endif
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td class="thick-line"></td>
                                        <td class="thick-line"></td>
                                        <td class="thick-line"></td>
                                        <td class="thick-line"></td>
                                        <td class="thick-line text-center"><strong>Subtotal</strong></td>
                                        <td class="thick-line text-right">{{ $orderDetails[0]['service_pricing_currency_unit'] . ' ' .$orderDetails[0]['service_order_amount'] }}</td>
                                        <td class="thick-line"></td>
                                        <td class="thick-line"></td>
                                    </tr>
                                    @if($orderDetails[0]['service_order_discount_price'] > 0)
                                        <tr>
                                            <td class="no-line"></td>
                                            <td class="no-line"></td>
                                            <td class="no-line"></td>
                                            <td class="no-line"></td>
                                            <td class="no-line text-center"><strong>Discount</strong></td>
                                            <td class="no-line text-right">{{ $orderDetails[0]['service_pricing_currency_unit'] . ' ' .$orderDetails[0]['service_order_discount_price'] }}</td>
                                            <td class="no-line"></td>
                                            <td class="no-line"></td>
                                        </tr>
                                    @endif
                                    <tr>
                                        <td class="no-line"></td>
                                        <td class="no-line"></td>
                                        <td class="no-line"></td>
                                        <td class="no-line"></td>
                                        <td class="no-line text-center"><strong>Tax</strong></td>
                                        <td class="no-line text-right">{{ $orderDetails[0]['service_pricing_currency_unit'] . ' ' .$orderDetails[0]['service_order_tax_amount'] }}</td>
                                        <td class="no-line"></td>
                                        <td class="no-line"></td>
                                    </tr>
                                    <tr>
                                        <td class="no-line"></td>
                                        <td class="no-line"></td>
                                        <td class="no-line"></td>
                                        <td class="no-line"></td>
                                        <td class="no-line text-center"><strong>Total</strong></td>
                                        <td class="no-line text-right">{{ $orderDetails[0]['service_pricing_currency_unit'] . ' ' .$orderDetails[0]['service_order_grand_total'] }}</td>
                                        <td class="no-line"></td>
                                        <td class="no-line"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
{{--data-dismiss="modal"--}}

