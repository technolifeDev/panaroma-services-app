<!DOCTYPE html>
<html>
<head>
    <title>Techno Life</title>
</head>
<body style="margin: 0; padding: 0; background: #ece8df;">

<div class="main-area" style="background: #f2efe9;	padding-bottom: 60px;">

    <div class="logo" style="text-align: center; padding-top: 50px;	padding-bottom: 36px;">
        <h1>Tehno Life</h1>
    </div>

    <div class="content" style="background: #fff; width: 420px;	padding: 60px 90px;	margin: 0 auto;">
        <h1 style="margin-top: 0; font-weight: normal; color: #38434d; font-family: Georgia,serif; font-size: 16px;	margin-bottom: 14px; line-height: 24px;">Hello,</h1>

        <p style="margin-top: 0; font-weight: 400; font-size: 14px;	line-height: 22px; color: #7c7e7f; font-family: Georgia,serif; margin-bottom: 22px;"><b>{{$customer_name}}</b> Your order no <b>#{{ $order_no }}</b> has been {{ $order_status }}. Our support team will contact will soon.</p>



        <div class="footer" style="padding-top: 25px;">
            <h2 style="margin-top: 0; font-weight: normal; color: #38434d; font-family: Georgia,serif; font-size: 14px;	margin-bottom: 7px;">Thanks</h2>
            <p style="margin-top: 0; font-weight: 400; font-size: 14px;	line-height: 22px; color: #7c7e7f; font-family: Georgia,serif; margin-bottom: 22px;">If you have any query, feel free to contact our support team :<a href="mailto:support@technolife.ee" style="text-decoration: none;"> support@technolife.ee </a></p>
        </div>

    </div>

</div>

</body>
</html>
