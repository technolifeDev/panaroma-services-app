<li class="{{isset($page_title) && ($page_title=='Dashboard') ? 'active' : ''}} ">
    <a href="{{url('dashboard')}}"><i class="clip-home-3"></i>
        <span class="title"> Dashboard </span>
        <span class="selected"></span>
    </a>
</li>
<li class="{{isset($page_title) && ($page_title=='Profile') ? 'active' : ''}} ">
    <a href="{{url('my/profile')}}"><i class="clip-user-2"></i>
        <span class="title"> My Profile </span>
        <span class="selected"></span>
    </a>
</li>
