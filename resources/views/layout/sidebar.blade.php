<div class="main-navigation navbar-collapse collapse">
    <!-- start: MAIN MENU TOGGLER BUTTON -->
    @inject('ACL', 'App\Repositories\RolePermissionForBlade')
    <div class="navigation-toggler">
        <i class="clip-chevron-left"></i>
        <i class="clip-chevron-right"></i>
    </div>
    <!-- end: MAIN MENU TOGGLER BUTTON -->
    <!-- start: MAIN NAVIGATION MENU -->
    <ul class="main-navigation-menu">
        @include('layout.common-sidebar')

        @if($ACL->hasRole('admin'))
            @include('layout.admin-sidebar')
        @endif

        @if($ACL->hasRole('partner'))
            @include('layout.partner-sidebar')
        @endif
    </ul>
    <!-- end: MAIN NAVIGATION MENU -->
</div>
