@extends('customer.mail.master-mail')

@section('mail-content')
	<h2 class="welcome-text" style="color: #336699;font-size: 36px;font-weight: 620;line-height: 70%;">TechnoLife</h2>

	<p class="detail-text" style="color: #999999;font-size: 14px;padding: .1% 5%;"><b>{{$user_info->name}}</b>, Your request is accepted for change user password. Please click below button for password reset.</p>
	<div class='more-info-button' style="margin: auto;flex: 5;padding: 6% 3%;">
		<a href="{{$reset_url}}"><button class="btn btn-large btn-primary" style="background-color: #5488BD; padding: .7rem 2rem;margin: auto;font-weight: bold;font-size: 14px;border-color: #1585D8;border-radius: 4px;cursor: pointer;color: #fff;" value="">Reset Password</button></a>													
	</div>
@endsection
