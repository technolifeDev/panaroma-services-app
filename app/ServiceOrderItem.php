<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceOrderItem extends Model
{
    //
    use SoftDeletes;
    protected $table = 'service_order_items';

    protected $fillable = [
        'service_orders_id',
        'service_items_id',
        'service_item_pricing_id',
        'orders_items_category_id',
        'orders_items_class_id',
        'orders_items_type_id',
        'orders_items_type_value',
        'orders_items_type_cost',
        'order_items_status',
        'created_by',
    ];

    public function service_orders()
    {
        return $this->belongsTo('App\ServiceOrder');
    }

//    public function image(){
//        return $this->morphMany(Image::class, 'imageable');
//    }

//    public function service_category() {
//        return $this->belongsTo('\App\ServiceCategory', 'id');
//    }
}
