<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class OrderConfirmationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;
    protected $template_name;
    protected $subject;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data, $template_name, $subject)
    {
        $this->data = $data;
        $this->template_name = $template_name;
        $this->subject = $subject;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $customer_email = $this->data['customer_email'];
        $customer_name = $this->data['customer_name'];
        \Mail::send($this->template_name, $this->data, function($message) use ($customer_email, $customer_name){
            $message->to($customer_email,$customer_name)->subject($this->subject);
        });
    }
}
