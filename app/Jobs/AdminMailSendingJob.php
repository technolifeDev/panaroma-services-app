<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AdminMailSendingJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $user_data;
    public function __construct($user_data)
    {
        $this->user_data = $user_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $now = date('Y-m-d H:i:s');

        try{

            $data['user_info'] = $this->user_data['user'];
            $data['password'] = $this->user_data['password'];
            $data['accept_url'] = url('/auth');

            $user_email = $this->user_data['user']->email;
            $user_name = $this->user_data['user']->name;


            \Mail::send('admin.mail.admin-invitation-mail', $data, function($message) use ($user_email,$user_name) {

                $message->to($user_email,$user_name)->subject('Service Invitation');

            });

            \App\System::EventLogWrite('send-mail,admin-user-invitation',json_encode($data));
            \App\System::CustomLogWritter("jobs","job_complete_log",json_encode($data));


        }catch (\Exception $e){

            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
        }
    }
}
