<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CustomerInvitationEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $user_data;
    protected $reset_url;
    public function __construct($user_data,$reset_url)
    {
        $this->user_data = $user_data;
        $this->reset_url = $reset_url;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $now = date('Y-m-d H:i:s');

        try{

            $data['user_info'] = $this->user_data;
            $data['reset_url']= $this->reset_url;
            //$data['reset_url']= url('/customer/email-verify/id-'.$this->user_data->id.'/verification').'?token='.$this->user_data->remember_token;

            $user_email = $this->user_data->email;
            $user_name = $this->user_data->name;


            \Mail::send('customer.mail.customer-invitation-mail', $data, function($message) use ($user_email,$user_name) {

                $message->to($user_email,$user_name)->subject('Techno Life Customer Invitation');

            });

            \App\System::EventLogWrite('send-mail,customer-invitation',json_encode($data));
            \App\System::CustomLogWritter("jobs","job_complete_log",json_encode($data));


        }catch (\Exception $e){

            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
        }
    }
}
