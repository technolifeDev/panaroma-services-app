<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CustomerForgetPasswordMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $user_id;
    protected $reset_url;
    public function __construct($user_id,$reset_url)
    {
        $this->user_id = $user_id;
        $this->reset_url = $reset_url;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {


        $now = date('Y-m-d H:i:s');

        try{
            $data['user_id'] = $this->user_id;
            $data['reset_url'] = $this->reset_url;
            \App\System::ForgotPasswordEmail($this->user_id, $this->reset_url);
            \App\System::EventLogWrite('send-mail,customer-forget-password',json_encode($data));
            \App\System::CustomLogWritter("jobs","job_complete_log",json_encode($data));

        }catch (\Exception $e){

            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
        }
    }
}
