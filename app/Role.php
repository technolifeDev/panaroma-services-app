<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Role extends Model
{
    //
    use SoftDeletes;
    protected $fillable = ['name', 'slug', 'description'];

    public function users() {
        return $this->belongsToMany('App\Users');
    }

    public function permissions() {
        return $this->belongsToMany('App\Permission');
    }

    public function role_user() {
        return $this->hasMany("\App\RoleUser", 'role_id');
    }
}
