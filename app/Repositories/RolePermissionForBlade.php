<?php

namespace App\Repositories;


use App\Traits\HasRoleAndPermission;

class RolePermissionForBlade
{
    use HasRoleAndPermission;
}