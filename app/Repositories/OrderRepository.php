<?php

namespace App\Repositories;


use App\Repositories\Interfaces\OrderRepositoryInterface;


class OrderRepository implements OrderRepositoryInterface
{
    public function getOrderListForDataTable($start, $limit){
        $getOrderData = \App\ServiceOrder::leftJoin('users', 'users.id', '=', 'service_orders.service_order_customer_id')
            ->leftJoin('service_order_items', 'service_order_items.service_orders_id', '=', 'service_orders.id')
            ->leftJoin('service_partner_info', 'service_partner_info.id', '=', 'service_orders.service_order_partner_id')
            ->leftJoin('service_config', 'service_config.id', '=', 'service_orders.service_order_config_id')
            ->whereNull('service_orders.deleted_at')
            ->select('service_orders.*', 'service_order_items.service_date', 'service_order_items.service_time', 'users.name', 'service_partner_info.partner_name', 'service_config.service_pricing_currency_unit')
            ->groupBy('service_orders.id')
            ->offset($start)
            ->limit($limit)
            ->orderBy('created_at', 'desc')
            ->get()
            ->toArray();
        return $getOrderData;
    }

    public function getOrderListForDataTableSearch($start, $limit, $search){
        $getOrderData = \App\ServiceOrder::leftJoin('users', 'users.id', '=', 'service_orders.service_order_customer_id')
            ->leftJoin('service_order_items', 'service_order_items.service_orders_id', '=', 'service_orders.id')
            ->leftJoin('service_partner_info', 'service_partner_info.id', '=', 'service_orders.service_order_partner_id')
            ->leftJoin('service_config', 'service_config.id', '=', 'service_orders.service_order_config_id')
            ->whereNull('service_orders.deleted_at')
            ->where('service_partner_info.partner_name', 'LIKE', '%' . $search . '%')
            ->orWhere('users.name', 'LIKE', '%' . $search . '%')
            ->orWhere('service_orders.service_order_status', 'LIKE', '%' . $search . '%')
            ->orWhere('service_orders.service_order_deliver_status', 'LIKE', '%' . $search . '%')
            ->orWhere('service_orders.service_order_payment_status', 'LIKE', '%' . $search . '%')
            ->select('service_orders.*', 'service_order_items.service_date', 'service_order_items.service_time', 'users.name', 'service_partner_info.partner_name', 'service_config.service_pricing_currency_unit')
            ->groupBy('service_orders.id')
            ->offset($start)
            ->limit($limit)
            ->orderBy('created_at', 'desc')
            ->get()
            ->toArray();

        return $getOrderData;
    }

    public function getOrderListByPartnerIdForDataTable($start, $limit, $my_partner_id){
        $getOrderData = \App\ServiceOrder::leftJoin('users', 'users.id', '=', 'service_orders.service_order_customer_id')
            ->leftJoin('service_order_items', 'service_order_items.service_orders_id', '=', 'service_orders.id')
            ->leftJoin('service_partner_info', 'service_partner_info.id', '=', 'service_orders.service_order_partner_id')
            ->leftJoin('service_config', 'service_config.id', '=', 'service_orders.service_order_config_id')
            ->where('service_orders.service_order_partner_id', '=', $my_partner_id)
            ->whereNull('service_orders.deleted_at')
            ->select('service_orders.*', 'service_order_items.service_date', 'service_order_items.service_time', 'users.name', 'service_partner_info.partner_name', 'service_config.service_pricing_currency_unit')
            ->groupBy('service_orders.id')
            ->offset($start)
            ->limit($limit)
            ->orderBy('created_at', 'desc')
            ->get()
            ->toArray();
        return $getOrderData;
    }

    public function getOrderListByPartnerIdForDataTableSearch($start, $limit, $search, $my_partner_id){
        $getOrderData = \App\ServiceOrder::leftJoin('users', 'users.id', '=', 'service_orders.service_order_customer_id')
            ->leftJoin('service_order_items', 'service_order_items.service_orders_id', '=', 'service_orders.id')
            ->leftJoin('service_partner_info', 'service_partner_info.id', '=', 'service_orders.service_order_partner_id')
            ->leftJoin('service_config', 'service_config.id', '=', 'service_orders.service_order_config_id')
            ->whereNull('service_orders.deleted_at')
            ->where('service_orders.service_order_partner_id', '=', $my_partner_id)
            ->orWhere('service_partner_info.partner_name', 'LIKE', '%' . $search . '%')
            ->orWhere('users.name', 'LIKE', '%' . $search . '%')
            ->orWhere('service_orders.service_order_status', 'LIKE', '%' . $search . '%')
            ->orWhere('service_orders.service_order_deliver_status', 'LIKE', '%' . $search . '%')
            ->orWhere('service_orders.service_order_payment_status', 'LIKE', '%' . $search . '%')
            ->select('service_orders.*', 'service_order_items.service_date', 'service_order_items.service_time', 'users.name', 'service_partner_info.partner_name', 'service_config.service_pricing_currency_unit')
            ->groupBy('service_orders.id')
            ->offset($start)
            ->limit($limit)
            ->orderBy('created_at', 'desc')
            ->get()
            ->toArray();

        return $getOrderData;
    }


    public function resizeOrderArrayForJson($orderData, $hasViewPermission){
        $data = array();
        $sl = 0;
        if(!empty($orderData)){
            foreach ($orderData as $key => $value) {
                $sl++;
                $service_order_status = '';
                if ($value['service_order_status'] == 'accepted') {
                    $service_order_status = '<span class="label label-success">Accepted</span>';
                } else if ($value['service_order_status'] == 'cancel') {
                    $service_order_status = '<span class="label label-danger">Cancel</span>';
                } else if ($value['service_order_status'] == 'rejected') {
                    $service_order_status = '<span class="label label-warning">Rejected</span>';
                } else {
                    $service_order_status = '<span class="label label-info">Requested</span>';
                }

                $service_order_deliver_status = '';
                if ($value['service_order_deliver_status'] == 'processed') {
                    $service_order_deliver_status = '<span class="label label-info">Processed</span>';
                } else if ($value['service_order_deliver_status'] == 'pending') {
                    $service_order_deliver_status = '<span class="label label-danger">Pending</span>';
                } else if ($value['service_order_deliver_status'] == 'completed') {
                    $service_order_deliver_status = '<span class="label label-success">Completed</span>';
                }

                $service_order_payment_status = '';
                if ($value['service_order_payment_status'] == 'paid') {
                    $service_order_payment_status = '<span class="label label-success">Paid</span>';
                } else if ($value['service_order_payment_status'] == 'unpaid') {
                    $service_order_payment_status = '<span class="label label-danger">Unpaid</span>';
                } else if ($value['service_order_payment_status'] == 'partly-paid') {
                    $service_order_payment_status = '<span class="label label-warning">Partly paid</span>';
                }
//                $data[$key]['expand'] = '<a href="#" class="btn btn-xs tooltips btn-primary expandOrderList" expand_order_id="' . $value['id'] . '"><i class="fa fa-plus-square"></i></a>';
                $data[$key]['expand'] = $sl;
                $data[$key]['created_at'] = substr($value['created_at'], 0, strrpos($value['created_at'], ' '));
                $data[$key]['order_no'] = str_pad($value['id'], 6, "0", STR_PAD_LEFT);
                $data[$key]['customer_name'] = $value['name'];
                $data[$key]['company_name'] = $value['partner_name'];
                $data[$key]['service_date'] = $value['service_date'];
                $data[$key]['cost'] = $value['service_pricing_currency_unit'] . ' ' . $value['service_order_amount'];
//                $data[$key]['service_order_tax_amount'] = $value['service_pricing_currency_unit'] . ' ' . $value['service_order_tax_amount'];
                $data[$key]['service_order_grand_total'] = $value['service_pricing_currency_unit'] . ' ' . $value['service_order_grand_total'];
                $data[$key]['service_order_status'] = $service_order_status;
                $data[$key]['service_order_deliver_status'] = $service_order_deliver_status;
                $data[$key]['service_order_payment_status'] = $service_order_payment_status;
                $data[$key]['action'] = ($hasViewPermission ? '<a href="#" class="btn btn-xs tooltips btn-primary previwOrderDtlsBtn" id="' . $value['id'] . '"><i class="clip-zoom-in"></i></a>' : '')
                    . ($hasViewPermission ? ' <a href="'.url('downloadPDF'). '/' .$value['id'] .'" id="' . $value['id'] . '" class="btn btn-xs tooltips btn-danger" target="_blank"><i class="fa fa-file-pdf-o"></i></a>' : '')
//                . ($hasDelPermission ? ' <a href="#" id="' . $value['id'] . '" class="btn btn-xs tooltips btn-danger deltServiceBtn"><i class="clip-close-2"></i></a>' : '')
                ;
            }
        }
        return $data;
    }


    //order details
    public function getOrderDetailsByOrderId($order_id){
        $getOrderDtls = \App\ServiceOrder::leftJoin('service_order_items', 'service_orders.id', '=', 'service_order_items.service_orders_id')
            ->leftJoin('service_partner_info', 'service_partner_info.id', '=', 'service_orders.service_order_partner_id')
            ->leftJoin('service_config', 'service_config.id', '=', 'service_orders.service_order_config_id')
            ->leftJoin('users', 'users.id', '=', 'service_orders.service_order_customer_id')
//            ->leftJoin('service_catalogue', 'service_catalogue.id', '=', 'service_order_items.orders_items_catalogue_id')
            ->leftJoin('services_categories', 'services_categories.id', '=', 'service_order_items.orders_items_category_id')
            ->where('service_orders.id', '=', $order_id)
            ->whereNull('service_orders.deleted_at')
            ->select('service_order_items.*', 'users.name', 'service_partner_info.partner_name',
                'service_config.service_pricing_currency_unit', 'service_orders.service_order_customer_address','service_orders.service_order_deliver_status',
                'service_orders.service_order_amount', 'service_orders.service_order_tax_amount',
                'service_orders.service_order_grand_total', 'service_orders.service_order_status',
//                'service_catalogue.service_name',
                'services_categories.category_name', 'service_orders.created_at',
                'service_config.service_pricing_title','service_config.service_pricing_unit_cost_amount'
            )
            ->get()
            ->toArray();

        return $getOrderDtls;
    }


    public function getOrderDetailsByOrderIdForPartner($order_id, $partner_id){
        $getOrderDtls = \App\ServiceOrder::leftJoin('service_order_items', 'service_orders.id', '=', 'service_order_items.service_orders_id')
            ->leftJoin('service_partner_info', 'service_partner_info.id', '=', 'service_orders.service_order_partner_id')
            ->leftJoin('service_config', 'service_config.id', '=', 'service_orders.service_order_config_id')
            ->leftJoin('users', 'users.id', '=', 'service_orders.service_order_customer_id')
            ->leftJoin('services_categories', 'services_categories.id', '=', 'service_order_items.orders_items_category_id')
            ->where('service_orders.id', '=', $order_id)
            ->andWhere('service_orders.service_order_partner_id', '=', $partner_id)
            ->whereNull('service_orders.deleted_at')
            ->select('service_order_items.*', 'users.name', 'service_partner_info.partner_name',
                'service_config.service_pricing_currency_unit', 'service_orders.service_order_customer_address',
                'service_orders.service_order_amount', 'service_orders.service_order_tax_amount',
                'service_orders.service_order_grand_total', 'service_orders.service_order_status',
                'services_categories.category_name', 'service_orders.created_at',
                'service_config.service_pricing_title'
            )
            ->get()
            ->toArray();

        return $getOrderDtls;
    }

    //get ordered service item name
//    public function getOrderItemName($type_value_id){
//        $getOrderItemName = \App\AttributeValue::where('id', '=', $type_value_id)->get()->toArray();
//        return isset($getOrderItemName[0]['attribute_field_value']) ? $getOrderItemName[0]['attribute_field_value'] : '';
//    }
}
