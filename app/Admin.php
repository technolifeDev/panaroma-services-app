<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DateTime;
//use Intervention\Image\Image;
use Image;
class Admin extends Model
{

    /**
     * Upload user image.
     *
     * @param string $img_location, $slug, $img_ext
     * @return user profile image.
     */
    public static function UserImageUpload($img_location, $email, $img_ext)
    {

        try {
			$filename  = $email.'-'.time().'-'.rand(1111111,9999999).'.'.$img_ext;
			if (!file_exists('images/user/admin/small/')) {
				mkdir('images/user/admin/small/', 0777, true);
			}
			$path2 = 'images/user/admin/small/' . $filename;
			Image::make($img_location)->resize(300, null, function ($constraint) {$constraint->aspectRatio();$constraint->upsize();})->crop(150,150)->save($path2);				
            return $path2;
			
        } catch(\Exception $e) {
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            return redirect('my/profile')->with('errormessage',"Password update failed !");
        }
    }

    /**
     * Upload user image.
     *
     * @param string $img_location, $slug, $img_ext
     * @return user profile image.
     */
    public static function PartnerUserImageUpload($img_location, $email, $img_ext)
    {

        try {
            $filename  = $email.'-'.time().'-'.rand(1111111,9999999).'.'.$img_ext;
            if (!file_exists('images/user/partner/small/')) {
                mkdir('images/user/partner/small/', 0777, true);
            }
            $path2 = 'images/user/partner/small/' . $filename;
            Image::make($img_location)->resize(150, 150)->save($path2);
            return $path2;
        } catch(\Exception $e) {
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            return redirect('my/profile')->with('errormessage',"Password update failed !");
        }
    }

    /**
     * Upload image_teaser
     *
     * @param string $img_location, $keywords_type, $img_ext
     * @return user image_teaser.
     * $category=album category/content category etc.
     */
    public static function poster_image($img_location, $keywords_type, $img_ext,$category)
    {
        $filename  = $keywords_type.'-'.time().'-'.rand(1111111,9999999).'.'.$img_ext;
        if($category=="album content"){
            if (!file_exists('poster-image/album_content/')) {
                mkdir('poster-image/album_content/', 0777, true);
            }
            $path = 'poster-image/album_content/'.$filename;
        }
        if($category=="album"){
            if (!file_exists('poster-image/album/')) {
                mkdir('poster-image/album/', 0777, true);
            }
            $path = 'poster-image/album/'.$filename;
        }
        if($category=="upcoming"){
            if (!file_exists('poster-image/upcoming/')) {
                mkdir('poster-image/upcoming/', 0777, true);
            }
            $path = 'poster-image/upcoming/'.$filename;
        }
        if($category=="behind-the-scene"){
            if (!file_exists('poster-image/behind-the-scene/')) {
                mkdir('poster-image/behind-the-scene/', 0777, true);
            }
            $path = 'poster-image/behind-the-scene/'.$filename;
        }
        \Image::make($img_location)->save($path);
        return $path;
    }

    //Image Upload for Live Streaming
    public static function poster_image_live_streaming($img_location, $keywords_type, $img_ext)
    {
        $filename  = $keywords_type.'-'.time().'-'.rand(1111111,9999999).'.'.$img_ext;

        if (!file_exists('images/poster-image/live-streaming/')) {
            mkdir('images/poster-image/live-streaming/', 0777, true);
        }
        $path = 'images/poster-image/live-streaming/'.$filename;
        \Image::make($img_location)->save($path);
        return $path;
    }

    //Image Upload for Data Pack
    public static function data_pack_image($img_location, $keywords_type, $img_ext)
    {
        $filename  = $keywords_type.'-'.time().'-'.rand(1111111,9999999).'.'.$img_ext;
        if (!file_exists('images/data-pack-images/')) {
            mkdir('images/data-pack-images/', 0777, true);
        }
        $path = 'images/data-pack-images/'.$filename;
        \Image::make($img_location)->save($path);
        return $path;
    }

    public static function multiArraySerach($search,$search_key,$array){

        foreach ($array as $key => $value) {
            if ($value[$search_key] == $search) {
                return $search;
            }
        }
        return null;
    }

    public static function AkamaiPoster_image($img_location, $keywords_type, $img_ext,$category){

        try{
            $key='oLz1zw7n1EwnNnSqHJpX5Od1BGsIa19L5spJtQHx1sxRK2LL';
            $keyName='ahmedreza';
            $cpCode='737977';
            $host="livetechnologies-nsu.akamaihd.net";


            $filename  = $keywords_type.'-'.time().'-'.rand(1111111,9999999).'.'.$img_ext;
            if($category=="album content")
                $path = 'poster-image/album_content/'.$filename;

            if($category=="album")
                $path = 'poster-image/album/'.$filename;

            if($category=="upcoming")
                $path = 'poster-image/upcoming/'.$filename;

            if($category=="behind-the-scene")
                $path = 'poster-image/behind-the-scene/'.$filename;



            $file = $img_location;
            $image_name = addslashes($filename);

            $signer = new \Akamai\NetStorage\Authentication();
            $signer->setKey($key, $keyName);

            $handler = new \Akamai\NetStorage\Handler\Authentication();
            $handler->setSigner($signer);

            $stack = \GuzzleHttp\HandlerStack::create();
            $stack->push($handler, 'netstorage-handler');

            $client = new \Akamai\Open\EdgeGrid\Client([
                'base_uri' => $host,
                'handler' => $stack
            ]);



            $adapter = new \Akamai\NetStorage\FileStoreAdapter($client, $cpCode);
            $fs = new \League\Flysystem\Filesystem($adapter);
            $fs = new \League\Flysystem\Filesystem($adapter);
            $stream = fopen($file, 'r+');
            $fs->writeStream('/content-management/'.$path , $stream);

            $string = print_r($fs,true);


            $result_arrays = explode("[path:protected] => /737977/",$string);
            $result_array=explode($img_ext,$result_arrays[1]);

            return $response = "https://live-technologies-vod.akamaized.net/".$result_array[0].$img_ext;

        }catch (\Exception $e) {

            $message = "Message : " . $e->getMessage() . ", File : " . $e->getFile() . ", Line : " . $e->getLine();
            \App\System::ErrorLogWrite($message);
            return false;
        }
    }

    public static function AkamaiUserImageUpload($img_location, $email, $img_ext){

        try{
            $key='oLz1zw7n1EwnNnSqHJpX5Od1BGsIa19L5spJtQHx1sxRK2LL';
            $keyName='ahmedreza';
            $cpCode='737977';
            $host="livetechnologies-nsu.akamaihd.net";


            $filename  = $email.'-'.time().'-'.rand(1111111,9999999).'.'.$img_ext;
            $path = 'admin/user/profile-images/'.$filename;


            $file = $img_location;
            $image_name = addslashes($filename);

            $signer = new \Akamai\NetStorage\Authentication();
            $signer->setKey($key, $keyName);

            $handler = new \Akamai\NetStorage\Handler\Authentication();
            $handler->setSigner($signer);

            $stack = \GuzzleHttp\HandlerStack::create();
            $stack->push($handler, 'netstorage-handler');

            $client = new \Akamai\Open\EdgeGrid\Client([
                'base_uri' => $host,
                'handler' => $stack
            ]);



            $adapter = new \Akamai\NetStorage\FileStoreAdapter($client, $cpCode);
            $fs = new \League\Flysystem\Filesystem($adapter);
            $fs = new \League\Flysystem\Filesystem($adapter);
            $stream = fopen($file, 'r+');
            $fs->writeStream('/content-management/'.$path , $stream);

            $string = print_r($fs,true);


            $result_arrays = explode("[path:protected] => /737977/",$string);
            $result_array=explode($img_ext,$result_arrays[1]);

            return $response = "https://live-technologies-vod.akamaized.net/".$result_array[0].$img_ext;

        }catch (\Exception $e) {

            $message = "Message : " . $e->getMessage() . ", File : " . $e->getFile() . ", Line : " . $e->getLine();
            \App\System::ErrorLogWrite($message);
            return false;
        }
    }



}
