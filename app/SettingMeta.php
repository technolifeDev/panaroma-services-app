<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SettingMeta extends Model
{
    use SoftDeletes;
    protected $table = 'settings_meta';

    protected $fillable = [
        'settings_id',
        'meta_field_name',
        'meta_field_value'
    ];

    public function settings()
    {
        return $this->belongsTo('App\Setting');
    }
}
