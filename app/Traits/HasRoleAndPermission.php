<?php

namespace App\Traits;

use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Str;


trait HasRoleAndPermission
{

    /**
     * Get All Role List
     *
     * @param null
     * @return array
     */
    public function roles()
    {
        return \App\Role::all();
    }

    /**
     * Get All Role List
     *
     * @param null
     * @return array
     */

    public function permission()
    {
        return \App\Permission::all();
    }
    /**
     * Get user Data
     *
     * @param int $id
     * @return array
     */
    private function user($id)
    {
        return \App\User::find($id);
    }

    /**
     * Get the User Role
     *
     * @param null
     * @return array
     */
    public function getRoles()
    {
       return \Auth::check()? count(\Auth::user()->roles->pluck('name')->toArray())>0?\Auth::user()->roles->pluck('name')->toArray():null:null;
    }


    /**
     * Get the User Role name by user id
     *
     * @param int  $id
     * @return array
     */
    public function userRoles($id)
    {
        return $this->user($id)? count($this->user($id)->roles->pluck('name')->toArray())>0?$this->user($id)->roles->pluck('name')->toArray():null:null;
    }

    /**
     *  Get the User Role ID
     *
     * @param null
     * @return array
     */
    public function getRoleId()
    {
        return \Auth::check()? count(\Auth::user()->roles->pluck('id')->toArray())>0?\Auth::user()->roles->pluck('id')->toArray():null:null;

    }

    /**
     * Get the User Role Id by user_id
     *
     * @param int  $id
     * @return array
     */
    public function userRolesId($id)
    {
        return $this->user($id)? count($this->user($id)->roles->pluck('id')->toArray())>0?$this->user($id)->roles->pluck('id')->toArray():null:null;
    }

    /**
     *  Merge the User Role & ID
     *
     * @param null
     * @return array
     */

    private function getAllRole(){
        return ($this->getRoles())?($this->getRoleId())? array_merge($this->getRoles(),$this->getRoleId()):$this->getRoles():null;
    }

    /**
     *  Merge the User Role by user_ID
     *
     * @param null
     * @return array
     */

    private function userAllRole($id){
        return ($this->userRoles($id))?($this->userRolesId($id))? array_merge($this->userRoles($id),$this->userRolesId($id)):$this->userRoles($id):null;
    }


    /**
     * Check if the user has a role or roles.
     *
     * @param int|string $role
     *
     * @return bool
     */
    public function hasRole($role)
    {
        if ($this->getAllRole()) {
            return (in_array($role,$this->getAllRole()))? true:false;
        }
        return false;
    }


    /**
     * Check if the user has a role or roles by user id.
     *
     * @param int|string $role
     *
     * @return bool
     */
    public function hasUserRole($role,$id)
    {
        if ($this->userAllRole($id)) {
            return (in_array($role,$this->userAllRole($id)))? true:false;
        }
        return false;
    }


    /**
     * Attach role to a user.
     *
     * @param int $role
     *
     * @return null|bool
     */
    public function attachRole($role,$level=0)
    {
        if ($this->getRoleId() && in_array($role,$this->getRoleId())) {
            return true;
        }
        return \Auth::check()? \Auth::user()->roles()->attach($role,['level' => $level]):false;
    }

    /**
     * Attach role to a user by user id.
     *
     * @param int $role
     *
     * @return null|bool
     */
    public function attachUserRole($role,$level=0,$id)
    {
        if ($this->userAllRole($id) && in_array($role,$this->userAllRole($id))) {
           return true;
        }
        return $this->user($id)? $this->user($id)->roles()->attach($role,['level' => $level]):false;
    }

    /**
     * Detach role from a user.
     *
     * @param int $role
     *
     * @return bool
     */
    public function detachRole($role)
    {
        return \Auth::check()? \Auth::user()->roles()->detach($role):false;
    }

    /**
     * Detach role from a user by user id.
     *
     * @param int $role
     * @param int id
     *
     * @return bool
     */
    public function detachUserRole($role,$id)
    {
        return $this->user($id)? $this->user($id)->roles()->detach($role):false;
    }

    /**
     * Sync roles for a user.
     *
     * @param array|$roles
     *
     * @return array
     */
    public function syncRoles($roles)
    {
         return \Auth::check()? \Auth::user()->roles()->sync($roles):false;
    }

    /**
     * Sync roles for a user by ID.
     *
     * @param array|$roles
     *
     * @return array
     */
    public function syncUserRoles($roles,$level=1,$id)
    {
        return $this->user($id)? $this->user($id)->roles()->sync($roles,['level' => $level]):false;
    }

    /**
     * Get role level of a user.
     *
     * @return int
     */
    public function level()
    {
        return \Auth::check()? count(\Auth::user()->role_user->pluck('level')->toArray())>0? max(\Auth::user()->role_user->pluck('level')->toArray()):0:0;
    }

    /**
     * Get role level of a user bu user id.
     *
     * @return int
     */
    public function userLevel($id)
    {
        return $this->user($id)? count($this->user($id)->role_user->pluck('level')->toArray())>0? max($this->user($id)->role_user->pluck('level')->toArray()):0:0;
    }

    /**
     * Get all permissions from roles.
     *
     *  @param array $roles
     * @return Builder
     */
    public function rolePermissions($roles)
    {
        return
            \App\Permission::select(['permissions.name as permission_name','permissions.slug as permission_slug','permissions.id as permission_id','roles.name as role_name', 'roles.slug as role_slug','roles.id as role_id'])
            ->join('permission_role', 'permission_role.permission_id', '=', 'permissions.id')
            ->join('roles', 'roles.id', '=', 'permission_role.role_id')
            ->whereIn('roles.id', $roles)
            ->get()->toArray();
    }

    /**
     * Get user permissions by user id.
     *
     * @param int $user_id
     * @return Builder
     */
    public function getPermissionByUser($user_id)
    {
        return
            \App\Permission::select(['permissions.name as permission_name','permissions.slug as permission_slug','permissions.id as permission_id','roles.name as role_name', 'roles.slug as role_slug','roles.id as role_id'])
                ->join('permission_role', 'permission_role.permission_id', '=', 'permissions.id')
                ->join('permission_user', 'permissions.id', '=', 'permission_user.permission_id')
                ->join('roles', 'roles.id', '=', 'permission_role.role_id')
                ->where('permission_user.user_id', $user_id)
                ->get()->toArray();
    }


    /**
     * Get the User permission
     *
     * @param null
     * @return array
     */
    public function getPermissions()
    {
        return \Auth::check()? count(\Auth::user()->permission->pluck('name')->toArray())>0?\Auth::user()->permission->pluck('name')->toArray():null:null;
    }

    /**
     * Get the User permission bu user id
     *
     * @param null
     * @return array
     */
    public function getUserPermissions($id)
    {
        return $this->user($id)? count($this->user($id)->permission->pluck('name')->toArray())>0?$this->user($id)->permission->pluck('name')->toArray():null:null;
    }


    /**
     * Get the User permission
     *
     * @param null
     * @return array
     */
    public function getPermissionId()
    {
        return \Auth::check()? count(\Auth::user()->permission->pluck('id')->toArray())>0?\Auth::user()->permission->pluck('id')->toArray():null:null;
    }

    /**
     * Get the User permission by user id
     *
     * @param int $id
     * @return array
     */
    public function getUserPermissionId($id)
    {
        return $this->user($id)? count($this->user($id)->permission->pluck('id')->toArray())>0?$this->user($id)->permission->pluck('id')->toArray():null:null;
    }


    /**
     *  Merge the User Role & ID
     *
     * @param null
     * @return array
     */

    private function getAllPermission(){
        return ($this->getPermissions())?($this->getPermissionId())? array_merge($this->getPermissions(),$this->getPermissionId()):$this->getPermissions():null;
    }


    /**
     *  Merge the User Role & id by user id
     *
     * @param int $id
     * @return array
     */

    private function getUserAllPermission($id){
        return ($this->getUserPermissions($id))?($this->getUserPermissionId($id))? array_merge($this->getUserPermissions($id),$this->getUserPermissionId($id)):$this->getUserPermissions($id):null;
    }

    /**
     * Check if the user has a permission or permissions.
     *
     * @param int|string $permission

     * @return bool
     */
    public function hasPermission($permission)
    {
        if ($this->getAllPermission()) {
            return (in_array($permission,$this->getAllPermission()))? true:false;
        }
        return false;
    }


    /**
     * Check if the user has a permission or permissions by user id
     *
     * @param int|string $permission
     * @param int  id

     * @return bool
     */
    public function hasUserPermission($permission,$id)
    {
        if ($this->getUserAllPermission($id)) {
            return (in_array($permission,$this->getUserAllPermission($id)))? true:false;
        }
        return false;
    }


    /**
     * Attach permission to a user.
     *
     * @param int $permission
     *
     * @return null|bool
     */
    public function attachPermission($permission)
    {
        if ($this->getPermissionId() && in_array($permission,$this->getPermissionId())) {
            return true;
        }
        return \Auth::check()? \Auth::user()->permission()->attach($permission):false;
    }

    /**
     * Attach permission to a user by user id.
     *
     * @param int $permission
     * @param int $id
     *
     * @return null|bool
     */
    public function attachUserPermission($permission,$id)
    {
        if ($this->getUserAllPermission($id) && in_array($permission,$this->getUserAllPermission($id))) {
            return true;
        }
        return $this->user($id)? $this->user($id)->permission()->attach($permission):false;
    }


    /**
     * Detach permission from a user.
     *
     * @param int $permission
     *
     * @return int
     */
    public function detachPermission($permission)
    {
        return \Auth::check()? \Auth::user()->permission()->detach($permission):false;
    }

    /**
     * Detach permission from a user by user id.
     *
     * @param int $permission
     *
     * @return int
     */
    public function detachUserPermission($permission,$id)
    {
        return $this->user($id)? $this->user($id)->permission()->detach($permission):false;
    }

    /**
     * Sync permissions for a user.
     *
     * @param array $permissions
     *
     * @return array
     */
    public function syncPermissions($permissions)
    {
        return \Auth::check()? \Auth::user()->permission()->sync($permissions):false;
    }

    /**
     * Sync permissions for a user by id.
     *
     * @param array $permissions
     *
     * @return array
     */
    public function syncUserPermissions($permissions,$id)
    {
        return $this->user($id)? $this->user($id)->permission()->sync($permissions):false;
    }


}
