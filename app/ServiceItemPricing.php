<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceItemPricing extends Model
{
    //
    use SoftDeletes;
    protected $table = 'service_item_pricing';

    protected $fillable = [
        'service_pricing_item_id',
        'service_pricing_category_id',
        'service_pricing_partner_id',
        'service_config_id',
        'service_pricing_type_cost',
        'status'
    ];

//    public function image(){
//        return $this->morphMany(Image::class, 'imageable');
//    }

//    public function service_category() {
//        return $this->belongsTo('\App\ServiceCategory', 'id');
//    }
}
