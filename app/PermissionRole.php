<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class PermissionRole extends Model
{
    //
    use SoftDeletes;
    protected $table="permission_role";

    public function permissions()
    {
        return $this->belongsTo('App\Permission');
    }
}
