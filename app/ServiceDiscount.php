<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceDiscount extends Model
{
    use SoftDeletes;
    protected $table = 'service_discounts';

    protected $fillable = [
        'partner_id',
        'service_discount_type',
        'service_discount_type_id',
        'service_discount_rate',
        'service_discount_from',
        'service_discount_to',
        'service_discount_status'
    ];

    /********************************************
    ## getDiscountTypeList
     *********************************************/
    public static function getDiscountTypeList($partner_id,$discount_type){

        $all_list = array();

        if($discount_type=='categories'){

            $types_list= \App\ServiceItem::where('service_items.service_item_partner_id',$partner_id)
                ->join('services_categories', 'service_items.service_item_category_id','=','services_categories.id')
                ->groupBy('services_categories.id')
                ->select('services_categories.*')
                ->get();
            if(count($types_list)>0){
                foreach ($types_list as $key => $type){
                    $list = array();
                    $list['discount_type_id'] = $type->id;
                    $list['discount_type_name'] = $type->category_name;
                    $all_list[]=$list;
                }
            }

        }elseif ($discount_type=='catalogues'){
            $types_list= \App\ServiceItem::where('service_items.service_item_partner_id',$partner_id)
                ->join('service_catalogue', 'service_items.service_item_catalogue_id','=','service_catalogue.id')
                ->groupBy('service_catalogue.id')
                ->select('service_catalogue.*')
                ->get();
            if(count($types_list)>0){
                foreach ($types_list as $key => $type){
                    $list = array();
                    $list['discount_type_id'] = $type->id;
                    $list['discount_type_name'] = $type->service_name;
                    $all_list[]=$list;
                }
            }
        }elseif ($discount_type=='items'){
            $types_list= \App\ServiceItem::where('service_items.service_item_partner_id',$partner_id)
                ->join('service_item_pricing','service_items.id','=','service_item_pricing.service_pricing_item_id')
                ->join('service_config','service_item_pricing.service_config_id','=','service_config.id')
                ->groupBy('service_config.id')
                ->select('service_config.*')
                ->get();
            if(count($types_list)>0){
                foreach ($types_list as $key => $type){
                    $list = array();
                    $list['discount_type_id'] = $type->id;
                    $list['discount_type_name'] = $type->service_pricing_title;
                    $all_list[]=$list;
                }
            }
        }else{

        }

        return $all_list;
    }

    /********************************************
    ## getDiscountInfoByType
     *********************************************/

    public static function getDiscountInfoByType($discount_type,$discount_type_id){
        $info ='';

        if($discount_type=='categories'){

            $type = \App\ServicesCategory::find($discount_type_id);
            $info = isset($type->category_name)?$type->category_name:'';

        }elseif ($discount_type=='catalogues'){

            $type = \App\ServiceCatalogue::find($discount_type_id);
            $info = isset($type->service_name)?$type->service_name:'';

        }elseif ($discount_type=='items'){
            $type = \App\ServiceConfig::find($discount_type_id);
            $info = isset($type->service_pricing_title)?$type->service_pricing_title:'';
        }else{

        }

        return $info;
    }
}
