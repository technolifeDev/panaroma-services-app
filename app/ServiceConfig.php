<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceConfig extends Model
{
    //
    use SoftDeletes;
    protected $table = 'service_config';

    protected $fillable = [
        'service_pricing_title',
        'service_pricing_title_est',
        'service_pricing_title_rus',
        'service_pricing_description',
        'service_pricing_description_est',
        'service_pricing_description_rus',
        'service_pricing_currency_unit',
        'service_pricing_unit_cost_amount',
        'service_commission_price',
        'service_tax_price',
        'is_service_date',
        'created_by',
    ];

//    public function image(){
//        return $this->morphMany(Image::class, 'imageable');
//    }

//    public function service_category() {
//        return $this->belongsTo('\App\ServiceCategory', 'id');
//    }
}
