<?php

namespace App;

use App\Http\Resources\ServiceItemsCollection as ServiceItemsCollection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceItem extends Model
{
    //
    use SoftDeletes;
    protected $table = 'service_items';

    protected $fillable = [
        'service_item_category_id',
        'service_item_partner_id'
    ];

    public function service_category() {
        return $this->belongsTo('\App\ServiceCategory', 'id');
    }

        public function image(){
        return $this->morphMany(Image::class, 'imageable');
    }


    public static function getServiceCatItemList(){
        $service_item =
            \App\ServiceItem::join('services_categories','service_items.service_item_category_id','=','services_categories.id')
                ->join('service_partner_info','service_items.service_item_partner_id','=','service_partner_info.id')
                ->join('service_item_pricing','service_items.id','=','service_item_pricing.service_pricing_item_id')
                ->leftJoin('service_item_frequencies', 'service_items.id', '=', 'service_item_frequencies.service_item_id')
                ->join('service_frequencies', 'service_frequencies.id', '=', 'service_item_frequencies.service_frequency_id')
                ->join('service_config','service_item_pricing.service_config_id','=','service_config.id')
                ->leftJoin('images', function($join)
                {
                    $join->on('service_items.id', '=', 'images.imageable_id');
                    $join->where('images.imageable_type', '=', 'App\ServiceItem');
                })
                ->groupBy('service_items.id')
                ->orderBy('services_categories.order')
                ->get()
            ->toArray()
        ;

        $ret['all_service_item'] = array();
        if(!empty($service_item)){
            foreach ($service_item as $key => $item){
                $ret['all_service_item']['dtl'][$item['service_item_category_id']][$item['service_pricing_item_id']] = $item;
                $ret['all_service_item']['category'][$item['service_item_category_id']] = $item['category_name'];
                $ret['all_service_item']['category_est'][$item['service_item_category_id']] = $item['category_name_est'];
                $ret['all_service_item']['category_rus'][$item['service_item_category_id']] = $item['category_name_rus'];
            }
        }
//cat images
        $get_cat_images = \App\ServicesCategory::leftJoin('images', function($join)
        {
            $join->on('services_categories.id', '=', 'images.imageable_id');
            $join->where('images.imageable_type', '=', 'App\ServicesCategory');
        })
            ->get()
            ->toArray()
        ;
        $ret['all_cat_images'] = array();
        if(!empty($get_cat_images)){
            foreach ($get_cat_images as $key => $item){
                $ret['all_cat_images'][$item['imageable_id']] = $item['image_name'];
            }
        }

        //partner logo
        $get_partner_images = \App\ServicePartner::leftJoin('images', function($join)
        {
            $join->on('service_partner_info.id', '=', 'images.imageable_id');
            $join->where('images.imageable_type', '=', 'App\ServicePartner');
        })
            ->get()
            ->toArray()
        ;
        $ret['all_part_images'] = array();
        if(!empty($get_partner_images)){
            foreach ($get_partner_images as $key => $item){
                $ret['all_part_images'][$item['imageable_id']] = $item['image_name'];
            }
        }
        return $ret;
    }
}
