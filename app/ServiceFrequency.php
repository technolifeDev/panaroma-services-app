<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceFrequency extends Model
{
    use SoftDeletes;
    protected $table = 'service_frequencies';

    protected $fillable = [
        'frequency_name',
        'frequency_value',
    ];
}
