<?php

namespace App;

use App\Jobs\OrderPlacedJob;
use App\Traits\NotificationSending;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Jobs\OrderConfirmationJob;
use Carbon\Carbon;

class ServiceOrder extends Model
{
    //
    use NotificationSending;
    use SoftDeletes;
    protected $table = 'service_orders';

    protected $fillable = [
        'service_order_customer_id',
        'service_order_partner_id',
        'service_order_config_id',
        'service_order_frequency_id',
        'service_order_customer_address',
        'service_order_amount',
        'service_order_tax_amount',
        'service_order_grand_total',
        'service_order_status',
        'service_order_deliver_status',
        'service_order_payment_status',
        'created_by',
    ];

    public function service_order_items()
    {
        return $this->hasMany('App\ServiceOrderItem', 'service_orders_id');
    }

    public static function orderStatusMail($order_id, $order_update){
        $now = date('Y-m-d H:i:s');

        try{
            $customer_details = \App\User::where('id', '=', $order_update->created_by)->first();
            if(isset($customer_details)){
                $customer_email = $customer_details->email;
                $customer_name = $customer_details->name;
            }else{
                return \Redirect::back()->with('message',"Invalid Order no!");
            }

            $customer_email = $customer_email;
            $customer_name = $customer_name;
            $data['order_no'] = str_pad($order_id, 6, "0", STR_PAD_LEFT);
            $data['order_status'] = $order_update->service_order_status;
            $data['customer_email'] = $customer_email;
            $data['customer_name'] = $customer_name;

            $template_name = 'order.mail.order-confirmation-mail';
            $subject = 'Order Confirmation Notification';

            dispatch(new OrderConfirmationJob($data, $template_name, $subject));

            \App\System::EventLogWrite('send-mail,order-confirmation-mail',json_encode($data));
            return true;
        }catch (\Exception $e){

            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            \Session::flash('errormessage', $e->getMessage());
            return false;
        }

    }

    //save  order notification
    public static function saveNotification($order_update, $notify_data){
        $notification['user_id'] = \Auth::user()->id;
        $notification['user_to_notify'] = $order_update->created_by;
        $notification['notify_type'] = $order_update->service_order_status;
        $notification['notify_data'] = json_encode($notify_data);
        $notification['read'] = 'unseen';
        \App\Notification::create($notification);
    }

//    public function image(){
//        return $this->morphMany(Image::class, 'imageable');
//    }

//    public function service_category() {
//        return $this->belongsTo('\App\ServiceCategory', 'id');
//    }

    public static function saveOrder($request){
//        $request = (object) $formData['data'];
        try{
            if (\Auth::check() && $request->service_item_id) {
                $item_pricing_obj  = ServiceItemPricing::where('service_pricing_item_id', $request->service_item_id)->first();
                $item_frequency_obj  = \App\ServiceItemFrequency::where('service_frequency_id', $request->service_item_id)->first();
                $item_config_obj  = \App\ServiceConfig::where('id', $item_pricing_obj->service_config_id)->first();
                $user = \Auth::user();
                $customer_info = \App\Customer::where('id', $user->company_id)->first();
                $address = (isset($customer_info->customer_address) ? $customer_info->customer_address : '');
                $total_sum_temp = 0;
                $get_tax_details = \App\Setting::getSettings('tax');
                $percentage=isset($get_tax_details[0]['meta_field_name']) && $get_tax_details[0]['meta_field_name'] == 'tax_percentage' ? $get_tax_details[0]['meta_field_value'] : 0;
                $total_sum_temp = $item_config_obj->service_pricing_unit_cost_amount;
                $commission_amount = $item_config_obj->service_commission_price;
                $tax_amount = ($percentage / 100) * ($total_sum_temp + $commission_amount);


                $newServiceOrderObj = New ServiceOrder;
                $newServiceOrderObj->service_order_customer_id = $user->id;
                $newServiceOrderObj->service_order_partner_id = $item_pricing_obj->service_pricing_partner_id;
                $newServiceOrderObj->service_order_config_id = $item_pricing_obj->service_config_id;
                $newServiceOrderObj->service_order_customer_address = $address;

                $newServiceOrderObj->service_order_amount  = $total_sum_temp;
                $newServiceOrderObj->service_order_tax_amount   = $tax_amount;
                $newServiceOrderObj->service_order_grand_total   = $total_sum_temp + $tax_amount + $commission_amount;
                $newServiceOrderObj->service_order_status = 'requested';
                $newServiceOrderObj->service_order_deliver_status = 'pending';
                $newServiceOrderObj->service_order_payment_status = 'unpaid';
                if(isset($item_frequency_obj->id)){
                    $newServiceOrderObj->service_order_frequency_id  = $item_frequency_obj->id;
                }
                $newServiceOrderObj->urgent = $request->is_urgent == 1 ? 1 : 0;
                $newServiceOrderObj->created_by  = $user->id;

                $newServiceOrderObj->save();


                $order_id = $newServiceOrderObj->id;
                $serviceOrderItemsObj = new ServiceOrderItem;
                $serviceOrderItemsObj->service_orders_id = $order_id;
                $serviceOrderItemsObj->orders_items_category_id  = $item_pricing_obj->service_pricing_category_id;
                $serviceOrderItemsObj->service_items_id = $item_pricing_obj->service_pricing_item_id;
                $serviceOrderItemsObj->service_item_pricing_id = $item_pricing_obj->id;
                $serviceOrderItemsObj->orders_items_type_cost = ($total_sum_temp + $commission_amount);
                $serviceOrderItemsObj->service_date = $request->order_date != ''? Carbon::parse($request->order_date)->format('Y-m-d') : '';
                $serviceOrderItemsObj->service_time	= $request->order_time != ''? $request->order_time : '';


                $serviceOrderItemsObj->order_items_status = 'pending';
                $serviceOrderItemsObj->created_by = \Auth::user()->id;
                $serviceOrderItemsObj->save();

                $email = \Auth::user()->email;
                $newServiceOrderObj->name = \Auth::user()->name;
                $newServiceOrderObj->order_id = str_pad($newServiceOrderObj->id, 6, "0", STR_PAD_LEFT);
                $data['order'] = $newServiceOrderObj;
                $data['item_details'] = \App\ServiceConfig::where('id', '=', $item_pricing_obj->service_config_id)->get()->toArray();
                $data['item_cost'] = $total_sum_temp + $commission_amount;
                $partner = ServicePartner::where('id',$item_pricing_obj->service_pricing_partner_id)->first();
                $data['partner_name'] = $partner->partner_name;

                if(isset($email)) {
                    $template_name = 'mail.order-content';
                    $subject = 'Technolife: order confirmation ';
                    dispatch(new OrderPlacedJob($data, $email, $template_name, $subject));
                }

                if(isset($item_pricing_obj->service_pricing_partner_id)) {
                    $data['customer_details'] = \App\Customer::where('id', '=', $user->company_id)->get()->toArray();
//                    print_r($data['customer_details']); die();

                    $email  = $partner->partner_email;
                    $template_name = 'mail.service-provider-mail';
                    $subject = 'Technolife: Customer Order ';
                    dispatch(new OrderPlacedJob($data, $email, $template_name, $subject));
                }

                $notification['user_id'] = \Auth::user()->id;
                $notification['user_to_notify'] = 0;
                $notification['company_id'] = $item_pricing_obj->service_pricing_partner_id;
                $notification['notify_type'] = $newServiceOrderObj->service_order_status;
                $notify_data = array(
                    'order_id' => $newServiceOrderObj->id,
                    'order_no' => str_pad($newServiceOrderObj->id, 6, "0", STR_PAD_LEFT),
                    'message' => 'Order No # ' . str_pad($newServiceOrderObj->id, 6, "0", STR_PAD_LEFT) . ' has been ' . $newServiceOrderObj->service_order_status
                );
                $notification['notify_data'] = json_encode($notify_data);
                $notification['read'] = 'unseen';

                \App\Notification::create($notification);

//                \session()->forget('form-data');
//                (new static())->PusherDataSend('order-status:'.$newServiceOrderObj->service_order_partner_id, $notify_data);
//                (new static())->PusherDataSend('order-status', $notify_data);
                return $newServiceOrderObj;
            }else {
                return false;
            }
        } catch(\Exception $e) {
            \DB::rollback();
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            return false;

        }
    }


    //save  order notification
    public static function saveUserNotification($order_update){
        $notification['user_id'] = \Auth::user()->id;
        $notification['user_to_notify'] = 0;
        $notification['company_id'] = $order_update->service_order_partner_id;
        $notification['notify_type'] = $order_update->service_order_status;
        $notify_data = array(
            'order_id' => $order_update->id,
            'order_no' => str_pad($order_update->id, 6, "0", STR_PAD_LEFT),
            'message' => 'Order No # ' . str_pad($order_update->id, 6, "0", STR_PAD_LEFT) . ' has been ' . $order_update->service_order_status
        );
        $notification['notify_data'] = json_encode($notify_data);
        $notification['read'] = 'unseen';
        \App\Notification::create($notification);
    }

    //service details
    public static function serviceDetailsData($service_item_id){
        $serviceData = \App\ServiceItemPricing::where(['service_item_pricing.service_pricing_item_id' => $service_item_id])
            ->leftJoin('service_partner_info', 'service_item_pricing.service_pricing_partner_id', '=', 'service_partner_info.id')
            ->leftJoin('service_config', 'service_item_pricing.service_config_id', '=', 'service_config.id')
            ->leftJoin('service_item_frequencies', 'service_item_pricing.id', '=', 'service_item_frequencies.service_item_id')
            ->leftJoin('service_frequencies', 'service_frequencies.id', '=', 'service_item_frequencies.service_frequency_id')
            ->leftJoin('images', function($join)
            {
                $join->on('service_item_pricing.service_pricing_item_id', '=', 'images.imageable_id');
                $join->where('images.imageable_type', '=', 'App\ServiceItem');
            })
            ->select(['service_config.service_pricing_title', 'service_config.service_pricing_title_est', 'service_config.service_pricing_title_rus',
                'service_config.service_pricing_currency_unit', 'service_config.service_pricing_unit_cost_amount',
                'service_frequencies.frequency_name', 'images.image_name'
            ])
            ->get()
            ->toArray();

        $reArrangeArr = array();
        foreach ($serviceData as $key => $data){
            $reArrangeArr['service_pricing_title'] = $data['service_pricing_title'];
            $reArrangeArr['service_pricing_title_est'] = $data['service_pricing_title_est'];
            $reArrangeArr['service_pricing_title_rus'] = $data['service_pricing_title_rus'];
            $reArrangeArr['service_image'] = url('/service_images/service_item_images').'/' . $data['image_name'];
            $reArrangeArr['service_pricing_currency_unit'] = $data['service_pricing_currency_unit'];
            $reArrangeArr['service_pricing_unit_cost_amount'] = $data['service_pricing_unit_cost_amount'];
            $reArrangeArr['service_frequency'] = $data['frequency_name'];
        }

        return $reArrangeArr;
    }

}
