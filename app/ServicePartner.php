<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class ServicePartner extends Model
{
    use SoftDeletes;
    protected $table = 'service_partner_info';

    protected $fillable = [
        'partner_name',
        'partner_name_est',
        'partner_name_rus',
        'partner_address',
        'partner_address_est',
        'partner_address_rus',
        'partner_txa_no',
        'partner_license',
        'partner_email',
    ];

    /********************************************
    ## PartnerVerificationMail
     *********************************************/
    public static function PartnerVerificationMail($users_id,$reset_url){



        $now = date('Y-m-d H:i:s');

        try{

            $user_info=\App\User::where('id',$users_id)->first();

            if(isset($user_info)){
                $users_id=$user_info->id;
                $users_type=$user_info->user_type;
                $users_email=$user_info->email;

            }else{
                return \Redirect::back()->with('message',"Invalid User ID!");
            }

            $data['user_info'] = $user_info;
            $data['reset_url'] = $reset_url;

            $user_email = $users_email;
            $user_name = $user_info->name;


            \Mail::send('partner.mail.partner-verify-mail', $data, function($message) use ($user_email,$user_name) {

                $message->to($user_email,$user_name)->subject('Email verification');

            });

            \App\System::EventLogWrite('send-mail,partner-password-verification',json_encode($data));
            \Session::flash('errormessage', 'Please Check your inbox for verify you email');
            return true;

        }catch (\Exception $e){

            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            \Session::flash('errormessage', $e->getMessage());
            return false;
        }

    }

    public function image(){
        return $this->morphOne(Image::class, 'imageable');
    }

    /********************************************
    ## getPartnerInfo
     *********************************************/
}
