<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends Model
{
    use SoftDeletes;
    protected $table = 'notifications';

    protected $fillable = [
        'user_id',
        'user_to_notify',
        'company_id',
        'notify_type',
        'notify_data',
        'read',
    ];
}
