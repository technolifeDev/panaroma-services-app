<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ServiceItemsCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'=>'items',
            'id' => $this->service_item_id,
            'data'=>[
                'title'=>$this->service_pricing_title,
                'description'=>$this->service_pricing_description,
                'currency_unit'=>$this->service_pricing_currency_unit,
                "commission_price" => $this->service_commission_price,
                "tax_price" => $this->service_tax_price,
                "pricing_unit_cost_amount" => $this->service_pricing_unit_cost_amount,
                "image" => url('/service_images/service_item_images').'/' . $this->item_image_url,
            ],
            'included'=>[
                'type'=>'partner',
                'id' => $this->service_item_partner_id,
                'attributes'=>
                    [
                        "name" =>$this->partner_name,
                        "address" => $this->partner_address,
                        "email" => $this->partner_email,
                        "image" => url('/service_images/partner_images').'/' . $this->partner_image_url
                    ]
            ]
        ];
    }
}
