<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role = Auth::user()->roles->pluck('name');
        if(!$role->contains('admin')) {
            return redirect('/dashboard')->with('errormessage', "Sorry, You don't have permission to access this page.");
        }
        return $next($request);
    }
}
