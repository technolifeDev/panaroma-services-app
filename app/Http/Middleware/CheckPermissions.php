<?php

namespace App\Http\Middleware;

use App\Permission;
use App\PermissionRole;
use Closure;
use Auth;
class CheckPermissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            $permissions = PermissionRole::where('role_id', Auth::user()->roles[0]->id)->get();
            $user_permissions = [];
            if (count($permissions) > 0) {
                foreach ($permissions as $permit) {
                    $permission = Permission::where('id', $permit->permission_id)->first();
                    if (isset($permission)){
                        $user_permissions[] = $permission->slug;
                    }
                }
            }

            Auth::user()->perimissions = $user_permissions;
        }
        return $next($request);
    }
}
