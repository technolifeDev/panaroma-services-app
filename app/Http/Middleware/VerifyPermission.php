<?php

namespace App\Http\Middleware;

use App\Traits\HasRoleAndPermission;
use Closure;

class VerifyPermission
{
    use HasRoleAndPermission;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role = $request->route()->getAction();


        if(isset($role['task']) && ($this->hasPermission($role['task'])))
        {
            return $next($request);

        }
        return redirect('/dashboard')->with('errormessage', "Sorry, You don't have permission to do this action.");

    }
}
