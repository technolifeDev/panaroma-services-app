<?php

namespace App\Http\Middleware;

use App\Exceptions\AccessDeniedException;
use App\Traits\HasRoleAndPermission;
use Closure;

class VerifyRole
{
    use HasRoleAndPermission;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $role = $request->route()->getAction();

        if(isset($role['channel']) && ($this->hasRole($role['channel'])))
        {
            return $next($request);

        }
        return redirect('/dashboard')->with('errormessage', "Sorry, You don't have role to access this page.");


    }
}
