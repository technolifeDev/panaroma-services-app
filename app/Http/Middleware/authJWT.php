<?php

namespace App\Http\Middleware;

use Closure;

use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class authJWT
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $now=date('Y-m-d H:i:s');
        try {
            $user = JWTAuth::parseToken()->authenticate();
            \App\System::CustomLogWritter("apilog","jwt_log",$user);

        } catch (JWTException $e) {

            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){

                $response= [
                    "statusCode"=> 503,
                    "errorMessage"=> 'Token is Invalid',
                    "serverReferenceCode"=> $now,
                ];

                $message = PHP_EOL."Request :".json_encode($request->all()).PHP_EOL."Response :".json_encode($response);

                \App\System::CustomLogWritter("apilog","jwt_log",$message);

                return response()->json($response);

            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){

                $response= [
                    "statusCode"=> 505,
                    "errorMessage"=> 'Token is Expired',
                    "serverReferenceCode"=> $now,
                ];

                $message = PHP_EOL."Request :".json_encode($request->all()).PHP_EOL."Response :".json_encode($response);

                \App\System::CustomLogWritter("apilog","jwt_log",$message);
                return response()->json($response);

            }else{

                $response= [
                    "statusCode"=> 507,
                    "errorMessage"=> 'Something is wrong Token',
                    "serverReferenceCode"=> $now,
                ];

                $message = PHP_EOL."Request :".json_encode($request->all()).PHP_EOL."Response :".json_encode($response);

                \App\System::CustomLogWritter("apilog","jwt_log",$message);
                return response()->json($response);

            }
        }
        return $next($request);
    }
}
