<?php

namespace App\Http\Middleware;

use Closure;

class CustomerAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if((\Auth::check()) && ((\Auth::user()->user_type=="customer") ))
        {
            return $next($request);

        }else{
            if ($request->ajax())
            {
                return response('Unauthorized.', 401);
            }
            else
            {
                return redirect()->guest('/customer/login')->with('errormessage',__('auth.login_to_continue'));
            }
        }
    }
}
