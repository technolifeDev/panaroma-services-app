<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Session\Session;


class LocaleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

            if (\Session::has('locale') && in_array(\Session::get('locale'), config('locale.languages'))) {
                \App::setLocale(\Session::get('locale'));
            } else
            {
                $language = \App::getLocale();
                \Session::put('locale',$language);
            }


        return $next($request);
    }
}
