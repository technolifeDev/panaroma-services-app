<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;
use Validator;
use Session;
use \App\System;
use App\Traits\HasRoleAndPermission;


class DiscountController extends Controller
{
    use HasRoleAndPermission;

    public function __construct(Request $request)
    {
        $this->page_title = $request->route()->getName();
        $description = \Request::route()->getAction();
        $this->page_desc = isset($description['desc']) ? $description['desc'] : $this->page_title;
        \App\System::AccessLogWrite();
    }

    /**********************************************************
    ## PartnerDiscountList
     *************************************************************/
    public function  PartnerDiscountList()
    {
        $data['page_title'] = $this->page_title;
        return view('discount.partner-discount',$data);
    }

    /**********************************************************
    ## PartnerDiscountAdd
     *************************************************************/

    public function PartnerDiscountAdd()
    {
        $now= date('Y-m-d H:i:s');

        $rule = [
            'partner_discount_name' => 'Required',
            'partner_discount_code' => 'Required|alpha_num|min:4',
            'partner_discount_rate' => 'Required|numeric',
            'partner_discount_from' => 'Required|date_format:Y-m-d',
            'partner_discount_to' => 'Required|date_format:Y-m-d',
        ];

        $v = \Validator::make(\Request::all(),$rule);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v)->withInput();
        }

        try{

            if(!isset(\Auth::user()->company_id) || empty(\Auth::user()->company_id))
                throw new \Exception('Partner Info needed.Please add ');

            $discount_data = [
                'partner_id'            =>\Auth::user()->company_id,
                'partner_discount_name' =>\Request::input('partner_discount_name'),
                'partner_discount_slug' =>\Str::slug(strtolower(\Request::input('partner_discount_name')), '_'),
                'partner_discount_code' =>\Request::input('partner_discount_code'),
                'partner_discount_rate' =>\Request::input('partner_discount_rate'),
                'partner_discount_from' =>\Request::input('partner_discount_from'),
                'partner_discount_to'   =>\Request::input('partner_discount_to'),
            ];

            $discount_data_insert = \App\Discount::firstOrCreate(
                [
                    'partner_id' => $discount_data['partner_id'],
                    'partner_discount_name' => $discount_data['partner_discount_name'],
                ],
                $discount_data
            );

            \App\System::EventLogWrite('insert,discount',json_encode($discount_data_insert));

            return redirect()->back()->with('message','Discount offer created successfully');


        }catch(\Exception $e){
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            return redirect()->back()->with('errormessage',$e->getMessage());
        }
    }

    /**********************************************************
    ## PartnerAjaxDiscountList
     *************************************************************/
    public function PartnerAjaxDiscountList(Request $request) {

        $limit = $request->input('length');
        $start = $request->input('start');
        $partner_id = $request->input('partner_id');
        $discounts_data = \App\Discount::orderBy('created_at','DESC')
            ->where('partner_id',$partner_id)
            ->offset($start)
            ->limit($limit)
            ->get();

        $meta_list = array();
        $totalData = count($discounts_data);
        $index = 1;

        if(count($discounts_data)>0){

            foreach($discounts_data as $key=>$meta_data) {

                $meta_list[$key]['sno'] = $index;
                $meta_list[$key]['discount_name'] = ucwords($meta_data->partner_discount_name);
                $meta_list[$key]['discount_code'] = ucfirst($meta_data->partner_discount_code);
                $meta_list[$key]['discount_rate'] = $meta_data->partner_discount_rate;
                $meta_list[$key]['discount_start'] = $meta_data->partner_discount_from;
                $meta_list[$key]['discount_end'] = $meta_data->partner_discount_to;
                $meta_list[$key]['discount_status'] = $meta_data->partner_discount_status;
                $meta_list[$key]['actions'] = '';

                if($this->hasPermission('edit')) {
                    $meta_list[$key]['actions'] .= "<a id=" . $meta_data->id . " href='".url('/partner/discount/edit',$meta_data->id)."' class='btn btn-xs btn-green category-edit' ><i class='clip-pencil-3'></i></a>";
                }
                if($this->hasPermission('delete')) {
                    $meta_list[$key]['actions'] .= " <a id='d_" . $meta_data->id . "'" . ' class="btn btn-xs btn-red discount-data-delete" data-discount='."$meta_data->id".'><i class="clip-remove"></i></a>';
                }

                $index++;
            }
        }

        return json_encode(
            array('data'=>$meta_list,
                "recordsTotal" => intval($totalData),
                "recordsFiltered" => intval($totalData))
        );
    }

    /**********************************************************
    ## PartnerDiscountEdit
     *************************************************************/
    public function  PartnerDiscountEdit($id)
    {
        $discount_info = \App\Discount::find($id);

        if(!isset($discount_info->id))
            return \Redirect::to('/partner/discount/list')->with('errormessage','Invalid Discount ID');

        $data['page_title'] = $this->page_title;
        $data['discount'] = $discount_info;

        return view('discount.edit-partner-discount',$data);
    }

    /**********************************************************
    ## PartnerDiscountUpdate
     *************************************************************/

    public function PartnerDiscountUpdate($id)
    {
        $now= date('Y-m-d H:i:s');

        $rule = [
            'partner_discount_name' => 'Required',
            'partner_discount_code' => 'Required|alpha_num|min:4',
            'partner_discount_rate' => 'Required|numeric',
            'partner_discount_from' => 'Required|date_format:Y-m-d',
            'partner_discount_to' => 'Required|date_format:Y-m-d',
        ];

        $v = \Validator::make(\Request::all(),$rule);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v)->withInput();
        }

        try{

            if(!isset(\Auth::user()->company_id) || empty(\Auth::user()->company_id))
                throw new \Exception('Partner Info needed.Please add ');

            $discount_data = [
                'partner_id'            =>\Auth::user()->company_id,
                'partner_discount_name' =>\Request::input('partner_discount_name'),
                'partner_discount_slug' =>\Str::slug(strtolower(\Request::input('partner_discount_name')), '_'),
                'partner_discount_code' =>\Request::input('partner_discount_code'),
                'partner_discount_rate' =>\Request::input('partner_discount_rate'),
                'partner_discount_from' =>\Request::input('partner_discount_from'),
                'partner_discount_to'   =>\Request::input('partner_discount_to'),
            ];

            $discount_data_insert = \App\Discount::updateOrCreate(
                [
                    'id' => $id,
                ],
                $discount_data
            );

            \App\System::EventLogWrite('update,discount',json_encode($discount_data_insert));

            return redirect()->back()->with('message','Discount offer updated successfully');


        }catch(\Exception $e){
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            return redirect()->back()->with('errormessage',$e->getMessage());
        }
    }

    /********************************************
    # PartnerAjaxDiscountDelete
     *********************************************/
    public function PartnerAjaxDiscountDelete($id){

        $discount = \App\Discount::find($id);

        if(isset($discount->id)){
            $discount->delete();
        }
        \App\System::EventLogWrite('delete,discount',json_encode($id));
        echo "Discount Deleted Successfully!";
    }

    /**********************************************************
    ## ServiceDiscountList
     *************************************************************/
    public function  ServiceDiscountList()
    {
        $data['page_title'] = $this->page_title;
        return view('discount.service-discount',$data);
    }

    /********************************************
    ## AjxServiceDiscountTypeList
     *********************************************/
    public function AjxServiceDiscountTypeList($discount_type){

        $partner_id = (isset(\Auth::user()->company_id)&&!empty(\Auth::user()->company_id))?\Auth::user()->company_id:0;
        $data['types_list'] = \App\ServiceDiscount::getDiscountTypeList($partner_id,$discount_type);
        $data['discount_type'] = $discount_type;

        return \View::make('discount.ajax-discount-type-list',$data);

    }

    /**********************************************************
    ## ServiceDiscountAdd
     *************************************************************/

    public function ServiceDiscountAdd()
    {
        $now= date('Y-m-d H:i:s');

        $rule = [
            'service_discount_type' => 'Required',
            'service_discount_type_id' => 'Required|numeric',
            'service_discount_rate' => 'Required|numeric',
            'service_discount_from' => 'Required|date_format:Y-m-d',
            'service_discount_to' => 'Required|date_format:Y-m-d',
        ];

        $v = \Validator::make(\Request::all(),$rule);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v)->withInput();
        }

        try{

            if(!isset(\Auth::user()->company_id) || empty(\Auth::user()->company_id))
                throw new \Exception('Partner Info needed.Please add ');

            $discount_data = [
                'partner_id'            =>\Auth::user()->company_id,
                'service_discount_type' =>\Request::input('service_discount_type'),
                'service_discount_type_id' =>\Request::input('service_discount_type_id'),
                'service_discount_rate' =>\Request::input('service_discount_rate'),
                'service_discount_from' =>\Request::input('service_discount_from'),
                'service_discount_to'   =>\Request::input('service_discount_to'),
            ];

            $discount_data_insert = \App\ServiceDiscount::updateOrCreate(
                [
                    'partner_id' => $discount_data['partner_id'],
                    'service_discount_type' => $discount_data['service_discount_type'],
                    'service_discount_type_id' => $discount_data['service_discount_type_id']
                ],
                $discount_data
            );

            if($discount_data_insert->wasRecentlyCreated){

                \App\System::EventLogWrite('insert,service-discount',json_encode($discount_data_insert));
                return redirect()->back()->with('message','Service Discount offer created successfully');

            }else return redirect()->back()->with('message','Service Discount offer updated successfully');



        }catch(\Exception $e){
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            return redirect()->back()->with('errormessage',$e->getMessage());
        }
    }

    /**********************************************************
    ## AjaxServiceDiscountList
     *************************************************************/
    public function AjaxServiceDiscountList(Request $request) {


        $limit = $request->input('length');
        $start = $request->input('start');
        $partner_id = $request->input('partner_id');
        $discounts_data = \App\ServiceDiscount::orderBy('created_at','DESC')
            ->where('partner_id',$partner_id)
            ->offset($start)
            ->limit($limit)
            ->get();

        $meta_list = array();
        $totalData = count($discounts_data);
        $index = 1;

        if(count($discounts_data)>0){

            foreach($discounts_data as $key=>$meta_data) {

                $meta_list[$key]['sno'] = $index;
                $meta_list[$key]['discount_type'] = ucfirst($meta_data->service_discount_type);
                $meta_list[$key]['discount_name'] = \App\ServiceDiscount::getDiscountInfoByType($meta_data->service_discount_type,$meta_data->service_discount_type_id);
                $meta_list[$key]['discount_rate'] = $meta_data->service_discount_rate;
                $meta_list[$key]['discount_start'] = $meta_data->service_discount_from;
                $meta_list[$key]['discount_end'] = $meta_data->service_discount_to;
                $meta_list[$key]['discount_status'] = $meta_data->service_discount_status;
                $meta_list[$key]['actions'] = '';

                if($this->hasPermission('edit')) {
                    $meta_list[$key]['actions'] .= "<a id=" . $meta_data->id . " href='".url('/partner/service/discount/edit',$meta_data->id)."' class='btn btn-xs btn-green category-edit' ><i class='clip-pencil-3'></i></a>";
                }
                if($this->hasPermission('delete')) {
                    $meta_list[$key]['actions'] .= " <a id='d_" . $meta_data->id . "'" . ' class="btn btn-xs btn-red service-discount-data-delete" data-service_discount='."$meta_data->id".'><i class="clip-remove"></i></a>';
                }

                $index++;
            }
        }

        return json_encode(
            array('data'=>$meta_list,
                "recordsTotal" => intval($totalData),
                "recordsFiltered" => intval($totalData))
        );
    }

    /********************************************
    # ServiceAjaxDiscountDelete
     *********************************************/
    public function ServiceAjaxDiscountDelete($id){

        $discount = \App\ServiceDiscount::find($id);

        if(isset($discount->id)){
            $discount->delete();
        }
        \App\System::EventLogWrite('delete,service-discount',json_encode($id));
        echo "Service Discount Deleted Successfully!";
    }

    /**********************************************************
    ## ServiceDiscountEdit
     *************************************************************/
    public function  ServiceDiscountEdit($id)
    {
        $discount_info = \App\ServiceDiscount::find($id);

        if(!isset($discount_info->id))
            return \Redirect::to('/partner/discount/list')->with('errormessage','Invalid Discount ID');

        $data['page_title'] = $this->page_title;
        $data['discount'] = $discount_info;
        $data['types_list'] = \App\ServiceDiscount::getDiscountTypeList($discount_info->partner_id,$discount_info->service_discount_type);

        return view('discount.edit-service-discount',$data);
    }

    /**********************************************************
    ## ServiceDiscountUpdate
     *************************************************************/

    public function ServiceDiscountUpdate($id)
    {
        $now= date('Y-m-d H:i:s');

        $rule = [
            'service_discount_type' => 'Required',
            'service_discount_type_id' => 'Required|numeric',
            'service_discount_rate' => 'Required|numeric',
            'service_discount_from' => 'Required|date_format:Y-m-d',
            'service_discount_to' => 'Required|date_format:Y-m-d',
        ];

        $v = \Validator::make(\Request::all(),$rule);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v)->withInput();
        }

        try{

            if(!isset(\Auth::user()->company_id) || empty(\Auth::user()->company_id))
                throw new \Exception('Partner Info needed.Please add ');

            $discount_data = [
                'partner_id'            =>\Auth::user()->company_id,
                'service_discount_type' =>\Request::input('service_discount_type'),
                'service_discount_type_id' =>\Request::input('service_discount_type_id'),
                'service_discount_rate' =>\Request::input('service_discount_rate'),
                'service_discount_from' =>\Request::input('service_discount_from'),
                'service_discount_to'   =>\Request::input('service_discount_to'),
            ];

            $discount_data_insert = \App\ServiceDiscount::updateOrCreate(
                [
                    'id' => $id,
                ],
                $discount_data
            );

            if($discount_data_insert->wasRecentlyCreated){

                \App\System::EventLogWrite('update,service-discount',json_encode($discount_data_insert));
                return redirect()->back()->with('message','Service Discount offer created successfully');

            }else return redirect()->back()->with('message','Service Discount offer updated successfully');



        }catch(\Exception $e){
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            return redirect()->back()->with('errormessage',$e->getMessage());
        }
    }
}
