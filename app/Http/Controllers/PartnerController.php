<?php

namespace App\Http\Controllers;

use App\Jobs\AdminMailSendingJob;
use App\RoleUser;
use App\Role;
use App\Traits\HasRoleAndPermission;
use Illuminate\Http\Request;
use Validator;
use Session;
use \App\System;
use Response;
use Image;

class PartnerController extends Controller
{

    use HasRoleAndPermission;

    public function __construct(Request $request)
    {
        $this->page_title = $request->route()->getName();
        $description = \Request::route()->getAction();
        $this->page_desc = isset($description['desc']) ? $description['desc'] : $this->page_title;
        \App\System::AccessLogWrite();
    }

    /**********************************************************
     * ## PartnerCreate
     *************************************************************/

    public function PartnerCreate()
    {
        if($this->hasPermission('view')) {
            $partner_all_data = \App\ServicePartner::orderBy('updated_at', 'DESC')->paginate(10);
            $partner_all_data->setPath(url('/admin/service/partner/create'));
            $pagination = $partner_all_data->render();
            $data['perPage'] = $partner_all_data->perPage();
            $data['pagination'] = $pagination;
            $data['partner_all_data'] = $partner_all_data;
            $data['page_title'] = $this->page_title;
            return view('partner.add-partner', $data);
        }
    }

    /**********************************************************
    ## PartnerSave
     *************************************************************/

    public function PartnerSave()
    {
        $now = date('Y-m-d H:i:s');
        $user = \Auth::user()->id;
        $rule = [
            'partner_name' => 'Required|max:100',
            'partner_name_est' => 'max:100',
            'partner_name_rus' => 'max:100',
            'partner_address' => 'Required',
            'partner_email' => 'Required|email',
            'partner_tax_no' => 'sometimes|alpha_num',
            'partner_license' => 'sometimes|alpha_num',
            'partner_image' => 'mimes:jpeg,jpg,png,svg'
        ];

        $validation = \Validator::make(\Request::all(), $rule);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation)->withInput();
        }

        try {

            #EmailCheck
            $email_verification = \App\User::where('email',\Request::input('partner_email'))->first();

            if(isset($email_verification->id))
                return redirect()->back()->with('errormessage', 'Partner Email '.\Request::input('partner_email').' already taken.');


            \DB::beginTransaction();

            $partner_data = [
                'partner_name' => \Request::input('partner_name'),
                'partner_name_est' => \Request::input('partner_name_est'),
                'partner_name_rus' => \Request::input('partner_name_rus'),
                'partner_address' => \Request::input('partner_address'),
                'partner_address_est' => \Request::input('partner_address_est'),
                'partner_address_rus' => \Request::input('partner_address_rus'),
                'partner_email' => \Request::input('partner_email'),
                'partner_txa_no' => \Request::input('partner_tax_no'),
                'partner_license' => \Request::input('partner_license'),
            ];



            $partner_insert = \App\ServicePartner::firstOrCreate(
                [
                    'partner_name' => $partner_data['partner_name'],
                ],
                $partner_data
            );

            #upload image
            $update_fun = 0;
            if (\Request::hasFile('partner_image')) {
                $service_image = \Request::file('partner_image');
                $service_id = $partner_insert->id;
                $imageable_type = 'App\ServicePartner';
                $folder_name = 'service_images/partner_images/';
                $this->_uploadImage($service_id, $service_image, $imageable_type, $update_fun, $folder_name);
            }


//            #UserAccountCreate
//            $token = \App\System::RandomStringNum(16);
//            $user_insert_data = array(
//                'name' => ucwords(\Request::input('partner_name')),
//                'name_slug' => \Str::slug(\Request::input('partner_name'), '-'),
//                'user_type' => 'partner',
//                'user_role' => 'partner',
//                'email' => \Request::input('partner_email'),
//                'password' => bcrypt('1234'),
//                'user_profile_image' => '',
//                'login_status' => 0,
//                'status' => "active",
//                'company_id' => $partner_insert->id,
//                'remember_token' => $token,
//                'created_at' => $now,
//                'updated_at' => $now,
//            );
//            $user_insert = \App\User::firstOrCreate(
//                [
//                    'email' => $user_insert_data['email'],
//                ],
//                $user_insert_data
//            );

//            #Role And Permission
//            $roles = \App\Role::where('slug', 'partner')->first();
//            $permissions = \App\Permission::all()->toArray();
//
//            if (isset($roles->id) && count($permissions) > 0) {
//
//                $this->attachUserRole($roles->id, 5, $user_insert->id);
//
//                \App\System::EventLogWrite('create,partner role of ' . $user_insert->id, json_encode($roles));
//
//                foreach ($permissions as $key => $permission) {
//                    $this->attachUserPermission($permission['id'], $user_insert->id);
//                }
//
//                \App\System::EventLogWrite('create,partner permission role of ' . $user_insert->id, json_encode($roles));
//            }
//
//
//            #eventLog
//            \App\System::EventLogWrite('insert,users', json_encode($user_insert));


            $message = '';
            if ($partner_insert->wasRecentlyCreated) {
                \App\System::EventLogWrite('insert,service_partner_info', json_encode($partner_insert));
                $message = 'Partner Created Successfully';
            } else $message = 'Partner  already created.';


//            $reset_url = url('/auth/email/' . $user_insert->id . '/verify') . '?token=' . $token;
//            \App\ServicePartner::PartnerVerificationMail($user_insert->id, $reset_url);
            \DB::commit();
            return redirect()->back()->with('message', 'Partner Account has been created.');


        } catch (\Exception $e) {
            \DB::rollback();
            $message = "Message : " . $e->getMessage() . ", File : " . $e->getFile() . ", Line : " . $e->getLine();
            \App\System::ErrorLogWrite($message);

            return \Redirect::to('/admin/service/partner/create')->with('errormessage', $e->getMessage());
        }

    }


    /**********************************************************
    ## PartnerEditPage
     *************************************************************/

    public function PartnerEditPage($partner_id)
    {
        $partner_info = \App\ServicePartner::where('id', $partner_id)->first();

        if (!isset($partner_info->id))
            return \Redirect::to('/admin/service/partner/create')->with('errormessage', "Invalid Partner ID");

        $partner_image = \App\Image::where(['imageable_id' => $partner_id, 'imageable_type' => 'App\ServicePartner'])->first();

        $partner_all_data = \App\ServicePartner::orderBy('updated_at', 'DESC')->paginate(10);
        $partner_all_data->setPath(url('/admin/service/partner/edit', $partner_info->id));
        $pagination = $partner_all_data->render();
        $data['perPage'] = $partner_all_data->perPage();
        $data['pagination'] = $pagination;
        $data['partner_all_data'] = $partner_all_data;
        $data['page_title'] = $this->page_title;
        $data['partner_info'] = $partner_info;
        $data['partner_image'] = $partner_image;

        return view('partner.edit-partner', $data);

    }

    /**********************************************************
    ## PartnerUpdate
     *************************************************************/

    public function PartnerUpdate($partner_id)
    {
        $now = date('Y-m-d H:i:s');
        $user = \Auth::user()->id;
        $rule = [
            'partner_name' => 'Required',
            'partner_address' => 'Required',
            'partner_txa_no' => 'alpha_num',
            'partner_license' => 'alpha_num',
        ];
        $customMessages = [
            'partner_txa_no.alpha_num' => 'The partner tax number may only contain letters and numbers',
        ];
        if (\Request::hasFile('partner_image')) {
            $rule['partner_image'] = 'mimes:jpeg,jpg,png,svg';
        }

        $validation = \Validator::make(\Request::all(), $rule, $customMessages);


        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation)->withInput();
        }

        try {
            $partner_data = [
                'partner_name' => \Request::input('partner_name'),
                'partner_address' => \Request::input('partner_address'),
                'partner_txa_no' => \Request::input('partner_txa_no'),
                'partner_license' => \Request::input('partner_license'),
                'partner_name_est' => \Request::input('partner_name_est'),
                'partner_name_rus' => \Request::input('partner_name_rus'),
                'partner_address_est' => \Request::input('partner_address_est'),
                'partner_address_rus' => \Request::input('partner_address_rus'),
            ];

            $partner_update = \App\ServicePartner::updateOrCreate(
                [
                    'id' => $partner_id,
                ],
                $partner_data
            );

            $update_fun = 0;
            if (\Request::input('partner_image_id')) {
                $update_fun = 1;
            }

            if (\Request::hasFile('partner_image')) {
                $service_image = \Request::file('partner_image');
                $service_id = $partner_update->id;
                $imageable_type = 'App\ServicePartner';
                $folder_name = 'service_images/partner_images/';
                $this->_uploadImage($service_id, $service_image, $imageable_type, $update_fun, $folder_name);
            }

            \App\System::EventLogWrite('update,partner_info', json_encode($partner_update));
            return \Redirect::to('/admin/service/partner/edit/' . $partner_id)->with('message', "partner updated Successfully");


        } catch (\Exception $e) {

            $message = "Message : " . $e->getMessage() . ", File : " . $e->getFile() . ", Line : " . $e->getLine();
            \App\System::ErrorLogWrite($message);
            return \Redirect::to('/admin/service/partner/edit/' . $partner_id)->with('errormessage', $e->getMessage());

        }
    }

    //partner details preview with related service list
    public function partnerView($partner_id){
        $partner_info = \App\ServicePartner::where('id', $partner_id)->first();

        if (!isset($partner_info->id))
            return \Redirect::to('/admin/service/partner/create')->with('errormessage', "Partner not found");

        $data['page_title'] = $this->page_title;
        $data['partner_info'] = $partner_info;
        return view('partner.view-partner-details', $data);
    }

    //partner wise service add
    /**
     * Show service add form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function serviceAddView($partner_id){
        $partner_info = \App\ServicePartner::where('id', $partner_id)->first();

        if (!isset($partner_info->id))
            return \Redirect::to('/admin/service/partner/create')->with('errormessage', "Partner not found");
//        $data['service_catalogue_all_data'] = \App\ServiceCatalogue::orderBy('service_name', 'DESC')->get();
        $data['category_all_data'] = \App\ServicesCategory::orderBy('category_name', 'DESC')->get();
        $data['page_title'] = $this->page_title;
        $data['partner_info'] = $partner_info;
        $data['service_frequency_list'] = \App\ServiceFrequency::orderBy('frequency_name', 'ASC')->get();
        return view('partner.add-partner-service', $data);
    }


    /********************************************
    # PartnerDelete
     *********************************************/
    public function PartnerDelete($partner_id){
        $partner_info = \App\ServicePartner::where('id', $partner_id)->first();

        if (!isset($partner_info->id))
            return \Redirect::to('/admin/service/partner/create')->with('errormessage', "Invalid Partner ID");

        \App\User::where('email', $partner_info->partner_email)->delete();
        \App\ServicePartner::where('id', $partner_id)->delete();
        \App\System::EventLogWrite('delete,service_partner_info', json_encode($partner_info));

        echo "Partener Deleted Successfully!";
    }

    //partner service list
    public function partnerService(){
        $data['page_title'] = $this->page_title;
        return view('partner.partner-service-list', $data);
    }

    //partner wise service list
    //data table ajax load
    public function getServiceData(Request $request){
        $hasEditPermission = $this->hasPermission('edit');
        $hasViewPermission = $this->hasPermission('view');
        $hasDelPermission = $this->hasPermission('delete');
        $request_partner_id = \Request::input('partner_id');
        $service_pricing_partner_id = '';
        if ($request_partner_id && $request_partner_id > 0) {
            $service_pricing_partner_id = $request_partner_id;
        } else {
            $service_pricing_partner_id = \Auth::user()->company_id;
        }

        if ($service_pricing_partner_id) {
            $condition = array('service_item_pricing.service_pricing_partner_id' => $service_pricing_partner_id);
            $totalData = \App\ServiceItemPricing::where('service_item_pricing.service_pricing_partner_id', '=', $service_pricing_partner_id)
                ->count();
        } else {
            $condition = array();
            $totalData = \App\ServiceItemPricing::count();
        }

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $serviceData = array();
        if (empty($request->input('search.value'))) {
            $serviceData = \App\ServiceItemPricing::leftJoin('service_items',
                'service_item_pricing.service_pricing_item_id', '=', 'service_items.id'
            )
                ->leftJoin('services_categories', 'service_items.service_item_category_id', '=', 'services_categories.id')
                ->leftJoin('service_catalogue', 'service_items.service_item_catalogue_id', '=', 'service_catalogue.id')
                ->leftJoin('service_partner_info', 'service_items.service_item_partner_id', '=', 'service_partner_info.id')
                ->leftJoin('service_config', 'service_item_pricing.service_config_id', '=', 'service_config.id')
                ->select(
                    'service_items.id as service_item_id', 'service_config.*', 'services_categories.category_name', 'service_catalogue.service_name',
                    'service_item_pricing.status'
                )
                ->where($condition)
                ->groupby('service_items.id')
                ->offset($start)
                ->limit($limit)
                ->orderBy('service_item_pricing.updated_at', 'DESC')
                ->get()->toArray();;
        } else {
            $search = $request->input('search.value');
            $serviceData = \App\ServiceItemPricing::leftJoin('service_items',
                'service_item_pricing.service_pricing_item_id', '=', 'service_items.id'
            )
                ->leftJoin('services_categories', 'service_items.service_item_category_id', '=', 'services_categories.id')
                ->leftJoin('service_catalogue', 'service_items.service_item_catalogue_id', '=', 'service_catalogue.id')
                ->leftJoin('service_partner_info', 'service_items.service_item_partner_id', '=', 'service_partner_info.id')
                ->leftJoin('service_config', 'service_item_pricing.service_config_id', '=', 'service_config.id')
                ->where($condition)
                ->orWhere('service_catalogue.service_name', 'LIKE', '%' . $search . '%')
                ->orWhere('services_categories.category_name', 'LIKE', '%' . $search . '%')
                ->orWhere('service_config.service_pricing_title', 'LIKE', '%' . $search . '%')
                ->orWhere('service_config.service_pricing_unit_cost_amount', 'LIKE', '%' . $search . '%')
                ->select(
                    'service_items.id as service_item_id', 'service_config.*', 'services_categories.category_name', 'service_catalogue.service_name',
                    'service_item_pricing.status'
                )
                ->groupby('service_items.id')
                ->offset($start)
                ->limit($limit)
                ->orderBy('service_item_pricing.updated_at', 'DESC')
                ->get()->toArray();
            $totalFiltered = sizeof($serviceData);
        }


        $data = array();
        $sl = 0;
        foreach ($serviceData as $key => $value) {
            $sl++;
            $status = '';
            if ($value['status'] == 'active') {
                $status = '<span class="label label-success">Active</span>';
            } else if ($value['status'] == 'inactive') {
                $status = '<span class="label label-danger">In Active</span>';
            } else if ($value['status'] == 'draft') {
                $status = '<span class="label label-info">Draft</span>';
            } else {
                $status = '<span class="label label-warning">Pending</span>';
            }
            $data[$key]['sl'] = $sl;
            $data[$key]['title'] = $value['service_pricing_title'];
            $data[$key]['service_name'] = $value['service_name'];
            $data[$key]['category_name'] = $value['category_name'];
//            $data[$key]['currency'] = $value['service_pricing_currency_unit'];
            $data[$key]['amount'] = $value['service_pricing_unit_cost_amount'] . ' ' . $value['service_pricing_currency_unit'];
            $data[$key]['status'] = $status;
            $data[$key]['action'] = ($hasEditPermission ? '<a href="' . url('/partner/edit-service/') . '/' . $value['service_item_id'] . '" class="btn btn-xs tooltips btn-green editServiceBtn"><i class="clip-pencil-3"></i></a>' : '')
                . ($hasViewPermission ? ' <a href="#" id="' . $value['service_item_id'] . '" class="btn btn-xs tooltips btn-primary viewDtlServiceBtn"><i class="clip-zoom-in"></i></a>' : '')
                . ($hasViewPermission ? ' <a href="#" id="' . $value['service_item_id'] . '" class="btn btn-xs tooltips btn-warning addMoreAttrBtn" title="Add Attribute"><i class="fa fa-sun-o"></i></a>' : '')
                . ($hasDelPermission ? ' <a href="#" id="' . $value['service_item_id'] . '" class="btn btn-xs tooltips btn-danger deltServiceBtn"><i class="clip-close-2"></i></a>' : '');
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        return json_encode($json_data);
    }

    //partner service create html form
    public function partnerServiceAddHtml(){
        $data['category_all_data'] = \App\ServicesCategory::orderBy('category_name', 'DESC')->get();
        $cat_list = $data['category_all_data']->toArray();
        $data['first_cat_id'] = isset($cat_list['0']['id']) ? $cat_list['0']['id'] : '';
        $catalogue_list = array();
        if(isset($cat_list['0']['id'])){
            $data['service_catalogue_all_data'] = \App\ServiceCatalogue::orderBy('service_name', 'DESC')->where('services_categories_id', '=', $cat_list['0']['id'])->get();
            $catalogue_list = $data['service_catalogue_all_data']->toArray();
        }
        $data['first_catalogue_id'] = isset($catalogue_list['0']['id']) ? $catalogue_list['0']['id'] : '';
        $data['service_frequency_list'] = \App\ServiceFrequency::orderBy('frequency_name', 'ASC')->get();
        $data['page_title'] = $this->page_title;
        return view('partner.partner-service-create', $data);
    }

    //save partner's created services
    public function partnerServiceStore(Request $request){
//        print_r($request->all()); die();
        $user_id = \Auth::user()->id;
        $url = '';
        $service_pricing_partner_id = \Auth::user()->company_id;
        $url = '/partner/services';

        $return_arr = array();
        $rule = [
            'service_pricing_catalogue_id' => 'Required',
            'service_pricing_category_id' => 'Required',
            'service_pricing_title.*' => 'Required',
            'service_pricing_description.*' => 'Required',
            'service_pricing_currency_unit.*' => 'Required',
            'service_pricing_unit_cost_amount.*' => 'Required|regex:/^\d+(\.\d{1,2})?$/'
        ];

        $v = \Validator::make(\Request::all(), $rule);

        if ($v->fails()) {
            $return_arr = array('status' => 'error', 'msg' => 'Please fill all required field 2.');
            return Response::json($return_arr);
        }

        $category_id = \Request::input('service_pricing_category_id');


        $return_status = $this->_saveDataInServiceItmTbl($request, $service_pricing_partner_id, $category_id);


        if ($return_status == 1) {
            $return_arr = array('status' => 'success', 'msg' => 'Service created successfully.', 'url' => $url);
        } else {
            $return_arr = array('status' => 'warning', 'msg' => 'Please fill all required input field.');
        }
        return Response::json($return_arr);

    }


    //save data in service_item
    private function _saveDataInServiceItmTbl($request, $service_pricing_partner_id, $category_id){
        $return_arr = array();
        try{
            $service_data = [
                'service_pricing_catalogue_id' =>\Request::input('service_pricing_catalogue_id'),
                'service_pricing_category_id' =>$category_id,
                'service_pricing_title' =>\Request::input('service_pricing_title'),
                'service_pricing_description' =>\Request::input('service_pricing_description'),
                'service_pricing_currency_unit' =>\Request::input('service_pricing_currency_unit'),
                'service_pricing_unit_cost_amount' =>\Request::input('service_pricing_unit_cost_amount'),
                'service_frequency_id' =>\Request::input('service_frequency_id'),
                'service_commission_price' =>\Request::input('service_commission_price'),
                'service_tax_price' =>\Request::input('service_tax_price'),
            ];
            $request_arr = $request->toArray();
            foreach($service_data['service_pricing_title'] as $key => $val){
                if(!empty($service_data['service_pricing_title'][$key]) && !empty($service_data['service_pricing_currency_unit'][$key])
                    && !empty($service_data['service_pricing_unit_cost_amount'][$key]) && !empty($request_arr['images-' . $key])){
                    $data['service_item_catalogue_id'] = $service_data['service_pricing_catalogue_id'];
                    $data['service_item_category_id'] = $service_data['service_pricing_category_id'];
                    $data['service_item_partner_id'] = $service_pricing_partner_id;
                    $service_item_price['service_pricing_catalogue_id'] = $service_data['service_pricing_catalogue_id'];
                    $service_item_price['service_pricing_category_id'] = $service_data['service_pricing_category_id'];
                    $service_item_price['service_pricing_partner_id'] = $service_pricing_partner_id;
                    $service_config['service_pricing_title'] = $service_data['service_pricing_title'][$key];
                    $service_config['service_pricing_description'] = $service_data['service_pricing_description'][$key];
                    $service_config['service_pricing_currency_unit'] = $service_data['service_pricing_currency_unit'][$key];
                    $service_config['service_pricing_unit_cost_amount'] = $service_data['service_pricing_unit_cost_amount'][$key];
                    $service_config['service_commission_price'] = $service_data['service_commission_price'][$key];
                    $service_config['service_tax_price'] = $service_data['service_tax_price'][$key];
                    $service_item_price['status'] = 'active';

                    $service_item_price['service_config_id'] = $this->_serviceItemConfig($service_config);
                    $service_item = \App\ServiceItem::create($data);
                    $service_item_price['service_pricing_item_id'] = $service_item->id;
                    $image_arr = array();

                    if(!empty($request_arr['images-' . $key])){
                        $image_arr = $request['images-' .$key];
                    }
                    $this->_saveDataInServiceItmPrcTbl($service_item_price, $image_arr);
                    //service frequency add
                    if(isset($service_data['service_frequency_id'][$key][0])){
                        $this->_saveServiceFrequency($service_item->id, $service_data['service_frequency_id'][$key]);
                    }
                    if($service_item->wasRecentlyCreated){
                        \App\System::EventLogWrite('insert,service_item',json_encode($data));
                        $return_arr = 1; //array('status' => 'success', 'msg' => 'Service created successfully.');
                    }
                } else {
                    $return_arr = 2;//array('status' => 'error', 'msg' => 'Please .');
                }
            }

        }catch(\Exception $e){
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            $return_arr = array('status' => 'error', 'msg' => $e->getMessage());
        }
        return $return_arr;
    }

    //save data in service_item_price
    private function _saveDataInServiceItmPrcTbl($service_item_price, $image_arr = array()){
        $return_arr = array();
        try{
            $service_item_price_ = \App\ServiceItemPricing::firstOrCreate(
                [
                    'service_pricing_catalogue_id'  => $service_item_price['service_pricing_catalogue_id'],
                    'service_pricing_category_id'  => $service_item_price['service_pricing_category_id'],
                    'service_pricing_partner_id' => $service_item_price['service_pricing_partner_id'],
                    'service_pricing_item_id' => $service_item_price['service_pricing_item_id']
                ],
                $service_item_price
            );
            $update_fun = 0;
            if(!empty($image_arr)){
                foreach ($image_arr as $img_key => $imag){
                    $service_image = $imag;
                    $service_id = $service_item_price['service_pricing_item_id'];
                    $imageable_type = 'App\ServiceItem';
                    $folder_name = 'service_images/service_item_images/';
                    $this->_uploadImage($service_id, $service_image, $imageable_type, $update_fun, $folder_name);
                }
            }
            if($service_item_price_->wasRecentlyCreated){
                \App\System::EventLogWrite('insert,service_item_price',json_encode($service_item_price_));
                $return_arr = array('status' => 'success', 'msg' => 'Service created successfully.');
            }
        }catch(\Exception $e){
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            $return_arr = array('status' => 'error', 'msg' => $e->getMessage());
        }
        return $return_arr;
    }

    //save service item configuration
    private function _serviceItemConfig($service_config){
        $return_config_id = 0;
        try{
            $service_config_ = \App\ServiceConfig::firstOrCreate(
                [
                    'service_pricing_title'  => $service_config['service_pricing_title'],
                ],
                $service_config
            );

            if($service_config_->wasRecentlyCreated){
                \App\System::EventLogWrite('insert,service_config',json_encode($service_config_));
                $return_config_id = $service_config_->id;
            }
        }catch(\Exception $e){
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            $return_config_id = 0;
        }
        return $return_config_id;
    }

    //save service frequency
    private function _saveServiceFrequency($service_item_id, $service_frequency_id){
        foreach($service_frequency_id as $key => $value){
            $service_frequency_ = \App\ServiceItemFrequency::updateOrCreate([
                'service_item_id' => $service_item_id,
//                'service_frequency_id' => $value,
                ],
                [
                    'service_item_id' => $service_item_id,
                    'service_frequency_id' => $value,
                ]
            );
        }
        return true;
    }

    //delete service frequency
    private function _deleteServiceFrequency($service_item_id, $service_frequency_arr = array()){
        $service_item_frequency = \App\ServiceItemFrequency::where('service_item_id', '=', $service_item_id);
        $service_item_frequency_ = $service_item_frequency->get()->toArray();
        $old_data = array();
        if(!empty($service_item_frequency_)){
            foreach($service_item_frequency_ as $key => $val){
                $old_data[$key] = $val['service_frequency_id'];
            }
        }
        $diff_arr = array_diff($old_data, $service_frequency_arr);
        if(!empty($diff_arr)){
            \App\ServiceItemFrequency::where('service_item_id', '=', $service_item_id)
                ->whereIn('service_frequency_id', $diff_arr)->delete();
        }
    }

    //partner service edit
    //partner service create html form
    public function partnerServiceEditHtml($service_item_id){
        $partner_info = \App\ServiceItemPricing::where('service_pricing_item_id', $service_item_id)->first();

        if (!isset($partner_info->id))
            return \Redirect::to('/partner/service-create')->with('errormessage', "Invalid Service Item");

        $serviceData = \App\ServiceItemPricing::where(['service_item_pricing.service_pricing_item_id' => $service_item_id])
            ->leftJoin('service_catalogue', 'service_item_pricing.service_pricing_catalogue_id', '=', 'service_catalogue.id')
            ->leftJoin('service_config', 'service_config.id', '=', 'service_item_pricing.service_config_id')
            ->select(['service_item_pricing.*', 'service_catalogue.service_name', 'service_config.service_pricing_title'
                , 'service_config.service_pricing_description', 'service_config.service_pricing_currency_unit',
                'service_config.service_pricing_unit_cost_amount', 'service_config.id as service_config_id',
                'service_config.service_commission_price','service_config.service_tax_price',])
            ->get()
            ->toArray();
//        print_r($serviceData); die();
        $data['service_catalogue_all_data'] = \App\ServiceCatalogue::orderBy('service_name', 'DESC')->where('services_categories_id', '=', $serviceData[0]['service_pricing_category_id'])->get();
        $data['category_all_data'] = \App\ServicesCategory::orderBy('category_name', 'DESC')->get();
        $data['page_title'] = $this->page_title;
        $data['service_item_pricing'] = $serviceData;
        $data['service_item_images'] = $this->_getServiceItemsImages($service_item_id);
        $data['service_frequency_list'] = \App\ServiceFrequency::orderBy('frequency_name', 'ASC')->get();
        $service_item_frequency_arr = \App\ServiceItemFrequency::where('service_item_id', '=', $service_item_id)->get()->toArray();
        $listArr = array();
        if(!empty($service_item_frequency_arr)){
            foreach($service_item_frequency_arr as $key => $val){
                $listArr[$val['service_frequency_id']] = $val['id'];
            }
        }
        $data['service_item_frequency'] = $listArr;
        \App\System::EventLogWrite('retrieve,services_item_pricing', json_encode($service_item_id));
        return view('partner.partner-service-edit', $data);
    }



    //update service item
    public function updatePartnerServiceItem(Request $request){
        $user_id = \Auth::user()->id;
        $service_pricing_partner_id = \Auth::user()->company_id;
        $return_arr = array();
        $rule = [
            'service_pricing_catalogue_id' => 'Required',
            'service_pricing_category_id' => 'Required',
            'service_pricing_title.*' => 'Required',
            'service_pricing_description.*' => 'Required',
            'service_pricing_currency_unit.*' => 'Required',
            'service_pricing_unit_cost_amount.*' => 'Required|regex:/^\d+(\.\d{1,2})?$/'
        ];

        $v = \Validator::make(\Request::all(), $rule);

        if ($v->fails()) {
            $return_arr = array('status' => 'error', 'msg' => 'Please fill all required field and amount must be numerical value.');
            return Response::json($return_arr);
        }

//        print_r($request->all()); die();
        $return_status = $this->_updateDataInServiceItmTbl($request);
        if ($return_status == 1) {
            $old_images = !empty(\Request::input('old_images')) ? \Request::input('old_images') : array();
            $old_image_from_db_json = \Request::input('service_item_images');
            $old_image_from_db = json_decode($old_image_from_db_json, true);
            $this->_delImages($old_images, $old_image_from_db);
            $return_arr = array('status' => 'success', 'msg' => 'Service updated successfully.');
        } else {
            $return_arr = array('status' => 'warning', 'msg' => 'Please fill all required input field.');
        }
        return Response::json($return_arr);
    }


    //update data in service_item_price
    private function _updateDataInServiceItmTbl($request){
        $return_arr = array();
        try{
            $service_data = [
                'service_pricing_catalogue_id' =>\Request::input('service_pricing_catalogue_id'),
                'service_pricing_category_id' =>\Request::input('service_pricing_category_id'),
                'service_pricing_title' =>\Request::input('service_pricing_title'),
                'service_pricing_description' =>\Request::input('service_pricing_description'),
                'service_pricing_currency_unit' =>\Request::input('service_pricing_currency_unit'),
                'service_pricing_unit_cost_amount' =>\Request::input('service_pricing_unit_cost_amount'),
                'service_pricing_item_id' =>\Request::input('service_pricing_item_id'),
                'id' => \Request::input('service_item_pricing_id'),
                'service_config_id' => \Request::input('service_config_id'),
                'service_pricing_partner_id' => \Request::input('service_pricing_partner_id'),
                'status' => 'active',
                'service_frequency_id' =>\Request::input('service_frequency_id'),
                'service_commission_price' =>\Request::input('service_commission_price'),
                'service_tax_price' =>\Request::input('service_tax_price'),
            ];
            $old_images_ = \Request::input('old_images');
            $request_arr = $request->toArray();
            foreach($service_data['service_pricing_title'] as $key => $val){
                if(!empty($service_data['service_pricing_title'][$key]) && !empty($service_data['service_pricing_currency_unit'][$key])
                    && !empty($service_data['service_pricing_unit_cost_amount'][$key]) && (!empty($request_arr['images-' . $key]) ||
                        !empty($old_images_)
                    )){
                    $service_item_price['service_pricing_catalogue_id'] = $service_data['service_pricing_catalogue_id'];
                    $service_item_price['service_pricing_category_id'] = $service_data['service_pricing_category_id'];
                    $service_item_price['service_pricing_partner_id'] = $service_data['service_pricing_partner_id'];
                    $service_config['service_config_id'] = $service_data['service_config_id'];
                    $service_config['service_pricing_title'] = $service_data['service_pricing_title'][$key];
                    $service_config['service_pricing_description'] = $service_data['service_pricing_description'][$key];
                    $service_config['service_pricing_currency_unit'] = $service_data['service_pricing_currency_unit'][$key];
                    $service_config['service_pricing_unit_cost_amount'] = $service_data['service_pricing_unit_cost_amount'][$key];
                    $service_config['service_commission_price'] = $service_data['service_commission_price'][$key];
                    $service_config['service_tax_price'] = $service_data['service_tax_price'][$key];
                    $service_item_price['status'] = 'active';
                    $service_item_price['service_config_id'] = $this->_updateServiceItemConfig($service_config);

                    $service_item = \App\ServiceItem::findOrFail($service_data['service_pricing_item_id']);
                    $service_item->service_item_catalogue_id = $service_data['service_pricing_catalogue_id'];
                    $service_item->service_item_category_id = $service_data['service_pricing_category_id'];
                    $service_item->service_item_partner_id = $service_data['service_pricing_partner_id'];
                    $service_item->save();
                    $service_item_price['service_pricing_item_id'] = $service_item->id;
                    $image_arr = array();
                    $request_arr = $request->toArray();
                    if(!empty($request_arr['images-' . $key])){
                        $image_arr = $request['images-' .$key];
                    }

                    //service frequency add
                    $service_frequency_arr = isset($service_data['service_frequency_id'][$key][0]) ? $service_data['service_frequency_id'][$key] : array();

                    $this->_deleteServiceFrequency($service_item->id, $service_frequency_arr);
                    if(isset($service_data['service_frequency_id'][$key][0])){
                        $this->_saveServiceFrequency($service_item->id, $service_data['service_frequency_id'][$key]);
                    }


                    foreach ($service_data['id'] as $key => $val){
                        $service_item_price['id'] = $val;
                        $this->_updateDataInServiceItmPrcTbl($service_item_price, $image_arr);
                    }
                    \App\System::EventLogWrite('update,service_item',json_encode($service_item));
                    $return_arr = 1; //array('status' => 'success', 'msg' => 'Service created successfully.');

                } else {
                    $return_arr = 2;//array('status' => 'error', 'msg' => 'Please .');
                }
            }

        }catch(\Exception $e){
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            $return_arr = array('status' => 'error', 'msg' => $e->getMessage());
        }
        return $return_arr;
    }


    //update
    private function _updateDataInServiceItmPrcTbl($service_item_price, $image_arr){
        $return_arr = array();
        try{
            $service_item_price_ = \App\ServiceItemPricing::updateOrCreate(
                [
                    'id' => $service_item_price['id']
                ],
                $service_item_price
            );
            if($service_item_price_->wasRecentlyCreated){
                \App\System::EventLogWrite('update,service_item_price',json_encode($service_item_price_));
                $return_arr = array('status' => 'success', 'msg' => 'Service created successfully.');
            }

            $update_fun = 0;
            if(!empty($image_arr)){
                foreach ($image_arr as $img_key => $imag){
                    $service_image = $imag;
                    $service_id = $service_item_price['service_pricing_item_id'];
                    $imageable_type = 'App\ServiceItem';
                    $folder_name = 'service_images/service_item_images/';
                    $this->_uploadImage($service_id, $service_image, $imageable_type, $update_fun, $folder_name);
                }
            }
        }catch(\Exception $e){
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            $return_arr = array('status' => 'error', 'msg' => $e->getMessage());
        }
        return $return_arr;
    }

    //update service item configuration
    private function _updateServiceItemConfig($service_config){
        $return_config_id = 0;
        try{
            $service_config_ = \App\ServiceConfig::updateOrCreate(
                [
                    'id'  => $service_config['service_config_id'],
                ],
                $service_config
            );

            \App\System::EventLogWrite('insert,service_config',json_encode($service_config_));
            $return_config_id = $service_config_->id;

        }catch(\Exception $e){
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            $return_config_id = 0;
        }
        return $return_config_id;
    }


    //get category wise service list
    public function getCatWiseServiceList($category_id){
        $getAllData = \App\ServiceCatalogue::where('services_categories_id', '=', $category_id)->get()->toArray();
        return json_encode($getAllData);
    }


    //upload service_catalogue image
    private function _uploadImage($service_id, $service_image, $imageable_type, $update_fun = 0, $folder_name){
        $ext = strtolower($service_image->getClientOriginalExtension());
        if(in_array($ext, ['jpg', 'jpeg','svg', 'png'])){
            $image_orginal_name = $service_image->getClientOriginalName();
            $filename = pathinfo($image_orginal_name, PATHINFO_FILENAME);
            $name = time() . '_' . $filename .'.'.$service_image->getClientOriginalExtension();

			//$destinationPath = $folder_name;//public_path('/partner_images');
           // $service_image->move($destinationPath, $name);

			$img_location = $service_image->getRealPath();
			$path2 		  = $folder_name . $name;
			if (!file_exists($folder_name)) {
				mkdir($folder_name, 0777, true);
			}

			$img_height = Image::make($img_location)->height();
			$img_width  = Image::make($img_location)->width();
			// when image height less than width
			// crop the image by it's height
			if($img_height < $img_width){
				Image::make($img_location)->crop($img_height,$img_height)->resize(400, null, function ($constraint) {$constraint->aspectRatio();$constraint->upsize();})->save($path2);
			}
			// when image width less than height
			// crop the image by it's width
			else{
				Image::make($img_location)->crop($img_width,$img_width)->resize(400, null, function ($constraint) {$constraint->aspectRatio();$constraint->upsize();})->save($path2);
			}


            if($update_fun == 0){
                $photo = new \App\Image();
                $photo->image_name = $name;
                $photo->imageable_id = $service_id;
                $photo->imageable_type = $imageable_type;
                $photo->save();
            } else {
                $image_data = [
                    'image_name' =>  $name,
                    'imageable_id' => $service_id,
                    'imageable_type' => $imageable_type
                ];
                $image_insert = \App\Image::updateOrCreate(
                    [
                        'imageable_id' => $service_id,
                        'imageable_type' => $imageable_type,
                    ],
                    $image_data
                );
            }
            \App\System::EventLogWrite('insert,images',json_encode($name));
        }
    }

    //get services images
    private function _getServiceItemsImages($servic_item_id){
        $getAllImages = \App\Image::where(['imageable_id' => $servic_item_id, 'imageable_type' => 'App\ServiceItem'])
            ->get()
            ->toArray()
        ;

        $json_arr = array();
        if(!empty($getAllImages)){
            foreach($getAllImages as $key => $val){
                $tm_arr['id'] = $val['id'];
                $tm_arr['src'] = url('/service_images/service_item_images') . '/' . $val['image_name'];
                $json_arr[] = $tm_arr;
            }
        }
//        print_r(json_encode($json_arr)); die();
        return json_encode($json_arr);
    }


    //check and delete images
    private function _delImages($old_images, $old_image_from_db){
        $prev_image_arr = array();
        $prev_image_loc_arr = array();
        foreach($old_image_from_db as $key => $val){
            $prev_image_arr[] = $val['id'];
            $prev_image_loc_arr[$val['id']] = $val['src'];
        }

        $diff_arr = array_diff($prev_image_arr, $old_images);
        if(!empty($diff_arr)){
            foreach ($diff_arr as $key => $val){
                $pathinfo = pathinfo($prev_image_loc_arr[$val]);
                $filename =  $pathinfo['filename'].'.'.$pathinfo['extension'];
                $image_path = public_path('/service_images/service_item_images') . '/' . $filename;  // Value is not URL but directory file path
                if(\File::exists($image_path)) {
                    \File::delete($image_path);
                }
            }
            \DB::table('images')->whereIn('id', $diff_arr)->delete();
            \App\System::EventLogWrite('delete,images',json_encode($diff_arr));
        }
    }

    /**
     * Show the form for creating a new user as a Partner
     * pass page title.
     *
     *@return HTML view Response.
     */
    public function UserManagement()
    {

        $data['page_title'] = $this->page_title;
        if (isset($_REQUEST['tab']) && !empty($_REQUEST['tab'])) {
            $tab = $_REQUEST['tab'];
        } else {
            $tab = 'create_user';
        }
        $data['tab'] = $tab;
        $data['block_users'] = \DB::table('users')->where('status','deactivate')->where('company_id',\Auth::user()->company_id)->get();
        $data['active_users'] = \DB::table('users')->where('status','active')->where('company_id',\Auth::user()->company_id)->get();
        $data['permissions'] = \DB::table('permissions')->get();

        return view('partner.partner-user-management',$data);
    }

    /**
     * Creating new User
     * insert user meta data if data input else insert null to user meta table.
     *
     * @param  Request  $request
     * @return Response
     */
    public function CreateUser(Request $request)
    {
        if ($this->level() >=5) {
            $v = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'permissions' => 'required|array',
                'password' => 'required',
                'confirm_password' => 'required|same:password',
            ]);
            if ($v->fails()) {
                return redirect()->back()->withErrors($v)->withInput();
            }
            if (count($request->input('permissions'))>0) {
                $now=date('Y-m-d H:i:s');
                $slug=explode(' ', strtolower($request->input('name')));
                $name_slug=implode('.', $slug);
                if (!empty($request->file('image_url'))) {
                    $image = $request->file('image_url');
                    $img_location = $image->getRealPath();
                    $img_ext = $image->getClientOriginalExtension();
                    $user_profile_image = \App\Admin::PartnerUserImageUpload($img_location, $request->input('email'), $img_ext);
                } else {
                    $user_profile_image='';
                }

                try {

                    #User Create
                    $token = \App\System::RandomStringNum(16);
                    $user_insert_data =\App\User::create([
                        'name' => ucwords($request->input('name')),
                        'name_slug' => $name_slug,
                        'email' => $request->input('email'),
                        'password' => bcrypt($request->input('confirm_password')),
                        'user_profile_image' => $user_profile_image,
                        'login_status' => 0,
                        'company_id' => \Auth::user()->company_id,
                        'remember_token' => $token,
                        'status' => "active",
                        'created_at' => $now,
                        'updated_at' => $now,
                    ]);

                    $update_password=array(
                        'password' => bcrypt($request->input('password')),
                        'updated_at' => $now
                    );

                    $update_pass=\App\User::where('id', $user_insert_data->id)->update($update_password);


                    #eventLog
                    \App\System::EventLogWrite('create,new-partner-users',json_encode($user_insert_data));


                    #Role And Permission
                    $roles = \App\Role::where('slug','partner')->first();
                    if(!isset($roles->id))
                        throw new \Exception('Role is required');


                    $this->attachUserRole($roles->id,4,$user_insert_data->id);

                    \App\System::EventLogWrite('create,partner user role of '.$user_insert_data->id,json_encode($roles));

                    $permissions = $request->input('permissions');
                    foreach ($permissions as $key => $permission) {
                        $this->attachUserPermission($permission, $user_insert_data->id);
                    }
                    \App\System::EventLogWrite('create,partner permission role of '.$user_insert_data->id,json_encode($roles));

                    #InviationMail
                    $data['user'] = $user_insert_data;
                    $data['password'] = $request->input('password');
                    dispatch(new AdminMailSendingJob($data));


                    \DB::commit();
                    return redirect('partner/user/management')->with('message',"User Account Created Successfully !");

                } catch(\Exception $e) {
                    $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
                    \DB::rollback();
                    \App\System::ErrorLogWrite($message);
                    return redirect('partner/user/management')->with('errormessage',"User Already Exist !");
                }
            } else {
                return redirect('partner/user/management')->with('errormessage',"Permission missing");
            }
        } else {
            return redirect('/dashboard')->with('errormessage',"You are not authorized user!");
        }
    }

    /**
     * Change status for individual user.
     *
     * @param int $user_id
     * @param int $status.
     *
     * @return Response.
     */
    public function ChangeUserStatus($user_id, $status)
    {
        $now = date('Y-m-d H:i:s');
        $update = \DB::table('users')
            ->where('id',$user_id)
            ->update(array(
                'status' => $status,
                'updated_at' => $now
            ));
        if($update) {
            echo 'User update successfully.';
        } else {
            echo 'User did not update.';
        }
    }

    //calculate tax and commission
    public function calculateTaxCommission(){
        $get_tax_details = \App\Setting::getSettings('tax');
        $commission = \App\Setting::getSettings('commission');
        $data['tax'] =isset($get_tax_details[0]['meta_field_name']) && $get_tax_details[0]['meta_field_name'] == 'tax_percentage' ? $get_tax_details[0]['meta_field_value'] : 0;
        $data['commission'] =isset($commission[0]['meta_field_name']) && $commission[0]['meta_field_name'] == 'commission' ? $commission[0]['meta_field_value'] : 0;
        return json_encode($data);
    }
}
