<?php

namespace App\Http\Controllers;

use App\Http\Resources\CategoryCollection as CategoryCollection;
use App\Http\Resources\ServiceItemsCollection as ServiceItemsCollection;
use App\Jobs\CustomerInvitationEmail;
use App\ServiceItemPricing;
use Illuminate\Http\Request;

use Tymon\JWTAuth\Facades\JWTAuth;

class APIControllerV1 extends Controller
{
    public function __construct(){
        $this->page_title = \Request::route()->getName();
        \App\System::AccessLogWrite();
    }

    /********************************************
    ## getAppLogin
     *********************************************/
    public function getAppLogin(Request $request){

        $now=date('Y-m-d H:i:s');

        try{
            $now = date('Y-m-d H:i:s');
            $validator = \Validator::make($request->all(), [
                'email' => 'required|email',
                'password' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    "status"=>'errors',
                    "errorMessage"=> $validator->errors(),
                    "serverReferenceCode"=>$now
                ], 501);
            }

            $credentials = [
                'email' => $request->input('email'),
                'password'=>$request->input('password'),
                'status'=> "active"
            ];
            $token = null;

            if(!$token = JWTAuth::attempt($credentials))
                throw new \Exception('Email or password incorrect');

            if (\Auth::user()->user_type != "customer") {
                $request->request->add(['token' => $token]);
                $response =$this->getForceLogout($request);

                #TokenCheck
                $error_response = [
                    "data"=>\Auth::user()->user_type,
                    'status'=>'errors',
                    "errorMessage"=> "You are not authorized. Contact with Administrator",
                    "serverReferenceCode"=>$now
                ];

                return \Response::json($error_response, 501, [], JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);

            }

            \App\User::LogInStatusUpdate("login");
            \App\System::AuthLogWrite('login');

            #TokenCheck
            $response = [
                "token" => $token,
                'status'=>'success',
                "successMessage"=> "Token has been created successfully",
                "serverReferenceCode"=>$now
            ];

            return \Response::json($response, 200, [], JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);

        }catch(\Exception $e){

            $error_response = [
                 "status"=>'errors',
                 "errorMessage"=> $e->getMessage(),
                 "serverReferenceCode"=>$now
            ];
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();

            \App\System::ErrorLogWrite($message);
            \App\System::APILogWrite(\Request::all(),$error_response);
            return \Response::json($error_response, 501, [], JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
        }


    }

    /********************************************
    ## getForceLogout
     *********************************************/
    public function getForceLogout(Request $request){
        $result = JWTAuth::invalidate(JWTAuth::getToken());
        return $result;
    }

    /********************************************
    ## getAppLogout
     *********************************************/
    public function getAppLogout(Request $request){

        $now=date('Y-m-d H:i:s');

        try{

            $result = JWTAuth::invalidate(JWTAuth::getToken());
            \App\User::LogInStatusUpdate("logout");
            \App\System::AuthLogWrite('logout');

            #TokenCheck
            $response = [
                'status'=>'success',
                "successMessage"=> "Customer Loggedout successfully",
                "serverReferenceCode"=>$now
            ];

            return \Response::json($response, 200, [], JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);

        }catch(\Exception $e){

            $error_response = [
                "status"=>'errors',
                "errorMessage"=> $e->getMessage(),
                "serverReferenceCode"=>$now
            ];
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();

            \App\System::ErrorLogWrite($message);
            \App\System::APILogWrite(\Request::all(),$error_response);
            return \Response::json($error_response, 501, [], JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
        }
    }

    /********************************************
    ## getAppCheck
     *********************************************/
    public function getAppCheck(Request $request){
        $refreshed = JWTAuth::refresh(JWTAuth::getToken());
        //var_dump(\Auth::user()->name);
        return \Auth::user()->name;
    }

    /********************************************
    ## getAppTokenRefresh
     *********************************************/
    public function getAppTokenRefresh(Request $request){

        $now=date('Y-m-d H:i:s');

        try{

            $refreshed = JWTAuth::refresh(JWTAuth::getToken());
            $user = JWTAuth::setToken($refreshed)->toUser();
            #TokenCheck
            $response = [
                "token" => $refreshed,
                'status'=>'success',
                "successMessage"=> "Token refresh  successfully",
                "serverReferenceCode"=>$now
            ];

            return \Response::json($response, 200, [], JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);

        }catch(\Exception $e){

            $error_response = [
                "status"=>'errors',
                "errorMessage"=> $e->getMessage(),
                "serverReferenceCode"=>$now
            ];
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();

            \App\System::ErrorLogWrite($message);
            \App\System::APILogWrite(\Request::all(),$error_response);
            return \Response::json($error_response, 501, [], JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
        }

    }

    /********************************************
    ## getAppRegistration
     *********************************************/
    public function getAppRegistration(Request $request){

        $now=date('Y-m-d H:i:s');

        try{
            $now = date('Y-m-d H:i:s');
            $validator = \Validator::make($request->all(), [
               'name' => 'Required|max:20',
                'address' => 'Required|max:255',
                'email' => 'Required|email|unique:users',
                'password' => 'required|min:4',
                'signature_image'=>['required','regex:(data:image\/[^;]+;base64[^"]+)']
            ]);

            if ($validator->fails()) {
                return response()->json([
                    "status"=>'errors',
                    "errorMessage"=> $validator->errors(),
                    "serverReferenceCode"=>$now
                ], 501);
            }


            #SignatureUpload
            $direcotory='images/customer-signature/';
            if (!file_exists('images/customer-signature/')) {
                mkdir('images/customer-signature/', 0777, true);
            }
            $file_path  = $direcotory.\Str::slug($request->input('name'),'-').'-'.time().'-'.rand(1111111,9999999).'.png';
            $visitor_signature = $request->input('signature_image');
            $visitor_signature = preg_replace('#^data:image/\w+;base64,#i', '', $visitor_signature);
            $visitor_signature = base64_decode($visitor_signature);
            file_put_contents($file_path,$visitor_signature);

            \DB::beginTransaction();
            $customer_data = [
                'customer_name' =>$request->input('name'),
                'customer_slug' =>\Str::slug($request->input('name'),'.'),
                'customer_email' =>$request->input('email'),
                'customer_address' =>$request->input('address'),
                'customer_email_verify' =>0,
                'customer_digital_signature' =>$file_path,
            ];

            $customer_insert = \App\Customer::firstOrCreate(
                [
                    'customer_email' => $customer_data['customer_email'],
                ],
                $customer_data
            );

            \App\System::EventLogWrite('customer,users',json_encode($customer_insert));


            #UserAccountCreate
            $remember_token = \App\System::RandomStringNum(16);
            $user_insert_data=array(
                'name' => $request->input('name'),
                'name_slug' => \Str::slug($request->input('name'),'.'),
                'user_type' => 'customer',
                'user_role' => 'customer',
                'email' => $request->input('email'),
                'user_profile_image' => '',
                'login_status' => 0,
                'company_id' => $customer_insert->id,
                'remember_token' =>$remember_token,
                'status' =>'active',
                'created_at' => $now,
                'updated_at' => $now,
            );
            $user_insert = \App\User::firstOrCreate(
                [
                    'email' => $user_insert_data['email'],
                ],
                $user_insert_data
            );

            $user_insert_pass = \App\User::where('id',$user_insert->id)->update(['password' => \Hash::make($request->input('password'))]);

            #eventLog
            \App\System::EventLogWrite('insert,users',json_encode($user_insert));
            dispatch(new CustomerInvitationEmail($user_insert));
            \DB::commit();


            $credentials = [
                'email' => $request->input('email'),
                'password'=>$request->input('password'),
                'status'=> "active"
            ];
            $token = null;

            if(!$token = JWTAuth::attempt($credentials))
                throw new \Exception('Email or password incorrect');


            \App\User::LogInStatusUpdate("login");
            \App\System::AuthLogWrite('login');

            #TokenCheck
            $response = [
                "token" => $token,
                'status'=>'success',
                "successMessage"=> "Registration completed and Token has been created successfully",
                "serverReferenceCode"=>$now
            ];

            return \Response::json($response, 200, [], JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);

        }catch(\Exception $e){

            $error_response = [
                "status"=>'errors',
                "errorMessage"=> $e->getMessage(),
                "serverReferenceCode"=>$now
            ];
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \DB::rollback();
            \App\System::ErrorLogWrite($message);
            \App\System::APILogWrite(\Request::all(),$error_response);
            return \Response::json($error_response, 501, [], JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
        }


    }

    /********************************************
    ## getAllCategory
     *********************************************/
    public function getAllCategory(Request $request){

        $now=date('Y-m-d H:i:s');

        try{
            $list = \App\ServicesCategory::all();
            $all_category = CategoryCollection::collection($list);

            $response = [
                "categoryList" => $all_category,
                'status'=>'success',
                "successMessage"=> "All category list",
                "serverReferenceCode"=>$now
            ];

            return \Response::json($response, 200, [], JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);

        }catch(\Exception $e){

            $error_response = [
                "status"=>'errors',
                "errorMessage"=> $e->getMessage(),
                "serverReferenceCode"=>$now
            ];
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();

            \App\System::ErrorLogWrite($message);
            \App\System::APILogWrite(\Request::all(),$error_response);
            return \Response::json($error_response, 501, [], JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
        }

    }

    /********************************************
    ## getServiceByCategory
     *********************************************/
    public function getServiceByCategory(Request $request,$category_id){

        $now=date('Y-m-d H:i:s');

        try{

            $service_item =
                \App\ServiceItem::where('service_items.service_item_category_id',$category_id)
                ->join('services_categories','service_items.service_item_category_id','=','services_categories.id')
                ->join('service_partner_info','service_items.service_item_partner_id','=','service_partner_info.id')
                ->join('service_item_pricing','service_items.id','=','service_item_pricing.service_pricing_item_id')
                ->leftJoin('service_item_frequencies', 'service_items.id', '=', 'service_item_frequencies.service_item_id')
                ->join('service_config','service_item_pricing.service_config_id','=','service_config.id')
                ->leftJoin('images', function($join)
                {
                    $join->on('service_items.id', '=', 'images.imageable_id');
                    $join->where('images.imageable_type', '=', 'App\ServiceItem');
                })
                ->groupBy('service_items.id')
                ->get();
            $all_service_item = ServiceItemsCollection::collection($service_item);

            $response = [
                "serviceItemList" => $all_service_item,
                'status'=>'success',
                "successMessage"=> "All Service Item list",
                "serverReferenceCode"=>$now
            ];

            return \Response::json($response, 200, [], JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);

        }catch(\Exception $e){

            $error_response = [
                "status"=>'errors',
                "errorMessage"=> $e->getMessage(),
                "serverReferenceCode"=>$now
            ];
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();

            \App\System::ErrorLogWrite($message);
            \App\System::APILogWrite(\Request::all(),$error_response);
            return \Response::json($error_response, 501, [], JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
        }

    }

    //get service details
    public function viewServiceDtlsById(Request $request, $service_item_id){
        $now=date('Y-m-d H:i:s');
        try {
            $serviceData = ServiceItemPricing::where(['service_item_pricing.service_pricing_item_id' => $service_item_id])
                ->leftJoin('services_categories', 'service_item_pricing.service_pricing_category_id', '=', 'services_categories.id')
                ->leftJoin('service_partner_info', 'service_item_pricing.service_pricing_partner_id', '=', 'service_partner_info.id')
                ->leftJoin('service_config', 'service_item_pricing.service_config_id', '=', 'service_config.id')
                ->leftJoin('service_item_frequencies', 'service_item_pricing.id', '=', 'service_item_frequencies.service_item_id')
                ->leftJoin('service_frequencies', 'service_frequencies.id', '=', 'service_item_frequencies.service_frequency_id')
                ->leftJoin('images as partner_images', function($join)
                {
                    $join->on('service_item_pricing.service_pricing_partner_id', '=', 'partner_images.imageable_id');
                    $join->where('partner_images.imageable_type', '=', 'App\ServicePartner');
                })
                ->select(['service_item_pricing.*', 'service_config.service_pricing_title', 'service_config.service_pricing_title_est',
                    'service_config.service_pricing_title_rus', 'service_config.service_pricing_description',
                    'service_config.service_pricing_description_est', 'service_config.service_pricing_description_rus',
                    'service_config.service_pricing_currency_unit', 'service_config.service_pricing_unit_cost_amount',
                    'services_categories.category_name', 'services_categories.category_name_est', 'services_categories.category_name_rus',
                    'service_frequencies.frequency_name', 'service_frequencies.frequency_name_est',
                    'service_frequencies.frequency_name_rus', 'service_partner_info.partner_name',
                    'service_partner_info.partner_name_est', 'service_partner_info.partner_name_rus',
                    'partner_images.image_name'
                ])
                ->get()
                ->toArray();
            $reArrangeArr = array();
            foreach ($serviceData as $key => $data){
                $reArrangeArr['service_pricing_title'] = $data['service_pricing_title'];
                $reArrangeArr['service_pricing_title_est'] = $data['service_pricing_title_est'];
                $reArrangeArr['service_pricing_title_rus'] = $data['service_pricing_title_rus'];
                $reArrangeArr['service_pricing_description'] = $data['service_pricing_description'];
                $reArrangeArr['service_pricing_description_est'] = $data['service_pricing_description_est'];
                $reArrangeArr['service_pricing_description_rus'] = $data['service_pricing_description_rus'];
                $reArrangeArr['partner_name'] = $data['partner_name'];
                $reArrangeArr['partner_name_est'] = $data['partner_name_est'];
                $reArrangeArr['partner_name_rus'] = $data['partner_name_rus'];
                $reArrangeArr['partner_logo'] = url('/service_images/partner_images').'/' . $data['image_name'];
                $reArrangeArr['service_pricing_currency_unit'] = $data['service_pricing_currency_unit'];
                $reArrangeArr['service_pricing_unit_cost_amount'] = $data['service_pricing_unit_cost_amount'];
                $reArrangeArr['service_frequency'] = $data['frequency_name'];
                $reArrangeArr['service_frequency_est'] = $data['frequency_name_est'];
                $reArrangeArr['service_frequency_rus'] = $data['frequency_name_rus'];
                $reArrangeArr['category_name'] = $data['category_name'];
                $reArrangeArr['category_name_est'] = $data['category_name_est'];
                $reArrangeArr['category_name_rus'] = $data['category_name_rus'];
            }
            $reArrangeArr['service_images'] = \App\Image::where(['imageable_id' => $service_item_id, 'imageable_type' => 'App\ServiceItem'])->get()->toArray();
            \App\System::EventLogWrite('retrieve,services_item_pricing', json_encode($service_item_id));

            $response = [
                "serviceDetails" => $reArrangeArr,
                'status'=>'success',
                "successMessage"=> "Service Details",
                "serverReferenceCode"=>$now
            ];

            return \Response::json($response, 200, [], JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
        } catch (\Exception $e) {
            $error_response = [
                "status"=>'errors',
                "errorMessage"=> $e->getMessage(),
                "serverReferenceCode"=>$now
            ];
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();

            \App\System::ErrorLogWrite($message);
            \App\System::APILogWrite(\Request::all(),$error_response);
            return \Response::json($error_response, 501, [], JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
        }
    }

    //SAVE ORDER
    public function saveOrderData(Request $request){
//        print_r($request->all()); die();
        $now=date('Y-m-d H:i:s');
//        print_r(\Auth::user()); die();
        try{
            $validator = \Validator::make($request->all(), [
                'service_item_id' => 'Required',
//                'order_date' => 'Required',
//                'order_time' => 'Required',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    "status"=>'errors',
                    "errorMessage"=> $validator->errors(),
                    "serverReferenceCode"=>$now
                ], 501);
            }

            $order_insert_obj = \App\ServiceOrder::saveOrder($request);

            if(isset($order_insert_obj->id)){
                $ordered_service_details = \App\ServiceOrder::serviceDetailsData($request->service_item_id);
                $response = [
                    "order_id" => $order_insert_obj->id,
                    'service_details' => $ordered_service_details,
                    'status'=>'success',
                    "successMessage"=> "Order placed successfully",
                    "serverReferenceCode"=>$now
                ];

                return \Response::json($response, 200, [], JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
            } else {
                $error_response = [
                    "status"=>'errors',
                    "errorMessage"=> 'Order not processed.',
                    "serverReferenceCode"=>$now
                ];
                return \Response::json($error_response, 501, [], JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
            }
        } catch (\Exception $e) {
            $error_response = [
                "status"=>'errors',
                "errorMessage"=> $e->getMessage(),
                "serverReferenceCode"=>$now
            ];
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();

            \App\System::ErrorLogWrite($message);
            \App\System::APILogWrite(\Request::all(),$error_response);
            return \Response::json($error_response, 501, [], JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
        }
    }

    /********************************************
    ## getAllCategoryServices
     *********************************************/
    public function getAllCategoryServices(Request $request){

        $now=date('Y-m-d H:i:s');

        try{

            /*$service_item =
                \App\ServiceItem::join('services_categories','service_items.service_item_category_id','=','services_categories.id')
                    ->join('service_partner_info','service_items.service_item_partner_id','=','service_partner_info.id')
                    ->join('service_item_pricing','service_items.id','=','service_item_pricing.service_pricing_item_id')
                    ->leftJoin('service_item_frequencies', 'service_items.id', '=', 'service_item_frequencies.service_item_id')
                    ->join('service_config','service_item_pricing.service_config_id','=','service_config.id')
                    ->leftJoin('images', function($join)
                    {
                        $join->on('service_items.id', '=', 'images.imageable_id');
                        $join->where('images.imageable_type', '=', 'App\ServiceItem');
                    })
                    ->groupBy('service_items.id')
                    ->get();

            $all_service_item = ServiceItemsCollection::collection($service_item);*/

            $service_item =
                \App\ServiceItem::join('services_categories','service_items.service_item_category_id','=','services_categories.id')
                    ->join('service_partner_info','service_items.service_item_partner_id','=','service_partner_info.id')
                    ->join('service_item_pricing','service_items.id','=','service_item_pricing.service_pricing_item_id')
                    ->leftJoin('service_item_frequencies', 'service_items.id', '=', 'service_item_frequencies.service_item_id')
                    ->join('service_config','service_item_pricing.service_config_id','=','service_config.id')
                    ->leftJoin('images', function($join)
                    {
                        $join->on('service_items.id', '=', 'images.imageable_id');
                        $join->where('images.imageable_type', '=', 'App\ServiceItem');
                    })
                    ->leftJoin('images as category_images', function($join)
                    {
                        $join->on('service_items.service_item_category_id', '=', 'category_images.imageable_id');
                        $join->where('category_images.imageable_type', '=', 'App\ServicesCategory');
                    })
                    ->leftJoin('images as partner_images', function($join)
                    {
                        $join->on('service_items.service_item_partner_id', '=', 'partner_images.imageable_id');
                        $join->where('partner_images.imageable_type', '=', 'App\ServicePartner');
                    })
                    ->select('services_categories.*','service_partner_info.*','service_items.*','service_item_pricing.*','service_item_frequencies.*','service_config.*','images.image_name as item_image_url','category_images.image_name as category_image_url','partner_images.image_name as partner_image_url')
                    ->groupBy('service_items.id')
                    ->get();

            $grouped = collect($service_item)->groupBy('category_name');
            $all_category = array();
            foreach ($grouped as $key => $group_item) {
                $temp = array();
                $temp['type'] = 'category';
                $temp['id'] = isset($group_item[0]->service_item_category_id) ? $group_item[0]->service_item_category_id : '';
                $temp['attributes']['name'] = isset($group_item[0]->category_name) ? $group_item[0]->category_name : '';
                $temp['image_link']['self'] = isset($group_item[0]->category_image_url) && !empty($group_item[0]->category_image_url) ? url('/service_images/category_images') . '/' . $group_item[0]->category_image_url : '';
                $temp['relationships'][] = ServiceItemsCollection::collection($group_item);
                $all_category = $temp;
            }

                $response = [
                "serviceItemList" => $all_category,
                'status'=>'success',
                "successMessage"=> "All category Service Item list",
                "serverReferenceCode"=>$now
            ];

            return \Response::json($response, 200, [], JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);

        }catch(\Exception $e){

            $error_response = [
                "status"=>'errors',
                "errorMessage"=> $e->getMessage(),
                "serverReferenceCode"=>$now
            ];
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();

            \App\System::ErrorLogWrite($message);
            \App\System::APILogWrite(\Request::all(),$error_response);
            return \Response::json($error_response, 501, [], JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
        }

    }

    /********************************************
    ## getPasswordReset
     *********************************************/
    public function getPasswordReset(Request $request)
    {
        $now = date('Y-m-d H:i:s');
        try{

            $rules = array(
                'new_password' => 'required|min:4',
                'current_password' => 'required',
            );

            $validator = \Validator::make(\Request::all(), $rules);

            if ($validator->fails()) {
                return response()->json([
                    "status"=>'errors',
                    "errorMessage"=> $validator->errors(),
                    "serverReferenceCode"=>$now
                ], 501);
            }

            $new_password = $request->input('new_password');
            $confirm_password = $request->input('confirm_password');

            if ($new_password != $request->input('current_password')) {
                if (\Hash::check($request->input('current_password'),
                    \Auth::user()->password)) {
                    $update_password=array(
                        'password' => bcrypt($request->input('new_password')),
                        'updated_at' => $now
                    );

                        \DB::table('users')
                            ->where('id', \Auth::user()->id)
                            ->update($update_password);
                        #eventLog
                        \App\System::EventLogWrite('update,customer-password-change',json_encode($update_password));

                    $credentials = [
                        'email' =>\Auth::user()->email,
                        'password'=>$request->input('new_password'),
                        'status'=> "active"
                    ];
                    $token = null;


                    if(!$token = JWTAuth::attempt($credentials))
                        throw new \Exception('Email or password incorrect');


                    \App\User::LogInStatusUpdate("login");
                    \App\System::AuthLogWrite('login');

                    #TokenCheck
                    $response = [
                        "token" => $token,
                        'status'=>'success',
                        "successMessage"=> "Password has been reset and Token has been updated successfully",
                        "serverReferenceCode"=>$now
                    ];

                    return \Response::json($response, 200, [], JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);

                }else {
                    throw new \Exception("Current Password Doesn't Match !");
                }
            } else {
                throw new \Exception("New and current password are same,try new password combination!");
            }

        }catch(\Exception $e){

            $error_response = [
                "status"=>'errors',
                "errorMessage"=> $e->getMessage(),
                "serverReferenceCode"=>$now
            ];
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            \App\System::APILogWrite(\Request::all(),$error_response);
            return \Response::json($error_response, 501, [], JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
        }
    }

}
