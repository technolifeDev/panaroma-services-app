<?php

namespace App\Http\Controllers;

use App\RoleUser;
use App\Role;
use App\ServiceOrderItem;
use App\Traits\HasRoleAndPermission;
use App\Traits\NotificationSending;
use Illuminate\Http\Request;
use Validator;
use Session;
use \App\System;
use Response;
use App\Repositories\Interfaces\OrderRepositoryInterface;
use Notification;
use App\Notifications\AccountApproved;
use Illuminate\Support\Facades\Config;

class OrderController extends Controller
{
    private $orderRepository;
    use HasRoleAndPermission;
    use NotificationSending;

    public function __construct(Request $request, OrderRepositoryInterface $orderRepository){
        $this->orderRepository = $orderRepository;
        $this->page_title = $request->route()->getName();
        $description = \Request::route()->getAction();
        $this->page_desc = isset($description['desc']) ? $description['desc'] : $this->page_title;
        \App\System::AccessLogWrite();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('order.index');
    }


    //get order json data
    public function getOrderJsonList(Request $request){
        $json_data = array();
        $hasEditPermission = $this->hasPermission('edit');
        $hasViewPermission = $this->hasPermission('view');
        $hasDelPermission = $this->hasPermission('delete');
        $totalData = \App\ServiceOrder::count();
        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $orderData = array();
        if (empty($request->input('search.value'))) {
            $orderData = $this->orderRepository->getOrderListForDataTable($start, $limit);
        } else {
            $search = $request->input('search.value');
            $orderData = $this->orderRepository->getOrderListForDataTableSearch($start, $limit, $search);
            $totalFiltered = sizeof($orderData);
        }

        $data = $this->orderRepository->resizeOrderArrayForJson($orderData, $hasViewPermission);

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        return json_encode($json_data);
    }


    //partner order list
    public function partnerOrderindex()
    {
        //
        return view('order.partner-order-index');
    }


    //get order json data
    public function getPartnerOrderJsonList(Request $request){
        $my_partner_id = \Auth::user()->company_id;
        $json_data = array();
        $hasEditPermission = $this->hasPermission('edit');
        $hasViewPermission = $this->hasPermission('view');
        $hasDelPermission = $this->hasPermission('delete');

        $totalData = \App\ServiceOrder::where('service_orders.service_order_partner_id', '=', $my_partner_id)
            ->count();
        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $orderData = array();
        if (empty($request->input('search.value'))) {
            $orderData = $this->orderRepository->getOrderListByPartnerIdForDataTable($start, $limit, $my_partner_id);
        } else {
            $search = $request->input('search.value');
            $orderData = $this->orderRepository->getOrderListByPartnerIdForDataTableSearch($start, $limit, $search, $my_partner_id);
            $totalFiltered = sizeof($orderData);
        }

        $data = $this->orderRepository->resizeOrderArrayForJson($orderData, $hasViewPermission);

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        return json_encode($json_data);
    }

    //order details
    public function serviceOrderDetails($order_id){
        $data['orderDetails'] = $this->orderRepository->getOrderDetailsByOrderId($order_id);
        $label_bt = array();
        foreach($data['orderDetails'] as $orderDtl){
            if(isset($orderDtl['order_items_status'])){
                if($orderDtl['order_items_status'] == 'accepted'){
                    $label_bt[$orderDtl['id']] = 'label-primary';
                } else if($orderDtl['order_items_status'] == 'cancel'){
                    $label_bt[$orderDtl['id']] = 'label-danger';
                } else if($orderDtl['order_items_status'] == 'rejected'){
                    $label_bt[$orderDtl['id']] = 'label-danger';
                } else if($orderDtl['order_items_status'] == 'processed'){
                    $label_bt[$orderDtl['id']] = 'label-info';
                } else if($orderDtl['order_items_status'] == 'completed'){
                    $label_bt[$orderDtl['id']] = 'label-success';
                } else {
                    $label_bt[$orderDtl['id']] = 'label-warning';
                }
            }
        }
        $data['order_label'] = $label_bt;
        return view('order.order-details', $data);
    }

    //pdf
    public function downloadPDF($order_id) {
        $orderDetails = $this->orderRepository->getOrderDetailsByOrderId($order_id);
        $pdf = \PDF::loadView('order.pdf.order-details-pdf', compact('orderDetails'));
        $order_no = str_pad($orderDetails[0]['service_orders_id'], 6, "0", STR_PAD_LEFT);
//        return view('order.pdf.order-details-pdf', $data);
        return $pdf->download($order_no . '.pdf');
    }


    //order status
    public function updateOrderStatus(){
        try {
            $order_id = \Request::input('order_id');
            $order_data = [
                'service_order_status' => \Request::input('order_status'),
            ];
            $order_update = \App\ServiceOrder::updateOrCreate(
                [
                    'id' => $order_id,
                ],
                $order_data
            );

            \App\ServiceOrder::orderStatusMail($order_id, $order_update);
            $notify_data = array(
                'order_id' => $order_update->id,
                'order_no' => str_pad($order_update->id, 6, "0", STR_PAD_LEFT),
                'message' => 'Order No # ' . str_pad($order_update->id, 6, "0", STR_PAD_LEFT) . ' has been ' . $order_update->service_order_status
            );
            \App\ServiceOrder::saveNotification($order_update, $notify_data);
            $this->PusherDataSend('order-status:' . $order_update->created_by, $notify_data);

            $auth_token_arr = \App\User::where('id', '=', $order_update->created_by)->get()->toArray();
            if(isset($auth_token_arr[0]['personal_code']) && $auth_token_arr[0]['personal_code']){
                $this->_sendNotification($auth_token_arr[0]['personal_code'], $notify_data['message']);
            }
//
            $return_arr = array('status' => 'info', 'msg' => 'Order status updated');

        } catch (\Exception $e) {
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            $return_arr = array('status' => 'error', 'msg' => $message);
//            return redirect('my/profile')->with('errormessage',"Something is wrong!");
        }
        return json_encode($return_arr);
    }



    //
    public function updateOrderItemStatus(Request $request){
        $order_id = \Request::input('order_id');
        $order_status = \Request::input('order_item_status');

        $order_data = [
            'order_items_status' => \Request::input('order_item_status'),
        ];
        $order_item_update = \App\ServiceOrderItem::updateOrCreate(
            [
                'id' => $order_id,
            ],
            $order_data
        );

        $order_item_name = '';//$this->orderRepository->getOrderItemName($order_item_update->orders_items_type_value);
        //update service_update
        $service_order_id = $order_item_update->service_orders_id;
        $check_is_all_complete = array();
        if($order_status == 'completed'){
            $check_is_all_complete = \App\ServiceOrderItem::where('service_orders_id', '=', $service_order_id)
                ->where(function ($query){
                    return $query->where('order_items_status', '=', 'pending')
                        ->orWhere('order_items_status', '=', 'processed')
                        ->orWhere('order_items_status', '=', 'accepted');
                })
                ->get()
                ->toArray()
            ;
        }

        $order_update = \App\ServiceOrder::find($service_order_id)->first();
        //if(empty($check_is_all_complete)){
            $service_order_status = 'pending';
            if($order_status == 'processed'){
                $service_order_status = 'processed';
            } else if($order_status == 'completed') {
                $service_order_status = 'completed';
            }

            $service_order_data = [
                'service_order_deliver_status' => $service_order_status,
            ];
            $order_update = \App\ServiceOrder::updateOrCreate(
                [
                    'id' => $service_order_id,
                ],
                $service_order_data
            );

            $notify_data = array(
                'order_id' => $order_update->id,
                'order_no' => str_pad($order_update->id, 6, "0", STR_PAD_LEFT),
                'message' => 'Order No # ' . str_pad($order_update->id, 6, "0", STR_PAD_LEFT) . ' has been ' . $order_update->service_order_status
            );

            \App\ServiceOrder::saveNotification($order_update, $notify_data);
            $this->PusherDataSend('order-status:' . $order_update->created_by, $notify_data);
        //}

        $notify_data = array(
            'order_id' => $order_update->id,
            'order_no' => str_pad($order_update->id, 6, "0", STR_PAD_LEFT),
            'message' => 'Your Order #' . str_pad($order_update->id, 6, "0", STR_PAD_LEFT) . ' has been ' . $order_item_update->order_items_status
        );
        \App\ServiceOrder::saveNotification($order_update, $notify_data);
        $this->PusherDataSend('order-status:' . $order_update->created_by, $notify_data);

        $return_arr = array('status' => 'info', 'msg' => 'Service Item '. $order_status, 'order_status' => $order_status);
        return json_encode($return_arr);
    }


    //order details html view
    public function orderDetailsHtmlView($order_id){
        $data['orderDetails'] = $this->orderRepository->getOrderDetailsByOrderId($order_id);
        $label_bt = array();
        foreach($data['orderDetails'] as $orderDtl){
            if(isset($orderDtl['order_items_status'])){
                if($orderDtl['order_items_status'] == 'accepted'){
                    $label_bt[$orderDtl['id']] = 'label-primary';
                } else if($orderDtl['order_items_status'] == 'cancel'){
                    $label_bt[$orderDtl['id']] = 'label-danger';
                } else if($orderDtl['order_items_status'] == 'rejected'){
                    $label_bt[$orderDtl['id']] = 'label-danger';
                } else if($orderDtl['order_items_status'] == 'processed'){
                    $label_bt[$orderDtl['id']] = 'label-info';
                } else if($orderDtl['order_items_status'] == 'completed'){
                    $label_bt[$orderDtl['id']] = 'label-success';
                } else {
                    $label_bt[$orderDtl['id']] = 'label-warning';
                }
            }
        }
        $data['order_label'] = $label_bt;
        return view('order.order-details-html', $data);
    }

    //order item list
    public function orderChildList($order_id){
        $getOrderDetailsByOrderId = $this->orderRepository->getOrderDetailsByOrderId($order_id);
        return json_encode($getOrderDetailsByOrderId);
    }


    public function orderHtml(){
        return view('order-store-html');
    }


    private function _sendNotification($exponent_token, $message)
    {
        $payload = array(
            'to' => $exponent_token,
            'body' => $message,
        );

        $curl = curl_init();

       curl_setopt_array($curl, array(
            CURLOPT_URL => "https://exp.host/--/api/v2/push/send",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($payload),
            CURLOPT_HTTPHEADER => array(
                "Accept: application/json",
                "Accept-Encoding: gzip, deflate",
                "Content-Type: application/json",
                "cache-control: no-cache",
//                "host: exp.host"
            ),
        ));
        curl_exec ($curl);
    }

    public function getExpoToken(Request $request){
        \Cache::put('expo_token', \Request::input('expo_token'));
        $data['status'] = 'success';
        echo json_encode($data);
    }

    public function noExpoToken(){
//        $value = Config::get('exponent-push-notifications.expo_token');
//        $value = \Session::get('expo_token');
        $value = \Cache::get('expo_token');
        echo $value;
    }

}
