<?php

namespace App\Http\Controllers;

use App\Permission;
use App\PermissionRole;
use App\Role;
use App\Traits\HasRoleAndPermission;
use Illuminate\Http\Request;
use App\Http\Requests\StoreRoleRequest;
use Auth;
class RolesController extends Controller
{
    use HasRoleAndPermission;
    /**
     * Class constructor.
     * get current route name for page title.
     *
     */
    public function __construct(Request $request)
    {
        $this->page_title = $request->route()->getName();
        $description = \Request::route()->getAction();
        $this->page_desc = isset($description['desc']) ? $description['desc'] : $this->page_title;
        \App\System::AccessLogWrite();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data['page_title'] = $this->page_title;
        try {
            $roles = Role::all();
            $data['roles'] = $roles;
            return view('roles.index', $data);
        } catch(\Exception $e) {
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            return redirect()->back()->with('errormessage',"Something is wrong!");
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Roles listing: ajax
     */
    public function ajaxRolesListing() {
        $roles = Role::orderBy('created_at','desc')->get();

        $roles_list = array();
        $index = 1;

        foreach($roles as $key=>$role) {

            $roles_list[$key]['sno'] = $index;
            $roles_list[$key]['name'] = $role->name;
            $roles_list[$key]['slug'] = $role->slug;

            $roles_list[$key]['description'] = $role->description;
            $roles_list[$key]['actions'] = '';
            if($this->hasPermission('edit')) {
                $roles_list[$key]['actions'] .= "<a id= " . $role->id . " href='" . url('/admin/roles/edit', $role->id) . "' class='btn btn-xs tooltips btn-green' >
                                                        <i class='clip-pencil-3'></i></a>";
            }
            if($this->hasPermission('delete')) {
                $roles_list[$key]['actions'] .= " <a id= " . $role->id . " href='#' class='btn btn-xs tooltips btn-red delete-role' >
                <i class='clip-remove'></i></a>";
            }
            //    ."<a id='d_".$role->id."'". ' class="btn btn-xs btn-red tooltips role-delete" ><i class="clip-remove"></i></a>';

            $index++;
        }
        return json_encode(array('data'=>$roles_list));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRoleRequest $request)
    {
        //
        try{

            $roleObj = new Role;
            $roleObj->name = $request->name;
            $roleObj->slug = $request->slug;
            $roleObj->description = $request->description;
            $roleObj->save();

            return redirect()->back()->with('message','New Role created successfully.');

        }catch(\Exception $e) {
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            return redirect()->back()->with('errormessage',"Something is wrong!");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try{
            $data['page_title'] = $this->page_title;
            $role = Role::find($id)->toArray();
            $data['role'] = $role;

            return json_encode($data);

        }catch(\Exception $e) {
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            return redirect()->back()->with('errormessage',"Something is wrong!");
        }
    }

    public function edit_role($id)
    {
        try {

            $data['page_title'] = $this->page_title;
            $role = Role::where('id',$id)->with('permissions')->first();
            $data['role'] = $role;
            $permissions = Permission::all();
            $p_array = array();
            if(isset($role->permissions)) {
                foreach($role->permissions as $permis) {
                    $p_array[] = $permis->id;
                }
            }
            $data['permissions'] = $permissions;
            $data['role_permissions'] = $p_array;

            return view("roles.edit", $data);
        } catch(\Exception $e) {
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            return redirect()->back()->with('errormessage',"Something is wrong!");
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try{

            $role = Role::findOrFail($request->role_id);

            $role->name = $request->name;
            $role->slug = $request->slug;
            $role->description = $request->description;
            $role->save();

            $permissions = PermissionRole::where('role_id', $request->role_id)->delete();
            if(isset($request->permissions) ) {
                foreach($request->permissions as $permis) {
                    $permissionObj = new PermissionRole;
                    $permissionObj->permission_id =  $permis;
                    $permissionObj->role_id =  $request->role_id;
                    $permissionObj->save();
                }
            }

            return redirect()->back()->with('message',"Role is updated Successfully");
        } catch(\Exception $e) {
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            return redirect()->back()->with('errormessage',"Something is wrong!");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $role->delete();

        echo "Role deleted successfully";
    }
}
