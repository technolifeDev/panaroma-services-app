<?php

namespace App\Http\Controllers;

use App\Jobs\CustomerForgetPasswordMail;
use App\Jobs\CustomerInvitationEmail;
use App\ServiceConfig;
use App\ServiceOrder;
use App\ServicePartner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use \App\System;
use Response;
use Image;
use App\User;
use Illuminate\Support\Facades\Session;
use Tymon\JWTAuth\Facades\JWTAuth;


class CustomerController extends Controller
{
    public function __construct()
    {
        $this->page_title = \Request::route()->getName();
        $description = \Request::route()->getAction();
        $this->page_desc = isset($description['desc']) ?  $description['desc']:$this->page_title;
        \App\System::AccessLogWrite();

    }

    /********************************************
    ## CustomerIndex
     *********************************************/
    public function CustomerIndex()
    {
        if (\Auth::check()) {
            if (!empty(\Auth::user()->user_type) && \Auth::user()->user_type == "customer") {
                \App\User::LogInStatusUpdate("login");
                return redirect('/customer/category-items');
            }else {
                \App\User::LogInStatusUpdate("logout");
                \App\System::AuthLogWrite('logout');
                \Auth::logout();
            }

        }

        $data['page_title'] = $this->page_title;
        $data['page_desc'] = $this->page_desc;
        return view('customer.pages.customer-index',$data);
    }

    /********************************************
    ## CustomerLoginPage
     *********************************************/
    public function CustomerLoginPage()
    {
        if (\Auth::check()) {
            if (!empty(\Auth::user()->user_type) && \Auth::user()->user_type == "customer") {
                \App\User::LogInStatusUpdate("login");
                return redirect('/customer/category-items');
            }else {
                \App\User::LogInStatusUpdate("logout");
                \App\System::AuthLogWrite('logout');
                \Auth::logout();
            }

        }

        $data['page_title'] = $this->page_title;
        $data['page_desc'] = $this->page_desc;
        return view('customer.pages.customer-login',$data);

    }

    /********************************************
    ## CustomerAuthentication
     *********************************************/
    public function CustomerAuthentication(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $remember_me = $request->has('remember_me') ? true : false;
        $credentials = [
            'email' => $request->input('email'),
            'password'=>$request->input('password'),
            'status'=> "active"
        ];
        if (\Auth::attempt($credentials,$remember_me)) {

            \Session::put('email', \Auth::user()->email);
            \Session::put('last_login', Auth::user()->last_login);

            if(\Cache::get('expo_token')){
                $user = \App\User::find(\Auth::user()->id);
                $user->personal_code = \Cache::get('expo_token');
                $user->save();
            }

            if (!empty(\Auth::user()->user_type) && \Auth::user()->user_type == "customer") {

                $token = JWTAuth::attempt($credentials);

                \Session::put('auth_token',$token);

                \App\User::LogInStatusUpdate("login");
                \App\System::AuthLogWrite('login');
                return redirect('/customer/category-items');
            }else {
                \App\User::LogInStatusUpdate("logout");
                \App\System::AuthLogWrite('logout');
                \Auth::logout();
                return redirect('/customer/login')->with('errormessage',__('validation.not_authorized'));
            }

        } else {
            return redirect('/customer/login')->with('errormessage',__('validation.incorrect_combination'));
        }
    }


    /********************************************
    ## CustomerLaguageChange
     *********************************************/
    public function CustomerLaguageChange($lang)
    {
        try {
            if (in_array($lang, config('locale.languages'))) {
                Session::put('locale', $lang);
                \App::setLocale($lang);
                return redirect()->back();
            }
            return redirect()->back();
        } catch (\Exception $exception) {
            return redirect()->back();
        }
    }



    /********************************************
    ## CustomerRegistrationProcessView
     *********************************************/
    public function CustomerRegistrationProcessView()
    {

        $now = date('Y-m-d H:i:s');
        try {

            if (\Auth::check()) {
                if (!empty(\Auth::user()->user_type) && \Auth::user()->user_type == "customer") {
                    \App\User::LogInStatusUpdate("login");
                    return redirect('/customer/category-items');
                }else {
                    \App\User::LogInStatusUpdate("logout");
                    \App\System::AuthLogWrite('logout');
                    \Auth::logout();
                }

            }

            if (\Session::has('registration_data')) {
                $step_data = \Session::get('registration_data');

                if (isset($step_data['step'])&&$step_data['step'] > 3)
                    return redirect('/customer/registration/complete');

            }else {
                $step_data['step'] =1;
                \Session::put('registration_data',$step_data);
            }

            $data['page_title'] = $this->page_title;
            $data['page_desc'] = $this->page_desc;
            $data['step_data'] = $step_data;

            if($step_data['step']==1){
                return view('customer.pages.customer-registration', $data);

            }else if ($step_data['step']==2){
                return view('customer.pages.general-term', $data);

            }else if ($step_data['step']==3){
                return view('customer.pages.customer-signature', $data);
            }else{
                return redirect('/')->with('errormessage', "Something wrong in registration");
            }

        } catch (\Exception $e) {
            $message = "Message : " . $e->getMessage() . ", File : " . $e->getFile() . ", Line : " . $e->getLine();
            \App\System::ErrorLogWrite($message);
            return redirect('/registration')->with('errormessage', "Email verification failed.Try again later!!");

        }
    }

    /********************************************
    ## CustomerRegistrationProcessUpdate
     *********************************************/
    public function CustomerRegistrationProcessUpdate()
    {
//echo 's'; die();
        $now = date('Y-m-d H:i:s');
        try {
            if(\Session::has('registration_data')){
                $step_data = \Session::get('registration_data');
                if(count($step_data)>0){


                    if($step_data['step']==1){

                        $rule = [
                            'name' => 'Required|max:20',
                            'email' => 'Required|email|unique:users',
                            'address' => 'Required|max:255',
                            'password' => 'required|min:4',
                            'confirm_password' => 'required|same:password',
                        ];
                        $validation = \Validator::make(\Request::all(),$rule);

                        if ($validation->fails()) {
                            return redirect('/customer/registration')->withErrors($validation)->withInput();
                        }
                    }else if ($step_data['step']==3){
                        $rule = [
                            'signature_image' => 'Required',
                        ];
                        $validation = \Validator::make(\Request::all(),$rule);

                        if ($validation->fails()) {
                            return redirect('/customer/registration')->withErrors($validation)->withInput();
                        }
                    }

                    if($step_data['step']==1){
                        $step_data['step'] = $step_data['step']+1;
                        $step_data['email'] = \Request::input('email');
                        $step_data['name'] = \Request::input('name');
                        $step_data['address'] = \Request::input('address');
                        $step_data['password'] = \Request::input('password');
                        \Session::put('registration_data',$step_data);
                        return redirect('/customer/registration');
                    }elseif ($step_data['step']==2){
                        $step_data['step'] = $step_data['step']+1;
                        $step_data['agreement'] = \Request::input('agreement');
                        \Session::put('registration_data',$step_data);
                        return redirect('/customer/registration');
                    }elseif ($step_data['step']==3){

                        #SignatureUpload
                        $direcotory='images/customer-signature/';
                        if (!file_exists('images/customer-signature/')) {
                            mkdir('images/customer-signature/', 0777, true);
                        }
                        $file_path  = $direcotory.\Str::slug($step_data['name'],'-').'-'.time().'-'.rand(1111111,9999999).'.png';
                        $visitor_signature = \Request::input('signature_image');
                        $visitor_signature = preg_replace('#^data:image/\w+;base64,#i', '', $visitor_signature);
                        $visitor_signature = base64_decode($visitor_signature);
                        file_put_contents($file_path,$visitor_signature);

                        $step_data['step'] = $step_data['step']+1;
                        $step_data['signature'] = $file_path;
                        \Session::put('registration_data',$step_data);

                        return redirect('/customer/registration/complete');

                    }
                }else throw new \Exception('Email Session data missing');

            }else throw new \Exception('Something wrong in Session data!');

        } catch(\Exception $e) {
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            return redirect('/customer/registration')->with('errormessage',$e->getMessage());
        }
    }

    /********************************************
    ## CustomerRegistrationProcessComplete
     *********************************************/
    public function CustomerRegistrationProcessComplete()
    {

        $now = date('Y-m-d H:i:s');
        try {
            if(\Session::has('registration_data')){
                $step_data = \Session::get('registration_data');
                if(isset($step_data['email'])&&($step_data['step'])>3){


                    \DB::beginTransaction();
                    $customer_data = [
                        'customer_name' =>$step_data['name'],
                        'customer_slug' =>\Str::slug($step_data['name'],'.'),
                        'customer_email' =>$step_data['email'],
                        'customer_address' =>$step_data['address'],
                        'customer_email_verify' =>0,
                        'customer_digital_signature' =>$step_data['signature'],
                    ];

                    $customer_insert = \App\Customer::firstOrCreate(
                        [
                            'customer_email' => $customer_data['customer_email'],
                        ],
                        $customer_data
                    );
                    #UserAccountCreate
                    $token = \App\System::RandomStringNum(16);
                    $personal_code = '';
                    if(\Cache::get('expo_token')){
                        $personal_code = \Cache::get('expo_token');
                    }
                    $user_insert_data=array(
                        'name' => ucwords($step_data['name']),
                        'name_slug' => \Str::slug($step_data['name'], '.'),
                        'user_type' => 'customer',
                        'user_role' => 'customer',
                        'email' => $step_data['email'],
                        'user_profile_image' => '',
                        'login_status' => 0,
                        'company_id' => $customer_insert->id,
                        'remember_token' =>$token,
                        'status' =>'active',
                        'personal_code' => $personal_code,
                        'created_at' => $now,
                        'updated_at' => $now,
                    );

                    //$user_insert = \App\User::insertGetId($user_insert_data);

                    $user_insert = \App\User::firstOrCreate(
                        [
                            'email' => $user_insert_data['email'],
                        ],
                        $user_insert_data
                    );

                    $user_insert_pass = \App\User::where('id',$user_insert->id)->update(['password' => \Hash::make($step_data['password'])]);

                    #eventLog
                    \App\System::EventLogWrite('update,users',json_encode($user_insert));

                    $reset_url= url('customer/email-verify/id-'.$user_insert->id.'/verification').'?token='.$user_insert->remember_token;
                    //$reset_url= config('app.url').'/customer/email-verify/id-'.$user_insert->id.'/verification'.'?token='.$user_insert->remember_token;

                    dispatch(new CustomerInvitationEmail($user_insert,$reset_url));
                    \DB::commit();


                    $message='';
                    if($customer_insert->wasRecentlyCreated){
                        \App\System::EventLogWrite('insert,customer_info',json_encode($customer_insert));
                        $message='Customer Created Successfully';
                    }else $message='Customer  already created.';

                    $remember_me =  true;
                    $credentials = [
                        'email' => $step_data['email'],
                        'password'=>$step_data['password'],
                        'status'=> "active"
                    ];

                    if(\Auth::attempt($credentials,$remember_me)) {

                        $token = JWTAuth::attempt($credentials);

                        \Session::put('auth_token',$token);

                        \Session::forget('registration_data');
                        return redirect('/customer/category-items')->with('Registration successfully completed');

                    }
                    \Session::forget('registration_data');
                    return redirect('/customer/login')->with('message',"Registration successfully completed. Please log in to continue!!");

                }else throw new \Exception('Email Session data missing');
            }else throw new \Exception('Session data missing');


        } catch(\Exception $e) {
            \DB::rollback();
            \Session::forget('registration_data');
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            return redirect('/customer')->with('errormessage',"Email verification failed.Try again later!!");
        }
    }

    /********************************************
    ## EmailVerification
     *********************************************/
    public function EmailVerification($user_id)
    {

        $now = date('Y-m-d H:i:s');
        try {

            $remember_token=isset($_GET['token'])?$_GET['token']:'';
            $user_info= \App\User::where('id','=',$user_id)->first();

            if(!empty($remember_token)&&isset($user_info->id) && !empty($user_info->remember_token) && ($user_info->remember_token==$remember_token)){

                \DB::beginTransaction();

                #UserinfoUpdate
                $update_user=array(
                    'remember_token' => null,
                    'updated_at' => $now
                );

                $update_pass=\App\User::where('id', $user_id)->update($update_user);
                #eventLog
                \App\System::EventLogWrite('update,email verified',json_encode($update_user));


                #CustomerInfoUpdate
                $customer_data = [
                    'customer_email_verify' =>1,
                    'updated_at' => $now
                ];
                $update_customer =\App\Customer::where('id', $user_info->company_id)->update($customer_data);
                #eventLog
                \App\System::EventLogWrite('update.customer_info,email verified',json_encode($update_user));

                \DB::commit();

                return redirect('/customer/login')->with('message',"Email verification successfully completed!");

            }else return redirect('/customer/login')->with('errormessage',"Sorry invalid token!");

        } catch(\Exception $e) {

            \DB::rollback();
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            return redirect('/customer/login')->with('errormessage',"Email verification failed.Try again later!!");
        }
    }

    /********************************************
    ## CustomerCategoryItems
     *********************************************/
    public function CustomerCategoryItems()
    {
        try {
            $data['page_title'] = $this->page_title;
            $data['page_desc'] = $this->page_desc;

           // echo \Session::get('auth_token');
            $data['service_all_item'] = \App\ServiceItem::getServiceCatItemList();
            return view('customer.pages.category-items', $data);

        } catch (\Exception $e) {
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            return redirect('/customer/login')->with('errormessage',"Email verification failed.Try again later!!");
        }
    }

    /********************************************
    ## CustomerAuthentication
     *********************************************/
    public function CustomerLogout($email)
    {
        if (\Auth::check()) {
            $user_info = \App\User::where('email',\Auth::user()->email)->first();
            if (!empty($user_info) && ($email==$user_info->email)) {

                \App\User::LogInStatusUpdate("logout");
                \App\System::AuthLogWrite('logout');
                \Auth::logout();
                return \Redirect::to('/customer');
            } else {
                return \Redirect::to('/customer');
            }
        } else {
            return \Redirect::to('/customer')->with('errormessage',"Error logout");
        }
    }


    /********************************************
    ## forgetPasswordAuthPage
     *********************************************/

    public function forgetPasswordAuthPage()
    {
        if (\Auth::check()) {
            return redirect('/customer/login')->with('errormessage', 'Whoops, looks like something went wrong!.');
        } else {
            $data['page_title'] = $this->page_title;
            return view('customer.pages.customer-forgetpass',$data);
        }
    }

    /********************************************
    ## authForgotPasswordConfirm
     *********************************************/

    public function authForgotPasswordConfirm(Request $request)
    {
        $v = \Validator::make($request->all(), [
            'email' => 'required|email',
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v)->withInput();
        }
        $email = $request->input('email');
        $user_email= \App\User::where('email','=',$email)->where('user_type','customer')->where('status','active')->first();

        if (!isset($user_email->id)) {
            return redirect('/customer/forget/password')->with('errormessage',"Sorry email does not match!");
        }


        #UpdateRememberToken
        $token = \App\System::RandomStringNum(16);
        \App\User::where('id',$user_email->id)->update(['remember_token'=>$token]);

        #eventLog
        \App\System::EventLogWrite('update,remember_token',json_encode(['remember_token'=>$token]));


        $reset_url= url('customer/forget/password/'.$user_email->id.'/verify').'?token='.$token;

        dispatch(new CustomerForgetPasswordMail($user_email->id,$reset_url));

        return redirect('/customer/forget/password')->with('message',"Please check your mail !.");
    }

    /********************************************
    ## authSystemForgotPasswordVerification
     *********************************************/

    public function authSystemForgotPasswordVerification($user_id)
    {
        $remember_token=isset($_GET['token'])?$_GET['token']:'';
        $user_info= \App\User::where('id','=',$user_id)->first();

        if(!empty($remember_token)&&isset($user_info->id) && !empty($user_info->remember_token) && ($user_info->remember_token==$remember_token)){

            $data['user_info']=$user_info;
            $data['page_title'] = $this->page_title;
            return \View::make('customer.pages.customer-set-new-pass',$data);

        }else return redirect('/customer/forget/password')->with('errormessage',"Sorry invalid token!");

    }


    /********************************************
    ## authSystemNewPasswordPost
     *********************************************/

    public function authSystemNewPasswordPost(Request $request,$user_id)
    {

        try {

            $now = date('Y-m-d H:i:s');
            $validator = \Validator::make($request->all(), [
                'password' => 'required|min:4',
                'confirm_password' => 'required|same:password',
            ]);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }


            $update_password=array(
                'password' => bcrypt($request->input('password')),
                'remember_token' => null,
                'updated_at' => $now
            );

            $update_pass=\App\User::where('id', $user_id)->update($update_password);

            #eventLog
            \App\System::EventLogWrite('update,foreget-set-new-password',json_encode($update_password));

            if($update_pass) {
                return redirect('/customer/login')->with('message',"Password updated successfully !");
            }
        } catch(\Exception $e) {
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            return redirect('/customer/forget/password')->with('errormessage',"Password update failed  !");
        }

    }

    //order history
    public function orderHistory()
    {
        $data['page_title'] = $this->page_title;
        $data['page_desc'] = $this->page_desc;

        $orders = ServiceOrder::with('service_order_items')
            ->where('service_order_customer_id', Auth::user()->id)
            ->where('service_order_status','!=', 'cancel' )
            ->orderBy('id','desc')->paginate(10);
        foreach($orders as $item) {

            $partner = ServicePartner::where('id', $item->service_order_partner_id )->first();
            $item['partner_name'] = $partner->partner_name;

            $serviceDetails = ServiceConfig::where("id", $item->service_order_config_id)->first();
            $item['service_config_title'] = $serviceDetails->service_pricing_title;
            $item['service_config_title_est'] = $serviceDetails->service_pricing_title_est;
            $item['service_config_title_rus'] = $serviceDetails->service_pricing_title_rus;
            $item['order_id'] = $serviceDetails->id;
        }

        $data['orders'] = $orders;

        return view('customer.pages.order-history',$data);
    }


    public function customerOrderDetails($order_id){
        $customer_id = \Auth::user()->id;
        $data['getOrderDtls'] = \App\ServiceOrder::leftJoin('service_order_items', 'service_orders.id', '=', 'service_order_items.service_orders_id')
            ->leftJoin('service_partner_info', 'service_partner_info.id', '=', 'service_orders.service_order_partner_id')
            ->leftJoin('service_config', 'service_config.id', '=', 'service_orders.service_order_config_id')
            ->leftJoin('users', 'users.id', '=', 'service_orders.service_order_customer_id')
            ->leftJoin('services_categories', 'services_categories.id', '=', 'service_order_items.orders_items_category_id')
            ->leftJoin('service_item_frequencies', 'service_item_frequencies.service_item_id', '=', 'service_order_items.service_items_id')
            ->leftJoin('service_frequencies', 'service_item_frequencies.service_frequency_id', '=', 'service_frequencies.id')
            ->leftJoin('images', function($join)
            {
                $join->on('service_partner_info.id', '=', 'images.imageable_id');
                $join->where('images.imageable_type', '=', 'App\ServicePartner');
            })
            ->where('service_orders.id', '=', $order_id)
//            ->where('service_orders.service_order_customer_id', '=', $customer_id)
            ->whereNull('service_orders.deleted_at')
            ->select('service_order_items.*', 'service_partner_info.partner_name', 'users.email as customer_email', 'users.name',
                'service_partner_info.partner_name_est', 'service_partner_info.partner_name_rus',
                'service_partner_info.partner_email', 'service_partner_info.partner_address', 'service_partner_info.partner_address_est',
                'service_partner_info.partner_address_rus', 'images.image_name',
                'service_config.service_pricing_currency_unit', 'service_orders.service_order_customer_address',
                'service_orders.service_order_amount', 'service_orders.service_order_tax_amount',
                'service_orders.service_order_grand_total', 'service_orders.service_order_status',
                'services_categories.category_name', 'services_categories.category_name_est', 'services_categories.category_name_rus',
                'service_orders.created_at as order_date', 'service_frequencies.*',
                'service_config.service_pricing_title', 'service_config.service_pricing_title_est', 'service_config.service_pricing_title_rus',
                'service_config.service_pricing_description', 'service_config.service_pricing_description_rus', 'service_config.service_pricing_description_est'
            )
            ->get()
            ->toArray();
        return view('customer.pages.order-details', $data);
    }
}
