<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use Session;
use \App\System;
use App\Traits\HasRoleAndPermission;

class AttributeManageController extends Controller
{
    use HasRoleAndPermission;

    public function __construct(Request $request)
    {
        $this->page_title = $request->route()->getName();
        $description = \Request::route()->getAction();
        $this->page_desc = isset($description['desc']) ? $description['desc'] : $this->page_title;
        \App\System::AccessLogWrite();
    }

    /**********************************************************
    ## AttributeClassCreate
     *************************************************************/

    public function AttributeClassCreate()
    {
        $attribute_class_all_data=\App\AttributeClass::orderBy('updated_at','DESC')->paginate(10);
        $attribute_class_all_data->setPath(url('/admin/service/attribute-class/create'));
        $pagination = $attribute_class_all_data->render();
        $data['perPage'] = $attribute_class_all_data->perPage();
        $data['pagination'] = $pagination;
        $data['attribute_class_all_data'] = $attribute_class_all_data;
        $data['page_title'] = $this->page_title;

        //var_dump($this->detachRole('admin'));
       // var_dump(\Auth::user()->roles->pluck('name'));
        //var_dump($this->level());
        //var_dump($this->rolePermissions([2]));
        //var_dump($this->userPermissions(\Auth::user()->id));
       // var_dump($this->userinfo(3));
        //var_dump($this->hasRole());
        return view('attribute.class-attribute',$data);


    }

    /**********************************************************
    ## AttributeClassSave
     *************************************************************/

    public function AttributeClassSave()
    {
        $now=date('Y-m-d H:i:s');
        $user =\Auth::user()->id;
        $rule = ['attribute_class_name' => 'Required',];
        $v = \Validator::make(\Request::all(),$rule);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v)->withInput();
        }

        try{

            $attribute_name_slug  = \Str::slug(\Request::input('attribute_class_name'), '-');
            $attribute_class_data = [
                'attribute_class_name' =>\Request::input('attribute_class_name'),
                'attribute_class_slug' => $attribute_name_slug,
            ];

            $attribute_insert = \App\AttributeClass::firstOrCreate(
                [
                    'attribute_class_name' => $attribute_class_data['attribute_class_name'],
                ],
                $attribute_class_data
            );

            if($attribute_insert->wasRecentlyCreated){

                \App\System::EventLogWrite('insert,service_attribute_class',json_encode($attribute_class_data));
                return redirect()->back()->with('message','Attribute Class Created Successfully');

            }else return redirect()->back()->with('errormessage','Attribute Class  already created.');


        }catch(\Exception $e){

            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);

            return \Redirect::to('/admin/service/attribute-class/create')->with('errormessage',$e->getMessage());
        }


    }

    /**********************************************************
    ## AttributeClassEditPage
     *************************************************************/

    public function AttributeClassEditPage($class_id)
    {

        $class_info = \App\AttributeClass::where('id',$class_id)->first();

        if(!isset($class_info->id))
            return \Redirect::to('/admin/service/attribute-class/create')->with('errormessage',"Invalid Class ID");

        $attribute_class_all_data=\App\AttributeClass::orderBy('updated_at','DESC')->paginate(10);
        $attribute_class_all_data->setPath(url('/admin/service/attribute-class/edit',$class_info->id));
        $pagination = $attribute_class_all_data->render();
        $data['perPage'] = $attribute_class_all_data->perPage();
        $data['pagination'] = $pagination;
        $data['attribute_class_all_data'] = $attribute_class_all_data;
        $data['page_title'] = $this->page_title;
        $data['class_info'] = $class_info;
        return view('attribute.edit-class-attribute',$data);
    }

    /**********************************************************
    ## AttributeClassUpdate
     *************************************************************/

    public function AttributeClassUpdate($class_id)
    {
        $now=date('Y-m-d H:i:s');
        $user =\Auth::user()->id;
        $rule = ['attribute_class_name' => 'Required',];
        $v = \Validator::make(\Request::all(),$rule);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v)->withInput();
        }

        try{

            $attribute_name_slug  = \Str::slug(\Request::input('attribute_class_name'), '-');
            $attribute_class_data = [
                'attribute_class_name' =>\Request::input('attribute_class_name'),
                'attribute_class_slug' => $attribute_name_slug,
            ];

            $attribute_insert = \App\AttributeClass::updateOrCreate(
                [
                    'id' => $class_id,
                ],
                $attribute_class_data
            );


            \App\System::EventLogWrite('update,service_attribute_class',json_encode($attribute_insert));
            return redirect()->back()->with('message','Attribute Class updated Successfully');



        }catch(\Exception $e){

            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);

            return \Redirect::to('/admin/service/attribute-class/edit',$class_id)->with('errormessage',$e->getMessage());
        }


    }

    /********************************************
    # AttributeClassDelete
     *********************************************/
    public function AttributeClassDelete($class_id){
        \App\AttributeValue::where('attribute_values_class_id',$class_id)->delete();
        \App\AttributeType::where('attribute_types_class_id',$class_id)->delete();
        \App\AttributeClass::where('id',$class_id)->delete();
        \App\System::EventLogWrite('delete,service_attribute_class',json_encode($class_id));
        echo "Class Deleted Successfully!";
    }


    /**********************************************************
    ## AttributeTypeCreate
     *************************************************************/

    public function AttributeTypeCreate()
    {
        $attribute_type_all_data=\App\AttributeType::leftJoin('service_attribute_class','service_attribute_type.attribute_types_class_id','=','service_attribute_class.id')
            ->select('service_attribute_class.*','service_attribute_type.*')
            ->orderBy('service_attribute_type.updated_at','DESC')->paginate(10);

        $attribute_type_all_data->setPath(url('/admin/service/attribute-type/create'));
        $pagination = $attribute_type_all_data->render();
        $data['perPage'] = $attribute_type_all_data->perPage();
        $data['pagination'] = $pagination;
        $data['attribute_type_all_data'] = $attribute_type_all_data;
        $data['attribute_class_all_data']=\App\AttributeClass::orderBy('updated_at','DESC')->get();

        $data['page_title'] = $this->page_title;
        return view('attribute.type-attribute',$data);
    }

    /**********************************************************
    ## AttributeTypeSave
     *************************************************************/

    public function AttributeTypeSave()
    {
        $now=date('Y-m-d H:i:s');
        $rule = [
            'attribute_type_name' => 'Required',
            'attribute_class_id' => 'Required',
        ];
        $v = \Validator::make(\Request::all(),$rule);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v)->withInput();
        }

        try{

            $attribute_type_name_slug = \Str::slug(\Request::input('attribute_type_name'), '-');
            $attribute_type_data = [
                'attribute_type_name' =>\Request::input('attribute_type_name'),
                'attribute_type_slug' => $attribute_type_name_slug,
                'attribute_types_class_id' =>\Request::input('attribute_class_id'),
            ];

            $attribute_insert = \App\AttributeType::firstOrCreate(
                [
                    'attribute_type_name' => $attribute_type_data['attribute_type_name'],
                    'attribute_types_class_id' => $attribute_type_data['attribute_types_class_id'],
                ],
                $attribute_type_data
            );

            if($attribute_insert->wasRecentlyCreated){

                \App\System::EventLogWrite('insert,service_attribute_type',json_encode($attribute_type_data));
                return redirect()->back()->with('message','Attribute Type Created Successfully');

            }else return redirect()->back()->with('errormessage','Attribute Type  already created.');


        }catch(\Exception $e){

            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);

            return \Redirect::to('/admin/service/attribute-type/create')->with('errormessage',$e->getMessage());
        }


    }

    /**********************************************************
    ## AttributeTypeEdit
     *************************************************************/

    public function AttributeTypeEdit($type_id)
    {

        $type_info = \App\AttributeType::where('id',$type_id)->first();


        if(!isset($type_info->id))
            return \Redirect::to('/admin/service/attribute-type/create')->with('errormessage',"Invalid Type ID");

        $attribute_type_all_data=\App\AttributeType::leftJoin('service_attribute_class','service_attribute_type.attribute_types_class_id','=','service_attribute_class.id')
            ->select('service_attribute_class.*','service_attribute_type.*')
            ->orderBy('service_attribute_type.updated_at','DESC')->paginate(10);

        $attribute_type_all_data->setPath(url('/admin/service/attribute-type/edit',$type_id));
        $pagination = $attribute_type_all_data->render();
        $data['perPage'] = $attribute_type_all_data->perPage();
        $data['pagination'] = $pagination;
        $data['attribute_type_all_data'] = $attribute_type_all_data;
        $data['attribute_class_all_data']=\App\AttributeClass::orderBy('updated_at','DESC')->get();

        $data['page_title'] = $this->page_title;
        $data['type_info'] = $type_info;
        return view('attribute.edit-type-attribute',$data);
    }

    /**********************************************************
    ## AttributeTypeUpdate
     *************************************************************/

    public function AttributeTypeUpdate($type_id)
    {
        $now=date('Y-m-d H:i:s');
        $rule = [
            'attribute_type_name' => 'Required',
            'attribute_class_id' => 'Required',
        ];
        $v = \Validator::make(\Request::all(),$rule);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v)->withInput();
        }

        try{

            $attribute_type_name_slug = \Str::slug(\Request::input('attribute_type_name'), '-');
            $attribute_type_data = [
                'attribute_type_name' =>\Request::input('attribute_type_name'),
                'attribute_type_slug' => $attribute_type_name_slug,
                'attribute_types_class_id' =>\Request::input('attribute_class_id'),
            ];

            $attribute_insert = \App\AttributeType::updateOrCreate(
                [
                    'id' => $type_id,
                ],
                $attribute_type_data
            );


            \App\System::EventLogWrite('update,service_attribute_type',json_encode($attribute_insert));
            return redirect()->back()->with('message','Attribute Type Updated Successfully');

        }catch(\Exception $e){

            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);

            return \Redirect::to('/admin/service/attribute-type/edit',$type_id)->with('errormessage',$e->getMessage());
        }


    }

    /********************************************
    # AttributeTypeDelete
     *********************************************/
    public function AttributeTypeDelete($type_id){

        \App\AttributeValue::where('attribute_values_type_id',$type_id)->delete();
        \App\AttributeType::where('id',$type_id)->delete();
        \App\System::EventLogWrite('delete,service_attribute_type',json_encode($type_id));
        echo "Type Deleted Successfully!";
    }

    /**********************************************************
    ## AttributeValuesCreate
     *************************************************************/

    public function AttributeValuesCreate()
    {
        $attribute_values_all_data=\App\AttributeValue::join('service_attribute_class','service_attribute_values.attribute_values_class_id','=','service_attribute_class.id')
            ->join('service_attribute_type','service_attribute_values.attribute_values_type_id','=','service_attribute_type.id')
            ->select('service_attribute_class.*','service_attribute_type.*','service_attribute_values.*')
            ->orderBy('service_attribute_values.updated_at','DESC')->paginate(10);

        $attribute_values_all_data->setPath(url('/admin/service/attribute-values/create'));
        $pagination = $attribute_values_all_data->render();
        $data['perPage'] = $attribute_values_all_data->perPage();
        $data['pagination'] = $pagination;
        $data['attribute_values_all_data'] = $attribute_values_all_data;
        $data['attribute_class_all_data']=\App\AttributeClass::orderBy('updated_at','DESC')->get();

        $data['page_title'] = $this->page_title;
        return view('attribute.values-type-attribute',$data);
    }

    /********************************************
    ## AjaxAttributeTypes
     *********************************************/
     public function AjaxAttributeTypes($attribute_class_id){
        $types_list=\App\AttributeType::where('attribute_types_class_id',$attribute_class_id)->get();
        $data['types_list'] = $types_list;

       return \View::make('attribute.ajax-attribute-type-list',$data);

    }

    /**********************************************************
    ## AttributeTypeSave
     *************************************************************/

    public function AttributeValuesSave()
    {
        $now=date('Y-m-d H:i:s');
        $rule = [
            'attribute_field_value' => 'Required',
            'attribute_class_id' => 'Required',
            'attribute_type_id' => 'Required',
        ];
        $v = \Validator::make(\Request::all(),$rule);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v)->withInput();
        }

        try{

            $attribute_values_data = [
                'attribute_field_value' =>\Request::input('attribute_field_value'),
                'attribute_values_class_id' =>\Request::input('attribute_class_id'),
                'attribute_values_type_id' =>\Request::input('attribute_type_id'),
            ];

            $attribute_insert = \App\AttributeValue::firstOrCreate(
                [
                    'attribute_values_class_id' => $attribute_values_data['attribute_values_class_id'],
                    'attribute_values_type_id' => $attribute_values_data['attribute_values_type_id'],
                ],
                $attribute_values_data
            );

            if($attribute_insert->wasRecentlyCreated){

                \App\System::EventLogWrite('insert,service_attribute_values',json_encode($attribute_insert));
                return redirect()->back()->with('message','Attribute Value added Successfully');

            }else return redirect()->back()->with('errormessage','Attribute Value  already existed.');


        }catch(\Exception $e){

            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);

            return \Redirect::to('/admin/service/attribute-values/create')->with('errormessage',$e->getMessage());
        }

    }


    /**********************************************************
    ## AttributeValuesEdit
     *************************************************************/

    public function AttributeValuesEdit($values_id)
    {

        $attribute_values_info = \App\AttributeValue::where('id',$values_id)->first();

        if(!isset($attribute_values_info->id))
            return \Redirect::to('/admin/service/attribute-values/create')->with('errormessage','Invalid Attribute values ID');

        $attribute_values_all_data=\App\AttributeValue::join('service_attribute_class','service_attribute_values.attribute_values_class_id','=','service_attribute_class.id')
            ->join('service_attribute_type','service_attribute_values.attribute_values_type_id','=','service_attribute_type.id')
            ->select('service_attribute_class.*','service_attribute_type.*','service_attribute_values.*')
            ->orderBy('service_attribute_values.updated_at','DESC')->paginate(10);
        $attribute_values_all_data->setPath(url('/admin/service/attribute-values/edit',$values_id));
        $pagination = $attribute_values_all_data->render();
        $data['perPage'] = $attribute_values_all_data->perPage();
        $data['pagination'] = $pagination;
        $data['attribute_values_all_data'] = $attribute_values_all_data;


        $data['attribute_class_all_data']=\App\AttributeClass::orderBy('updated_at','DESC')->get();
        $data['attribute_type_all_data']=\App\AttributeType::where('attribute_types_class_id',$attribute_values_info->attribute_values_class_id)->get();

        $data['attribute_values_info'] =$attribute_values_info;
        $data['page_title'] = $this->page_title;

        return view('attribute.edit-values-type-attribute',$data);
    }


    /**********************************************************
    ## AttributeValuesUpdate
     *************************************************************/

    public function AttributeValuesUpdate($values_id)
    {
        $now=date('Y-m-d H:i:s');
        $rule = [
            'attribute_field_value' => 'Required',
            'attribute_class_id' => 'Required',
            'attribute_type_id' => 'Required',
        ];
        $v = \Validator::make(\Request::all(),$rule);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v)->withInput();
        }

        try{

            $attribute_values_data = [
                'attribute_field_value' =>\Request::input('attribute_field_value'),
                'attribute_values_class_id' =>\Request::input('attribute_class_id'),
                'attribute_values_type_id' =>\Request::input('attribute_type_id'),
            ];

            $attribute_insert = \App\AttributeValue::updateOrCreate(
                [
                    'id' => $values_id,
                ],
                $attribute_values_data
            );


            \App\System::EventLogWrite('update,service_attribute_values',json_encode($attribute_insert));
            return redirect()->back()->with('message','Attribute Value updated Successfully');


        }catch(\Exception $e){

            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);

            return \Redirect::to('/admin/service/attribute-values/edit',$values_id)->with('errormessage',$e->getMessage());
        }

    }



    /********************************************
    # AttributeValuesDelete
     *********************************************/
    public function AttributeValuesDelete($values_id){
        \App\AttributeValue::where('id',$values_id)->delete();
        \App\System::EventLogWrite('delete,service_attribute_values',json_encode($values_id));
        echo "Values Deleted Successfully!";
    }



}
