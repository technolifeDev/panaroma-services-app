<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;
use Validator;
use Session;
use \App\System;
use App\Traits\HasRoleAndPermission;

class SettingController extends Controller
{
    use HasRoleAndPermission;

    public function __construct(Request $request)
    {
        $this->page_title = $request->route()->getName();
        $description = \Request::route()->getAction();
        $this->page_desc = isset($description['desc']) ? $description['desc'] : $this->page_title;
        \App\System::AccessLogWrite();
    }

    /**********************************************************
    ## settingsMetaList
     *************************************************************/
    public function  settingsMetaList()
    {
        $data['page_title'] = $this->page_title;
        return view('settings.meta-list',$data);
    }

    /**********************************************************
    ## settingsMetaInsert
     *************************************************************/

    public function settingsMetaInsert()
    {
        $now= date('Y-m-d H:i:s');

        $rule = [
                'setting_name' => 'Required',
                'setting_data_type' => 'Required',
            ];

        if(\Request::input('setting_data_type')=='array'){

            $rule['meta_name_list']='required|array|min:1';
            $rule['meta_name_list.*']='required';
            $rule['meta_value_list']='required|array|min:1';
            $rule['meta_value_list.*']='required';

        }else{
            $rule['meta_name']='required';
            $rule['meta_value']='required';
        }

        $v = \Validator::make(\Request::all(),$rule);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v)->withInput();
        }

        try{

            \DB::beginTransaction();

            $setting_data = [
                'settings_name' =>\Request::input('setting_name'),
                'settings_data_type' =>\Request::input('setting_data_type'),
            ];

            $setting_data_insert = \App\Setting::firstOrCreate(
                [
                    'settings_name' => $setting_data['settings_name'],
                ],
                $setting_data
            );

            \App\System::EventLogWrite('insert,settings',json_encode($setting_data_insert));

            if(\Request::input('setting_data_type')=='array'){

                $meta_name_list = \Request::input('meta_name_list');
                $meta_value_list = \Request::input('meta_value_list');

                foreach($meta_name_list as $key =>$meta_name){

                    $settings_meta_data =[
                        'settings_id' =>$setting_data_insert->id,
                        'meta_field_name' =>\Str::slug(strtolower($meta_name), '_'),
                        'meta_field_value' =>$meta_value_list[$key]
                    ];
                    $settings_meta_data_insert = \App\SettingMeta::firstOrCreate(
                        [
                            'settings_id' => $settings_meta_data['settings_id'],
                            'meta_field_name' => $settings_meta_data['meta_field_name'],
                        ],
                        $settings_meta_data
                    );
                }

            }else{
                $meta_name = \Str::slug(strtolower(\Request::input('meta_name')), '_');
                $meta_value = \Request::input('meta_value');
                $settings_meta_data =[
                    'settings_id' =>$setting_data_insert->id,
                    'meta_field_name' =>$meta_name,
                    'meta_field_value' =>$meta_value
                ];
                $settings_meta_data_insert = \App\SettingMeta::firstOrCreate(
                    [
                        'settings_id' => $settings_meta_data['settings_id'],
                        'meta_field_name' => $settings_meta_data['meta_field_name'],
                    ],
                    $settings_meta_data
                );
            }

            \App\System::EventLogWrite('insert,settings_meta',json_encode($settings_meta_data_insert));
            \DB::commit();
            return redirect()->back()->with('message','Settings Meta Created Successfully');


        }catch(\Exception $e){
            \DB::rollback();
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            return redirect()->back()->with('errormessage',$e->getMessage());
        }
    }

    /**********************************************************
    ## ajaxSettingsMetaList
     *************************************************************/
    public function ajaxSettingsMetaList(Request $request) {

        $limit = $request->input('length');
        $start = $request->input('start');
        $settings_meta = \App\Setting::orderBy('settings.created_at','DESC')
            ->join('settings_meta','settings.id','=','settings_meta.settings_id')
            ->select('settings.*','settings_meta.*','settings_meta.id as meta_id')
            ->whereNull('settings_meta.deleted_at')
            ->offset($start)
            ->limit($limit)
            ->get();

        $meta_list = array();
        $totalData = count($settings_meta);
        $index = 1;

        if(count($settings_meta)>0){

            foreach($settings_meta as $key=>$meta_data) {


                $meta_list[$key]['sno'] = $index;
                $meta_list[$key]['settings_name'] = ucwords($meta_data->settings_name);
                $meta_list[$key]['settings_type'] = ucfirst($meta_data->settings_data_type);
                $meta_list[$key]['field_name'] = $meta_data->meta_field_name;
                $meta_list[$key]['field_value'] = $meta_data->meta_field_value;
                $meta_list[$key]['actions'] = '';

                if($this->hasPermission('edit')) {
                    $meta_list[$key]['actions'] .= "<a id=" . $meta_data->meta_id . " href='".url('/admin/service/settings-meta/edit',$meta_data->meta_id)."' class='btn btn-xs btn-green category-edit' ><i class='clip-pencil-3'></i></a>";
                }
                if($this->hasPermission('delete')) {
                    if($meta_data->settings_data_type !='array')
                        $meta_list[$key]['actions'] .= " <a id='d_" . $meta_data->meta_id . "'" . ' class="btn btn-xs btn-red meta-data-delete" data-settings='."$meta_data->settings_id".'><i class="clip-remove"></i></a>';
                    else
                        $meta_list[$key]['actions'] .= " <a id='d_" . $meta_data->meta_id . "'" . ' class="btn btn-xs btn-red meta-data-delete" data-settings='."$meta_data->meta_id".'><i class="clip-remove"></i></a>';
                }

                $index++;
            }
        }

        return json_encode(
            array('data'=>$meta_list,
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalData))
        );
    }

    /********************************************
    # ajaxSettingsMetaDelete
     *********************************************/
    public function ajaxSettingsMetaDelete($setting_id){

        $setting = \App\Setting::find($setting_id);

        if(isset($setting->settings_data_type) && $setting->settings_data_type !='array'){
            $setting->settingmeta()->delete();
            $setting->delete();
        }else{
            \App\SettingMeta::find($setting_id)->delete();
        }
        \App\System::EventLogWrite('delete,settings_meta',json_encode($setting_id));
        echo "Data Deleted Successfully!";
    }

    /**********************************************************
    ## settingsMetaEditPage
     *************************************************************/
    public function  settingsMetaEditPage($id)
    {
        $data['page_title'] = $this->page_title;
        $data['setting_meta'] = \App\SettingMeta::find($id);

        if(!isset($data['setting_meta']->id))
            return \Redirect::to('/admin/service/settings-meta/list')->with('errormessage','Invalid Meta ID');

        $data['settings'] = \App\SettingMeta::find($id)->settings;



        return view('settings.edit-meta-list',$data);
    }

    /**********************************************************
    ## settingsMetaUpdate
     *************************************************************/

    public function settingsMetaUpdate($id)
    {
        $now= date('Y-m-d H:i:s');

        $rule = [
            'setting_name' => 'Required',
            'setting_data_type' => 'Required',
        ];

        $setting_meta_info = \App\SettingMeta::find($id);

        if(\Request::input('setting_data_type')=='array'){

            $rule['meta_name_list']='required|array|min:1';
            $rule['meta_name_list.*']='required';
            $rule['meta_value_list']='required|array|min:1';
            $rule['meta_value_list.*']='required';

        }else{
            $rule['meta_name']='required';
            $rule['meta_value']='required';
        }

        $v = \Validator::make(\Request::all(),$rule);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v)->withInput();
        }

        try{

            \DB::beginTransaction();

            $setting_data = [
                'settings_name' =>\Request::input('setting_name'),
                'settings_data_type' =>\Request::input('setting_data_type'),
            ];

            $setting_data_insert = \App\Setting::updateOrCreate(
                [
                    'id' => $setting_meta_info->settings_id,
                ],
                $setting_data
            );

            \App\System::EventLogWrite('insert,settings',json_encode($setting_data_insert));

            if(\Request::input('setting_data_type')=='array'){

                $meta_name_list = \Request::input('meta_name_list');
                $meta_value_list = \Request::input('meta_value_list');

                foreach($meta_name_list as $key =>$meta_name){

                    $settings_meta_data =[
                        'settings_id' =>$setting_meta_info->settings_id,
                        'meta_field_name' =>\Str::slug(strtolower($meta_name), '_'),
                        'meta_field_value' =>$meta_value_list[$key]
                    ];
                    $settings_meta_data_insert = \App\SettingMeta::updateOrCreate(
                        [
                            'id' => $setting_meta_info->id,
                            'meta_field_name' => $settings_meta_data['meta_field_name'],
                        ],
                        $settings_meta_data
                    );

                }

            }else{
                $meta_name = \Str::slug(strtolower(\Request::input('meta_name')), '_');
                $meta_value = \Request::input('meta_value');
                $settings_meta_data =[
                    'settings_id' =>$setting_meta_info->settings_id,
                    'meta_field_name' =>$meta_name,
                    'meta_field_value' =>$meta_value
                ];
                $settings_meta_data_insert = \App\SettingMeta::updateOrCreate(
                    [
                        'id' => $setting_meta_info->id
                    ],
                    $settings_meta_data
                );

            }

            \App\System::EventLogWrite('insert,settings_meta',json_encode($settings_meta_data_insert));
            \DB::commit();

           return redirect()->back()->with('message','Settings Meta Created Successfully');


        }catch(\Exception $e){
            \DB::rollback();
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            return redirect()->back()->with('errormessage',$e->getMessage());
        }
    }
}
