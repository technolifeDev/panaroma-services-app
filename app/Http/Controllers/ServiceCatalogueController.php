<?php

namespace App\Http\Controllers;

//use App\Image;
use App\ServiceCatalogue;
use App\ServiceConfig;
use App\ServiceItem;
use App\ServiceItemPricing;
use Illuminate\Http\Request;
use Validator;
use Session;
use \App\System;
use Response;
use Illuminate\Support\Facades\File;
use App\Traits\HasRoleAndPermission;
use Image;

class ServiceCatalogueController extends Controller
{
    use HasRoleAndPermission;

    public function __construct(Request $request)
    {
        $this->page_title = $request->route()->getName();
        $description = \Request::route()->getAction();
        $this->page_desc = isset($description['desc']) ? $description['desc'] : $this->page_title;
        \App\System::AccessLogWrite();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->_checkIsUserLogin();
        $data['page_title'] = $this->page_title;
        $data['category_all_data'] = \App\ServicesCategory::orderBy('category_name', 'DESC')->get();
        return view('service_config.index', $data);
    }

    //service catalogue index
    public function serviceCatalogueIndex()
    {
        $this->_checkIsUserLogin();
        $data['page_title'] = $this->page_title;
        $data['category_all_data']= \App\ServicesCategory::orderBy('category_name','DESC')->get();
        return view('service_config.service-catalogue', $data);
    }


    //data table ajax load
    public function getServiceData(Request $request){
        $json_data = array();
        $hasEditPermission = $this->hasPermission('edit');
        $hasViewPermission = $this->hasPermission('view');
        $hasDelPermission = $this->hasPermission('delete');
        $request_partner_id = \Request::input('partner_id');
        $service_pricing_partner_id = '';
        if ($request_partner_id && $request_partner_id > 0) {
            $service_pricing_partner_id = $request_partner_id;
        } else {
            $service_pricing_partner_id = \Auth::user()->company_id;
        }

        if ($service_pricing_partner_id) {
            $condition = array('service_item_pricing.service_pricing_partner_id' => $service_pricing_partner_id);
            $totalData = ServiceItemPricing::where('service_item_pricing.service_pricing_partner_id', '=', $service_pricing_partner_id)
                ->groupBy('service_item_pricing.service_pricing_item_id')
                ->count();
        } else {
            $condition = array();
            $totalData = ServiceItem::count();
        }

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $serviceData = array();
        if (empty($request->input('search.value'))) {
            $serviceData = \App\ServiceItemPricing::leftJoin('service_items',
                'service_item_pricing.service_pricing_item_id', '=', 'service_items.id'
            )
                ->leftJoin('services_categories', 'service_items.service_item_category_id', '=', 'services_categories.id')
//                ->leftJoin('service_catalogue', 'service_items.service_item_catalogue_id', '=', 'service_catalogue.id')
                ->leftJoin('service_partner_info', 'service_items.service_item_partner_id', '=', 'service_partner_info.id')
                ->leftJoin('service_config', 'service_item_pricing.service_config_id', '=', 'service_config.id')
                ->select(
                    'service_items.id as service_item_id', 'service_config.*', 'services_categories.category_name',
//                    'service_catalogue.service_name',
                    'service_item_pricing.status'
                )
                ->where($condition)
                ->groupby('service_items.id')
                ->offset($start)
                ->limit($limit)
                ->orderBy('service_item_pricing.updated_at', 'DESC')
                ->get()->toArray();;
        } else {
            $search = $request->input('search.value');
            $serviceData = \App\ServiceItemPricing::leftJoin('service_items',
                'service_item_pricing.service_pricing_item_id', '=', 'service_items.id'
            )
                ->leftJoin('services_categories', 'service_items.service_item_category_id', '=', 'services_categories.id')
//                ->leftJoin('service_catalogue', 'service_items.service_item_catalogue_id', '=', 'service_catalogue.id')
                ->leftJoin('service_partner_info', 'service_items.service_item_partner_id', '=', 'service_partner_info.id')
                ->leftJoin('service_config', 'service_item_pricing.service_config_id', '=', 'service_config.id')
                ->where($condition)
                ->orWhere('services_categories.category_name', 'LIKE', '%' . $search . '%')
                ->orWhere('service_config.service_pricing_title', 'LIKE', '%' . $search . '%')
                ->orWhere('service_config.service_pricing_unit_cost_amount', 'LIKE', '%' . $search . '%')
                ->select(
                    'service_items.id as service_item_id', 'service_config.*', 'services_categories.category_name',
//                    'service_catalogue.service_name',
                    'service_item_pricing.status'
                )
                ->groupby('service_items.id')
                ->offset($start)
                ->limit($limit)
                ->orderBy('service_item_pricing.updated_at', 'DESC')
                ->get()->toArray();
            $totalFiltered = sizeof($serviceData);
        }


        $data = array();
        $sl = 0;
        foreach ($serviceData as $key => $value) {
            $sl++;
            $status = '';
            if ($value['status'] == 'active') {
                $status = '<span class="label label-success">Active</span>';
            } else if ($value['status'] == 'inactive') {
                $status = '<span class="label label-danger">In Active</span>';
            } else if ($value['status'] == 'draft') {
                $status = '<span class="label label-info">Draft</span>';
            } else {
                $status = '<span class="label label-warning">Pending</span>';
            }
            $data[$key]['sl'] = $sl;
            $data[$key]['title'] = $value['service_pricing_title'];
//            $data[$key]['service_name'] = $value['service_name'];
            $data[$key]['status'] = $status;
            $data[$key]['category_name'] = $value['category_name'];
//            $data[$key]['currency'] = $value['service_pricing_currency_unit'];
            $data[$key]['amount'] = $value['service_pricing_unit_cost_amount'] . ' ' . $value['service_pricing_currency_unit'];
            $data[$key]['action'] = ($hasEditPermission ? '<a href="' . url('/admin/get-service/') . '/' . $value['service_item_id'] . '" class="btn btn-xs tooltips btn-green editServiceBtn" id="' . $value['service_item_id'] . '"><i class="clip-pencil-3"></i></a>' : '')
                . ($hasViewPermission ? ' <a href="#" id="' . $value['service_item_id'] . '" class="btn btn-xs tooltips btn-primary viewDtlServiceBtn"><i class="clip-zoom-in"></i></a>' : '')
//                . ($hasViewPermission ? ' <a href="#" id="' . $value['service_item_id'] . '" class="btn btn-xs tooltips btn-warning addMoreAttrBtn" title="Add Attribute"><i class="fa fa-sun-o"></i></a>' : '')
                . ($hasDelPermission ? ' <a href="#" id="' . $value['service_item_id'] . '" class="btn btn-xs tooltips btn-danger deltServiceBtn"><i class="clip-close-2"></i></a>' : '');
        }
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        return json_encode($json_data);
    }


    //show all services catalogue
    public function getServiceCatalougeData(Request $request){
        $json_data = array();
        $hasEditPermission = $this->hasPermission('edit');
        $hasViewPermission = $this->hasPermission('view');
        $hasDelPermission = $this->hasPermission('delete');
        $totalData = \App\ServiceCatalogue::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $serviceData = array();
        if (empty($request->input('search.value'))) {
            $serviceData = \App\ServiceCatalogue::leftjoin('services_categories', 'services_categories.id', '=', 'service_catalogue.services_categories_id')
                ->select(
                    'service_catalogue.*', 'services_categories.category_name'
                )
                ->offset($start)
                ->limit($limit)
                ->orderBy('service_catalogue.updated_at', 'DESC')
                ->get()->toArray();;
        } else {
            $search = $request->input('search.value');
            $serviceData = \App\ServiceCatalogue::leftjoin('services_categories', 'services_categories.id', '=', 'service_catalogue.services_categories_id')
                ->Where('service_catalogue.service_name', 'LIKE', '%' . $search . '%')
                ->select(
                    'service_catalogue.*', 'services_categories.category_name'
                )
                ->offset($start)
                ->limit($limit)
                ->orderBy('service_catalogue.updated_at', 'DESC')
                ->get()->toArray();
            $totalFiltered = sizeof($serviceData);
        }


        $data = array();
        $sl = 0;
        foreach ($serviceData as $key => $value) {
            $sl++;
            $status = '';
            if ($value['status'] == 'active') {
                $status = '<span class="label label-success statusEditor">Active</span>';
            } else if ($value['status'] == 'inactive') {
                $status = '<span class="label label-danger statusEditor">In Active</span>';
            } else if ($value['status'] == 'draft') {
                $status = '<span class="label label-info statusEditor">Draft</span>';
            } else {
                $status = '<span class="label label-warning statusEditor">Pending</span>';
            }
            $data[$key]['sl'] = $sl;
            $data[$key]['service_name'] = $value['service_name'];
            $data[$key]['category'] = $value['category_name'];
            $data[$key]['status'] = $status;
            $data[$key]['service_description'] = $value['service_description'];
            $data[$key]['action'] = ($hasEditPermission ? '<a href="#" id="' . $value['id'] . '" class="btn btn-xs tooltips btn-green editServiceCatalogueBtn"><i class="clip-pencil-3"></i></a>' : '')
//                .( $hasViewPermission ? ' <a href="#" id="' . $value['id'] . '" class="btn btn-xs tooltips btn-primary viewDtlServiceCatalogueBtn"><i class="clip-zoom-in"></i></a>' : '')
                . ($hasDelPermission ? ' <a href="#" id="' . $value['id'] . '" class="btn btn-xs tooltips btn-danger deltCatalogueServiceBtn"><i class="clip-close-2"></i></a>' : '');
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        return json_encode($json_data);
    }


    //get single service
    public function getServiceById($service_item_id){
        $data =  array();
        try {
            $serviceData = ServiceItemPricing::where(['service_item_pricing.service_pricing_item_id' => $service_item_id])
//                ->leftJoin('service_catalogue', 'service_item_pricing.service_pricing_catalogue_id', '=', 'service_catalogue.id')
                ->leftJoin('service_config', 'service_config.id', '=', 'service_item_pricing.service_config_id')
                ->select(['service_item_pricing.*',
//                    'service_catalogue.service_name',
                    'service_config.service_pricing_title', 'service_config.service_pricing_title_est', 'service_config.service_pricing_title_rus'
                    , 'service_config.service_pricing_description', 'service_config.service_pricing_description_est', 'service_config.service_pricing_description_rus',
                    'service_config.service_pricing_currency_unit',
                    'service_config.service_pricing_unit_cost_amount', 'service_config.id as service_config_id',
                    'service_config.service_commission_price', 'service_config.service_tax_price',
                    'service_config.is_service_date'])
//                ->groupBy('service_item_pricing.service_pricing_item_id')
                ->get()
                ->toArray();

//            $data['service_catalogue_all_data'] = \App\ServiceCatalogue::orderBy('service_name', 'DESC')->get();
            $data['category_all_data'] = \App\ServicesCategory::orderBy('category_name', 'DESC')->get();
            $data['page_title'] = $this->page_title;
            $data['service_item_pricing'] = $serviceData;
            $data['service_item_images'] = $this->_getServiceItemsImages($service_item_id);

            $a = url()->previous();
            if (strpos($a, 'partner') !== false) {
                $data['from_partner'] = 1;
            } else {
                $data['from_partner'] = 0;
            }

			$statuses   = ['draft','active','inactive','pending'];
            $sel_status = '<option value="">Select Status</option>';
            foreach($statuses as $status) {
                if($serviceData[0]['status'] === $status){
                    $sel_status .= '<option  value="' . $status .'" selected>'.ucfirst($status).'</option>';
                }else {
                    $sel_status .= '<option value="' . $status .'">'.ucfirst($status).'</option>';
                }
            }
            $data['select_status'] = $sel_status;
            $data['service_frequency_list'] = \App\ServiceFrequency::orderBy('frequency_name', 'ASC')->get();
            $service_item_frequency_arr = \App\ServiceItemFrequency::where('service_item_id', '=', $service_item_id)->get()->toArray();
            $listArr = array();
            if(!empty($service_item_frequency_arr)){
                foreach($service_item_frequency_arr as $key => $val){
                    $listArr[$val['service_frequency_id']] = $val['id'];
                }
            }
            $data['service_item_frequency'] = $listArr;

			//print_r($data['select_status']); die();

            \App\System::EventLogWrite('retrieve,services_item', json_encode($service_item_id));
        } catch (\Exception $e) {
            $message = "Message : " . $e->getMessage() . ", File : " . $e->getFile() . ", Line : " . $e->getLine();
            \App\System::ErrorLogWrite($message);
        }
        return view('service_config.edit-service', $data);
    }


    //view service details by id
    public function viewServiceDtlsById($service_item_id){
        $return_arr = array();
        try {
            $serviceData = ServiceItemPricing::where(['service_item_pricing.service_pricing_item_id' => $service_item_id])
//                ->leftJoin('service_catalogue', 'service_item_pricing.service_pricing_catalogue_id', '=', 'service_catalogue.id')
                ->leftJoin('services_categories', 'service_item_pricing.service_pricing_category_id', '=', 'services_categories.id')
                ->leftJoin('service_config', 'service_item_pricing.service_config_id', '=', 'service_config.id')
                ->where(['service_item_pricing.service_pricing_item_id' => $service_item_id])
                ->select(['service_item_pricing.*', 'service_config.service_pricing_title', 'service_config.service_pricing_description',
                    'service_config.service_pricing_currency_unit', 'service_config.service_pricing_unit_cost_amount',
//                    'service_catalogue.service_name',
                    'services_categories.category_name',
//                    'service_attribute_type.attribute_type_name', 'service_attribute_values.attribute_field_value'
                ])
                ->get()
                ->toArray();
            $reArrangeArr = array();
            foreach ($serviceData as $key => $data){
                $reArrangeArr['service_pricing_title'] = $data['service_pricing_title'];
                $reArrangeArr['service_pricing_description'] = $data['service_pricing_description'];
                $reArrangeArr['service_pricing_currency_unit'] = $data['service_pricing_currency_unit'];
                $reArrangeArr['service_pricing_unit_cost_amount'] = $data['service_pricing_unit_cost_amount'];
//                $reArrangeArr['service_name'] = $data['service_name'];
                $reArrangeArr['category_name'] = $data['category_name'];
//                if($data['service_pricing_class_id']) {
//                    $reArrangeArr['dtl'][$data['attribute_type_name']][$data['attribute_field_value']][$data['service_pricing_type_cost']] = $data['service_pricing_type_cost'];
//                }
            }
            $service_item_images = \App\Image::where(['imageable_id' => $service_item_id, 'imageable_type' => 'App\ServiceItem'])->get()->toArray();
            $return_arr = array('status' => 'success', 'data' => $reArrangeArr, 'images' => $service_item_images);
            \App\System::EventLogWrite('retrieve,services_item_pricing', json_encode($service_item_id));
        } catch (\Exception $e) {
            $message = "Message : " . $e->getMessage() . ", File : " . $e->getFile() . ", Line : " . $e->getLine();
            $return_arr = array('status' => 'error', 'msg' => 'Service not found');
            \App\System::ErrorLogWrite($message);
        }
        return json_encode($return_arr);
    }

    //edit service catlogue
    public function serviceCatalogueEdit($service_catalogue_id){
        $return_arr = array();
        $getServiceCatalogue = ServiceCatalogue::where(['service_catalogue.id' => $service_catalogue_id])
            ->get()
            ->toArray();;
        $catalogue_image = \App\Image::where(['imageable_id' => $service_catalogue_id, 'imageable_type' => 'App\ServiceCatalogue'])->first();
//        print_r($getServiceCatalogue); die();
        $return_arr = array('status' => 'success', 'data' => $getServiceCatalogue, 'image' => $catalogue_image);
        \App\System::EventLogWrite('retrieve,service_catalogue', json_encode($getServiceCatalogue));
        return json_encode($return_arr);

    }

    /**
     * Show service add form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function serviceAddView(){
        $data['service_catalogue_all_data'] = \App\ServiceCatalogue::orderBy('service_name', 'DESC')->get();
        $data['category_all_data'] = \App\ServicesCategory::orderBy('category_name', 'DESC')->get();
        $data['page_title'] = $this->page_title;
        $data['service_frequency_list'] = \App\ServiceFrequency::orderBy('frequency_name', 'ASC')->get();
        return view('service_config.add-service', $data);
    }



    //store service item
    public function storeServiceItem(Request $request){
        $add_service_partner_id = \Request::input('add_service_partner_id');
        $user_id = \Auth::user()->id;
        $url = '';
        if (isset($add_service_partner_id) && $add_service_partner_id) {
            $service_pricing_partner_id = $add_service_partner_id;
            $url = '/admin/service/partner/view/' . $add_service_partner_id;
        } else {
            $service_pricing_partner_id = \Auth::user()->company_id;
            $url = '/admin/services';
        }

        $return_arr = array();
        $rule = [
//            'service_pricing_name' => 'Required',
            'service_pricing_category_id' => 'Required',
            'service_pricing_title.*' => 'Required',
            'service_pricing_description.*' => 'Required',
            'service_pricing_currency_unit.*' => 'Required',
            'service_pricing_unit_cost_amount.*' => 'Required|regex:/^\d+(\.\d{1,2})?$/'
        ];




        $v = \Validator::make(\Request::all(), $rule);

        if ($v->fails()) {
            $return_arr = array('status' => 'error', 'msg' => 'Please fill all required field.');
            return Response::json($return_arr);
        }

        $category_id = \Request::input('service_pricing_category_id')[0];

        $return_status = 0;
//        foreach ($service_pricing_category_arr as $key => $category_id) {
        $return_status = $this->_saveDataInServiceItmTbl($request, $service_pricing_partner_id, $category_id);
//        }

        if ($return_status == 1) {
            $return_arr = array('status' => 'success', 'msg' => 'Service created successfully.', 'url' => $url);
        } else {
            $return_arr = array('status' => 'warning', 'msg' => 'Please fill all required input field.');
        }
        return Response::json($return_arr);

    }


    //save data in service_item
    private function _saveDataInServiceItmTbl($request, $service_pricing_partner_id, $category_id){
        $return_arr = array();
        try{
            $service_data = [
//                'service_pricing_catalogue_id' =>\Request::input('service_pricing_catalogue_id'),
                'service_pricing_category_id' =>$category_id,
                'service_pricing_title' =>\Request::input('service_pricing_title'),
                'service_pricing_title_est' =>\Request::input('service_pricing_title_est'),
                'service_pricing_title_rus' =>\Request::input('service_pricing_title_rus'),
                'service_pricing_description' =>\Request::input('service_pricing_description'),
                'service_pricing_description_est' =>\Request::input('service_pricing_description_est'),
                'service_pricing_description_rus' =>\Request::input('service_pricing_description_rus'),
                'service_pricing_currency_unit' =>\Request::input('service_pricing_currency_unit'),
                'service_pricing_unit_cost_amount' =>\Request::input('service_pricing_unit_cost_amount'),
                'status' =>\Request::input('status'),
                'service_frequency_id' =>\Request::input('service_frequency_id'),
                'service_commission_price' =>\Request::input('service_commission_price'),
                'service_tax_price' =>\Request::input('service_tax_price'),
                'is_service_date' =>\Request::input('is_service_date'),
            ];
//            if(!$service_data['service_pricing_catalogue_id']){
//                $service_data['service_pricing_catalogue_id'] = $this->_saveServiceName($request, $category_id);
//            }
            $request_arr = $request->toArray();
            foreach($service_data['service_pricing_title'] as $key => $val){
                if(!empty($service_data['service_pricing_title'][$key]) && !empty($service_data['service_pricing_currency_unit'][$key])
                && !empty($service_data['service_pricing_unit_cost_amount'][$key]) && $request_arr['images-' . $key]){
//                    $data['service_item_catalogue_id'] = $service_data['service_pricing_catalogue_id'];
                    $data['service_item_category_id'] = $service_data['service_pricing_category_id'];
                    $data['service_item_partner_id'] = $service_pricing_partner_id;
//                    $service_item_price['service_pricing_catalogue_id'] = $service_data['service_pricing_catalogue_id'];
                    $service_item_price['service_pricing_category_id'] = $service_data['service_pricing_category_id'];
                    $service_item_price['service_pricing_partner_id'] = $service_pricing_partner_id;
                    $service_config['service_pricing_title'] = $service_data['service_pricing_title'][$key];
                    $service_config['service_pricing_title_est'] = $service_data['service_pricing_title_est'][$key];
                    $service_config['service_pricing_title_rus'] = $service_data['service_pricing_title_rus'][$key];
                    $service_config['service_pricing_description'] = $service_data['service_pricing_description'][$key];
                    $service_config['service_pricing_description_est'] = $service_data['service_pricing_description_est'][$key];
                    $service_config['service_pricing_description_rus'] = $service_data['service_pricing_description_rus'][$key];
                    $service_config['service_pricing_currency_unit'] = $service_data['service_pricing_currency_unit'][$key];
                    $service_config['service_pricing_unit_cost_amount'] = $service_data['service_pricing_unit_cost_amount'][$key];
                    $service_config['service_commission_price'] = $service_data['service_commission_price'][$key];
                    $service_config['service_tax_price'] = $service_data['service_tax_price'][$key];
                    $service_config['is_service_date'] = $service_data['is_service_date'][$key];
                    $service_item_price['status'] = $service_data['status'][$key];

                    $service_item_price['service_config_id'] = $this->_serviceItemConfig($service_config);

                    $service_item = \App\ServiceItem::create($data);
                    $service_item_price['service_pricing_item_id'] = $service_item->id;
                    $image_arr = array();

                    if(!empty($request_arr['images-' . $key])){
                        $image_arr = $request['images-' .$key];
                    }
                    $this->_saveDataInServiceItmPrcTbl($service_item_price, $image_arr);
                    //service frequency add
                    if(isset($service_data['service_frequency_id'][$key][0])){
                        $this->_saveServiceFrequency($service_item->id, $service_data['service_frequency_id'][$key]);
                    }
                    if($service_item->wasRecentlyCreated){
                        \App\System::EventLogWrite('insert,service_item',json_encode($data));
                        $return_arr = 1; //array('status' => 'success', 'msg' => 'Service created successfully.');
                    }
                } else {
                    $return_arr = 2;//array('status' => 'error', 'msg' => 'Please .');
                }
            }

        }catch(\Exception $e){
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            $return_arr = array('status' => 'error', 'msg' => $e->getMessage());
        }
        return $return_arr;
    }

    private function _saveServiceName($request, $category_id){
        try {
            $service_name_slug  = \Str::slug(\Request::input('service_pricing_name'), '-');
            $service_data = [
                'services_categories_id' => $category_id,
                'service_name' =>\Request::input('service_pricing_name'),
                'service_name_slug' => $service_name_slug,
                'service_description' => \Request::input('description'),
            ];

            $service_insert = \App\ServiceCatalogue::firstOrCreate(
                [
                    'service_name' => $service_data['service_name'],
                    'services_categories_id' => $category_id,
                ],
                $service_data
            );

//            if($service_insert->wasRecentlyCreated){
                \App\System::EventLogWrite('insert,service_catalogue',json_encode($service_data));
                $return_arr = $service_insert->id;
//            }else {
//                $return_arr = 0;
//            }
        }   catch(\Exception $e){

            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            $return_arr = 0;
        }
        return $return_arr;
    }


    //save data in service_item_price
    private function _saveDataInServiceItmPrcTbl($service_item_price, $image_arr = array()){
        $return_arr = array();
        try{
            $service_item_price_ = \App\ServiceItemPricing::firstOrCreate(
                [
//                    'service_pricing_catalogue_id'  => $service_item_price['service_pricing_catalogue_id'],
                    'service_pricing_category_id'  => $service_item_price['service_pricing_category_id'],
                    'service_pricing_partner_id' => $service_item_price['service_pricing_partner_id'],
                    'service_pricing_item_id' => $service_item_price['service_pricing_item_id']
                ],
                $service_item_price
            );
            $update_fun = 0;
            if(!empty($image_arr)){
                foreach ($image_arr as $img_key => $imag){
                    $service_image = $imag;
                    $service_id = $service_item_price['service_pricing_item_id'];
                    $imageable_type = 'App\ServiceItem';
                    $folder_name = 'service_images/service_item_images/';
                    $this->_uploadImage($service_id, $service_image, $imageable_type, $update_fun, $folder_name);
                }
            }
            if($service_item_price_->wasRecentlyCreated){
                \App\System::EventLogWrite('insert,service_item_price',json_encode($service_item_price_));
                $return_arr = array('status' => 'success', 'msg' => 'Service created successfully.');
            }
        }catch(\Exception $e){
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            $return_arr = array('status' => 'error', 'msg' => $e->getMessage());
        }
        return $return_arr;
    }


    //save service item configuration
    private function _serviceItemConfig($service_config){
        $return_config_id = 0;
        try{
            $service_config_ = \App\ServiceConfig::firstOrCreate(
                [
                    'service_pricing_title'  => $service_config['service_pricing_title'],
                ],
                $service_config
            );

            if($service_config_->wasRecentlyCreated){
                \App\System::EventLogWrite('insert,service_config',json_encode($service_config_));
                $return_config_id = $service_config_->id;
            }
        }catch(\Exception $e){
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            $return_config_id = 0;
        }
        return $return_config_id;
    }


    //save service frequency
    private function _saveServiceFrequency($service_item_id, $service_frequency_id){
        foreach($service_frequency_id as $key => $value){
            $service_frequency_ = \App\ServiceItemFrequency::updateOrCreate([
                'service_item_id' => $service_item_id,
//                'service_frequency_id' => $value,
            ],
                [
                    'service_item_id' => $service_item_id,
                    'service_frequency_id' => $value,
                ]
            );
        }
        return true;
    }

    //delete service frequency
    private function _deleteServiceFrequency($service_item_id, $service_frequency_arr = array()){
        $service_item_frequency = \App\ServiceItemFrequency::where('service_item_id', '=', $service_item_id);
        $service_item_frequency_ = $service_item_frequency->get()->toArray();
        $old_data = array();
        if(!empty($service_item_frequency_)){
            foreach($service_item_frequency_ as $key => $val){
                $old_data[$key] = $val['service_frequency_id'];
            }
        }
        $diff_arr = array_diff($old_data, $service_frequency_arr);
        if(!empty($diff_arr)){
            \App\ServiceItemFrequency::where('service_item_id', '=', $service_item_id)
                ->whereIn('service_frequency_id', $diff_arr)->delete();
        }
    }

    //update service item
    public function updateServiceItem(Request $request){
//        print_r($request->all()); die();
        $return_arr = array();
        $user_id = \Auth::user()->id;
//        $service_pricing_partner_id = \Auth::user()->company_id;
        $add_service_partner_id = \Request::input('add_service_partner_id');
        $user_id = \Auth::user()->id;
        $url = '';
        if (isset($add_service_partner_id) && $add_service_partner_id) {
            $service_pricing_partner_id = $add_service_partner_id;
            $url = '/admin/service/partner/view/' . $add_service_partner_id;
        } else {
            $service_pricing_partner_id = \Auth::user()->company_id;
            $url = '/admin/services';
        }
        $return_arr = array();
        $rule = [
//            'service_pricing_name' => 'Required',
            'service_pricing_category_id' => 'Required',
            'service_pricing_title.*' => 'Required',
            'service_pricing_description.*' => 'Required',
            'service_pricing_currency_unit.*' => 'Required',
            'service_pricing_unit_cost_amount.*' => 'Required|regex:/^\d+(\.\d{1,2})?$/'
        ];
        $v = \Validator::make(\Request::all(), $rule);

        if ($v->fails()) {
            $return_arr = array('status' => 'error', 'msg' => 'Please fill all required field.');
            return Response::json($return_arr);
        }
//        print_r($request->all()); die();
        $return_status = $this->_updateDataInServiceItmTbl($request);
        if ($return_status == 1) {
            $old_images = !empty(\Request::input('old_images')) ? \Request::input('old_images') : array();
            $old_image_from_db_json = \Request::input('service_item_images');
            $old_image_from_db = json_decode($old_image_from_db_json, true);
            $this->_delImages($old_images, $old_image_from_db);
            $return_arr = array('status' => 'success', 'msg' => 'Service updated successfully.', 'url' => $url);
        } else {
            $return_arr = array('status' => 'warning', 'msg' => 'Please fill all required input field.');
        }

        return Response::json($return_arr);
    }



    //update data in service_item_price
    private function _updateDataInServiceItmTbl($request){
        $return_arr = array();
        try{
            $service_data = [
//                'service_pricing_catalogue_id' =>\Request::input('service_pricing_catalogue_id'),
                'service_pricing_category_id' =>\Request::input('service_pricing_category_id'),
                'service_pricing_title' =>\Request::input('service_pricing_title'),
                'service_pricing_title_est' =>\Request::input('service_pricing_title_est'),
                'service_pricing_title_rus' =>\Request::input('service_pricing_title_rus'),
                'service_pricing_description' =>\Request::input('service_pricing_description'),
                'service_pricing_description_est' =>\Request::input('service_pricing_description_est'),
                'service_pricing_description_rus' =>\Request::input('service_pricing_description_rus'),
                'service_pricing_currency_unit' =>\Request::input('service_pricing_currency_unit'),
                'service_pricing_unit_cost_amount' =>\Request::input('service_pricing_unit_cost_amount'),
                'service_pricing_item_id' =>\Request::input('service_pricing_item_id'),
                'id' => \Request::input('service_item_pricing_id'),
                'service_config_id' => \Request::input('service_config_id'),
                'service_pricing_partner_id' => \Request::input('service_pricing_partner_id'),
                'status' => \Request::input('status'),
                'service_frequency_id' =>\Request::input('service_frequency_id'),
                'service_commission_price' =>\Request::input('service_commission_price'),
                'service_tax_price' =>\Request::input('service_tax_price'),
                'is_service_date' =>\Request::input('is_service_date'),
            ];

            $old_images_ = \Request::input('old_images');
            $category_id = \Request::input('service_pricing_category_id');
//            if(!$service_data['service_pricing_catalogue_id']){
//                $service_data['service_pricing_catalogue_id'] = $this->_saveServiceName($request, $category_id);
//            }
            $request_arr = $request->toArray();
            foreach($service_data['service_pricing_title'] as $key => $val){
                if(!empty($service_data['service_pricing_title'][$key]) && !empty($service_data['service_pricing_currency_unit'][$key])
                    && !empty($service_data['service_pricing_unit_cost_amount'][$key]) && (!empty($request_arr['images-' . $key]) ||
                        !empty($old_images_)
                    )){
//                    $service_item_price['service_pricing_catalogue_id'] = $service_data['service_pricing_catalogue_id'];
                    $service_item_price['service_pricing_category_id'] = $service_data['service_pricing_category_id'];
                    $service_item_price['service_pricing_partner_id'] = $service_data['service_pricing_partner_id'];
                    $service_config['service_config_id'] = $service_data['service_config_id'];
                    $service_config['service_pricing_title'] = $service_data['service_pricing_title'][$key];
                    $service_config['service_pricing_title_est'] = $service_data['service_pricing_title_est'][$key];
                    $service_config['service_pricing_title_rus'] = $service_data['service_pricing_title_rus'][$key];
                    $service_config['service_pricing_description'] = $service_data['service_pricing_description'][$key];
                    $service_config['service_pricing_description_est'] = $service_data['service_pricing_description_est'][$key];
                    $service_config['service_pricing_description_rus'] = $service_data['service_pricing_description_rus'][$key];
                    $service_config['service_pricing_currency_unit'] = $service_data['service_pricing_currency_unit'][$key];
                    $service_config['service_pricing_unit_cost_amount'] = $service_data['service_pricing_unit_cost_amount'][$key];
                    $service_config['service_commission_price'] = $service_data['service_commission_price'][$key];
                    $service_config['service_tax_price'] = $service_data['service_tax_price'][$key];
                    $service_config['is_service_date'] = $service_data['is_service_date'][$key];
                    $service_item_price['status'] = $service_data['status'][$key];
//                    $service_item_price['id'] = $service_data['id'];

                    $service_item_price['service_config_id'] = $this->_updateServiceItemConfig($service_config);
//                    print_r($service_item_price); die();
                    $service_item = \App\ServiceItem::findOrFail($service_data['service_pricing_item_id']);
//                    $service_item->service_item_catalogue_id = $service_data['service_pricing_catalogue_id'];
                    $service_item->service_item_category_id = $service_data['service_pricing_category_id'];
                    $service_item->service_item_partner_id = $service_data['service_pricing_partner_id'];
                    $service_item->save();
                    $service_item_price['service_pricing_item_id'] = $service_item->id;

                    $image_arr = array();

                    if(!empty($request_arr['images-' . $key])){
                        $image_arr = $request['images-' .$key];
                    }

                    //service frequency add
                    $service_frequency_arr = isset($service_data['service_frequency_id'][$key][0]) ? $service_data['service_frequency_id'][$key] : array();

//                    $this->_deleteServiceFrequency($service_item->id, $service_frequency_arr);
                    if(isset($service_data['service_frequency_id'][$key][0])){
                        $this->_saveServiceFrequency($service_item->id, $service_data['service_frequency_id'][$key]);
                    }

                    foreach ($service_data['id'] as $key => $val){
                        $service_item_price['id'] = $val;
                        $this->_updateDataInServiceItmPrcTbl($service_item_price, $image_arr);
                    }

                    \App\System::EventLogWrite('update,service_item',json_encode($service_item));
                    $return_arr = 1; //array('status' => 'success', 'msg' => 'Service created successfully.');

                } else {
                    $return_arr = 2;//array('status' => 'error', 'msg' => 'Please .');
                }
            }

        }catch(\Exception $e){
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            $return_arr = array('status' => 'error', 'msg' => $e->getMessage());
        }
        return $return_arr;
    }


    //update
    private function _updateDataInServiceItmPrcTbl($service_item_price, $image_arr = array()){
//        print_r($service_item_price); die();
        $return_arr = array();
        try{
            $service_item_price_ = \App\ServiceItemPricing::updateOrCreate(
                [
                    'id' => $service_item_price['id']
                ],
                $service_item_price
            );
            if($service_item_price_->wasRecentlyCreated){
                \App\System::EventLogWrite('update,service_item_price',json_encode($service_item_price_));
                $return_arr = array('status' => 'success', 'msg' => 'Service created successfully.');
            }

            $update_fun = 0;
            if(!empty($image_arr)){
                foreach ($image_arr as $img_key => $imag){
                    $service_image = $imag;
                    $service_id = $service_item_price['service_pricing_item_id'];
                    $imageable_type = 'App\ServiceItem';
                    $folder_name = 'service_images/service_item_images/';
                    $this->_uploadImage($service_id, $service_image, $imageable_type, $update_fun, $folder_name);
                }
            }
        }catch(\Exception $e){
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            $return_arr = array('status' => 'error', 'msg' => $e->getMessage());
        }
        return $return_arr;
    }

    //update service item configuration
    private function _updateServiceItemConfig($service_config){
        $return_config_id = 0;
        try{
            $service_config_ = \App\ServiceConfig::updateOrCreate(
                [
                    'id'  => $service_config['service_config_id'],
                ],
                $service_config
            );

            \App\System::EventLogWrite('insert,service_config',json_encode($service_config_));
            $return_config_id = $service_config_->id;

        }catch(\Exception $e){
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            $return_config_id = 0;
        }
        return $return_config_id;
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteServiceItem($service_id)
    {

        $service_item = \App\ServiceItemPricing::find($service_id);
        \App\ServiceItem::where('id', $service_item->service_pricing_item_id)->delete();

        $service_item->delete();
        \App\System::EventLogWrite('delete,service_item_pricing', json_encode($service_id));
        $return_arr = array('status' => 'error', 'msg' => 'Service deleted.');
        return Response::json($return_arr);

    }

    public function deleteServiceCatalogue($service_id){
        $service_item = \App\ServiceCatalogue::where('id', $service_id)->delete();

//        \App\ServiceItem::where('id',$service_item->service_pricing_item_id)->delete();
        \App\System::EventLogWrite('delete,service_catalogue', json_encode($service_id));
        $return_arr = array('status' => 'error', 'msg' => 'Service Catalogue deleted.');
        return Response::json($return_arr);
    }


    //save service
    public function storeService(Request $request){
		//print_r($request->all()); die();
        $return_arr = array();
        $rule = [
            'service_name' => 'Required',
            'services_categories_id' => 'Required',
//            'service_catalogue_image' => 'Required | mimes:jpeg,jpg,png',
        ];
        $service_id = \Request::input('service_id');

        if (!$service_id) {
            $rule['service_catalogue_image'] = 'Required | mimes:jpeg,jpg,png';
        }


        $v = \Validator::make(\Request::all(), $rule);

        if ($v->fails()) {
            $return_arr = array('status' => 'error', 'msg' => 'Please fill all required field.');
            return Response::json($return_arr);
        }

        try {
            $service_name_slug = \Str::slug(\Request::input('service_name'), '-');
            if (!empty($request->status)) {
                $status = $request->status;
            } else {
                $status = 'draft';
            }

            $service_data = [
                'service_name' => \Request::input('service_name'),
                'services_categories_id' => \Request::input('services_categories_id'),
                'service_name_slug' => $service_name_slug,
                'service_description' => \Request::input('description'),
                'status' => $status
            ];


            $update_fun = 0;
            if (\Request::input('service_catalogue_image_id')) {
                $update_fun = 1;
            }
            if ($service_id) {
                $service_insert = \App\ServiceCatalogue::updateOrCreate(
                    [
                        'id' => $service_id,
                    ],
                    $service_data
                );
                $msg = 'Service updated successfully.';
            } else {
                $service_insert = \App\ServiceCatalogue::firstOrCreate(
                    [
                        'service_name' => $service_data['service_name'],
                    ],
                    $service_data
                );
                $msg = 'Service created successfully.';
            }
            if ($request->hasFile('service_catalogue_image')) {
                $service_image = \Request::file('service_catalogue_image');
                $service_id = $service_insert->id;
                $imageable_type = 'App\ServiceCatalogue';
                $folder_name = "service_images/catalogue_images/";
                $this->_uploadImage($service_id, $service_image, $imageable_type, $update_fun, $folder_name);
            }

            \App\System::EventLogWrite('insert,service_catalogue', json_encode($service_data));
            $return_arr = array('status' => 'success', 'msg' => $msg);

        } catch (\Exception $e) {

            $message = "Message : " . $e->getMessage() . ", File : " . $e->getFile() . ", Line : " . $e->getLine();
            \App\System::ErrorLogWrite($message);
            $return_arr = array('status' => 'error', 'msg' => $e->getMessage());
        }
        return $return_arr;

    }


    //upload service_catalogue image
    private function _uploadImage($service_id, $service_image, $imageable_type, $update_fun = 0, $folder_name){
        $ext = strtolower($service_image->getClientOriginalExtension());
        if(in_array($ext, ['jpg', 'jpeg', 'png'])){
            $image_orginal_name = $service_image->getClientOriginalName();
            $filename = pathinfo($image_orginal_name, PATHINFO_FILENAME);
            $name = time() . '_' . $filename .'.'.$service_image->getClientOriginalExtension();

			//$destinationPath = $folder_name; // /service_images/catalogue_images
           // $service_image->move($destinationPath, $name);

			$img_location = $service_image->getRealPath();
			if (!file_exists($folder_name)) {
				mkdir($folder_name, 0777, true);
			}
			$path2 		  = $folder_name . $name;


			$img_height = Image::make($img_location)->height();
			$img_width  = Image::make($img_location)->width();
			// when image height less than width
			// crop the image by it's height
			if($img_height < $img_width){
				Image::make($img_location)->crop($img_height,$img_height)->resize(400, null, function ($constraint) {$constraint->aspectRatio();$constraint->upsize();})->save($path2);
			}
			// when image width less than height
			// crop the image by it's width
			else{
				Image::make($img_location)->crop($img_width,$img_width)->resize(400, null, function ($constraint) {$constraint->aspectRatio();$constraint->upsize();})->save($path2);
			}


            if($update_fun == 0){
                $photo = new \App\Image();
                $photo->image_name = $name;
                $photo->imageable_id = $service_id;
                $photo->imageable_type = $imageable_type;
                $photo->save();
            } else {
                $image_data = [
                    'image_name' =>  $name,
                    'imageable_id' => $service_id,
                    'imageable_type' => $imageable_type
                ];
                $image_insert = \App\Image::updateOrCreate(
                    [
                        'imageable_id' => $service_id,
                        'imageable_type' => $imageable_type,
                    ],
                    $image_data
                );
            }
            \App\System::EventLogWrite('insert,images',json_encode($name));
        }
    }


    public function getAllServiceList(){
        $getData = \App\ServiceCatalogue::orderBy('service_name', 'DESC')
            ->select(['id', 'service_name'])
            ->get()->toArray();
        $return_arr = array();
        foreach ($getData as $key => $val) {
            $return_arr[$key]['id'] = $val['id'];
            $return_arr[$key]['value'] = $val['service_name'];
        }
//        print_r($return_arr); die();
        return Response::json($return_arr);
    }


    private function _checkIsUserLogin(){
        if(\Auth::check()){
            return redirect('auth/login');
        }
    }

    //get services images
    private function _getServiceItemsImages($servic_item_id){
        $getAllImages = \App\Image::where(['imageable_id' => $servic_item_id, 'imageable_type' => 'App\ServiceItem'])
            ->get()
            ->toArray()
        ;

        $json_arr = array();
        if(!empty($getAllImages)){
            foreach($getAllImages as $key => $val){
                $tm_arr['id'] = $val['id'];
                $tm_arr['src'] = url('/service_images/service_item_images') . '/' . $val['image_name'];
                $json_arr[] = $tm_arr;
            }
        }
//        print_r(json_encode($json_arr)); die();
        return json_encode($json_arr);
    }

    //check and delete images
    private function _delImages($old_images, $old_image_from_db){
        $prev_image_arr = array();
        $prev_image_loc_arr = array();
        foreach($old_image_from_db as $key => $val){
            $prev_image_arr[] = $val['id'];
            $prev_image_loc_arr[$val['id']] = $val['src'];
        }

        $diff_arr = array_diff($prev_image_arr, $old_images);
        if(!empty($diff_arr)){
            foreach ($diff_arr as $key => $val){
                $pathinfo = pathinfo($prev_image_loc_arr[$val]);
                $filename =  $pathinfo['filename'].'.'.$pathinfo['extension'];
                $image_path = public_path('/service_images/service_item_images') . '/' . $filename;  // Value is not URL but directory file path
                if(\File::exists($image_path)) {
                    \File::delete($image_path);
                }
            }
            \DB::table('images')->whereIn('id', $diff_arr)->delete();
            \App\System::EventLogWrite('delete,images',json_encode($diff_arr));
        }
    }

    //update status
    public function updateCatalogueStatus(Request $request){
        $service_data = [
            'status' => \Request::input('status'),
        ];
        $service_id = \Request::input('service_catalogue_id');
        $service_insert = \App\ServiceCatalogue::updateOrCreate(
            [
                'id' => $service_id,
            ],
            $service_data
        );
        $msg = 'Service updated successfully.';

        if (\Request::input('status') == 'active') {
            $status = '<span class="label label-success">Active</span>';
        } else if (\Request::input('status') == 'inactive') {
            $status = '<span class="label label-danger">In Active</span>';
        } else if (\Request::input('status') == 'draft') {
            $status = '<span class="label label-info">Draft</span>';
        } else {
            $status = '<span class="label label-warning">Pending</span>';
        }

        \App\System::EventLogWrite('update,service_catalogue', json_encode($status));
        $return_arr = array('status' => 'success', 'msg' => $status);
        return json_encode($return_arr);
    }


    public function addServiceAttribute($service_item_id){
        $data['service_item_id'] = $service_item_id;
        $getServiceAttrDtls = \App\ServiceItemPricing::join('service_attribute_class', 'service_item_pricing.service_pricing_class_id', '=', 'service_attribute_class.id')
            ->join('service_attribute_type', 'service_item_pricing.service_pricing_type_id', '=', 'service_attribute_type.id')
            ->join('service_attribute_values', 'service_item_pricing.service_pricing_type_value', '=', 'service_attribute_values.id')
            ->where(['service_item_pricing.service_pricing_item_id' => $service_item_id])
            ->select(['service_item_pricing.*', 'service_attribute_class.attribute_class_name',
                'service_attribute_type.attribute_type_name', 'service_attribute_values.attribute_field_value'
                ])
            ->get()
            ->toArray()
        ;
        $resizeArr = array();
        if(!empty($getServiceAttrDtls)){
            foreach($getServiceAttrDtls as $dtl){
                $resizeArr[$dtl['service_pricing_class_id']][$dtl['service_pricing_type_id']][$dtl['service_pricing_type_value']]['class_name'] = $dtl['attribute_class_name'];
                $resizeArr[$dtl['service_pricing_class_id']][$dtl['service_pricing_type_id']][$dtl['service_pricing_type_value']]['type_name'] = $dtl['attribute_type_name'];
                $resizeArr[$dtl['service_pricing_class_id']][$dtl['service_pricing_type_id']][$dtl['service_pricing_type_value']]['type_value'] = $dtl['attribute_field_value'];
                $resizeArr[$dtl['service_pricing_class_id']][$dtl['service_pricing_type_id']][$dtl['service_pricing_type_value']]['service_pricing_type_cost'] = $dtl['service_pricing_type_cost'];
                $resizeArr[$dtl['service_pricing_class_id']][$dtl['service_pricing_type_id']][$dtl['service_pricing_type_value']]['id'] = $dtl['id'];
                $resizeArr[$dtl['service_pricing_class_id']][$dtl['service_pricing_type_id']][$dtl['service_pricing_type_value']]['is_selected'] = $dtl['is_selected'];
            }
            $data['attr_details'] = $this->_generateTable($resizeArr);
        } else {
            $data['attr_details'] = '';
        }

        return view('service_config.service-attribute', $data);
    }

    //get all attribute class list
    public function getAllAttrClassList(){
        $getData = \App\AttributeClass::orderBy('attribute_class_name', 'DESC')
            ->select(['id', 'attribute_class_name'])
            ->get()->toArray();
        $return_arr = array();
        foreach ($getData as $key => $val) {
            $return_arr[$key]['id'] = $val['id'];
            $return_arr[$key]['value'] = $val['attribute_class_name'];
        }
//        print_r($return_arr); die();
        \App\System::EventLogWrite('retrieve,service_attribute_class', json_encode($getData));
        return Response::json($return_arr);
    }

    //get all attribute type list by class name
    public function getAllAttrTypeList($attr_class_id){
        $getData = \App\AttributeType::where(['attribute_types_class_id' => $attr_class_id])
            ->orderBy('attribute_type_name', 'DESC')
            ->select(['id', 'attribute_type_name'])
            ->get()->toArray();
        $return_arr = array();
        foreach ($getData as $key => $val) {
            $return_arr[$key]['id'] = $val['id'];
            $return_arr[$key]['value'] = $val['attribute_type_name'];
        }
//        print_r($return_arr); die();
        return Response::json($return_arr);
    }

    //get all attribute value list by class and types
    public function getAllAttrValList($attr_class_id, $attr_type_id){
        $getData = \App\AttributeValue::where(['attribute_values_type_id' => $attr_type_id, 'attribute_values_class_id' => $attr_class_id])
            ->orderBy('attribute_field_value', 'DESC')
            ->select(['id', 'attribute_field_value'])
            ->get()->toArray();
        $return_arr = array();
        foreach ($getData as $key => $val) {
            $return_arr[$key]['id'] = $val['id'];
            $return_arr[$key]['value'] = $val['attribute_field_value'];
        }
//        print_r($return_arr); die();
        return Response::json($return_arr);
    }

    //save attribute
    public function saveAttributeData(Request $request){
        $return_arr = array();
        $user_id = \Auth::user()->id;
        $service_pricing_partner_id = \Auth::user()->company_id;
        $return_arr = array();
        $rule = [
            'attribute_class_name.*' => 'Required',
            'attribute_type_name.*' => 'Required',
            'attribute_field_value.*' => 'Required',
            'service_pricing_type_cost.*' => 'Required'//|regex:/^\d+(\.\d{1,2})?$/'
        ];
        $v = \Validator::make(\Request::all(), $rule);

        if ($v->fails()) {
            $return_arr = array('status' => 'error', 'msg' => 'Please fill all required field.');
            return Response::json($return_arr);
        }

        $return_arr = $this->_saveServiceAttrDtls($request);

        if($return_arr['status'] == 'success'){
            if (\Auth::user()->user_type == 'admin') {
                $return_arr['url'] = '/admin/service/partner/view/' . $return_arr['partner_id'];
            } else {
                $return_arr['url'] = '/partner/services';
            }
        }


        return json_encode($return_arr);
    }


    //save attribute
    private function _saveServiceAttrDtls($request){
        $return_arr = array();
        try {
//            \DB::beginTransaction();
            $service_attr_dtls = [
                'service_pricing_item_id' =>\Request::input('service_pricing_item_id'),
                'attribute_class_name' =>\Request::input('attribute_class_name'),
                'service_pricing_class_id' =>\Request::input('service_pricing_class_id'),
                'attribute_type_name' =>\Request::input('attribute_type_name'),
                'service_pricing_type_id' =>\Request::input('service_pricing_type_id'),
                'attribute_field_value' =>\Request::input('attribute_field_value'),
                'service_pricing_type_value' =>\Request::input('service_pricing_type_value'),
                'service_pricing_type_cost' =>\Request::input('service_pricing_type_cost'),
                'is_selected' =>\Request::input('is_selected'),
            ];
//            print_r($service_attr_dtls); die();

            if(isset($request->service_item_pricing_tbl_id)){
                $service_attr_dtls['service_item_pricing_tbl_id'] = \Request::input('service_item_pricing_tbl_id');
            }

            $get_service_item_details = \App\ServiceItemPricing::where('service_pricing_item_id', '=', $service_attr_dtls['service_pricing_item_id'])->first();
            if(isset($get_service_item_details->id)){
                $totalPrice = 0; $service_config_id = 0;
                foreach($service_attr_dtls['attribute_class_name'] as $class_key => $class_val){
                    if($service_attr_dtls['attribute_class_name'][$class_key]){
                        $service_item_pricing = array();
                        $service_item_pricing['service_pricing_item_id'] = $service_attr_dtls['service_pricing_item_id'];
                        $service_item_pricing['service_pricing_category_id'] = $get_service_item_details->service_pricing_category_id;
                        $service_item_pricing['service_pricing_catalogue_id'] = $get_service_item_details->service_pricing_catalogue_id;
                        $service_item_pricing['service_pricing_partner_id'] = $get_service_item_details->service_pricing_partner_id;
                        $service_item_pricing['service_config_id'] = $get_service_item_details->service_config_id;
                        $service_config_id = $service_item_pricing['service_config_id'];
                        if($service_attr_dtls['service_pricing_class_id'][$class_key]){
                            $service_item_pricing['service_pricing_class_id'] = $service_attr_dtls['service_pricing_class_id'][$class_key];
                        } else {
//                            $service_item_pricing['service_pricing_class_id'] = $this->_saveAttrClass($service_attr_dtls['attribute_class_name'][$class_key]);
                            $totalPrice = 0;
                            $return_arr = array('status' => 'error', 'msg' => 'Please select the config from auto suggestion.');
                            break;
                        }

                        foreach ($service_attr_dtls['attribute_type_name'][$class_key] as $type_key => $type_values){
                            if($service_attr_dtls['attribute_type_name'][$class_key][$type_key]){
                                if($service_attr_dtls['service_pricing_type_id'][$class_key][$type_key]){
                                    $service_item_pricing['service_pricing_type_id'] = $service_attr_dtls['service_pricing_type_id'][$class_key][$type_key];
                                } else {
                                    $service_item_pricing['service_pricing_type_id'] = $this->_saveAttrType($service_attr_dtls['attribute_type_name'][$class_key][$type_key], $service_item_pricing['service_pricing_class_id']);
                                }

                                foreach ($service_attr_dtls['attribute_field_value'][$class_key][$type_key] as $val_key => $val_values){
//                                    if($service_attr_dtls['attribute_field_value'][$class_key][$type_key][$val_key] &&
//                                        $service_attr_dtls['service_pricing_type_cost'][$class_key][$type_key][$val_key]){
                                        $price_validation = $this->_validatePrice($service_attr_dtls['service_pricing_type_cost'][$class_key][$type_key][$val_key]);
                                        if($price_validation == true){
                                            if(isset($service_attr_dtls['service_item_pricing_tbl_id'][$class_key][$type_key][$val_key])){
                                                $service_item_pricing['id'] = $service_attr_dtls['service_item_pricing_tbl_id'][$class_key][$type_key][$val_key];
                                            } else {
                                                unset($service_item_pricing['id']);
                                            }
                                            if($service_attr_dtls['service_pricing_type_value'][$class_key][$type_key][$val_key]){
                                                $service_item_pricing['service_pricing_type_value'] = $service_attr_dtls['service_pricing_type_value'][$class_key][$type_key][$val_key];
                                            } else {
                                                $service_item_pricing['service_pricing_type_value'] = $this->_saveAttrTypeVal($service_attr_dtls['attribute_field_value'][$class_key][$type_key][$val_key], $service_item_pricing['service_pricing_type_id'], $service_item_pricing['service_pricing_class_id']);
                                            }
                                            $service_item_pricing['service_pricing_type_cost'] = $service_attr_dtls['service_pricing_type_cost'][$class_key][$type_key][$val_key];
                                            $service_item_pricing['is_selected'] = isset($service_attr_dtls['is_selected'][$class_key][$type_key][$val_key]) ? 1 : 0;
//                                            $totalPrice += $service_item_pricing['service_pricing_type_cost'];
                                            $this->_saveServiceAttrOnItemPricing($service_item_pricing);

                                        } else {
                                            $totalPrice = 0;
                                            $return_arr = array('status' => 'error', 'msg' => 'Amount must be decimal or float or number.');
                                            break;
                                        }
//                                    } else {
//                                        $totalPrice = 0;
//                                        $return_arr = array('status' => 'error', 'msg' => 'Please provide Service Attribute value.');
//                                        break;
//                                    }
                                }
                            } else {
                                $totalPrice = 0;
                                $return_arr = array('status' => 'error', 'msg' => 'Please provide Service Attribute type.');
                                break;
                            }
                        }
                    } else {
                        $return_arr = array('status' => 'error', 'msg' => 'Please provide Service Attribute class.');
                        break;
                    }
                }
//                if($totalPrice > 0){
//                    $this->_serviceConfigUpdatePrice($totalPrice, $service_config_id);
//                }
            }
            if(empty($return_arr)){
                $return_arr = array('status' => 'success', 'msg' => 'Service Attribute added successfully', 'partner_id' => $get_service_item_details->service_pricing_partner_id);
            }
//            \DB::commit();
        } catch (\Exception $e) {
//            \DB::rollback();
            $message = "Message : " . $e->getMessage() . ", File : " . $e->getFile() . ", Line : " . $e->getLine();
            \App\System::ErrorLogWrite($message);
            $return_arr = array('status' => 'error', 'msg' => $message);
        }
        return $return_arr;
    }

    //update price
    private function _serviceConfigUpdatePrice($totalPrice, $service_config_id){
        \App\ServiceConfig::where('id', $service_config_id)->update(array('service_pricing_unit_cost_amount' => $totalPrice));
    }

    //validate price
    private function _validatePrice($price) {
        if(!$price){
            return true;
        }
        if (preg_match("/^[0-9]+(\.[0-9]{2})?$/", $price)) {
            return true;
        }
        return false;
    }

    //save attribute class
    private function _saveAttrClass($class_name){
        $attr_class_dtl = [
            'attribute_class_name' => $class_name,
            'attribute_class_slug' => \Str::slug($class_name, '-')
        ];
        $service_class = \App\AttributeClass::updateOrCreate(
            [
                'attribute_class_name' => $class_name,
            ],
            $attr_class_dtl
        );
        \App\System::EventLogWrite('insert,service_attribute_class', json_encode($service_class));
        return $service_class->id;
    }

    //save attr type
    private function _saveAttrType($type_name, $class_id){
        $attr_type_dtl = [
            'attribute_type_name' => $type_name,
            'attribute_type_slug' => \Str::slug($type_name, '-'),
            'attribute_types_class_id' => $class_id,
        ];

        $check_if_type_exist = \App\AttributeType::where([
            'attribute_type_name' => $type_name,
            'attribute_types_class_id' => $class_id
        ])->first();

        if(!isset($check_if_type_exist->id)){
            $service_type = \App\AttributeType::create($attr_type_dtl);
            $service_type_id = $service_type->id;
            \App\System::EventLogWrite('insert,service_attribute_type', json_encode($service_type));
        } else {
            $service_type_id = $check_if_type_exist->id;
            \App\System::EventLogWrite('update,service_attribute_type', json_encode($check_if_type_exist));
        }

        return $service_type_id;
    }


    private function _saveAttrTypeVal($attribute_field_value, $attribute_values_type_id, $attribute_values_class_id){
        $attr_type_val_dtl = [
            'attribute_field_value' => $attribute_field_value,
            'attribute_values_type_id' => $attribute_values_type_id,
            'attribute_values_class_id' => $attribute_values_class_id,
        ];
        $check_if_value_exist = \App\AttributeValue::where([
            'attribute_field_value' => $attribute_field_value,
            'attribute_values_type_id' => $attribute_values_type_id,
            'attribute_values_class_id' => $attribute_values_class_id,
        ])->first();

        if(!isset($check_if_type_exist->id)){
            $attribute_insert = \App\AttributeValue::create(
                $attr_type_val_dtl
            );
            $service_value_id = $attribute_insert->id;
            \App\System::EventLogWrite('insert,service_attribute_values', json_encode($attribute_insert));
        } else {
            $service_value_id = $check_if_value_exist->id;
            \App\System::EventLogWrite('update,service_attribute_values', json_encode($check_if_value_exist));
        }

        return $service_value_id;
    }

    //save attr data in service_item_price
    private function _saveServiceAttrOnItemPricing($service_item_pricing){
        if(isset($service_item_pricing['id'])){
            $check_if_service_item_exist = \App\ServiceItemPricing::find($service_item_pricing['id']);
        } else {
            $check_if_service_item_exist = \App\ServiceItemPricing::where([
                'service_pricing_item_id' => $service_item_pricing['service_pricing_item_id'],
                'service_pricing_class_id' => $service_item_pricing['service_pricing_class_id'],
                'service_pricing_type_id' => $service_item_pricing['service_pricing_type_id'],
                'service_pricing_type_value' => $service_item_pricing['service_pricing_type_value'],
            ])->first();
        }
//        print_r($check_if_service_item_exist->id); die();
//
        if(!isset($check_if_service_item_exist->id)){
            $service_item_pricing_insert = \App\ServiceItemPricing::create($service_item_pricing);
            $service_item_pricing_id = $service_item_pricing_insert->id;
            \App\System::EventLogWrite('insert,service_item_pricing', json_encode($service_item_pricing_insert));
        } else {
            $service_item_pricing_insert = \App\ServiceItemPricing::updateOrCreate(
                [
                    'id' => $check_if_service_item_exist->id,
                ],
                $service_item_pricing
            );
            $service_item_pricing_id = $service_item_pricing_insert->id;
            \App\System::EventLogWrite('update,service_item_pricing', json_encode($service_item_pricing_insert));
        }

    }


    //generate dynamic table
    private function _generateTable($resizeArr){
//        print_r($resizeArr); die();
        $table = ''; $class_index = 0;
        foreach($resizeArr as $class_id => $cls_dtls){
            if($class_index != 0) {
                $table .= "<hr class='half-rule'>";
            }
            $table .= '<table class="table attributeTable">';
            $table .= '<tbody>' .
                    '<tr>' .
                        '<th colspan="3" class="attrTitle">' .
                            'Attribute '. ($class_index + 1).
                         '</th>' .
                        '<th colspan="2" style="text-align: right;">'.
                            '<input type="hidden" name="tbl_serial" class="tbl_serial" value="'. $class_index .'">';
            if($class_index == 0){
                $table .= '<a class="btn btn-xs btn-success addMoreAttrsBtn" href="#">' .
                '<i class="fa fa-plus"></i> Add More Attribute'.
                '</a>';
            } else {
                $table .=  '<a class="btn btn-xs btn-danger removeMoreAttrsBtn" href="#" attr_class_id="'. $class_id.'">'.
                    '<i class="fa fa-minus"></i> Remove Attribute' .
                    '</a>';
            }

            $table .= '</th>'.
                    '</tr>'
            ;
            $type_index = 0;
            foreach($cls_dtls as $type_id => $type_dtls){
                $val_index = 0;
                foreach($type_dtls as $val_id => $val_details){
                    if($val_index == 0 && $type_index == 0){
                        $table .= '<tr class="attributeTypeClone">' .
                                    '<td>' .
                                        '<div class="entry input-group col-xs-12">' .
                                            '<input class="form-control attribute_class_name" name="attribute_class_name['. $class_index . ']" type="text" value="'. $val_details['class_name'] .'" />' .
                                            '<input class="service_pricing_class_id" name="service_pricing_class_id['. $class_index . ']" type="hidden" value="'. $class_id . '" />' .
                                        '</div>' .
                                    '</td>' .
                                    '<td>' .
                                        '<div class="entry input-group col-xs-12">' .
                                            '<input class="form-control attribute_type_name" name="attribute_type_name['. $class_index . '][' . $type_index . ']" type="text" value="' . $val_details['type_name'] .'" />' .
                                            '<input class="service_pricing_type_id" name="service_pricing_type_id['. $class_index . '][' . $type_index . ']" type="hidden" value="' . $type_id. '" />' .
                                            '<span class="input-group-btn">' .
                                            '<button class="btn btn-success btn-add addMoreAttrType" type="button">' .
                                            '<span class="glyphicon glyphicon-plus" style="font-size: 12px;"></span>' .
                                            '</button>' .
                                            '</span>' .
                                        '</div>' .
                                    '</td>' .
                                    '<td>' .
                                        '<div class="entry input-group col-xs-12">' .
                                            '<input class="form-control attribute_field_value" name="attribute_field_value['. $class_index . '][' . $type_index . '][' . $val_index . ']" type="text" value="' . $val_details['type_value'] . '" />' .
                                            '<input class="service_pricing_type_value" name="service_pricing_type_value['. $class_index . '][' . $type_index . '][' . $val_index . ']" type="hidden" value="' . $val_id . '" />' .
                                        '</div>' .
                                    '</td>' .
                                    '<td style="width: 15%">' .
                                        '<div class="entry input-group col-xs-12">' .
                                            '<input class="form-control service_pricing_type_cost" name="service_pricing_type_cost['. $class_index . '][' . $type_index . '][' . $val_index . ']" type="text" value="'. $val_details['service_pricing_type_cost'] .'" />' .
                                            '<input class="service_item_pricing_tbl_id" name="service_item_pricing_tbl_id['. $class_index . '][' . $type_index . '][' . $val_index . ']" type="hidden" value="' . $val_details['id']. '" />' .
                                            '<span class="input-group-btn">' .
                                                '<button class="btn btn-success btn-add addMoreAttrValue" type="button">' .
                                                    '<span class="glyphicon glyphicon-plus" style="font-size: 12px;"></span>' .
                                                '</button>' .
                                            '</span>' .
                                        '</div>' .
                                    '</td>' .
                                    '<td style="width: 5%">' .
                                        '<label class="switch"><input type="checkbox" class="is_selected" name="is_selected['. $class_index . '][' . $type_index . '][' . $val_index . ']" '. ($val_details['is_selected'] == 1 ? 'checked' : '') .'><div class="slider round"></div></label>' .
                                    '</td>' .
                                '</tr>';
                    }
                    if($val_index > 0){
                        $table .= '<tr class="attributeValueClone">' .
                                   ' <td colspan="2"></td>' .
                                    '<td>' .
                                        '<div class="entry input-group col-xs-12">' .
                                            '<input class="form-control attribute_field_value" name="attribute_field_value['. $class_index . '][' . $type_index . '][' . $val_index . ']" type="text" value="' . $val_details['type_value'] . '" />' .
                                            '<input class="service_pricing_type_value" name="service_pricing_type_value['. $class_index . '][' . $type_index . '][' . $val_index . ']" type="hidden" value="' . $val_id . '" />' .

                                    '</div>' .
                                    '</td>' .
                                    '<td style="width: 15%">' .
                                        '<div class="entry input-group col-xs-12">' .
                                            '<input class="form-control service_pricing_type_cost" name="service_pricing_type_cost['. $class_index . '][' . $type_index . '][' . $val_index . ']" type="text" value="'. $val_details['service_pricing_type_cost'] .'" />' .
                                            '<input class="service_item_pricing_tbl_id" name="service_item_pricing_tbl_id['. $class_index . '][' . $type_index . '][' . $val_index . ']" type="hidden" value="' . $val_details['id']. '" />' .
                                            '<span class="input-group-btn">' .
                                                '<button class="btn btn-danger btn-add removeMoreAttrValue" type="button" attr_val_id="'. $val_id.'">' .
                                                    '<span class="glyphicon glyphicon-minus" style="font-size: 12px;"></span>' .
                                                '</button>' .
                                            '</span>' .
                                        '</div>' .
                                    '</td>' .
                                    '<td style="width: 5%">' .
                                        '<label class="switch"><input type="checkbox" class="is_selected" name="is_selected['. $class_index . '][' . $type_index . '][' . $val_index . ']" '. ($val_details['is_selected'] == 1 ? 'checked' : '') .'><div class="slider round"></div></label>' .
                                    '</td>' .
                                '</tr>';
                    }

                    if($type_index > 0 && $val_index == 0){
                        $table .= '<tr class="attributeTypeClone">' .
                                    '<td></td>' .
                                    '<td>' .
                                        '<div class="entry input-group col-xs-12">' .
                                            '<input class="form-control attribute_type_name" name="attribute_type_name['. $class_index . '][' . $type_index . ']" type="text" value="' . $val_details['type_name'] .'" />' .
                                            '<input class="service_pricing_type_id" name="service_pricing_type_id['. $class_index . '][' . $type_index . ']" type="hidden" value="' . $type_id. '" />' .
                                            '<span class="input-group-btn">' .
                                                '<button class="btn btn-danger btn-add removeMoreAttrType" type="button" attr_typ_id="'.$type_id.'">' .
                                                    '<span class="glyphicon glyphicon-minus" style="font-size: 12px;"></span>' .
                                                '</button>'.
                                            '</span>' .
                                        '</div>'.
                                    '</td>' .
                                    '<td>' .
                                        '<div class="entry input-group col-xs-12">' .
                                            '<input class="form-control attribute_field_value" name="attribute_field_value['. $class_index . '][' . $type_index . '][' . $val_index . ']" type="text" value="' . $val_details['type_value'] . '" />' .
                                            '<input class="service_pricing_type_value" name="service_pricing_type_value['. $class_index . '][' . $type_index . '][' . $val_index . ']" type="hidden" value="' . $val_id . '" />' .
                                        '</div>' .
                                    '</td>' .
                                    '<td style="width: 15%">' .
                                        '<div class="entry input-group col-xs-12">' .
                                            '<input class="form-control service_pricing_type_cost" name="service_pricing_type_cost['. $class_index . '][' . $type_index . '][' . $val_index . ']" type="text" value="'. $val_details['service_pricing_type_cost'] .'" />' .
                                            '<input class="service_item_pricing_tbl_id" name="service_item_pricing_tbl_id['. $class_index . '][' . $type_index . '][' . $val_index . ']" type="hidden" value="' . $val_details['id']. '" />' .
                                            '<span class="input-group-btn">'.
                                                '<button class="btn btn-success btn-add addMoreAttrValue" type="button">' .
                                                    '<span class="glyphicon glyphicon-plus" style="font-size: 12px;"></span>'.
                                                '</button>' .
                                            '</span>' .
                                        '</div>'.
                                    '</td>' .
                                    '<td style="width: 5%">' .
                                        '<label class="switch"><input type="checkbox" class="is_selected" name="is_selected['. $class_index . '][' . $type_index . '][' . $val_index . ']" '. ($val_details['is_selected'] == 1 ? 'checked' : '') .'><div class="slider round"></div></label>' .
                                    '</td>' .
                                '</tr>';
                    }
                    $val_index++;
                }
                $type_index++;
            }
            $table .= '</tbody>' .
                '</table>';
            $class_index++;
        }
        return $table;
    }

    //delete service attribute
    public function deleteServiceAttributeData(Request $request){
        $field = \Request::input('field');
        $attr_id = \Request::input('attr_id');
        $service_pricing_item_id = \Request::input('service_pricing_item_id');
        $del_json = array('field' => $field, 'attr_id' => $attr_id, 'service_pricing_item_id' => $service_pricing_item_id);
        \App\ServiceItemPricing::where([$field => $attr_id, 'service_pricing_item_id' => $service_pricing_item_id])
            ->delete();
        \App\System::EventLogWrite('delete,service_item_pricing', json_encode($del_json));
        $return_arr = array('status' => 'error', 'msg' => 'Service Attribute deleted successfully');
        return json_encode($return_arr);
    }
}
