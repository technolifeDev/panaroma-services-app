<?php

namespace App\Http\Controllers;

use App\Permission;
use Illuminate\Http\Request;
use App\Exceptions\Handler;
use App\ServicesCategory;
use App\PermissionRole;
use App\Http\Requests\StoreCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use Auth;
use App\User;
use App\Traits\HasRoleAndPermission;
use Image;
class CategoryController extends Controller
{
    use HasRoleAndPermission;
    /**
     * Class constructor.
     * get current route name for page title.
     *
     */
    public function __construct(Request $request)
    {
        $this->page_title = $request->route()->getName();
        $description = \Request::route()->getAction();
        $this->page_desc = isset($description['desc']) ? $description['desc'] : $this->page_title;
        \App\System::AccessLogWrite();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //

        $data['page_title'] = $this->page_title;
        try {

            $categories = ServicesCategory::orderBy('created_at','desc')->get();
            $data['categories'] = $categories;
            $data['user_permissions'] = Auth::user()->perimissions;

            return view('categories.index', $data);

        } catch(Exception $e) {
            $message = "Message : " . $e->getMessage() . ", File : " . $e->getFile() . ", Line : " . $e->getLine();
            \App\System::ErrorLogWrite($message);
            return redirect()->back()->with('errormessage', "Something is went wrong!");
        }

    }

    public function ajaxCategoryList(Request $request) {
        $role = $request->route()->getAction();
        $categories = ServicesCategory::orderBy('order')->get();

        $cat_list = array();
        $index = 1;

        foreach($categories as $key=>$cat) {
            $status = '';
            if($cat['status'] == 'active'){
                $status = '<span class="label label-success">Active</span>';
            } else if($cat['status'] == 'inactive') {
                $status = '<span class="label label-danger">In Active</span>';
            } else if($cat['status'] == 'draft'){
                $status = '<span class="label label-info">Draft</span>';
            } else {
                $status = '<span class="label label-warning">Pending</span>';
            }

            $cat_list[$key]['sno'] = $index;
            $cat_list[$key]['category_name'] = $cat->category_name;
            if($cat->parent_id != -1){
                $cat_list[$key]['parent_category_name'] = $cat->parent->category_name;
            }else{
                $cat_list[$key]['parent_category_name'] = 'no parent';
            }

            $cat_list[$key]['description'] = $cat->description;
            $cat_list[$key]['status'] = $status;
            $cat_list[$key]['actions'] = '';
            if($this->hasPermission('edit')) {
                $cat_list[$key]['actions'] .= "<a id=" . $cat->id . " href='#' class='btn btn-xs btn-green category-edit' ><i class='clip-pencil-3'></i></a>";
            }
            if($this->hasPermission('view')) {
                $cat_list[$key]['actions'] .= " <a id='v_" . $cat->id . "' href='#' class='btn btn-xs btn-primary category-view' ><i class='clip-zoom-in'></i></a>";
            }
            if($this->hasPermission('delete')) {
                $cat_list[$key]['actions'] .= " <a id='d_" . $cat->id . "'" . ' class="btn btn-xs btn-red category-delete" ><i class="clip-remove"></i></a>';
            }

            $index++;
        }

        return json_encode(array('data'=>$cat_list));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCategoryRequest $request)
    {
        //
//        print_r($request->all()); die();
//        try {
            $category = new ServicesCategory();

            $category->category_name = $request->category_name;
            $category->category_name_est = $request->category_name_est;
            $category->category_name_rus = $request->category_name_rus;
            $category->parent_id = $request->parent_id;
            $category->description = $request->description;
            $category->description_est = $request->description_est;
            $category->description_rus = $request->description_rus;

            if (!empty($request->status)) {
                $category->status = $request->status;
            } else {
                $category->status = 'draft';
            }
			//print_r($request->all()); die();

            $category->save();
            if($request->hasFile('category_image')){
                $service_image = \Request::file('category_image');
                $service_id = $category->id;
                $imageable_type = 'App\ServicesCategory';
                $update_fun = 0;
                $this->_uploadImage($service_id, $service_image, $imageable_type, $update_fun);
            }
            return redirect()->back()->with('message','Category created successfully.');

//        }catch(\Exception $e ) {
//            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
//            \App\System::ErrorLogWrite($message);
//            return redirect()->back()->with('errormessage',"Something is went wrong!");
//        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $categories = ServicesCategory::where('id', $id)->orderBy('created_at','desc')->first();
        if($categories->parent_id != -1){
            $categories->parent_category_name = $categories->parent->category_name;
        }else{
            $categories->parent_category_name = 'no parent';
        }
        if(!empty($categories->image->image_name)){
            $categories->cimage = $categories->image->image_name;
        }else{
            $categories->cimage = '';
        }
        return json_encode(array('category'=>$categories));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try{
            $data['page_title'] = $this->page_title;
            $category = ServicesCategory::find($id)->toArray();
            $categories = ServicesCategory::where('parent_id','<>', $category['id'])->get();
            $data['category'] = $category;
            $statuses = ['draft','active','inactive','pending'];

            $select='';
            $select .= '<label for="exampleInputPassword1">Parent Category</label>';
            $select .= '<select class="form-control" name="parent_id" id="parent_id">';
            $select .= '<option value="-1">No parent</option>';

            foreach($categories as $cat) {

                if($cat->id != $id) {

                    if ($cat->id == $category['parent_id']) {
                        $select .= '<option selected value="' . $cat->id . '">' . $cat->category_name . '</option>';
                    } else {
                        $select .= '<option value="' . $cat->id . '">' . $cat->category_name . '</option>';
                    }
                }
            }

            $select .= '</select>';

            $sel_status='';
            $sel_status .= '<option value="">Select Status</option>';
            foreach($statuses as $status) {
                if($category['status'] === $status){
                    $sel_status .= '<option selected value="' . $status .'">'.ucfirst($status).'</option>';
                }else {
                    $sel_status .= '<option value="' . $status .'">'.ucfirst($status).'</option>';
                }
            }

            $data['select'] = $select;
            $data['select_status'] = $sel_status;
            $data['categories'] = $categories;

            $cat_image = \App\Image::where(['imageable_id' => $id, 'imageable_type' => 'App\ServicesCategory'])->first();
            $data['image'] = $cat_image;
            return json_encode($data);
            //return view('categories.edit', compact('categories','category'));

        }catch(Exception $e) {
            $message = "Message : " . $e->getMessage() . ", File : " . $e->getFile() . ", Line : " . $e->getLine();
            \App\System::ErrorLogWrite($message);
            return redirect()->back()->with('errormessage', "Something is went wrong!");
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategoryRequest $request)
    {
        $res = ServicesCategory::where('category_name',$request->category_name)->get();

        if(count($res) >0 && $request->category_id != $res[0]->id) {
            return redirect()->back()->with('errormessage', "This category name already exist!");
        }else {
            $response = $this->checkParentChildRelation($request->category_id, $request->parent_id);
            if(!$response){
                try {
                    $id = $request->category_id;

                    $category = ServicesCategory::find($id);
                    $category->category_name = $request->category_name;
                    $category->parent_id = $request->parent_id;
                    $category->description = $request->description;
                    $category->category_name_est = $request->category_name_est;
                    $category->category_name_rus = $request->category_name_rus;
                    $category->description_est = $request->description_est;
                    $category->description_rus = $request->description_rus;



                    if (!empty($request->status)) {
                        $category->status = $request->status;
                    } else {
                        $category->status = 'draft';
                    }

                    $category->save();

                    $update_fun = 0;
                    if($request->category_image_id){
                        $update_fun = 1;
                    }
                    if($request->hasFile('category_image')){
                        $service_image = \Request::file('category_image');
                        $service_id = $category->id;
                        $imageable_type = 'App\ServicesCategory';
                        $this->_uploadImage($service_id, $service_image, $imageable_type, $update_fun);
                    }
                    return redirect()->back()->with('message', 'Category updated successfully.');

                } catch (\Exception $e) {
                    $message = "Message : " . $e->getMessage() . ", File : " . $e->getFile() . ", Line : " . $e->getLine();
                    \App\System::ErrorLogWrite($message);
                    return redirect()->back()->with('errormessage', "Something is went wrong!");
                }
            }else{
                return redirect()->back()->with('message', 'Category is already parent');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        ServicesCategory::where('parent_id', $request->category_id)->delete();

        $category = ServicesCategory::find($request->category_id);
        $category->delete();

        echo "Category deleted successfully";
    }

    public function checkParentChildRelation($id, $parent_id) {
        $response = ServicesCategory::where('id', $parent_id)
                        ->where('parent_id', $id)->get();

        if(count($response)){
            return true;
        }
        return false;
    }


    //upload category image
    private function _uploadImage($service_id, $service_image, $imageable_type, $update_fun = 0){
//        var_dump($service_image);die();
        $ext = strtolower($service_image->getClientOriginalExtension());
        if(in_array($ext, ['jpg', 'jpeg', 'svg', 'png'])) {
            $image_orginal_name = $service_image->getClientOriginalName();
            $filename = pathinfo($image_orginal_name, PATHINFO_FILENAME);
            $name = time() . '_' . $filename . '.' . $service_image->getClientOriginalExtension();

			$destinationPath = public_path('/service_images/category_images');
            $service_image->move($destinationPath, $name);
			if ($update_fun == 0) {
                $photo = new \App\Image();
                $photo->image_name = $name;
                $photo->imageable_id = $service_id;
                $photo->imageable_type = $imageable_type;
                $photo->save();
            } else {
                $image_data = [
                    'image_name' => $name,
                    'imageable_id' => $service_id,
                    'imageable_type' => $imageable_type
                ];
                $image_insert = \App\Image::updateOrCreate(
                    [
                        'imageable_id' => $service_id,
                        'imageable_type' => $imageable_type,
                    ],
                    $image_data
                );
            }
            \App\System::EventLogWrite('insert,images', json_encode($name));
        }
    }

    //save category order
    public function categorySort(Request $request){
        try {
            $cat_list = \Request::input('sort_data');
            foreach($cat_list as $ordr => $cat_id){
                $category = ServicesCategory::find($cat_id);
                $category->order = $ordr;
                $category->save();
            }
            $return_arr = array('status' => 'success', 'msg' => 'Category order updated.');
            return json_encode($return_arr);
        } catch (\Exception $e){
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            $return_arr = array('status' => 'error', 'msg' => $e->getMessage());
            return json_encode($return_arr);
        }
    }
}
