<?php

namespace App\Http\Controllers;

use App\PermissionRole;
use App\Permission;
use App\ServicesCategory;
use App\Traits\HasRoleAndPermission;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\RequestStorePermission;
use Auth;


class PermissionController extends Controller
{
    use HasRoleAndPermission;
    /**
     * Class constructor.
     * get current route name for page title.
     *
     */
    public function __construct(Request $request)
    {
        $this->page_title = $request->route()->getName();
        $description = \Request::route()->getAction();
        $this->page_desc = isset($description['desc']) ? $description['desc'] : $this->page_title;
        \App\System::AccessLogWrite();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = User::with('roles','roles.permissions')->get();

        $data['page_title'] = $this->page_title;
        try {
            $permissions = Permission::all();
            $data['permissions'] = $permissions;
            return view('permissions.index', $data);
        } catch(\Exception $e) {
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            return redirect()->back()->with('errormessage',"Something is wrong!");
        }
    }

    public function ajaxPermissionList() {
        $permissions = Permission::orderBy('created_at','desc')->get();

        $permissions_list = array();
        $index = 1;
        $user_permissions = Auth::user()->perimissions;
        foreach($permissions as $key=>$permission) {

            $permissions_list[$key]['sno'] = $index;
            $permissions_list[$key]['name'] = $permission->name;
            $permissions_list[$key]['slug'] = $permission->slug;

            $permissions_list[$key]['description'] = $permission->description;
            $permissions_list[$key]['actions'] = "";
            if($this->hasPermission('edit')) {
                $permissions_list[$key]['actions'] .= "<a id= " . $permission->id . " href='#' class='btn btn-xs tooltips btn-green edit-permission' ><i class='clip-pencil-3'></i></a>";
            }
            if($this->hasPermission('delete')) {
                $permissions_list[$key]['actions'] .= " <a id= " . $permission->id . " href='#' class='btn btn-xs tooltips btn-red delete-permission' ><i class='clip-remove'></i></a>";
            }
            $index++;
        }
        return json_encode(array('data'=>$permissions_list));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestStorePermission $request)
    {

        // Create different permissions
        try {
            if (isset($request->permission) && count($request->permission) > 0) {
                foreach ($request->permission as $permission) {
                    $permissionObj = new Permission;
                    $permissionObj->name = ucfirst(strtolower($permission)) . ' ' . ucfirst(strtolower($request->name));
                    $permissionObj->slug = strtolower($permission) . '-' . strtolower($request->name);
                    $permissionObj->description = 'Allows user to ' . strtolower($permission) . ' ' . strtolower($request->name);
                    $permissionObj->save();
                }
                return redirect()->back()->with('message','New permission created successfully.');
            }
            else {
                return redirect()->back()->with('errormessage','No permission is selected to add.');
            }

        } catch(\Exception $e) {
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            return redirect()->back()->with('errormessage',"Something is wrong!");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $data['page_title'] = $this->page_title;
            $permission = Permission::findOrFail($id)->toArray();
            $data['permission'] = $permission;

            return json_encode($data);

        } catch(\Exception $e) {

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try{
            $permission = Permission::findOrFail($request->permission_id);

            $permission->name = $request->name;
            $permission->slug = $request->slug;
            $permission->description = $request->description;
            $permission->save();

            return redirect()->back()->with('message',"Role is updated successfully");
        } catch(\Exception $e) {

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $permission = Permission::findOrFail($id);

        $permission->delete();

        echo "Permission deleted successfully";
    }

    /**
     * Partner Assign Role & Permission for individual user.
     *
     * @param int $user_id
     * @param int $status.
     *
     * @return Response.
     */

    public function partnerAssignPermissionsToRole(Request $request) {

        if ($this->level() >=5) {
            $v = \Validator::make($request->all(), [
                'assign_user_id' => 'required|numeric',
                'user_level' => 'required|numeric',
                'permissions' => 'required|array',

            ]);
            if ($v->fails()) {
                return redirect()->back()->withErrors($v)->withInput();
            }
            if (count($request->input('permissions'))>0) {
                $now=date('Y-m-d H:i:s');

                try {

                    #Role And Permission
                    $permissions = $request->input('permissions');
                    $assign_user_id = $request->input('assign_user_id');
                    $level = $request->input('user_level');


                    #Role And Permission
                    $roles = \App\Role::where('slug','partner')->first();
                    if(!isset($roles->id))
                        throw new \Exception('Role is required');


                    $this->detachUserRole($roles->id,$assign_user_id);
                    $this->attachUserRole($roles->id,$level,$assign_user_id);

                    \App\System::EventLogWrite('update,partner user role of '.$assign_user_id,json_encode($roles));


                    $this->syncUserPermissions($permissions,$assign_user_id);

                    \App\System::EventLogWrite('update,partner permission role of '.$assign_user_id,json_encode($roles));

                    return redirect('partner/user/management?tab=assign_permission')->with('message',"User Role and Permission updated Successfully !");

                } catch(\Exception $e) {
                    $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
                    \App\System::ErrorLogWrite($message);
                    return redirect('partner/user/management?tab=assign_permission')->with('errormessage',"User Already Exist !");
                }
            } else {
                return redirect('partner/user/management?tab=assign_permission')->with('errormessage',"Role or permission missing");
            }
        } else {
            return redirect('/dashboard')->with('errormessage',"You are not authorized user!");
        }
    }

    /**
     * Assign Role & Permission for individual user.
     *
     * @param int $user_id
     * @param int $status.
     *
     * @return Response.
     */

    public function  assignPermissionsToRole(Request $request) {

        if ($this->level() >=5) {
            $v = \Validator::make($request->all(), [
                'assign_user_id' => 'required|numeric',
                'user_level' => 'required|numeric',
                'roles' => 'required|array',
                'permissions' => 'required|array',

            ]);
            if ($v->fails()) {
                return redirect()->back()->withErrors($v)->withInput();
            }
            if (count($request->input('roles'))>0 && count($request->input('permissions'))>0) {
                $now=date('Y-m-d H:i:s');

                try {

                    #Role And Permission
                    $roles = $request->input('roles');
                    $permissions = $request->input('permissions');
                    $assign_user_id = $request->input('assign_user_id');
                    $level = $request->input('user_level');
                    /*foreach ($roles as $key => $role){
                        $this->detachUserRole($role,$assign_user_id);
                        $this->attachUserRole($role,$level,$assign_user_id);
                    }*/
                    $this->syncUserRoles($roles,$level,$assign_user_id);
                    \App\System::EventLogWrite('update,user role of '.$assign_user_id,json_encode($roles));

                    /*foreach ($permissions as $key => $permission){
                        $this->syncUserPermissions($permission,$assign_user_id);
                    }*/

                    $this->syncUserPermissions($permissions,$assign_user_id);

                    \App\System::EventLogWrite('update,permission role of '.$assign_user_id,json_encode($roles));

                    return redirect('admin/user/management?tab=assign_permission')->with('message',"User Role and Permission updated Successfully !");

                } catch(\Exception $e) {
                    $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
                    \App\System::ErrorLogWrite($message);
                    return redirect('admin/user/management?tab=assign_permission')->with('errormessage',"User Already Exist !");
                }
            } else {
                return redirect('admin/user/management?tab=assign_permission')->with('errormessage',"Role or permission missing");
            }
        } else {
            return redirect('/dashboard')->with('errormessage',"You are not authorized user!");
        }
    }
}
