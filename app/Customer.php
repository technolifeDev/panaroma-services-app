<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Customer extends Model
{
    use SoftDeletes;
    protected $table = 'customer_info';

    protected $fillable = [
        'customer_name',
        'customer_slug',
        'customer_address',
        'customer_licence',
        'customer_email',
        'customer_email_verify',
        'customer_digital_signature',
        'customer_agreement_status',
        'customer_agreement_accept_date',
        'customer_profile_image'
    ];

    /********************************************
    ## CustomerVerificationMail
     *********************************************/
    public static function CustomerVerificationMail($users_id,$reset_url){



        $now = date('Y-m-d H:i:s');

        try{

            $user_info=\App\User::where('id',$users_id)->first();

            if(isset($user_info)){
                $users_id=$user_info->id;
                $users_type=$user_info->user_type;
                $users_email=$user_info->email;

            }else{
                return \Redirect::back()->with('message',"Invalid User ID!");
            }

            $data['user_info'] = $user_info;
            $data['reset_url'] = $reset_url;

            $user_email = $users_email;
            $user_name = $user_info->name;


            \Mail::send('mail.customer-verify-mail', $data, function($message) use ($user_email,$user_name) {

                $message->to($user_email,$user_name)->subject('Email verification');

            });

            \App\System::EventLogWrite('send-mail,customer-verification',json_encode($data));
            \Session::flash('message', 'Email confirmation Mail has been sent. Please check your mailbox!');
            return true;

        }catch (\Exception $e){

            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            \Session::flash('errormessage', $e->getMessage());
            return false;
        }

    }

    /********************************************
    ## CustomerRegistrationMail
     *********************************************/
    public static function CustomerRegistrationMail($users_id,$reset_url){



        $now = date('Y-m-d H:i:s');

        try{

            $user_info=\App\User::where('id',$users_id)->first();

            if(isset($user_info)){
                $users_id=$user_info->id;
                $users_type=$user_info->user_type;
                $users_email=$user_info->email;

            }else{
                return \Redirect::back()->with('message',"Invalid User ID!");
            }

            $data['user_info'] = $user_info;
            $data['reset_url'] = $reset_url;

            $user_email = $users_email;
            $user_name = $user_info->name;


            \Mail::send('mail.customer-reg-mail', $data, function($message) use ($user_email,$user_name) {

                $message->to($user_email,$user_name)->subject('Email verification');

            });

            \App\System::EventLogWrite('send-mail,customer-verification',json_encode($data));
            \Session::flash('message', 'Email confirmation Mail has been sent. Please check your mailbox!');
            return true;

        }catch (\Exception $e){

            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            \Session::flash('errormessage', $e->getMessage());
            return false;
        }

    }

    public function image(){
        return $this->morphOne(Image::class, 'imageable');
    }

}
