<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServicesCategory extends Model
{
    use SoftDeletes;
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_name',
        'category_name_est',
        'category_name_rus',
        'parent_id ',
        'description',
        'description_est',
        'description_rus',
        'status',
        'order'
    ];


    public function children()
    {
        return $this->hasMany('App\ServicesCategory', 'parent_id', 'id');
    }
    public function parent()
    {
        return $this->belongsTo('App\ServicesCategory', 'parent_id');
    }

    public function image(){
        return $this->morphOne(Image::class, 'imageable');
    }

    public function service_item() {
        return $this->hasMany('\App\ServiceItem', 'service_item_category_id');
    }
    public function service_item_pricing() {
        return $this->hasMany('\App\ServiceItemPricing', 'service_pricing_category_id');
    }

    /*public function service_catalogue() {
        return $this->hasMany('\App\ServiceCatalogue', 'services_categories_id', 'id');
    }*/



}
