<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Setting extends Model
{
    use SoftDeletes;
    protected $table = 'settings';


    protected $fillable = [
        'settings_name',
        'settings_data_type'
    ];

    public function settingmeta()
    {
        return $this->hasMany('App\SettingMeta','settings_id');
    }

    public static function getSettings($meta_key){
        $getSettingsData = Setting::leftJoin('settings_meta', 'settings_meta.settings_id', '=', 'settings.id')
            ->where('settings_name', '=', $meta_key)->get()->toArray();
        return $getSettingsData;
    }
}
