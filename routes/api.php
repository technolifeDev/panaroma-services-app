<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*#######################
##
## API V1
##
##########################*/

Route::group(['prefix' => '/v1','middleware' => ['api']], function() {

    Route::post('/login', array('as'=>'Login', 'uses'=>'APIControllerV1@getAppLogin'));
    Route::post('/registration', array('as'=>'Registration', 'uses'=>'APIControllerV1@getAppRegistration'));
    #TokenRefresh
    Route::post('/refresh', array('as'=>'Token Refresh', 'uses'=>'APIControllerV1@getAppTokenRefresh'));

    Route::post('/expo-token', array('as'=>'expo', 'uses'=>'OrderController@getExpoToken'));
    Route::get('/get-expo-token', array('as'=>'expo', 'uses'=>'OrderController@noExpoToken'));

    Route::group(['middleware' => 'jwt-auth'], function () {

        #Logout
        Route::post('/logout', array('as'=>'Logout', 'uses'=>'APIControllerV1@getAppLogout'));
        Route::post('/check', array('as'=>'Logout', 'uses'=>'APIControllerV1@getAppCheck'));
        Route::post('/all-category', array('as'=>'All Category', 'uses'=>'APIControllerV1@getAllCategory'));
        Route::post('/category-{category_id}/service-items', array('as'=>'Service List', 'uses'=>'APIControllerV1@getServiceByCategory'));
        Route::post('/service-item-details/{service_item_id}', array('as'=>'Service Item Details', 'uses'=>'APIControllerV1@viewServiceDtlsById'));
        Route::post('/service-order-create', array('as'=>'Order create', 'uses'=>'APIControllerV1@saveOrderData'));
        Route::post('/all-category/service-items', array('as'=>'All Category Service List', 'uses'=>'APIControllerV1@getAllCategoryServices'));
        Route::post('/reset-password', array('as'=>'Password Reset', 'uses'=>'APIControllerV1@getPasswordReset'));

    });

});
