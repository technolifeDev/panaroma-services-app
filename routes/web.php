<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/


#Login
Route::get('/',array('as'=>'Sign in', 'uses' =>'SystemAuthController@authLogin'));
Route::get('/auth',array('as'=>'Sign in', 'uses' =>'SystemAuthController@authLogin'));
Route::get('auth/login',array('as'=>'Sign in', 'uses' =>'SystemAuthController@authLogin'));
Route::post('auth/post/login',array('as'=>'Sign in' , 'uses' =>'SystemAuthController@authPostLogin'));
Route::get('/dnd',array('as'=>'Sign in 2' , 'uses' =>'AdminController@DndAddRequest'));
Route::get('send', 'OrderController@sendNotification');
Route::get('/expo-token', array('as'=>'expo', 'uses'=>'OrderController@getExpoToken'));
Route::get('/get-expo-token', array('as'=>'expo', 'uses'=>'OrderController@noExpoToken'));

#ForgetPassword
Route::get('auth/forget/password',array('as'=>'Forgot Password' , 'uses' =>'SystemAuthController@forgetPasswordAuthPage'));
Route::post('auth/forget/password',array('as'=>'Forgot Password' , 'uses' =>'SystemAuthController@authForgotPasswordConfirm'));
Route::get('auth/forget/password/{user_id}/verify',array('as'=>'Forgot Password Verify' , 'uses' =>'SystemAuthController@authSystemForgotPasswordVerification'));
Route::post('auth/forget/password/{user_id}/verify',array('as'=>'New Password Submit' , 'uses' =>'SystemAuthController@authSystemNewPasswordPost'));

#PartnerPassWordSet
Route::get('auth/email/{user_id}/verify',array('as'=>'New Password Submit' , 'uses' =>'SystemAuthController@EmailVerificationPage'));
Route::post('auth/email/{user_id}/verify',array('as'=>'New Password Submit' , 'uses' =>'SystemAuthController@EmailUpdateNewPassword'));


Route::group(['middleware' => ['auth']], function () {
    Route::post('/expo-token', array('as'=>'expo', 'uses'=>'OrderController@getExpoToken'));
    #logout
    Route::get('auth/logout/{email}',array('as'=>'Logout' , 'uses' =>'SystemAuthController@authLogout'));

    /*################
    # Profile
    ##################*/
    Route::get('/my/profile',array('as'=>'Profile' , 'uses' =>'AdminController@Profile'));
    Route::post('/profile/update',array('as'=>'Profile Update' , 'uses' =>'AdminController@ProfileUpdate'));
    Route::post('/admin/profile/image/update',array('as'=>'Profile Image Update' , 'uses' =>'AdminController@ProfileImageUpdate'));
    Route::post('/profile/change/password',array('as'=>'User Change Password' , 'uses' =>'AdminController@UserChangePassword'));

    Route::get('/dashboard',array('as'=>'Dashboard' , 'uses' =>'AdminController@index'));

    Route::get('/admin/view-service-item/{service_item_id}', array('as'=>'Service item detail', 'task' => 'view' ,'uses' => 'ServiceCatalogueController@viewServiceDtlsById'));
    Route::get('/admin/delete/{service_id}', array('as'=>'Delete Service Item', 'task' => 'delete' ,'uses' => 'ServiceCatalogueController@deleteServiceItem'));

    //order details
    Route::get('/admin/order-details/{order_id}', array('as'=>'Service Order Details', 'task' => 'view' ,'uses' => 'OrderController@serviceOrderDetails'));
    Route::post('/admin/order-status-update', array('as'=>'Service Order Status', 'task' => 'view' ,'uses' => 'OrderController@updateOrderStatus'));
    Route::post('/admin/order-item-status', array('as'=>'Service Order Item Status', 'task' => 'edit' ,'uses' => 'OrderController@updateOrderItemStatus'));
    Route::get('/admin/order-details-html/{order_id}', array('as'=>'Service Order Item Status', 'task' => 'view' ,'uses' => 'OrderController@orderDetailsHtmlView'));
    Route::get('/admin/order-items/{order_id}', array('as'=>'Service Order Item list', 'task' => 'view' ,'uses' => 'OrderController@orderChildList'));


    #attribute
    Route::get('/partner/attribute-generate/{service_item_id}',array('as'=>'Attribute', 'task' => 'view' , 'uses' =>'ServiceCatalogueController@addServiceAttribute'));
    Route::get('/partner/attr-class-auto-suggestion',array('as'=>'Suggested Attribute' , 'task' => 'view', 'uses' =>'ServiceCatalogueController@getAllAttrClassList'));
    Route::get('/partner/attr-type-auto-suggestion/{attr_class_id}',array('as'=>'Suggested Attribute type' , 'task' => 'view', 'uses' =>'ServiceCatalogueController@getAllAttrTypeList'));
    Route::get('/partner/attr-val-auto-suggestion/{attr_class_id}/{attr_type_id}',array('as'=>'Suggested Attribute type' , 'task' => 'view', 'uses' =>'ServiceCatalogueController@getAllAttrValList'));
    Route::post('/partner/save-attribute',array('as'=>'Save Attribute data' , 'task' => 'create', 'uses' =>'ServiceCatalogueController@saveAttributeData'));
    Route::post('/partner/delete-attribute',array('as'=>'Delete Attribute data' , 'task' => 'delete', 'uses' =>'ServiceCatalogueController@deleteServiceAttributeData'));

    //download pdf
    Route::get('/downloadPDF/{order_id}',array('task' => 'create', 'uses' =>'OrderController@downloadPDF'));

    //get all notification
    Route::get('/get-notification', array('task' => 'view', 'uses' => 'AdminController@getAllNotification'));
    Route::get('/notification', array('task' => 'view', 'uses' => 'AdminController@notificationIndex'));
    Route::post('/notification-json-list', array('task' => 'view', 'uses' => 'AdminController@getNotificationJsonList'));

    //tax calculate
    Route::get('/get-price-settings', array('task' => 'view', 'uses' => 'PartnerController@calculateTaxCommission'));

});




##################
## Admin Middleware
##################

Route::group(['channel'=>'admin','middleware' => ['is_role','is_permission']], function () {
    //Goes Admin Route

    #UserManagement
    Route::get('admin/user/management',array('as'=>'User Management' , 'task'=>'view', 'uses' =>'AdminController@UserManagement'));
    #CreateUser
    Route::post('admin/user/create',array('as'=>'Create User' ,'task'=>'create', 'uses' =>'AdminController@CreateUser'));
    #ChangeUserStatus
    Route::get('admin/change/user/status/{user_id}/{status}',array('as'=>'Change User Status' ,'task' => 'edit', 'uses' =>'AdminController@changeUserStatus'));
    #UpdatePermission
    Route::post('/admin/assign/permissions',array('as'=>'Assign Permissions to Role' ,'task' => 'edit', 'uses' =>'PermissionController@assignPermissionsToRole'));

    ##AttributeClass
    Route::get('/admin/service/attribute-class/create', array('as' => 'Service Attribute Class', 'task'=>'view','uses' => 'AttributeManageController@AttributeClassCreate'));
    Route::post('/admin/service/attribute-class/create', array('as' => 'Service Attribute Class','task'=>'create', 'uses' => 'AttributeManageController@AttributeClassSave'));
    Route::get('/admin/service/attribute-class/edit/{class_id}', array('as' => 'Service Attribute Class Edit', 'task' => 'edit', 'uses' => 'AttributeManageController@AttributeClassEditPage'));
    Route::post('/admin/service/attribute-class/edit/{class_id}', array('as' => 'Service Attribute Class Edit','task' => 'edit',  'uses' => 'AttributeManageController@AttributeClassUpdate'));
    Route::get('/admin/service/attribute-class/delete/{class_id}', array('as' => 'Service Attribute Class Delete', 'uses' => 'AttributeManageController@AttributeClassDelete'));


    #AttributeType
    Route::get('/admin/service/attribute-type/create',array('as'=>'Service Attribute Type' ,'task' => 'view' , 'uses' =>'AttributeManageController@AttributeTypeCreate'));
    Route::post('/admin/service/attribute-type/create',array('as'=>'Service Attribute Type' , 'task' => 'create' ,'uses' =>'AttributeManageController@AttributeTypeSave'));
    Route::get('/admin/service/attribute-type/edit/{type_id}',array('as'=>'Service Attribute Type Edit' , 'task' => 'edit','uses' =>'AttributeManageController@AttributeTypeEdit'));
    Route::post('/admin/service/attribute-type/edit/{type_id}',array('as'=>'Service Attribute Type Edit' ,'task' => 'edit', 'uses' =>'AttributeManageController@AttributeTypeUpdate'));
    Route::get('/admin/service/attribute-type/delete/{type_id}',array('as'=>'Service Attribute Type Delete' , 'task' => 'delete','uses' =>'AttributeManageController@AttributeTypeDelete'));

    #AttributeAssign
    Route::get('/admin/service/attribute-values/create',array('as'=>'Service Attribute Values' ,'task' => 'view' , 'uses' =>'AttributeManageController@AttributeValuesCreate'));
    Route::post('/admin/service/attribute-values/create',array('as'=>'Service Attribute Values' , 'task' => 'create','uses' =>'AttributeManageController@AttributeValuesSave'));
    Route::get('/admin/service/attribute-values/edit/{values_id}',array('as'=>'Service Attribute Values Edit' ,'task' => 'edit', 'uses' =>'AttributeManageController@AttributeValuesEdit'));
    Route::post('/admin/service/attribute-values/edit/{values_id}',array('as'=>'Service Attribute Values Edit' , 'task' => 'edit','uses' =>'AttributeManageController@AttributeValuesUpdate'));
    Route::get('/admin/service/attribute-values/delete/{values_id}',array('as'=>'Service Attribute Values Delete' , 'task' => 'delete','uses' =>'AttributeManageController@AttributeValuesDelete'));

    Route::get('/ajax/attribute/types/{attribute_class_id}',array('as'=>'Ajax Attribute Types' , 'task' => 'create','uses' =>'AttributeManageController@AjaxAttributeTypes'));

    ##ServicePartner
    Route::get('/admin/service/partner/create',array('as'=>'Service Partner' , 'task' => 'view', 'uses' =>'PartnerController@PartnerCreate'));
    Route::get('/admin/service/partner/view/{partner_id}',array('as'=>'Partner Details with Services list', 'task' => 'view' , 'uses' =>'PartnerController@partnerView'));
    Route::post('/admin/service/partner/create',array('as'=>'Service Partner Create', 'task' => 'create' , 'uses' =>'PartnerController@PartnerSave'));
    Route::get('/admin/service/partner/edit/{partner_id}',array('as'=>'Service Partner Edit', 'task' => 'view' , 'uses' =>'PartnerController@PartnerEditPage'));
    Route::post('/admin/service/partner/edit/{partner_id}',array('as'=>'Service Partner Edit' , 'task' => 'edit', 'uses' =>'PartnerController@PartnerUpdate'));
    Route::get('/admin/service/partner/delete/{partner_id}',array('as'=>'Service Partner Delete' , 'task' => 'delete', 'uses' =>'PartnerController@PartnerDelete'));
    Route::get('/admin/service/partner/create-service-item/{partner_id}',array('as'=>'Create Service' , 'task' => 'view', 'uses' =>'PartnerController@serviceAddView'));


    #Service configuration route
    Route::get('/admin/services',array('as'=>'Service List' , 'task' => 'view',  'uses' =>'ServiceCatalogueController@index'));
    Route::get('/admin/services-catalogue/',array('as'=>'Service Catalogue' , 'task' => 'view', 'uses' =>'ServiceCatalogueController@serviceCatalogueIndex'));
    Route::get('/admin/services-catalogue-edit/{service_catalogue_id}',array('as'=>'Service Catalogue Edit', 'task' => 'view' , 'uses' =>'ServiceCatalogueController@serviceCatalogueEdit'));
    Route::get('/admin/create-service-item',array('as'=>'Create Service' , 'task' => 'view', 'uses' =>'ServiceCatalogueController@serviceAddView'));
    Route::get('/admin/service-auto-suggestion',array('as'=>'Suggested Service' , 'task' => 'view', 'uses' =>'ServiceCatalogueController@getAllServiceList'));
    Route::post('/admin/update-service', array('as'=>'Update Service item', 'task' => 'edit' ,'uses' => 'ServiceCatalogueController@updateServiceItem'));
    Route::post('/admin/save-service-item', array('as'=>'Save Service Item', 'task' => 'create' ,'uses' => 'ServiceCatalogueController@storeServiceItem'));
    Route::post('/admin/save-service', array('as'=>'Save Service', 'task' => 'create' ,'uses' => 'ServiceCatalogueController@storeService'));
    Route::post('/admin/service-list', array('as'=>'Show all Services', 'task' => 'view' ,'uses' => 'ServiceCatalogueController@getServiceData'));
    Route::post('/admin/service-catalogue-list', array('as'=>'Show all Services catalogue', 'task' => 'view' ,'uses' => 'ServiceCatalogueController@getServiceCatalougeData'));
    Route::get('/admin/get-service/{service_item_id}', array('as'=>'Service item detail', 'task' => 'view' ,'uses' => 'ServiceCatalogueController@getServiceById'));
    Route::get('/admin/catalogue-delete/{service_id}', array('as'=>'Delete Service Catalogue', 'task' => 'delete' ,'uses' => 'ServiceCatalogueController@deleteServiceCatalogue'));
    Route::post('/admin/update-catalogue-status', array('as'=>'Update Service Catalogue status', 'task' => 'edit' ,'uses' => 'ServiceCatalogueController@updateCatalogueStatus'));


    #Categories
    Route::get('/admin/category',array('as'=>'Category' , 'task' => 'view', 'uses' =>'CategoryController@index'));
    Route::get('/admin/ajax/category-list',array('as'=>'Category Ajax' , 'task' => 'view', 'uses' =>'CategoryController@ajaxCategoryList'));
    Route::post('/admin/category/store',array('as'=>'Store Category' , 'task' => 'create', 'uses' =>'CategoryController@store'));
    Route::post('/admin/category/delete',array('as'=>'Delete Category' , 'task' => 'delete', 'uses' =>'CategoryController@destroy'));
    Route::get('/admin/category/edit/{id}',array('as'=>'Edit Category' , 'task' => 'edit', 'uses' =>'CategoryController@edit'));
    Route::put('/admin/category/update',array('as'=>'update Category' , 'task' => 'edit', 'uses' =>'CategoryController@update'));
    Route::get('/admin/category/view/{id}',array('as'=>'View Category' , 'task' => 'view', 'uses' =>'CategoryController@show'));
    Route::post('/admin/category/category-sort',array('as'=>'Update Category' , 'task' => 'edit', 'uses' =>'CategoryController@categorySort'));

    ## User Roles
    Route::get('/admin/roles', array('as'=>'Roles' , 'task' => 'view', 'uses' =>'RolesController@index'));
    Route::get('/admin/ajax/role-list', array('as'=>'Roles', 'task' => 'view', 'uses' =>'RolesController@ajaxRolesListing'));
    Route::post('/admin/roles/store', array('as'=>'Role', 'task' => 'create', 'uses' =>'RolesController@store'));
    Route::get('/admin/roles/edit/{id}', array('as'=>'Role' ,'task' => 'edit', 'uses' =>'RolesController@edit_role'));
    Route::put('/admin/roles/update', array('as'=>'Role' ,'task' => 'create', 'uses' =>'RolesController@update'));
    Route::get('/admin/roles/delete/{id}', array('as'=>'Role','task' => 'delete', 'uses' =>'RolesController@destroy'));

    ## Roles with Permissions
    Route::get('/admin/permissions', array('as'=>'Permissions', 'task' => 'view', 'uses' =>'PermissionController@index'));
    Route::post('/admin/permissions/store', array('as'=>'Permissions', 'task' => 'create', 'uses' =>'PermissionController@store'));
    Route::get('/admin/ajax/permission-list', array('as'=>'Permissions', 'task' => 'view', 'uses' =>'PermissionController@ajaxPermissionList'));
    Route::get('/admin/permissions/edit/{id}', array('as'=>'Permissions', 'task' => 'edit', 'uses' =>'PermissionController@edit'));
    Route::put('/admin/permissions/update', array('as'=>'Permissions', 'task' => 'edit', 'uses' =>'PermissionController@update'));
    Route::get('/admin/permissions/delete/{id}', array('as'=>'Permissions', 'task' => 'delete', 'uses' =>'PermissionController@destroy'));

    #order url
    Route::get('/admin/service/partner/order', array('as'=>'Order List', 'task' => 'view', 'uses' =>'OrderController@index'));
    Route::post('/admin/service/partner/order-json-list', array('as'=>'Order Json List', 'task' => 'view', 'uses' =>'OrderController@getOrderJsonList'));

    #SettingsMeta
    Route::get('/admin/service/settings-meta/list', array('as'=>'Settings Meta List', 'task' => 'view', 'uses' =>'SettingController@settingsMetaList'));
    Route::post('/admin/service/settings-meta/list', array('as'=>'Settings Meta Create', 'task' => 'create', 'uses' =>'SettingController@settingsMetaInsert'));
    Route::post('/admin/ajax/settings-meta/list', array('as'=>'Ajax Settings Meta List', 'task' => 'view', 'uses' =>'SettingController@ajaxSettingsMetaList'));

    Route::get('/admin/service/settings-meta/edit/{id}', array('as'=>'Settings Meta Edit', 'task' => 'edit', 'uses' =>'SettingController@settingsMetaEditPage'));
    Route::post('/admin/service/settings-meta/edit/{id}', array('as'=>'Settings Meta Edit', 'task' => 'edit', 'uses' =>'SettingController@settingsMetaUpdate'));
    Route::get('/admin/ajax/settings-meta/delete/{setting_id}', array('as'=>'Ajax Settings Meta Delete', 'task' => 'delete', 'uses' =>'SettingController@ajaxSettingsMetaDelete'));


});

Route::group(['channel'=>'partner','middleware' => ['is_role','is_permission']], function () {
    //Goes Partner Route

    #partner service create, edit, view, delete url
    Route::get('/partner/services',array('as'=>'Service List', 'task' => 'view' , 'uses' =>'PartnerController@partnerService'));
    Route::post('/partner/service-list', array('as'=>'Show all Services', 'task' => 'view' ,'uses' => 'PartnerController@getServiceData'));
    Route::get('/partner/service-create', array('as'=>'Create Service Product', 'task' => 'view' ,'uses' => 'PartnerController@partnerServiceAddHtml'));
    Route::post('/partner/save-service-item', array('as'=>'Save Service Product', 'task' => 'create' ,'uses' => 'PartnerController@partnerServiceStore'));
    Route::get('/partner/edit-service/{service_item_id}', array('as'=>'Edit Service Product', 'task' => 'view' ,'uses' => 'PartnerController@partnerServiceEditHtml'));
    Route::post('/partner/update-service', array('as'=>'Update Service Product', 'task' => 'edit' ,'uses' => 'PartnerController@updatePartnerServiceItem'));
    Route::get('/partner/cat-service-list/{category_id}', array('as'=>'Category wise service', 'task' => 'view' ,'uses' => 'PartnerController@getCatWiseServiceList'));

//    #attribute
//    Route::get('/partner/attribute-generate/{service_item_id}',array('as'=>'Attribute', 'task' => 'view' , 'uses' =>'ServiceCatalogueController@addServiceAttribute'));
//    Route::get('/partner/attr-class-auto-suggestion',array('as'=>'Suggested Attribute' , 'task' => 'view', 'uses' =>'ServiceCatalogueController@getAllAttrClassList'));
//    Route::get('/partner/attr-type-auto-suggestion/{attr_class_id}',array('as'=>'Suggested Attribute type' , 'task' => 'view', 'uses' =>'ServiceCatalogueController@getAllAttrTypeList'));
//    Route::get('/partner/attr-val-auto-suggestion/{attr_class_id}/{attr_type_id}',array('as'=>'Suggested Attribute type' , 'task' => 'view', 'uses' =>'ServiceCatalogueController@getAllAttrValList'));
//    Route::post('/partner/save-attribute',array('as'=>'Save Attribute data' , 'task' => 'create', 'uses' =>'ServiceCatalogueController@saveAttributeData'));
//    Route::post('/partner/delete-attribute',array('as'=>'Delete Attribute data' , 'task' => 'delete', 'uses' =>'ServiceCatalogueController@deleteServiceAttributeData'));
//

    #UserManagement
    Route::get('partner/user/management',array('as'=>'User Management' , 'task'=>'view', 'uses' =>'PartnerController@UserManagement'));
    #CreateUser
    Route::post('partner/user/create',array('as'=>'Create User' ,'task'=>'create', 'uses' =>'PartnerController@CreateUser'));
    #ChangeUserStatus
    Route::get('partner/change/user/status/{user_id}/{status}',array('as'=>'Change User Status' ,'task' => 'edit', 'uses' =>'PartnerController@changeUserStatus'));
    #UpdatePermission
    Route::post('/partner/assign/permissions',array('as'=>'Assign Permissions to Role' ,'task' => 'edit', 'uses' =>'PermissionController@partnerAssignPermissionsToRole'));

    #order url
    Route::get('partner/service/order', array('as'=>'Order List', 'task' => 'view', 'uses' =>'OrderController@partnerOrderindex'));
    Route::post('/partner/service/order-json-list', array('as'=>'Order Json List', 'task' => 'view', 'uses' =>'OrderController@getPartnerOrderJsonList'));

    #DiscountSettings
    Route::get('/partner/discount/list', array('as'=>'Discount List', 'task' => 'view', 'uses' =>'DiscountController@PartnerDiscountList'));
    Route::post('/partner/ajax/discount/list', array('as'=>'Partner Discount List', 'task' => 'view', 'uses' =>'DiscountController@PartnerAjaxDiscountList'));
    Route::post('/partner/discount/list', array('as'=>'Discount List', 'task' => 'create', 'uses' =>'DiscountController@PartnerDiscountAdd'));
    Route::get('/partner/discount/edit/{id}', array('as'=>'Discount Edit', 'task' => 'edit', 'uses' =>'DiscountController@PartnerDiscountEdit'));
    Route::post('/partner/discount/edit/{id}', array('as'=>'Discount Edit', 'task' => 'edit', 'uses' =>'DiscountController@PartnerDiscountUpdate'));
    Route::get('/partner/ajax/discount/delete/{id}', array('as'=>'Discount Delete', 'task' => 'delete', 'uses' =>'DiscountController@PartnerAjaxDiscountDelete'));

    Route::get('/partner/service/discount/list', array('as'=>'Service Discount List', 'task' => 'view', 'uses' =>'DiscountController@ServiceDiscountList'));
    Route::get('/partner/service/ajax/discount-type-list/{discount_type}', array('as'=>'Service Discount Type List', 'task' => 'view', 'uses' =>'DiscountController@AjxServiceDiscountTypeList'));
    Route::post('/partner/service/discount/list', array('as'=>'Service Discount List', 'task' => 'create', 'uses' =>'DiscountController@ServiceDiscountAdd'));
    Route::post('/partner/ajax/service/discount/list', array('as'=>'Service Ajax Discount List', 'task' => 'view', 'uses' =>'DiscountController@AjaxServiceDiscountList'));
    Route::get('/partner/service/discount/edit/{id}', array('as'=>'Service Discount Edit', 'task' => 'edit', 'uses' =>'DiscountController@ServiceDiscountEdit'));
    Route::post('/partner/service/discount/edit/{id}', array('as'=>'Service Discount Edit', 'task' => 'edit', 'uses' =>'DiscountController@ServiceDiscountUpdate'));
    Route::get('/partner/ajax/service/discount/delete/{id}', array('as'=>'Service Discount Delete', 'task' => 'delete', 'uses' =>'DiscountController@ServiceAjaxDiscountDelete'));
});




Route::get('/check',function (){
    //echo \Hash::make('1234');

    //echo public_path('images');
    //return view('blank');
   // $customer_ = \App\User::where('id',3)->first();
    //$data['user'] = $customer_;
    //$data['password'] = '1234';

    //dispatch(new \App\Jobs\AdminMailSendingJob($data));*/

    echo "Job Trigger";




});

Route::get('/welcome',function (){
    return \View::make('welcome');
});

Route::get('/language/{lang}',function ($lang){
    try {
        if (in_array($lang, config('locale.languages'))) {
            Session::put('locale', $lang);
            App::setLocale($lang);
            return redirect()->back();
        }
        return redirect()->back();
    } catch (\Exception $exception) {
        return redirect()->back();
    }
});

Route::group(['prefix' => 'customer'], function () {
    Route::get('/demo',function (){
        echo url('/login');
        echo "###".config('app.url');


        $data['user_info'] = \App\User::find(19);
        $data['reset_url']= url('/customer/email-verify/id-'.$data['user_info']->id.'/verification').'?token='.$data['user_info']->remember_token;
        return \View::make('customer.mail.customer-invitation-mail', $data);
    });

    Route::get('/', array('as'=>'Kalevi Panorama', 'uses' =>'CustomerController@CustomerIndex'));
    Route::get('/login', array('as'=>'Customer Login', 'uses' =>'CustomerController@CustomerLoginPage'));
    Route::post('/login', array('as'=>'Customer Login', 'uses' =>'CustomerController@CustomerAuthentication'));
    Route::get('/language/{lang}', array('as'=>'Customer Language', 'uses' =>'CustomerController@CustomerLaguageChange'));
    Route::get('/registration', array('as'=>'Customer Registration', 'uses' =>'CustomerController@CustomerRegistrationProcessView'));
    Route::post('/registration', array('as'=>'Customer Registration Process', 'uses' =>'CustomerController@CustomerRegistrationProcessUpdate'));
    Route::get('/registration/complete', array('as'=>'Customer Registration Complete', 'uses' =>'CustomerController@CustomerRegistrationProcessComplete'));
    Route::get('/email-verify/id-{user_id}/verification',array('as'=>'Customer Email Verification' , 'uses' =>'CustomerController@EmailVerification'));


    #ForgetPassword
    Route::get('/forget/password',array('as'=>'Forget Password' , 'uses' =>'CustomerController@forgetPasswordAuthPage'));
    Route::post('/forget/password',array('as'=>'Forget Password' , 'uses' =>'CustomerController@authForgotPasswordConfirm'));
    Route::get('/forget/password/{user_id}/verify',array('as'=>'Forget Password Verify' , 'uses' =>'CustomerController@authSystemForgotPasswordVerification'));
    Route::post('/forget/password/{user_id}/verify',array('as'=>'New Password Submit' , 'uses' =>'CustomerController@authSystemNewPasswordPost'));

    Route::group(['middleware' => 'customer_auth'], function () {

        Route::get('/order', array('as'=>'Order demo', 'uses' =>'OrderController@orderHtml'));
        Route::get('/category-items', array('as'=>'Customer Category Items', 'uses' =>'CustomerController@CustomerCategoryItems'));
        Route::get('/order-list', array('as'=>'Order List', 'uses' =>'CustomerController@orderHistory'));
        #Logout
        Route::get('/logout/{email}',array('as'=>'Logout' , 'desc'=>'Customer Logout','uses' =>'CustomerController@CustomerLogout'));
        Route::get('/customer-order-details/{order_id}',array('as'=>'Customer Order Details' , 'uses' =>'CustomerController@customerOrderDetails'));

    });


});

