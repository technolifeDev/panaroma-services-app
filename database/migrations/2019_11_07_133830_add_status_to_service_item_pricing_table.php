<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusToServiceItemPricingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_item_pricing', function (Blueprint $table) {
            //
            $table->enum('status', ['active', 'inactive','draft','pending']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_item_pricing', function (Blueprint $table) {
            //
            Schema::dropIfExists('service_item_pricing');
        });
    }
}
