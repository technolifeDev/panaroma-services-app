<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateErrorLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql2_logger')->defaultStringLength(191);
        Schema::connection('mysql2_logger')->create('error_log', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('error_client_ip');
            $table->string('error_user_id');
            $table->string('error_request_url');
            $table->longText('error_data');
            $table->string('error_browser');
            $table->string('error_platform');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('error_log');
    }
}
