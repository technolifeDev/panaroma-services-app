<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        Schema::create('service_order_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('service_orders_id');
            $table->bigInteger('orders_items_catalogue_id');
            $table->bigInteger('orders_items_category_id');
//            $table->bigInteger('orders_items_class_id');
//            $table->bigInteger('orders_items_type_id');
//            $table->string('orders_items_type_value');
            $table->decimal('orders_items_type_cost',8,2);
            $table->enum('order_items_status', ['accepted','cancel','rejected','processed','pending','completed']);
            $table->string('created_by');
            $table->softDeletes();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_order_items');
    }
}
