<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        Schema::create('service_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('service_order_customer_id');
            $table->bigInteger('service_order_partner_id');
            $table->bigInteger('service_order_config_id');
            $table->string('service_order_customer_address');
            $table->decimal('service_order_amount',8,2);
            $table->decimal('service_order_tax_amount',8,2);
            $table->decimal('service_order_grand_total',8,2);
            $table->enum('service_order_status', ['accepted','cancel','rejected']);
            $table->enum('service_order_deliver_status', ['processed','pending','completed']);
            $table->enum('service_order_payment_status', ['paid','unpaid','partly-paid']);
            $table->string('created_by');
            $table->softDeletes();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_orders');
    }
}
