<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql2_logger')->defaultStringLength(191);
        Schema::connection('mysql2_logger')->create('event_log', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('event_client_ip');
            $table->string('event_user_id');
            $table->string('event_request_url');
            $table->string('event_type');
            $table->longText('event_data');
            $table->string('event_browser');
            $table->string('event_platform');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('event_log');
    }
}
