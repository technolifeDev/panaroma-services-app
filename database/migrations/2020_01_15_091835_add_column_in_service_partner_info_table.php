<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnInServicePartnerInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_partner_info', function (Blueprint $table) {
            $table->string('partner_name_est')->after('partner_name')->nullable();
            $table->string('partner_name_rus')->after('partner_name_est')->nullable();
            $table->string('partner_address_est')->after('partner_address')->nullable();
            $table->string('partner_address_rus')->after('partner_address_est')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_partner_info', function (Blueprint $table) {
            //
        });
    }
}
