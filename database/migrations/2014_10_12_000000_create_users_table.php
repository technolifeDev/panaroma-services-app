<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::defaultStringLength(191);
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('name_slug');
            $table->string('email')->nullable()->unique();
            $table->string('user_type')->nullable();
            $table->string('user_role')->nullable();
            $table->string('password')->nullable();
            $table->string('personal_code')->index()->nullable();
            $table->string('user_profile_image')->nullable();
            $table->integer('login_status')->nullable();
            $table->string('status')->nullable();
            $table->integer('company_id')->nullable();
            $table->rememberToken();
            $table->timestamp('last_login');
            $table->softDeletes();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
