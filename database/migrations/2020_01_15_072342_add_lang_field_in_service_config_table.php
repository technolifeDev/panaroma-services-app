<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLangFieldInServiceConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_config', function (Blueprint $table) {
            //
            $table->string('service_pricing_title_est')->after('service_pricing_title')->nullable();
            $table->string('service_pricing_title_rus')->after('service_pricing_title')->nullable();
            $table->mediumText('service_pricing_description_est')->after('service_pricing_description')->nullable();
            $table->mediumText('service_pricing_description_rus')->after('service_pricing_description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_config', function (Blueprint $table) {
            //
        });
    }
}
