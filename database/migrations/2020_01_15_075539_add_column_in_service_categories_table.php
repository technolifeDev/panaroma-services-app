<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnInServiceCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('services_categories', function (Blueprint $table) {
            $table->string('category_name_est')->after('category_name')->nullable();
            $table->string('category_name_rus')->after('category_name_est')->nullable();
            $table->text('description_est')->after('description')->nullable();
            $table->text('description_rus')->after('description_est')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_categories', function (Blueprint $table) {
            //
        });
    }
}
