<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceItemPricingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        Schema::create('service_item_pricing', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('service_pricing_item_id')->index()->unsigned();
            $table->bigInteger('service_pricing_category_id')->index()->unsigned();
//            $table->bigInteger('service_pricing_catalogue_id')->index()->unsigned();
            $table->bigInteger('service_pricing_partner_id')->index()->unsigned();
            $table->string('service_pricing_title');
            $table->mediumText('service_pricing_description');
            $table->string('service_pricing_currency_unit'); //Dollar,euro
            $table->decimal('service_pricing_unit_cost_amount',8,2);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_item_pricing');
    }
}
