<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql2_logger')->defaultStringLength(191);
        Schema::connection('mysql2_logger')->create('api_log', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('api_client_ip');
            $table->string('api_user_id');
			$table->string('api_url');
			$table->longText('api_request');
			$table->string('api_response_type');
            $table->longText('api_response');
            $table->string('api_browser');
            $table->string('api_platform');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('api_log');
    }
}
