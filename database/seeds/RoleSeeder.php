<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Role Types
         *
         */
        $RoleItems = [
            [
                'name'        => 'admin',
                'slug'        => 'admin',
                'description' => 'Admin Role',
                'level'       => 5,
            ],
            [
                'name'        => 'partner',
                'slug'        => 'partner',
                'description' => 'Partner Role',
                'level'       => 1,
            ],
            [
                'name'        => 'customer',
                'slug'        => 'customer',
                'description' => 'Customer Role',
                'level'       => 1,
            ],
        ];

        /*
         * Add Role Items
         *
         */
        $now = date('Y-m-d H:i:s');
        foreach ($RoleItems as $RoleItem) {
            $newRoleItem = \App\Role::where('slug', '=', $RoleItem['slug'])->first();
            if ($newRoleItem === null) {
                $newRoleItem = \App\Role::create([
                    'name'          => $RoleItem['name'],
                    'slug'          => $RoleItem['slug'],
                    'description'   => $RoleItem['description'],
                    'level'         => $RoleItem['level'],
                    'created_at'   => $now,
                    'updated_at'   => $now,
                ]);
            }
        }
        echo "\e[32mSeeding:\e[0m DefaultRoleItemsTableSeeder\r\n";
        /*
         * Attach Role
         *
         */
        $attachrole = \App\User::where('email', 'admin@technolife.ee')->first();
        $roles = \App\Role::where('slug', 'admin')->first();
        if(isset($attachrole->id)&&isset($roles->id)){
            $attachrole->roles()->attach($roles->id,['level'=>5]);
        }
        echo "\e[32mSeeding:\e[0m AdminRoleSeeder\r\n";


    }
}
