<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = \App\User::where('email', 'admin@technolife.ee')->first();

        if(!isset($admin->id)){
            \DB::table('users')->insert([
                'name' => 'Administrator',
                'name_slug' => 'administrator',
                'email' => 'admin@technolife.ee',
                'user_type' => 'admin',
                'user_role' => 'admin',
                'status' => 'active',
                'password' => bcrypt('1234'),
            ]);
        }

        echo "\e[32mSeeding:\e[0m Admin Creation done.\r\n";

    }
}
