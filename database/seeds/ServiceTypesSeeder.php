<?php

use Illuminate\Database\Seeder;

class ServiceTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Role Types
         *
         */
        $ServiceTypes = [
            [
                'frequency_name'        => 'Mothly',
                'frequency_value'        => 'monthly',
            ],
            [
                'frequency_name'        => 'Weekly',
                'frequency_value'        => 'weekly',
            ],
            [
                'frequency_name'        => 'Daily',
                'frequency_value'        => 'daily',
            ],
            [
                'frequency_name'        => 'Once',
                'frequency_value'        => 'once',
            ]
        ];

        /*
         * Add Role Items
         *
         */
        $now = date('Y-m-d H:i:s');
        foreach ($ServiceTypes as $type) {
            $newServiceType = \App\ServiceFrequency::where('frequency_name', '=', $type['frequency_name'])->first();
            if ($newServiceType === null) {
                \App\ServiceFrequency::create([
                    'frequency_name' => $type['frequency_name'],
                    'frequency_value'           => $type['frequency_value'],
                ]);
            }
        }
        echo "\e[32mSeeding:\e[0m DefaultServiceTypesSeeder\r\n";

    }
}
