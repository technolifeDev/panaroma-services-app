
$(function(){
    var site_url = $('.site_url').val();

	// globaly variable
	var is_refreshed = 1;


    $('[data-toggle="tooltip"]').tooltip();


	//----------------------------------Language dropdown------------------------------------
	//@Momit

	var language = localStorage.getItem('language');
	if($.trim(language)=="" || $.trim(language)=="undefined"){
		var default_language = $('.language-dropdown:first').html();
		localStorage.setItem('language', default_language);
		language = localStorage.getItem('language');
	}


	// keep track of selected item and category and steps


	//--------------- initialize 4 variables in local storage------------------
	//@momit
	var selected_category = localStorage.getItem('selected_category');
	if($.trim(selected_category)=="" || $.trim(selected_category)=="undefined"){
		var sel_cat = $('.category-button-group').find('.active').attr('id')
		localStorage.setItem('selected_category', sel_cat);
	}
	var selected_id = localStorage.getItem('selected_id');
	if($.trim(selected_id)=="" || $.trim(selected_id)=="undefined"){
		localStorage.setItem('selected_id', "");
	}
	var selected_date = localStorage.getItem('selected_date');
	if($.trim(selected_date)=="" || $.trim(selected_date)=="undefined"){
		localStorage.setItem('selected_date', "");
	}
	var selected_time = localStorage.getItem('selected_time');
	if($.trim(selected_time)=="" || $.trim(selected_time)=="undefined"){
		localStorage.setItem('selected_time', "");
	}
	var selected_urgent = localStorage.getItem('selected_urgent');
	if($.trim(selected_urgent)=="" || $.trim(selected_urgent)=="undefined"){
		localStorage.setItem('selected_urgent', "");
	}
	var selected_date_html = localStorage.getItem('selected_date_html');
	if($.trim(selected_date_html)=="" || $.trim(selected_date_html)=="undefined"){
		localStorage.setItem('selected_date_html', "");
	}
	// step: 1=cat (select category only)
	// step: 2=date_select (select date popup)
	// step: 3=selected_item (selected the date)
	// step: 4=agreement (popup of agreemet of a service to order)
	// step: 5=service_details (details of the service page)

	var order_step = localStorage.getItem('order_step');
	if($.trim(order_step)=="" || $.trim(order_step)=="undefined"){
		localStorage.setItem('order_step', 1);
	}


	var selected_category = localStorage.getItem('selected_category');
	var selected_id 	  = localStorage.getItem('selected_id');
	var selected_date 	  = localStorage.getItem('selected_date');
	var selected_time 	  = localStorage.getItem('selected_time');
	var selected_urgent   = localStorage.getItem('selected_urgent');
	var selected_date_html= localStorage.getItem('selected_date_html');
	var order_step 		  = localStorage.getItem('order_step');


	//alert(selected_category+"---"+selected_id+"---"+selected_date+"---"+order_step);
	//the functionality written in below part of this js page
	//-----------------------------------------------------------------------------



	$('#dropdownMenuButton').html(language);
	$('.language-dropdown').on('click', function (e) {
		var selected_language = $(this).html();
		$('#dropdownMenuButton').html(selected_language);
		localStorage.setItem('language', selected_language);
	})

	/*-------------------------------- end of language ---------------------------------*/


    //select a service item
    $('.book_service').on("click", function(e){
        e.preventDefault();
        e.stopPropagation();
        getCurrentHour()
        selected_id =$(this).parents('.items').find('.service_item_id').val();
		//alert(selected_id)
        startDay = 0;
        orderCalender(startDay);
        $('#dateModal').find('.selected_service_id').val(selected_id);
        $('#dateModal').modal();

		//customer in this state: 2=date_select (select date popup), show the popup
		localStorage.setItem('selected_id', selected_id);
		localStorage.setItem('order_step', 2);
    });

    // after confirmation from date selection popup
    $('#confirm').on("click", function(){
		// customer in this state : selected_item (selected the date)
		localStorage.setItem('order_step', 3);

	   if($('#dateModal').find('.order_time').val() == '' || $('#dateModal').find('.order_date').val() == ''){
	       $(".selected_order_date").html("<span style='color:#d22b2bb0'>Please Select specific date & time.</span>")
            return false;
        }

        // if($('#dateModal').find('.order_hour_from').val() > $('#dateModal').find('.order_hour_to').val()){
        //     return false;
        // }



        $('#'+selected_id).find('.selected_order_date').val($('#dateModal').find('.order_date').val());
        $('#'+selected_id).find('.selected_order_time').val($('#dateModal').find('.order_time').val());
        $('#'+selected_id).find('.is_urgent').val($('#dateModal').find('.is_urgent_selected').val());
        // $('#'+selected_id).find('.is_urgent').addClass('d-none');
        $('#dateModal').modal('hide');
        //selected item designing
        $('#'+selected_id).addClass('no-top-padding');
        $('#'+selected_id).addClass('selected-item');
        $('#'+selected_id).find('.package').addClass('no-top-padding');

        //add the remove button
        $('#'+selected_id).find('.package').prepend('<div class="remove-selection"> <span class="glyphicon" id="remove_selection"><img  class="img-20px" src="'+site_url+'/front-end/images/x.png"/></span></div>');
        // get and set selected date into local storage
        selected_date = $('#dateModal').find('.order_date').val();
		selected_time = $('#dateModal').find('.order_time').val();
		selected_urgent = $('#dateModal').find('.is_urgent_selected').val();
		selected_date_html = $('#selected_date_time').html();

		localStorage.setItem('selected_date', selected_date);
		localStorage.setItem('selected_time', selected_time);
		localStorage.setItem('selected_urgent', selected_urgent);
		localStorage.setItem('selected_date_html', selected_date_html);



        $('#'+selected_id).find('.uss-security-logo').append('<div class="selected-date">'+selected_date_html+'</div>');
        //show confirm button
        $('#'+selected_id).find('.confirm_btn').removeClass('d-none');
        // hide book service button
        $('#'+selected_id).find('.book_service').addClass('d-none');

		//alert(selected_id)

        // disable all other items
        $('.items').each(function(){
            item_id = $(this).attr('id');
            if(item_id != selected_id){
                $(this).addClass('low-opacity');
                $(this).find('.more_info').attr('disabled','disabled');
                $(this).find('.book_service').attr('disabled','disabled');
            }
        })

        // remove the selecetd service item and open all items for selection
        $('#remove_selection').on("click", function(){
			// customer in this state : selected_item removed and 1st state

			localStorage.setItem('selected_id', "");
			localStorage.setItem('selected_date', "");
			localStorage.setItem('selected_time', "");
			localStorage.setItem('selected_urgent', "");
			localStorage.setItem('selected_date_html', "");
			localStorage.setItem('order_step', 1);

            //selected item designing
            $('#'+selected_id).removeClass('no-top-padding');
            $('#'+selected_id).removeClass('selected-item');
            $('#'+selected_id).find('.package').removeClass('no-top-padding');

            //add the remove button
            $('#'+selected_id).find('.remove-selection').remove();
            $('#'+selected_id).find('.selected-date').remove();

            $('#'+selected_id).find('.confirm_btn').addClass('d-none');
            // hide book service button
            $('#'+selected_id).find('.book_service').removeClass('d-none');

            // remove disable for each items
            $('.items').each(function(){
                item_id = $(this).attr('id');
                if(item_id != selected_id){
                    $(this).removeClass('low-opacity');
                    $(this).find('.more_info').removeAttr('disabled','disabled');
                    $(this).find('.book_service').removeAttr('disabled','disabled');
                }
            })
        });


		if($('#items-subcontent').hasClass('d-none')){
			$('#items-subcontent').removeClass('d-none');
			$('#detail-subcontent').addClass('d-none');
		}

	});

    // after click on confirmation button from selected service item
    $('.confirm_btn').on("click", function(){
        $('#agreementModal').modal('show');
        if(!selected_id){
            selected_id =$(this).parents('.items').find('.service_item_id').val();
        }
		// customer in this state : agreement (popup of agreemet of a service to order)
			localStorage.setItem('order_step', 4);
    });

    // when oreder is confirmed after check on agreement
    $('#item_terms').on("click", function(){
        if($("#agreement_checkbox").is(':checked')){
            // submit order and give confirmation
            $('#items-subcontent').addClass('d-none');
            $('#agreementModal').modal('hide');
            var lng = $('.lng').val();
            $.post(site_url+'/api/v1/service-order-create', {
                token: $('.albela').val(),
                service_item_id : $('#'+selected_id).find('.service_item_id').val(),
                order_date : $('#'+selected_id).find('.selected_order_date').val(),
                order_time : $('#'+selected_id).find('.selected_order_time').val(),
                is_urgent : $('#'+selected_id).find('.is_urgent').val()},
                function (data) {
                if(data.status == 'success'){
                    $('#order-subcontent').find('.service_item_logo').attr('src', data.service_details.service_image)
                    if(lng == 'est'){
                        $('#order-subcontent').find('.package').html(data.service_details.service_pricing_title_est ? data.service_details.service_pricing_title_est : data.service_details.service_pricing_title)
                    } else if(lng == 'rus'){
                        $('#order-subcontent').find('.package').html(data.service_details.service_pricing_title_rus ? data.service_details.service_pricing_title_rus : data.service_details.service_pricing_title)
                    } else {
                        $('#order-subcontent').find('.package').html(data.service_details.service_pricing_title)
                    }
                    $('#order-subcontent').find('.rate').html(data.service_details.service_pricing_unit_cost_amount+' '+ data.service_details.service_pricing_currency_unit)
                    $('#order-subcontent').find('.per-month').html('/' + data.service_details.service_frequency);
                    $('#order-subcontent').removeClass('d-none');
                    setTimeout(function(){ $('#order_complete_btn').trigger('click'); }, 10000)
                }

            }, 'json')
            // remove the selection of service item
            $('#remove_selection').trigger('click');
        } else {
            // not checked then give error message
            $('.message').addClass("danger");
            $('.message').html("Please check first");
        }
    });

	$('.category-button').mouseenter(function() {
	  $(this).find('img').addClass('gray-svg');
	});
	$('.category-button').mouseleave(function() {
	   $(this).find('img').removeClass('gray-svg');
	});


    // back to service items again after order confirmation
    $('#order_complete_btn').on("click", function(){
        $('#items-subcontent').removeClass('d-none');
        $('#order-subcontent').addClass('d-none');

		// clear local storage
		localStorage.setItem('selected_id', "");
		localStorage.setItem('selected_date', "");
		localStorage.setItem('selected_time', "");
		localStorage.setItem('selected_urgent', "");
		localStorage.setItem('selected_date_html', "");
		//alert(order_step)
		localStorage.setItem('order_step', 1);
		$('.category-button:first').trigger('click');

    });

    // details of items
    $('.more_info').on("click", function(){
        var service_item_details = $(this).parents('.items').find('.service_item_id').val();
        var lng = $('.lng').val();
        $.post(site_url+'/api/v1/service-item-details/' + service_item_details, {token : $('.albela').val()}, function (data) {
            if(lng == 'rus'){
                $('.packg_title').html(data.serviceDetails.service_pricing_title_rus ? data.serviceDetails.service_pricing_title_rus : data.serviceDetails.service_pricing_title);
                $('.package_details').html(data.serviceDetails.service_pricing_description_rus ? data.serviceDetails.service_pricing_description_rus : data.serviceDetails.service_pricing_description);
                $('.packg_dtl_cat').html(data.serviceDetails.category_name_rus ? data.serviceDetails.category_name_rus : data.serviceDetails.category_name);
                $('.packg_dtl_prtnr').html(data.serviceDetails.partner_name_rus ? data.serviceDetails.partner_name_rus : data.serviceDetails.partner_name_rus);
            } else if(lng == 'est'){
                $('.packg_title').html(data.serviceDetails.service_pricing_title_est ? data.serviceDetails.service_pricing_title_est : data.serviceDetails.service_pricing_title);
                $('.package_details').html(data.serviceDetails.service_pricing_description_est ? data.serviceDetails.service_pricing_description_est : data.serviceDetails.service_pricing_description);
                $('.packg_dtl_cat').html(data.serviceDetails.category_name_est ? data.serviceDetails.category_name_est : data.serviceDetails.category_name);
                $('.packg_dtl_prtnr').html(data.serviceDetails.partner_name_est ? data.serviceDetails.partner_name_est : data.serviceDetails.partner_name);
            } else{
                $('.packg_title').html(data.serviceDetails.service_pricing_title);
                $('.package_details').html(data.serviceDetails.service_pricing_description);
                $('.packg_dtl_cat').html(data.serviceDetails.category_name);
                $('.packg_dtl_prtnr').html(data.serviceDetails.partner_name);
            }

            $('.packg_dtl_price').html(data.serviceDetails.service_pricing_unit_cost_amount +  ' ' + data.serviceDetails.service_pricing_currency_unit);
            if(lng == 'rus') {
                $('.packg_dtl_frequncy').html('/' + data.serviceDetails.service_frequency_rus);
            } else if(lng == 'est') {
                $('.packg_dtl_frequncy').html('/' + data.serviceDetails.service_frequency_est);
            } else {
                $('.packg_dtl_frequncy').html('/' + data.serviceDetails.service_frequency);
            }
            $('.dtl_partner_logo').attr('src', site_url+ '/service_images/service_item_images/' + data.serviceDetails.service_images[0].image_name);
            var service_img_all = '';

            if(data.serviceDetails.service_images.length > 1){
                $(data.serviceDetails.service_images).each(function (i,j) {
                    service_img_all += '<img src="'+ site_url+ '/service_images/service_item_images/' + j.image_name +'" class="thumbImage">'
                });
            }
            $('.myDiv').html(service_img_all)

        }, 'json')
        $('#items-subcontent').addClass('d-none');
        $('#detail-subcontent').removeClass('d-none');
        $('#order-subcontent').addClass('d-none');
		// customer currently in service details page
		localStorage.setItem('selected_id', service_item_details);
		localStorage.setItem('order_step', 5);
    });

    // back to items list from item details
    $('.back_item_list').on("click", function(){
        $('#items-subcontent').removeClass('d-none');
        $('#detail-subcontent').addClass('d-none');


		if(selected_urgent =="" && selected_date==""){ // if no selected item
			localStorage.setItem('order_step', 1);
		}
		else{// if item is selected to order
			localStorage.setItem('order_step', 3);
		}

    });


    //urgent button on select
    $('#dateModal').on('click', '.urgent_btn', function (e) {
        if($('#dateModal').find('.is_urgent_selected').val() == 0){
            $(this).addClass('date-type-selected');
            $('#dateModal').find('.is_urgent_selected').val(1);
        } else {
            $(this).removeClass('date-type-selected');
            $('#dateModal').find('.is_urgent_selected').val(0);
        }
    })

	// $('.date-type').on( 'click',  function() {
	// 	$('.date-type').removeClass('date-type-selected');
    //     $(this).addClass('date-type-selected');
    // });


    $('.saveOrder').on('click', function (e) {
        e.preventDefault();
        $.post(site_url+'/api/v1/service-order-create', $('.orderForm').serialize(), function (data) {
            alert(data.successMessage);
        }, 'json')
    });

    $('.viewItemDetails').on('click', function (e) {
        e.preventDefault();
        var service_item_details = $('.service_item_details').val();
        $.post(site_url+'/api/v1/service-item-details/' + service_item_details, $('.detailsForm').serialize(), function (data) {
            $('.viewDetails').html(JSON.stringify(data))
        }, 'json')
    })




	//---------------retain the curront item stage for a customer ------------------
	//@momit

	$('.category-button').on("click", function(){
		localStorage.setItem('selected_category', $(this).attr('id'));
		localStorage.setItem('order_step', 1);
	})
	//active the previously selected category and show the items of selected category
	$('#'+selected_category).trigger('click');

	$('#date_modal_close').on('click', function (e) {
		$('#dateModal').modal('hide');
		localStorage.setItem('order_step', 1);
	})

	$('#agreement_modal_close, #close_terms').on('click', function (e) {
		$('#agreementModal').modal('hide');
		localStorage.setItem('order_step', 3);
	})

    $('#order_details_modal_close, #close_terms').on('click', function (e) {
        $('.customer_order_preview').modal('hide');
        // localStorage.setItem('order_step', 3);
    })



	$('#book_from_details_btn').on('click', function (e) {
		orderCalender(startDay);
        $('#dateModal').find('.selected_service_id').val(selected_id);
        $('#dateModal').modal();
	})


	if(is_refreshed==1 && order_step==2){// date selection modal open
        startDay = 0;
        orderCalender(startDay);
        $('#dateModal').find('.selected_service_id').val(selected_id);
        $('#dateModal').modal();
	}
	else if(is_refreshed==1 && order_step==3){ // date selected
	    $('#dateModal').find('.order_date').val(selected_date);
        $('#dateModal').find('.order_time').val(selected_time);
        $('#dateModal').find('.is_urgent_selected').val(selected_urgent);
		$('#selected_date_time').html(selected_date_html);
		$('#confirm').trigger("click");
	}
	else if(is_refreshed==1 && order_step==4){ // agreement modal open
		//alert(selected_id+"---"+selected_date+"---"+selected_time+"---"+selected_urgent);
		$('#'+selected_id).find('.selected_order_date').val(selected_date);
        $('#'+selected_id).find('.selected_order_time').val(selected_time);
        $('#'+selected_id).find('.is_urgent').val(selected_urgent);
		$('#'+selected_id).find('.uss-security-logo').append('<div class="selected-date">'+selected_date_html+'</div>');
		$('.confirm_btn').trigger("click");
	}
	else if(is_refreshed==1 && order_step==5){ // service item details open
	    if(selected_urgent =="" && selected_date==""){
			$('#more_info_'+selected_id).trigger("click");
		}
		else{
			$('#dateModal').find('.order_date').val(selected_date);
			$('#dateModal').find('.order_time').val(selected_time);
			$('#dateModal').find('.is_urgent_selected').val(selected_urgent);
			$('#selected_date_time').html(selected_date_html);
			$('#confirm').trigger("click");
			localStorage.setItem('order_step', 5);
			// on page load show if previously selected item details
			if(is_refreshed==1 && order_step==5){
				$('#more_info_'+selected_id).trigger("click");
			}
		}
	}
	//alert("fontendjs--"+is_refreshed)
	//alert(selected_category+"---"+selected_id+"---"+selected_date+"---"+selected_time+"---"+order_step);

    $('.orderHistoryTbl').on('click', '.orderDetailsTd', function (e) {
        e.preventDefault();
        e.stopPropagation();
        // alert('s');
        // window.location.href = site_url + '/customer/customer-order-details';
        order_id = $(this).find('.cus_order_id').val();
        $.get(site_url + '/customer/customer-order-details/'+ order_id, function (data) {
            // $('.viewOrderDetailsModal').html(data);
            $('.customer_order_preview').find('#invoice').html(data)
            $('.customer_order_preview').modal();//now its working
        }, 'html')
    })

    if($('.expo_token').val()){
        $.post(site_url + '/expo-token', {expo_toke : $('.expo_token').val()}, function (data) {
            console.log(data)
        }, 'json')
    }
    console.log($('.expo_token').val())

});
