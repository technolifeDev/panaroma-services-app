// function changeImage(event) {
//     event= event|| window.event;
//     var targetElement = event.target || event.srcElement;
//     document.getElementsByClassName('dtl_partner_logo').src = targetElement.getAttribute("src");
//
// }


$(function () {
    $('.myDiv').on('click', '.thumbImage', function (e) {
        e.preventDefault();
        if ($('.thumbImage').hasClass('selected_slide_image')){
            $('.thumbImage').removeClass("selected_slide_image");
        }
        var imgSrc = $(this).attr('src');
        $('.dtl_partner_logo').attr('src', imgSrc);
        $(this).addClass("selected_slide_image");
    })
})
