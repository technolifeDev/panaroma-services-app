
$(function(){
    $.fn.editable.defaults.mode = 'inline';
    $('.addPartnerServiceItemForm .service_pricing_description').summernote();
    $('.addServiceItemForm .service_pricing_description').summernote();
    $('.addServiceItemForm .service_pricing_description_est').summernote();
    $('.addServiceItemForm .service_pricing_description_rus').summernote();
    $('.editPartnerServiceItemForm .service_pricing_description').summernote();
    $('.editServiceItemForm .service_pricing_description').summernote();
    $('.editServiceItemForm .service_pricing_description_est').summernote();
    $('.editServiceItemForm .service_pricing_description_rus').summernote();
    $('.addServiceItemForm .input-images').imageUploader({
        imagesInputName: 'images-0',
        extensions: ['.jpg', '.jpeg', '.png', '.gif', '.svg'],
        mimes: ['image/jpeg', 'image/png', 'image/gif', 'image/svg+xml'],
    });

    $('.addPartnerServiceItemForm .input-images').imageUploader({
        imagesInputName: 'images-0',
        extensions: ['.jpg', '.jpeg', '.png', '.gif', '.svg'],
        mimes: ['image/jpeg', 'image/png', 'image/gif', 'image/svg+xml'],
    });
    // $('.input-images').imageUploader();


    function updateAmount(){
        $('.addServiceModal').on('input', '.base_price', function(){
            var base_price = $(this).val();
            $('.addServiceModal').find('.amount').val(base_price);
        })
    }
    $('.addServiceItemForm').find('.service_pricing_unit_cost_amount').on('keypress keyup blur', function(event){
        $(this).val($(this).val().replace(/[^0-9\.]/g,''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    var site_url = $('.site_url').val();

    //modal reset for static html it will be replaced lately by the dynamic content load
    $(".addServiceModal").on("hidden.bs.modal", function(){
        $(this).find('form')[0].reset();
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    //load modal
    $(".createService").click(function(e){
        e.preventDefault();
        if($('.service_partner_id').length > 0){
            partner_id = $('.service_partner_id').val();
            window.location.href = site_url+'/admin/service/partner/create-service-item/' + partner_id;
        } else {
            window.location.href = site_url+'/admin/create-service-item';
        }
    });

    //load partner wise service html
    $('.createPartnerService').on('click', function (e) {
        e.preventDefault();
        window.location.href = site_url+'/partner/service-create';
    });

    //service catalogue create
    $('.createServiceCatalogue').on('click', function (e) {
        e.preventDefault();
        $('.addServiceCatalogueModal').find('form')[0].reset();
        $('.addServiceCatalogueModal').find('.service_id').val('')
        $('.addServiceCatalogueModal').modal();
    })

    //add more products
    var countServiceAddedDiv = 0;
    $('.servicePanel').on('click', '.addMoreProductsBtn',function (e) {
        e.preventDefault();
        e.stopPropagation();
        countServiceAddedDiv++;
        var addProductPanelClone = $('.addProductPanelClone').find('.addMoreProductsPanel').clone();
        $(addProductPanelClone).find('.service_pricing_description').summernote();
        $(addProductPanelClone).find('.service_pricing_description_est').summernote();
        $(addProductPanelClone).find('.service_pricing_description_rus').summernote();
        $(addProductPanelClone).find('.tab_eng').attr('href', '#en' + countServiceAddedDiv);
        $(addProductPanelClone).find('.tab_est').attr('href', '#est' + countServiceAddedDiv);
        $(addProductPanelClone).find('.tab_rs').attr('href', '#rs' + countServiceAddedDiv);
        $(addProductPanelClone).find('.tab_cont_eng').attr('id', 'en' + countServiceAddedDiv);
        $(addProductPanelClone).find('.tab_cont_est').attr('id', 'est' + countServiceAddedDiv);
        $(addProductPanelClone).find('.tab_cont_rs').attr('id', 'rs' + countServiceAddedDiv);
        $(addProductPanelClone).find('.service_frequency_id').attr('name', 'service_frequency_id['+ countServiceAddedDiv + '][]');
        $(addProductPanelClone).find('.input-images').imageUploader({
            id:countServiceAddedDiv,
            imagesInputName: 'images-' + countServiceAddedDiv
        });
        $('.addServiceItemForm').append(addProductPanelClone);
    });

    var countPartnerAddedDiv = 0;
    $('.addPartnerServiceItemForm').on('click', '.addMoreProductsBtn',function (e) {
        e.preventDefault();
        e.stopPropagation();
        countPartnerAddedDiv++;
        var addProductPanelClone = $('.addProductPanelClone').find('.addMoreProductsPanel').clone();
        $(addProductPanelClone).find('.service_pricing_description').summernote();
        $(addProductPanelClone).find('.service_pricing_description_est').summernote();
        $(addProductPanelClone).find('.service_pricing_description_rus').summernote();
        $(addProductPanelClone).find('.service_frequency_id').attr('name', 'service_frequency_id['+ countPartnerAddedDiv + '][]');
        $(addProductPanelClone).find('.input-images').imageUploader({
            id:countPartnerAddedDiv,
            imagesInputName: 'images-' + countPartnerAddedDiv
        });
        $('.addPartnerServiceItemForm').append(addProductPanelClone);
    });


    //remove service form.eq(i)
    $('.servicePanel').on('click', '.removeProducts',function (e) {
        e.preventDefault();
        e.stopPropagation();
        countServiceAddedDiv--;
        console.log(countServiceAddedDiv);
        $(this).parents('.addMoreProductsPanel').remove();
        for(i=0;i<=countServiceAddedDiv;i++){
            $('.addServiceItemForm').find('.input-images').eq(i).find('input:file').attr('name', 'images-'+i+'[]');
        }
    });

    //remove service form
    $('.addPartnerServiceItemForm').on('click', '.removeProducts',function (e) {
        e.preventDefault();
        e.stopPropagation();
        countPartnerAddedDiv--;
        $(this).parents('.addMoreProductsPanel').remove();
        for(i=0;i<=countPartnerAddedDiv;i++){
            $('.addPartnerServiceItemForm').find('.input-images').eq(i).find('input:file').attr('name', 'images-'+i+'[]');
        }
    });

    //add new service
    $('.addServiceItemForm').on('change', '.service_pricing_catalogue_id', function (e) {
        e.preventDefault();
        if($(this).val() == 'addNewService'){
            $(".addServiceModal").find('form')[0].reset();
            $(".addServiceModal").find('.service_id').val('')
            $(".addServiceModal").modal();
        }
    });

    //save Service catalogue
    $('.saveServiceCatalogueBtn').on('click', function(e){
        e.preventDefault();
        var formData = new FormData($('.addServiceCatalogueForm') [0] );
        $.ajax({
            url: site_url+'/admin/save-service',
            type: 'POST',
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (data) {
                if(data.status == 'success'){
                    $.notify(data.msg, "success");
                    window.location.href = site_url + '/admin/services-catalogue';
                } else {
                    $.notify(data.msg, "error");
                }
            },
            error: function (data) {
                // $.notify(data.msg, "error");
            }
        });
    });

    //edit service catalogue
    $('.serviceCatalogueTbl').on('click', '.editServiceCatalogueBtn', function (e) {
        e.preventDefault();
        $('.editServiceCatalogueModal').find('form')[0].reset();
        $('.editServiceCatalogueModal').find('.service_id').val('')

        var service_catalogue_id = $(this).attr('id');
        $.get(site_url+'/admin/services-catalogue-edit/' + service_catalogue_id, function (data) {
            if(data.status == 'success'){
                $('.editServiceCatalogueModal').find('.service_id').val(data.data[0].id);
                $('.editServiceCatalogueModal').find('.services_categories_id').val(data.data[0].services_categories_id);
                $('.editServiceCatalogueModal').find('#status').val(data.data[0].status);
                $('.editServiceCatalogueModal').find('.service_name').val(data.data[0].service_name);
                $('.editServiceCatalogueModal').find('.description').val(data.data[0].service_description);
                if(data.image != null){
                    $('.editServiceCatalogueModal').find('.fileupload-preview').html('<img src="' + site_url+ '/service_images/catalogue_images/'+ data.image.image_name + '">');
                    $('.editServiceCatalogueModal').find('.service_catalogue_image_id').val(data.image.id);;
                }
                $('.editServiceCatalogueModal').modal();
            }
        }, 'json')

    });

    $('.updateServiceCatalogueBtn').on('click', function(e){
        e.preventDefault();
        var formData = new FormData($('.editServiceCatalogueForm') [0] );
        $.ajax({
            url: site_url+'/admin/save-service',
            type: 'POST',
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (data) {
                if(data.status == 'success'){
                    $.notify(data.msg, "success");
                    window.location.href = site_url + '/admin/services-catalogue';
                } else {
                    $.notify(data.msg, "error");
                }
            },
            error: function (data) {
                // $.notify(data.msg, "error");
            }
        });
    });

    //save service and populate the option
    $('.saveServiceBtn').on('click', function(e){
        e.preventDefault();
        $.post(site_url+'/admin/save-service', $('.addServiceForm').serialize(), function(data){
            if(data.status == 'success'){
                $('.service_pricing_catalogue_id').select2("destroy");
                $('.service_pricing_catalogue_id').append('<option value="'+ data.service_id +'" selected>'+ data.service_name +'</option>');
                $.notify(data.msg, "success");
            } else {
                $.notify(data.msg, "error");
            }
        }, 'json');
    });

    function multipleValueStatus(){
        var multipleSlectVld = 0;
        if($(".service_pricing_category_id").select2('data').length == 0){
            multipleSlectVld = 1;
        } else{
            multipleSlectVld = 0;
        }

        if(multipleSlectVld == 1){
            $(".service_pricing_category_id").parents('.form-group').find('.errorMsg').removeClass('hide');
        } else{
            $(".service_pricing_category_id").parents('.form-group').find('.errorMsg').addClass('hide');
        }
        return multipleSlectVld;
    }

    $('.addServiceItemForm').on('keyup change paste', '.service_pricing_description', function () {
        textareaValidation();
    });

    function textareaValidation() {
        var texareaiDationField = 0;
        $('.addServiceItemForm').find('.service_pricing_description').each(function (i) {
            if($('.addServiceItemForm').find('.service_pricing_description').eq(i).val() == ''){
                texareaiDationField = 1;
                return false;
            } else {
                texareaiDationField =0;
            }
        });

        $('.addServiceItemForm').find('.service_pricing_description').each(function (i) {
            if($('.addServiceItemForm').find('.service_pricing_description').eq(i).val() == ''){
                $('.note-codable').eq(i).parents('.form-group').find('.errorMsg').removeClass('hide');
            } else {
                $('.note-codable').eq(i).parents('.form-group').find('.errorMsg').addClass('hide');
            }
        });

        return texareaiDationField;
    }

//form validation function
    function addFormValidation(){
        var inputiDationField = 0;
        var selectiDationField = 0;
        var valiDationField = 0;
        $('.addServiceItemForm input').each(function (i) {
            if($('input').eq(i).attr('class') != undefined){
                if(($('input').eq(i).attr('class') != 'select2-input' &&
                    $('input').eq(i).attr('class') != 'select2-focusser select2-offscreen') &&
                    $('input').eq(i).attr('class').indexOf("note") == -1
                ){
                    if($('input').eq(i).attr('class') != 'service_pricing_catalogue_id'
                        &&
                        $('input').eq(i).attr('class') != 'form-control service_pricing_title_est'
                        &&
                        $('input').eq(i).attr('class') != 'form-control service_pricing_title_rus'){

                        if ($('input').eq(i).val() == '') {
                            console.log($('input').eq(i).attr('class'))
                            inputiDationField = 1;
                            return false;
                        } else {
                            inputiDationField = 0;
                        }
                    }
                }
            }

        });

        $('.addServiceItemForm input').each(function (i) {
            // console.log($('input').eq(i).attr('class').indexOf("note"));
            if($('input').eq(i).attr('class') != undefined){
                if(($('input').eq(i).attr('class') != 'select2-input' &&
                    $('input').eq(i).attr('class') != 'select2-focusser select2-offscreen') &&
                    $('input').eq(i).attr('class').indexOf("note") == -1
                ){
                    if($('input').eq(i).attr('class') != 'service_pricing_catalogue_id' ||
                        $('input').eq(i).attr('class') != 'form-control service_pricing_title_est'
                        ||
                        $('input').eq(i).attr('class') != 'form-control service_pricing_title_rus'){
                        if($('input').eq(i).val() == ''){
                            $('input').eq(i).parents('.form-group').find('.errorMsg').removeClass('hide');
                        } else {
                            $('input').eq(i).parents('.form-group').find('.errorMsg').addClass('hide');
                        }
                    }
                }
            }
        });

        $('.addServiceItemForm select').each(function (i) {
            // if($('select').eq(i).attr('class') != 'form-control service_pricing_category_id search-select select2-offscreen'){
            if($('select').eq(i).val() == ''){
                selectiDationField = 1;
                return false;
            } else {
                selectiDationField =0;
            }
        });

        $('.addServiceItemForm select').each(function (i) {
            if($('select').eq(i).attr('class') != 'form-control service_pricing_category_id search-select select2-offscreen'){
                if($('select').eq(i).val() == ''){
                    $('select').eq(i).parents('.form-group').find('.errorMsg').removeClass('hide');
                } else {
                    $('select').eq(i).parents('.form-group').find('.errorMsg').addClass('hide');
                }
            } else {
                multipleValueStatus();
            }
        });



        // console.log(texareaiDationField + ' //' + selectiDationField + ' //' + inputiDationField);
        if(selectiDationField == 0 && inputiDationField == 0){
            valiDationField = 0;
        } else {
            valiDationField = 1;
        }
        return valiDationField;
    }
    $('.addServiceItemForm').on('keyup change paste', 'input, select, textarea', function(e){
        addFormValidation();
        textareaValidation();
    });

    //save service item
    $('.saveServiceItem').on('click',function (e) {
        e.preventDefault();
        var multiple_select = multipleValueStatus();
        var textAreaVal =  textareaValidation();
         var form_status = addFormValidation();
         // console.log(multiple_select + ', ' + textAreaVal + ', ' + form_status)
        if(form_status == 0 && multiple_select == 0 && textAreaVal == 0){
            // $(this).attr('disabled','disabled');
            var formData = new FormData($('.addServiceItemForm') [0] );
            $.ajax({
                url: site_url+'/admin/save-service-item',
                type: 'POST',
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                dataType: "json",
                success: function (data) {
                    if(data.status == 'success'){
                        $.notify(data.msg, "success");
                        window.location.href = site_url+ data.url;
                    } else {
                        $.notify(data.msg, "error");
                    }
                },
                error: function (data) {
                    // $.notify(data.msg, "error");
                }
            });
        }

    });

    //save service item for every partner
    $('.savePartnerServiceItem').on('click', function(e){
        e.preventDefault();
        var formData = new FormData($('.addPartnerServiceItemForm') [0] );
        $.ajax({
            url: site_url+'/partner/save-service-item',
            type: 'POST',
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (data) {
                if(data.status == 'success'){
                    $.notify(data.msg, "success");
                    window.location.href = site_url+ data.url;
                } else {
                    $.notify(data.msg, "error");
                }
            },
            error: function (data) {
                // $.notify(data.msg, "error");
            }
        });
        // $.post(site_url+'/partner/save-service-item', $('.addPartnerServiceItemForm').serialize(), function(data){
        //     if(data.status == 'success'){
        //         window.location.href = site_url+ data.url;
        //     }
        //     $.notify(data.msg, data.status);
        // }, 'json');
    });

    //update service form
    $('.updateServiceItem').on('click',function (e) {
        e.preventDefault();
        var formData = new FormData($('.editServiceItemForm') [0] );
        $.ajax({
            url: site_url+'/admin/update-service',
            type: 'POST',
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (data) {
                if(data.status == 'success'){
                    $.notify(data.msg, "success");
                    if(data.url){
                        window.location.href = site_url+data.url;
                    }
                } else {
                    $.notify(data.msg, "error");
                }
            },
            error: function (data) {
                // $.notify(data.msg, "error");
            }
        });
        // $.post(site_url+'/admin/update-service', $('.editServiceItemForm').serialize(), function (data) {
        //     if(data.status == 'success'){
        //         window.location.href = site_url+'/admin/services';
        //     }
        //     $.notify(data.msg, data.status);
        // }, 'json');

    });

    //partner update service
    $('.updatePartnerServiceItem').on('click',function (e) {
        e.preventDefault();
        var formData = new FormData($('.editPartnerServiceItemForm') [0] );
        $.ajax({
            url: site_url+'/partner/update-service',
            type: 'POST',
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (data) {
                if(data.status == 'success'){
                    $.notify(data.msg, "success");
                    window.location.href = site_url+'/partner/services';
                } else {
                    $.notify(data.msg, "error");
                }
            },
            error: function (data) {
                // $.notify(data.msg, "error");
            }
        });
        // $.post(site_url+'/partner/update-service', $('.editPartnerServiceItemForm').serialize(), function (data) {
        //     if(data.status == 'success'){
        //         window.location.href = site_url+'/partner/services';
        //     }
        //     $.notify(data.msg, data.status);
        // }, 'json');

    })

    //service autocomplete
    $(".addServiceItemForm").on('focus', '.service_pricing_name', function (event) {
        var This = $(this)
        $.get(site_url+'/admin/service-auto-suggestion', function(data){
            This.autocomplete({
                source: function(req, response) {
                    var re = $.ui.autocomplete.escapeRegex(req.term);
                    var matcher = new RegExp( "^" + re, "i" );
                    response($.grep( data, function(item){
                        return matcher.test(item.value); }) );
                },
                select: function( event, ui ) {
                    console.log(event);
                    This.parents('.serviceDiv').find('.service_pricing_catalogue_id').val( ui.item.id )
                    $(this).trigger('change');
                },
                change: function (event, ui) {
                    console.log(event);
                    if (ui.item === null) {
                        This.parents('.serviceDiv').find('.service_pricing_catalogue_id').val('');
                    }
                }
            });
        }, 'json');
    });

    $(".editServiceItemForm").on('focus', '.service_pricing_name', function (event) {
        var This = $(this)
        $.get(site_url+'/admin/service-auto-suggestion', function(data){
            This.autocomplete({
                source: function(req, response) {
                    var re = $.ui.autocomplete.escapeRegex(req.term);
                    var matcher = new RegExp( "^" + re, "i" );
                    response($.grep( data, function(item){
                        return matcher.test(item.value); }) );
                },
                select: function( event, ui ) {
                    console.log(event);
                    This.parents('.serviceDiv').find('.service_pricing_catalogue_id').val( ui.item.id )
                    $(this).trigger('change');
                },
                change: function (event, ui) {
                    console.log(event);
                    if (ui.item === null) {
                        This.parents('.serviceDiv').find('.service_pricing_catalogue_id').val('');
                    }
                }
            });
        }, 'json');
    });


    //data table ajax load
    var partner_id = 0
    if($('.service_partner_id').length > 0){
        partner_id = $('.service_partner_id').val();
    }
    $('.serviceTbl').DataTable({
        "processing": false,
        "serverSide": true,
        "ordering": false,
        "ajax": {
            "url": site_url+"/admin/service-list",
            "dataType": "json",
            "type": "POST",
            "data":{ _token: $('#token').val(), 'partner_id' : partner_id}
        },
        "aoColumns": [
            { mData: 'sl'},
            { mData: 'title'},
            // { mData: 'service_name' },
            { mData: 'category_name'},
            // { mData: 'currency'},
            { mData: 'amount'},
            { mData: 'status'},
            { mData: 'action'},
        ]
    });

    $('.partnerServiceListTbl').DataTable({
        "processing": false,
        "serverSide": true,
        "ordering": false,
        "ajax": {
            "url": site_url+"/partner/service-list",
            "dataType": "json",
            "type": "POST",
            "data":{ _token: $('#token').val()}
        },
        "aoColumns": [
            { mData: 'sl'},
            { mData: 'title'},
            { mData: 'service_name' },
            { mData: 'category_name'},
            // { mData: 'currency'},
            { mData: 'amount'},
            { mData: 'status'},
            { mData: 'action'},
        ]
    });




    //service catalogue list
    $('.serviceCatalogueTbl').DataTable({
        "processing": false,
        "serverSide": true,
        "ordering": false,
        "ajax": {
            "url": site_url+"/admin/service-catalogue-list",
            "dataType": "json",
            "type": "POST",
            "data":{ _token: $('#token').val()}
        },
        "aoColumns": [
            { mData: 'sl'},
            { mData: 'service_name' },
            { mData: 'category' },
            { mData: 'status' },
            { mData: 'service_description' },
            { mData: 'action' },
        ],
        columnDefs: [{
            targets: 3,
            className: 'statusEditorTd'
        }]
    });


    $('.serviceTbl').on('click', '.deltServiceBtn', function (e) {
        e.preventDefault();
        var id = $(this).attr('id');
        bootbox.dialog({
            message: "Are you sure you want to delete this Service?",
            title: "<i class='glyphicon glyphicon-trash'></i> Delete !",
            buttons: {
                success: {
                    label: "No",
                    className: "btn-success btn-squared",
                    callback: function() {
                        $('.bootbox').modal('hide');
                    }
                },
                danger: {
                    label: "Delete!",
                    className: "btn-danger btn-squared",
                    callback: function() {
                        $.ajax({
                            type: 'GET',
                            url: site_url+'/admin/delete/'+id,
                        }).done(function(response){
                            $('.serviceTbl').DataTable().ajax.reload();
                            $.notify('Successfully deleted', "error");
                        }).fail(function(response){
                            bootbox.alert(response);
                        })
                    }
                }
            }
        });
    });

    //delete partner service
    $('.partnerServiceListTbl').on('click', '.deltServiceBtn', function (e) {
        e.preventDefault();
        var id = $(this).attr('id');
        bootbox.dialog({
            message: "Are you sure you want to delete this Service?",
            title: "<i class='glyphicon glyphicon-trash'></i> Delete !",
            buttons: {
                success: {
                    label: "No",
                    className: "btn-success btn-squared",
                    callback: function() {
                        $('.bootbox').modal('hide');
                    }
                },
                danger: {
                    label: "Delete!",
                    className: "btn-danger btn-squared",
                    callback: function() {
                        $.ajax({
                            type: 'GET',
                            url: site_url+'/admin/delete/'+id,
                        }).done(function(response){
                            $('.partnerServiceListTbl').DataTable().ajax.reload();
                            $.notify('Successfully deleted', "error");
                        }).fail(function(response){
                            bootbox.alert(response);
                        })
                    }
                }
            }
        });
    });

    //delete partner service
    $('.serviceCatalogueTbl ').on('click', '.deltCatalogueServiceBtn', function (e) {
        e.preventDefault();
        var id = $(this).attr('id');
        bootbox.dialog({
            message: "Are you sure you want to delete this Service Catalogue?",
            title: "<i class='glyphicon glyphicon-trash'></i> Delete !",
            buttons: {
                success: {
                    label: "No",
                    className: "btn-success btn-squared",
                    callback: function() {
                        $('.bootbox').modal('hide');
                    }
                },
                danger: {
                    label: "Delete!",
                    className: "btn-danger btn-squared",
                    callback: function() {
                        $.ajax({
                            type: 'GET',
                            url: site_url+'/admin/catalogue-delete/'+id,
                        }).done(function(response){
                            $('.serviceCatalogueTbl').DataTable().ajax.reload();
                            $.notify('Successfully deleted', "error");
                        }).fail(function(response){
                            bootbox.alert(response);
                        })
                    }
                }
            }
        });
    });


    //view individual service
    $('.serviceTbl').on('click', '.viewDtlServiceBtn', function (e) {
        var service_item_id = $(this).attr('id');
        $.get(site_url+'/admin/view-service-item/'+ service_item_id, function (data) {
            console.log(data)
            if(data.status == 'success'){
                $(".viewServiceModal").find('.modal-title').empty().append(data.data.service_pricing_title + ' Details');
                $(".viewServiceModal").find('.service_desc').empty().append(data.data.service_pricing_description);
                $(".viewServiceModal").find('.service_category').empty().append(data.data.category_name);
                $(".viewServiceModal").find('.service_name').empty().append(data.data.service_name);
                $(".viewServiceModal").find('.service_price').empty().append(data.data.service_pricing_unit_cost_amount + ' ' + data.data.service_pricing_currency_unit);
                $(".viewServiceModal").find('.attr_list').remove()
                var html_data = '';
                $(data.images).each(function(i,j){
                    html_data += '<div class="col-xs-3"><img src="' + site_url+ '/service_images/service_item_images/'+ j.image_name + '" alt="Snow" style="width:100%"></div>';
                });
                $(".viewServiceModal").find('.images_ser').empty().append(html_data);

                $(".viewServiceModal").modal();
            } else{
                $.notify(data.msg, data.status);
            }
        }, 'json')

    });

    //view partner service details
    //view individual service
    $('.partnerServiceListTbl').on('click', '.viewDtlServiceBtn', function (e) {
        var service_item_id = $(this).attr('id');
        $.get(site_url+'/admin/view-service-item/'+ service_item_id, function (data) {
            if(data.status == 'success'){
                $(".viewServiceModal").find('.modal-title').empty().append(data.data.service_pricing_title + ' Details');
                $(".viewServiceModal").find('.service_desc').empty().append(data.data.service_pricing_description);
                $(".viewServiceModal").find('.service_category').empty().append(data.data.category_name);
                $(".viewServiceModal").find('.service_name').empty().append(data.data.service_name);
                $(".viewServiceModal").find('.service_price').empty().append(data.data.service_pricing_unit_cost_amount + ' ' + data.data.service_pricing_currency_unit);
                $(".viewServiceModal").find('.attr_list').empty()
                if(data.data.dtl){
                    var ul_html = '';
                    $(data.data.dtl).each(function (i, j) {
                        $.each(j, function (k,l) {
                            ul_html += '<li class="list-group-item list-group-item-info">'+ k + '</li><ul class="list-group">'
                            $.each(l, function (m, n) {
                                ul_html += '<li class="list-group-item">' + m;
                                $.each(n, function (o,p) {
                                    ul_html +=  ': ' + p + '</li>';
                                });
                            });
                            ul_html += '</ul>';
                        })
                    })
                    $(".viewServiceModal").find('.attr_list').append(ul_html);
                }

                var html_data = '';
                $(data.images).each(function(i,j){
                    html_data += '<div class="col-xs-3"><img src="' + site_url+ '/service_images/service_item_images/'+ j.image_name + '" alt="Snow" style="width:100%"></div>';
                });
                $(".viewServiceModal").find('.images_ser').empty().append(html_data);

                $(".viewServiceModal").modal();
            } else{
                $.notify(data.msg, data.status);
            }
        }, 'json')

    });

    //data table ajax load
    $('#category_table').DataTable({
        "processing": true,
        "serverSide": false,
        "ajax": site_url+"/admin/ajax/category-list",
        "aoColumns": [
            { mData: 'sno'},
            { mData: 'category_name' },
            { mData: 'parent_category_name'},
            { mData: 'description'},
            { mData: 'status'},
            { mData: 'actions'},
        ],
        "ordering": false
    });

    $('.category_table').on('click', '.category-delete', function (e) {
        e.preventDefault();
        var del_id =  $(this).attr('id');
        var res = del_id.split("_");

        var data= {
            category_id: res[1],
        };
        bootbox.dialog({
            message: "Are you sure you want to delete category?",
            title: "<i class='glyphicon glyphicon-trash'></i> Delete !",
            buttons: {
                success: {
                    label: "No",
                    className: "btn-success btn-squared",
                    callback: function() {
                        $('.bootbox').modal('hide');
                    }
                },
                danger: {
                    label: "Delete!",
                    className: "btn-danger btn-squared",
                    callback: function() {
                        $.ajax({
                            type: 'POST',
                            url: site_url+'/admin/category/delete',
                            data: data,
                            dataType:'json'
                        }).done(function(response){
                            bootbox.alert(response,
                                function(){
                                    location.reload(true);
                                }
                            );
                        }).fail(function(response){
                            bootbox.alert(response.responseText,function(){
                                location.reload(true);
                            });

                        })
                    }
                }
            }
        });
    });

    //edit service
    $('.category_table').on('click', '.category-edit', function (e) {
        e.preventDefault();
        var category_id= $(this).attr('id');
        console.log(category_id);

        $.get(site_url+'/admin/category/edit/'+ category_id, function (data) {
            console.log(data.image);
            $(".categoryModal").find('#category_name_e').val(data.category.category_name);
            $(".categoryModal").find('#category_name_est').val(data.category.category_name_est);
            $(".categoryModal").find('#category_name_rus').val(data.category.category_name_rus);
            $(".categoryModal").find('#parent_id_e').html(data.select);
            $(".categoryModal").find('#status_e').html(data.select_status);

            $(".categoryModal").find('#description_e').val(data.category.description);
            $(".categoryModal").find('#description_est').val(data.category.description_est);
            $(".categoryModal").find('#description_rus').val(data.category.description_rus);
            $(".categoryModal").find('#category_id').val(data.category.id);
            if(data.image !== null) {
                $(".categoryModal").find('.category_image_id').val(data.image.id);
                $(".categoryModal").find('.fileupload-preview').html('<img src="' + site_url+ '/service_images/category_images/'+ data.image.image_name + '">');
            }

            $(".categoryModal").modal();
        }, 'json')
    });

    //edit service
    $('.category_table').on('click', '.category-view', function (e) {
        e.preventDefault();
        var _id =  $(this).attr('id');
        var res = _id.split("_");
        var category_id = res[1];

        $.get(site_url+'/admin/category/view/'+ category_id, function (data) {
            var image_url = site_url+"/service_images/category_images/"+data.category.cimage;
            $(".categoryViewModal").find("#image_v").attr('src', image_url);
            $(".categoryViewModal").find('#name_v').html(data.category.category_name);
            $(".categoryViewModal").find('#parent_v').html(data.category.parent_category_name);
            $(".categoryViewModal").find('#des_v').html(data.category.description);
            $(".categoryViewModal").find('#status_v').html(data.category.status);
            $(".categoryViewModal").modal();
        }, 'json')
    });

    //Roles data table on ajax
    $('#role_table').DataTable({
        "processing": true,
        "serverSide": false,
        "ajax": site_url+"/admin/ajax/role-list",
        "aoColumns": [
            { mData: 'sno'},
            { mData: 'name' },
            { mData: 'slug'},
            { mData: 'description'},
            { mData: 'actions'},
        ]
    });

    //edit service
    $('.role_table').on('click', '.edit-role', function (e) {
        e.preventDefault();
        var role_id= $(this).attr('id');
        $.get(site_url+'/admin/roles/edit/'+ role_id, function (data) {

            $(".roleModal").find('#role_name').val(data.role.name);
            $(".roleModal").find('#role_slug').val(data.role.slug);

            $(".roleModal").find('#role_description').val(data.role.description);
            $(".roleModal").find('#role_id').val(data.role.id);
            $(".roleModal").modal();
        }, 'json')
    });

    //Permissions data table on ajax
    $('#permission_table').DataTable({
        "processing": true,
        "serverSide": false,
        "ajax": site_url+"/admin/ajax/permission-list",
        "aoColumns": [
            { mData: 'sno'},
            { mData: 'name' },
            { mData: 'slug'},
            { mData: 'description'},
            { mData: 'actions'},
        ]
    });

    //edit role
    $('.permission_table').on('click', '.edit-permission', function (e) {
        e.preventDefault();
        var role_id= $(this).attr('id');

        $.get(site_url+'/admin/permissions/edit/'+ role_id, function (data) {

            $(".permission-modal").find('#permission_name').val(data.permission.name);
            $(".permission-modal").find('#permission_slug').val(data.permission.slug);

            $(".permission-modal").find('#permission_description').val(data.permission.description);
            $(".permission-modal").find('#permission_id').val(data.permission.id);
            $(".permission-modal").modal();
        }, 'json')
    });

    $("#schrollmenu .paginate_items").eq(0).trigger("click");
    //partner category show
    $('.partnerServiceCategoryDiv').on('click', '.service-cat', function (e) {
        e.preventDefault();
        var service_category_id = $(this).attr('id');
        $('.partnerServiceCatalogueDiv').removeClass('hide');
        $('.partnerServiceCategoryDiv a.selectedCat').removeClass('selectedCat');
        $('.service_pricing_category_id').val(service_category_id);
        $(this).addClass('selectedCat');
        $('.partnerServiceCatalogueDiv .box-body').remove();
        $.get(site_url+'/partner/cat-service-list/'+ service_category_id, function(data){
            var html_data = '';
            $(data).each(function (i,j) {
                html_data += '<a class="btn btn-app service-catalogue '+ (i==0? 'selectedCat' : '')+'" href="#" id="'+ j.id +'"><i class="fa fa-folder-open fa-2x"></i> <br>'+ j.service_name +'</a><input type="hidden" name="service_pricing_catalogue_id" class="service_pricing_catalogue_id" value="'+j.id+'">';
            });
            $('.partnerServiceCatalogueDiv').append('<div class="box-body">'+ html_data +'</div>');
        }, 'json')
    });

    $('.partnerServiceCatalogueDiv').on('click', '.service-catalogue', function (e) {
        e.preventDefault();
        $('.productItemPanel').removeClass('hide');
        var service_catalogue_id = $(this).attr('id');
        $('.partnerServiceCatalogueDiv a.selectedCat').removeClass('selectedCat');
        $('.service_pricing_catalogue_id').val(service_catalogue_id);
        $(this).addClass('selectedCat');
    });

    //delete permission
    $('.permission_table ').on('click', '.delete-permission', function (e) {
        e.preventDefault();
        var id = $(this).attr('id');
        bootbox.dialog({
            message: "Are you sure you want to delete this permissions?",
            title: "<i class='glyphicon glyphicon-trash'></i> Delete !",
            buttons: {
                success: {
                    label: "No",
                    className: "btn-success btn-squared",
                    callback: function() {
                        $('.bootbox').modal('hide');
                    }
                },
                danger: {
                    label: "Delete!",
                    className: "btn-danger btn-squared",
                    callback: function() {
                        $.ajax({
                            type: 'GET',
                            url: site_url+'/admin/permissions/delete/'+id,
                        }).done(function(response){

                            bootbox.alert(response,
                                function(){
                                    location.reload(true);
                                }
                            );
                        }).fail(function(response){
                            bootbox.alert(response.responseText,function(){
                                location.reload(true);
                            });
                        })
                    }
                }
            }
        });
    });

    //delete role
    $('.role_table ').on('click', '.delete-role', function (e) {
        e.preventDefault();
        var id = $(this).attr('id');
        bootbox.dialog({
            message: "Are you sure you want to delete this role?",
            title: "<i class='glyphicon glyphicon-trash'></i> Delete !",
            buttons: {
                success: {
                    label: "No",
                    className: "btn-success btn-squared",
                    callback: function() {
                        $('.bootbox').modal('hide');
                    }
                },
                danger: {
                    label: "Delete!",
                    className: "btn-danger btn-squared",
                    callback: function() {
                        $.ajax({
                            type: 'GET',
                            url: site_url+'/admin/roles/delete/'+id,
                        }).done(function(response){

                            bootbox.alert(response,
                                function(){
                                    location.reload(true);
                                }
                            );
                        }).fail(function(response){
                            bootbox.alert(response.responseText,function(){
                                location.reload(true);
                            });
                        })
                    }
                }
            }
        });
    });


    $(document).on('click', '.delete-image', function(closeEvent) {
        $(this).parents('span').remove();
        var fileInput = $('#uploadFile')[0];

        //if the checkbox is checked, then remove all selected files
        // if ($('#clearOnDelete:checked').length) {
        //     fileInput.value = '';
        // }
        var files = fileInput.files;
        var index = closeEvent.target.id.replace('file_','');

        console.log('s');
        //this is not possible, since the FileList is read-only
        //files[index].remove();
        //files = Array.prototype.splice.call(files,closeEvent.target.id.replace('file_',''),1);
        //console.log('files: ',files);
    })
    //edit service item from services item create
    // $('.serviceTbl').on('click', '.editServiceBtn', function () {
    //     service_item_id = $(this).attr('id');
    //     $.get(site_url+'/admin/service-item-images/'+ service_item_id, function(data){
    //
    //     }, 'json')
    // });
    var source = [
        {'value': 'active', 'text': 'Active'},
        {'value': 'inactive', 'text': 'Inactive'},
        {'value': 'draft', 'text': 'Draft'},
        {'value': 'pending', 'text': 'Pending'}
        ];
    //test editable
    $('.serviceCatalogueTbl').on('click', '.statusEditor', function (e) {
        e.preventDefault();
        $(this).editable({
            prepend: "not selected",
            mode:'popup',
            type: 'select',
            'source': function() {
                return source;
            },
            // url: '/post',
            pk: 1,
            placement: 'top',
            title: 'Enter username'
        });
    });


    $('.serviceCatalogueTbl').on('click', '.editable-submit', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var this_id = $(this).parents('tr').find('.editServiceCatalogueBtn').attr('id');
        var selectedvalue = $(this).parents('tr').find('.input-sm').val();
        var This = $(this);
        $.post('/admin/update-catalogue-status', {'service_catalogue_id' : this_id, 'status' : selectedvalue}, function (data) {
            $('.serviceCatalogueTbl').DataTable().ajax.reload();
            $.notify('Status updated', 'success');
        }, 'json');
    });

    //get tax value
    $('.addPartnerServiceItemForm').on('input', '.service_pricing_unit_cost_amount', function () {
        var This = $(this);
        var service_price = $(this).val();
        $.get(site_url + '/get-price-settings', function (data) {
            var commission = parseFloat(service_price) *(parseFloat(data.commission) / 100);
            var tax = (parseFloat(service_price) + commission) * (parseFloat(data.tax) / 100);
            This.parents('.addMoreProductsPanel').find('.service_commission_price').val(commission.toFixed(2))
            This.parents('.addMoreProductsPanel').find('.service_tax_price').val(tax.toFixed(2))
        }, 'json')
    });

    $('.editPartnerServiceItemForm').on('input', '.service_pricing_unit_cost_amount', function () {
        var This = $(this);
        var service_price = $(this).val();
        $.get(site_url + '/get-price-settings', function (data) {
            var commission = parseFloat(service_price) *(parseFloat(data.commission) / 100);
            var tax = (parseFloat(service_price) + commission) * (parseFloat(data.tax) / 100);
            This.parents('.addMoreProductsPanel').find('.service_commission_price').val(commission.toFixed(2))
            This.parents('.addMoreProductsPanel').find('.service_tax_price').val(tax.toFixed(2))
        }, 'json')
    });

    $('.addServiceItemForm').on('input', '.service_pricing_unit_cost_amount', function () {
        var This = $(this);
        var service_price = $(this).val();
        $.get(site_url + '/get-price-settings', function (data) {
            var commission = parseFloat(service_price) *(parseFloat(data.commission) / 100);
            var tax = (parseFloat(service_price) + commission) * (parseFloat(data.tax) / 100);
            This.parents('.addMoreProductsPanel').find('.service_commission_price').val(commission.toFixed(2))
            This.parents('.addMoreProductsPanel').find('.service_tax_price').val(tax.toFixed(2))
        }, 'json')
    });

    $('.editServiceItemForm').on('input', '.service_pricing_unit_cost_amount', function () {
        var This = $(this);
        var service_price = $(this).val();
        $.get(site_url + '/get-price-settings', function (data) {
            var commission = parseFloat(service_price) *(parseFloat(data.commission) / 100);
            var tax = (parseFloat(service_price) + commission) * (parseFloat(data.tax) / 100);
            This.parents('.addMoreProductsPanel').find('.service_commission_price').val(commission.toFixed(2))
            This.parents('.addMoreProductsPanel').find('.service_tax_price').val(tax.toFixed(2))
        }, 'json')
    });

    //checkbox value update
    $('.addServiceItemForm').on('change', '.check_service_date', function (e) {
        if($(this).is( ':checked' )){
            $(this).parents('.dateCheckDiv').find('.is_service_date').val(0);
        } else {
            $(this).parents('.dateCheckDiv').find('.is_service_date').val(1);
        }
    })

    $('.editServiceItemForm').on('change', '.check_service_date', function (e) {
        if($(this).is( ':checked' )){
            $(this).parents('.dateCheckDiv').find('.is_service_date').val(0);
        } else {
            $(this).parents('.dateCheckDiv').find('.is_service_date').val(1);
        }
    })

    // $( ".category_table tbody" ).sortable({
    //     update: function() {
    //         // sendOrderToServer();
    //     }
    // });

    // Sortable rows
    var sortable_arr = [];
    $('.category_table').sortable({
        containerSelector: 'table',
        itemPath: '> tbody',
        itemSelector: 'tr',
        placeholder: '<tr class="placeholder"/>',
        onDrop: function($item, container, _super) {
            sortable_arr = [];
            $(container.el[0].rows).each(function (i,j) {
                if(typeof $(j).find('.category-edit').attr('id') != 'undefined'){
                    sortable_arr[i-1] = $(j).find('.category-edit').attr('id');
                }
            });
            saveOrder(sortable_arr);
        },
    });

    function saveOrder() {
        $.post(site_url+ '/admin/category/category-sort', {sort_data : sortable_arr, token: $('#token').val()}, function (data) {
            $('.category_table').DataTable().ajax.reload();
            $.notify(data.msg, data.status);
        }, 'json')
    }
    // $( "#tablecontents" ).sortable();
    // $( "#tablecontents" ).disableSelection();

});
