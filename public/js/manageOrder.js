
$(function(){
    var site_url = $('.site_url').val();

    function getAllNotification(){
        $.get(site_url + '/get-notification', function (data) {
            if(data.new){
                $('.notificationCounter').html(data.new);
                $('.notificationCounter').attr('notificationCount', data.new);
                $('.notificationCountertext').html(' You have ' + data.new + ' new notifications')
            }

            var html = '';
            if(data.data){
                $(data.data).each(function (i,j) {
                    var notify_data = JSON.parse(j.notify_data)
                    html+= '<li>' +
                        '<a href="javascript:void(0)">' +
                        '<span class="label label-primary" notify_id="'+ j.id +'"><i class="fa fa-user"></i></span>' +
                        '<span class="message">'+ notify_data.message +'</span>' +
                        // '<span class="time"> 1 min</span>' +
                        '</a>' +
                        '</li>'
                });
                $('.notificationList ul').html(html);
            }
        }, 'json')
    }
    getAllNotification();

    var table = $('.orderAdminTbl').DataTable({
        "processing": false,
        "serverSide": true,
        "ordering": false,
        "ajax": {
            "url": site_url+"/admin/service/partner/order-json-list",
            "dataType": "json",
            "type": "POST",
            "data":{ _token: $('#token').val()}
        },
        "aoColumns": [
            { mData: 'expand'},
            { mData: 'created_at'},
            { mData: 'order_no'},
            { mData: 'customer_name' },
            { mData: 'company_name'},
            { mData: 'service_date'},
            { mData: 'cost'},
            { mData: 'service_order_grand_total'},
            { mData: 'service_order_status'},
            { mData: 'service_order_deliver_status'},
            { mData: 'service_order_payment_status'},
            { mData: 'action'},
        ]
    });

    var partnertable = $('.orderPartnerTbl').DataTable({
        "processing": false,
        "serverSide": true,
        "ordering": false,
        "ajax": {
            "url": site_url+"/partner/service/order-json-list",
            "dataType": "json",
            "type": "POST",
            "data":{ _token: $('#token').val()}
        },
        "aoColumns": [
            { mData: 'expand'},
            { mData: 'created_at'},
            { mData: 'order_no'},
            { mData: 'customer_name' },
            { mData: 'company_name'},
            { mData: 'service_date'},
            { mData: 'cost'},
            { mData: 'service_order_grand_total'},
            { mData: 'service_order_status'},
            { mData: 'service_order_deliver_status'},
            { mData: 'service_order_payment_status'},
            { mData: 'action'},
        ]
    });

//notification json
    $('.notificationDataTbl').DataTable({
        "processing": false,
        "serverSide": true,
        "ordering": false,
        "ajax": {
            "url": site_url+"/notification-json-list",
            "dataType": "json",
            "type": "POST",
            "data":{ _token: $('#token').val()}
        },
        "aoColumns": [
            { mData: 'sl'},
            { mData: 'message'},
            { mData: 'url'},
        ]
    });

    //order details
    $('.orderAdminTbl').on('click', '.previwOrderDtlsBtn', function (e) {
        e.preventDefault();
        e.stopPropagation();

        var order_id = $(this).attr('id');
        $.get(site_url+'/admin/order-details/' + order_id, function (data) {

            $('.viewOrderDetailsModal').html(data);
            $('.viewOrderDetailsModal').modal();//now its working
        }, 'html')

    })

    //order details
    $('.orderPartnerTbl').on('click', '.previwOrderDtlsBtn', function (e) {
        e.preventDefault();
        e.stopPropagation();

        var order_id = $(this).attr('id');
        $.get(site_url+'/admin/order-details/' + order_id, function (data) {

            $('.viewOrderDetailsModal').html(data);
            $('.viewOrderDetailsModal').modal();//now its working
        }, 'html')
    });

    $('.viewOrderDetailsModal').on('click', '.orderStatusBtn', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).attr('disabled','disabled');
        var order_id = $('.viewOrderDetailsModal').find('.order_id').val();
        var order_status = $(this).attr('btnattr');
        $.post(site_url+'/admin/order-status-update', {'order_id':order_id, order_status : order_status}, function (data) {
            $(".viewOrderDetailsModal .close").click();
            $('.orderTblCommonClass').DataTable().ajax.reload(null,false);
            $.notify(data.msg, data.status);
        },'json');

        // return false;
    });


    $('.viewOrderDetailsModal').on('click', '.orderItemStatusBtn', function (e) {
        $(this).attr('disabled','disabled');
        var order_id = $(this).attr('td_ord_id');
        var order_item_status = $(this).attr('td_ord_type');
        var This = $(this);
        $.post(site_url+'/admin/order-item-status', {order_id : order_id, order_item_status : order_item_status}, function (data) {
            console.log(data);
            if(data.order_status == 'accepted'){
                This.parents('.orderDtlPreviewTable tr').find('.orderitemStatusLable').html('<span class="label label-primary">'+ data.order_status + '</span>')
                This.parents('.orderDtlPreviewTable tr').find('.orderItemTd').html('<a href="#" class="btn btn-xs tooltips btn-info orderItemStatusBtn processBtn" td_ord_id="'+ order_id +'" title="Process Start" td_ord_type="processed">' +
                    '<i class="fa fa-wrench" aria-hidden="true"></i>' +
                    '</a>');
            }else if(data.order_status == 'rejected'){
                This.parents('.orderDtlPreviewTable tr').find('.orderitemStatusLable').html('<span class="label label-danger">'+ data.order_status + '</span>')
            }else if(data.order_status == 'processed'){
                This.parents('.orderDtlPreviewTable tr').find('.orderitemStatusLable').html('<span class="label label-info">'+ data.order_status + '</span>')
                This.parents('.orderDtlPreviewTable tr').find('.orderItemTd').html('<a href="#" class="btn btn-xs tooltips btn-success orderItemStatusBtn processBtn" td_ord_id="'+ order_id +'" title="Process Start" td_ord_type="completed">' +
                    '<i class="fa fa-thumbs-o-up" aria-hidden="true"></i>' +
                    '</a>');
            }else if(data.order_status == 'completed'){
                This.parents('.orderDtlPreviewTable tr').find('.orderitemStatusLable').html('<span class="label label-success">'+ data.order_status + '</span>')
                This.parents('.orderDtlPreviewTable tr').find('.orderItemTd').html('');
            }
            $('.orderTblCommonClass').DataTable().ajax.reload(null, false);
            $.notify(data.msg, data.status);
        }, 'json')
    });

    function format ( d, data ) {
        var htm_table = '';
        htm_table+= '<table class="table table-hover" style="width:50%;"  align="center"><tr>';
        var once_added = 0;
        $(data).each(function (i,j) {
            if(j.orders_items_class_id == 0){
                htm_table += '<th>Main Service</th>' +
                    '<td>' + j.service_pricing_title + '</td>' +
                    '<td>' + j.service_pricing_currency_unit + ' '+ j.orders_items_type_cost + '</td>'+
                    '<tr>';
            } else if(j.orders_items_class_id > 0) {
                if(once_added == 0){
                    htm_table += '<tr><th>Additional Elements</th>';
                    once_added = 1;
                } else {
                    htm_table += '<tr><th></th>';
                }
                htm_table += '<td>' + j.attribute_field_value + '</td>' +
                    '<td>' + j.service_pricing_currency_unit + ' '+ j.orders_items_type_cost + '</td>'+
                    '<tr>';
            }
        });
        htm_table+= '</table>';
        return htm_table;
    }

    $('.orderAdminTbl').on('click', '.expandOrderList', function (e) {
        e.preventDefault();
        var order_id = $(this).attr('expand_order_id');
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        $.get(site_url + '/admin/order-items/' + order_id, function (data) {
            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child( format(row.data(), data) ).show();
                tr.addClass('shown');
            }
        }, 'json')
    })

    $('.orderPartnerTbl').on('click', '.expandOrderList', function (e) {
        e.preventDefault();
        var order_id = $(this).attr('expand_order_id');
        var tr = $(this).closest('tr');
        var row = partnertable.row( tr );

        $.get(site_url + '/admin/order-items/' + order_id, function (data) {
            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child( format(row.data(), data) ).show();
                tr.addClass('shown');
            }
        }, 'json')
    })


});
