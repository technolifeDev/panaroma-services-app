
$(function(){
    var site_url = $('.site_url').val();

    $('.saveOrder').on('click', function (e) {
        e.preventDefault();
        $.post(site_url+'/api/v1/service-order-create', $('.orderForm').serialize(), function (data) {
            alert(data.successMessage);
        }, 'json')
    });

    $('.viewItemDetails').on('click', function (e) {
        e.preventDefault();
        var service_item_details = $('.service_item_details').val();
        $.post(site_url+'/api/v1/service-item-details/' + service_item_details, $('.detailsForm').serialize(), function (data) {
            console.log(data);
            $('.viewDetails').html(JSON.stringify(data))
        }, 'json')
    })


});
